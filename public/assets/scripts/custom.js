// $(document).ready(function() {
//         var path = window.location.href; 
//     $('ul a').each(function() {
//      if (this.href === path) {
//       $(this).addClass('active');
//      }
//     });
//    });

$.sidebarMenu = function (menu) {
    var animationSpeed = 300,
      subMenuSelector = '.sidebar-submenu';

    $(menu).on('click', 'li a', function (e) {
      var $this = $(this);
      var checkElement = $this.next();

      if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {
        checkElement.slideUp(animationSpeed, function () {
          checkElement.removeClass('menu-open');
        });
        checkElement.parent("li").removeClass("active");
      }

      //If the menu is not visible
      else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {
        //Get the parent menu
        var parent = $this.parents('ul').first();
        //Close all open menus within the parent
        var ul = parent.find('ul:visible').slideUp(animationSpeed);
        //Remove the menu-open class from the parent
        ul.removeClass('menu-open');
        //Get the parent li
        var parent_li = $this.parent("li");

        //Open the target menu and add the menu-open class
        checkElement.slideDown(animationSpeed, function () {
          //Add the class active to the parent li
          checkElement.addClass('menu-open');
          parent.find('li.active').removeClass('active');
          parent_li.addClass('active');
        });
      }
      //if this isn't a link, prevent the page from being redirected
      if (checkElement.is(subMenuSelector)) {
        e.preventDefault();
      }
    });
   }
   $.sidebarMenu($('.side-nav'))


  

   var searchedLoginClaims = sessionStorage.getItem("product");
   if (searchedLoginClaims != undefined || searchedLoginClaims != null) {
    $("#menu-select-filter").first().find(":selected").removeAttr("selected");
    $("#menu-select-filter").find("option").each(function () {
      if ($(this).val() == searchedLoginClaims) {
        $(this).attr("selected", true);
        var current = this.value;
        // alert(current);
        if (current == 'all') {
          $('.side-nav').find('li.all').show();
        } else {
          $('.side-nav').find('li').hide();
          $('.side-nav').find('li.all.' + current).show();
          $('.side-nav').find('li.all.' + current).addClass('current');
            $('.current').addClass('active');
            $('.current').children('.sidebar-submenu').addClass('menu-open');


        }

      }


    });
   }



   jQuery(document).ready(function ($) {

    var current = location.pathname.split("/").pop();
    var string = location.pathname;
   var segments = string.split("/");
   var lastUrl = segments[segments.length-2];
    $('.side-nav li a').each(function () {

        hrefurl=$(this).attr("href");
      var hrefCurrent = hrefurl.substr(hrefurl.lastIndexOf('/') + 1)
      
      var $this = $(this);
      // if the current path is like this link, make it active
      // if($this.attr('href').indexOf(current) !== -1){
      if (current == hrefCurrent || lastUrl == hrefCurrent) {
        $this.addClass('active');
        if ($(this).hasClass('active')) {
          $(this).parent('li').addClass('second-level');
          $(this).parent().parent('ul').addClass('menu-open');
          $(this).parent().parent('ul').parent('li').addClass('active');

        }
      }

    })


//     var string = location.pathname;
//    var segments = string.split("/");
//    var lastUrl = segments[segments.length-2];

//     var current = location.pathname.split('/').pop();

//     $('.third-lavel .second-level a').each(function () {
//       var hrefCurrent = $(this).attr("href").split('/').pop()

//       var $this = $(this);
//       // if the current path is like this link, make it active
//       // if($this.attr('href').indexOf(current) !== -1){
//       if (current == hrefCurrent || lastUrl == hrefCurrent) {
//         $(this).parent().parent().parent().parent('ul').addClass('menu-open');
//         $(this).parent().parent().parent().parent('ul').parent('li').addClass('active');


//       }

//     })
   })