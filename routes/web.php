<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['middleware' => ['session', 'roleCheck']], function () {

    //Login
    Route::get('/', 'Auth\LoginController@showLogin');
    Route::get('/logout', 'Auth\LoginController@doLogout');

    //Google Authentication Routes
    $s = 'social.';
    Route::get('/social/redirect/{provider}', ['as' => $s . 'redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => $s . 'handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

    //Dashboard
    Route::get('dashboard', 'DashboardController@index');

    Route::group(['middleware' => ['role:admin']], function () {
        //Role
        Route::resource('role', 'Users\RoleController');
        Route::get('role/{id}/permissions', 'Users\RoleController@showPermissions');
        Route::post('role/{id}/assign-permissions', 'Users\RoleController@assignPermissions');
        Route::get('role/{id}/delete-permission/{permissionId}', 'Users\RoleController@deletePermission');
        Route::get('user/{userId}/assign-roles', 'Users\UsersListController@showRoles');
        Route::post('user/{userId}/save-roles', 'Users\UsersListController@saveRoles');
        Route::get('user/{userId}/delete-role/{roleId}', 'Users\UsersListController@deleteRole');

        //Permission
        Route::resource('permission', 'Users\PermissionController');
        Route::get('permission/{id}/roles', 'Users\PermissionController@showRoles');
        Route::post('permission/{id}/assign-roles', 'Users\PermissionController@assignRoles');
        Route::get('permission/{id}/delete-role/{roleId}', 'Users\PermissionController@deleteRole');
        Route::get('user/{userId}/assign-permissions', 'Users\UsersListController@showPermissions');
        Route::post('user/{userId}/save-permissions', 'Users\UsersListController@savePermissions');
        Route::get('user/{userId}/delete-permission/{permissionId}', 'Users\UsersListController@deletePermission');

        //Appraisal
        Route::resource('appraisal/appraisal-component-type', 'Appraisal\AppraisalComponentTypeController');
        Route::resource('appraisal/appraisal-bonus-type', 'Appraisal\AppraisalBonusTypeController');
        Route::resource('appraisal/appraisal-type', 'Appraisal\AppraisalTypeController');

        //Bank
        Route::resource('bank/bank-account-meta-key', 'Bank\AccountMetaKeyController');
        Route::resource('bank/bank-account-type', 'Bank\AccountTypeController');
        Route::get('bank/bank-account/create/{accountTypeId?}/{referId?}', 'Bank\AccountController@create');
        Route::resource('bank/bank-account', 'Bank\AccountController');

        //Prep Salary Component Type
        Route::resource('prep-salary/prep-salary-component-type', 'PrepSalary\PrepSalaryComponentTypeController');

    });

    Route::group(['middleware' => 'role_or_permission:admin|hr|appraisal-access'], function () {
        Route::get('appraisal/monthly-summary/{monthId?}', 'Appraisal\AppraisalController@monthlySummary');
        Route::resource('appraisal', 'Appraisal\AppraisalController');
    });
    //Loans
    Route::group(['middleware' => 'role_or_permission:admin|hr|loan-management'], function () {
        Route::get('loans/request', 'Loan\LoanController@loanRequest');
        Route::get('loans/{id}/custom-payment', 'Loan\LoanController@customPayment');
        Route::post('loans/{id}/custom-payment', 'Loan\LoanController@customPaymentStore');
        Route::resource('loans', 'Loan\LoanController');
        Route::put('loans/{id}/complete', 'Loan\LoanController@completeLoan');
    });
    Route::group(['middleware' => ['role:admin|hr|account']], function () {
        //TDS Summary
        Route::get('user/{userId}/tds-summary/{yearId?}', 'Tds\TdsController@index');

        /* Users */
        Route::get('user', 'Users\UsersListController@index');
        Route::get('user/{userId}/edit', 'Users\UsersListController@edit');
        Route::post('user/{userId}/store', 'Users\UsersListController@store');
        Route::post('user/updateOrCreate/{userId}/{bankAccountId?}', 'Users\UsersListController@updateOrCreate');
        Route::get('user/{userId}/{bankAccountId}/deleteAccount', 'Users\UsersListController@deleteAccount');
        Route::put('user/{userId}', 'Users\UsersListController@update');
        Route::get('user/{userId}/active-toggle', 'Users\UsersListController@activeToggle');
        Route::get('user/create', 'Users\UsersListController@create');
        Route::post('user', 'Users\UsersListController@store');

        //Payslip
        Route::get('payslips/{monthId?}', 'PrepSalary\PrepSalaryController@showPayslips');
        Route::get('payslips/{monthId}/{user_id}', 'PrepSalary\PrepSalaryController@showUserPayslip');
        Route::get('payslips/{monthId}/{user_id}/download', 'PrepSalary\PrepSalaryController@downloadUserPayslip');


        //Tds Breakdown
        Route::get('tds-breakdown/{monthId}/{user_id}', 'Tds\TdsController@showUserTDS');
        Route::get('tds-breakdown/{monthId}/{user_id}/download', 'Tds\TdsController@downloadUserTDS');

        //Salary Dependency
        Route::get('salary-dependency/{monthId?}', 'SalaryDependency\SalaryDependencyController@index');

        //User Working Day
        Route::get('user-working-day/{id}/delete', 'WorkingDay\UserWorkingDayController@destroy');
        Route::post('user-working-day/{id}/addUser', 'WorkingDay\UserWorkingDayController@storeUser');

        Route::resource('user-working-day', 'WorkingDay\UserWorkingDayController');
        Route::get('user-working-day/{monthId}/status-month', 'WorkingDay\UserWorkingDayController@lockMonth');

        //fixed bonus
        Route::resource('deduction/fixed-bonus', 'Deduction\FixedBonusController');
        Route::get('deduction/fixed-bonus/{monthId}/delete', 'Deduction\FixedBonusController@destroy');
        Route::get('deduction/fixed-bonus/{monthId}/status-month', 'Deduction\FixedBonusController@lockMonth');
        Route::post('deduction/fixed-bonus/regenerate', 'Deduction\FixedBonusController@regenerate');

        //Variable Pay
        Route::resource('deduction/variable-pay', 'Deduction\VariablePayController');
        Route::get('deduction/variable-pay/{monthId}/{variablePayId}/delete', 'Deduction\VariablePayController@destroy');
        Route::get('deduction/variable-pay/{monthId}/status-month', 'Deduction\VariablePayController@lockMonth');
        Route::post('deduction/variable-pay/regenerate', 'Deduction\VariablePayController@regenerate');

        //Non Ctc Bonus
        Route::get('non-ctc-bonus/confirmed', 'NonCtcBonus\NonCtcBonusController@confirmedBonus');
        Route::get('non-ctc-bonus/confirmed/delete-all', 'NonCtcBonus\NonCtcBonusController@deleteAll');
        Route::get('non-ctc-bonus/due', 'NonCtcBonus\NonCtcBonusController@dueBonus');
        Route::get('non-ctc-bonus/{monthId?}', 'NonCtcBonus\NonCtcBonusController@index');
        Route::get('non-ctc-bonus/{monthId}/status-month', 'NonCtcBonus\NonCtcBonusController@lockMonth');
        Route::get('bonus-due/{monthId}/{userId}', 'NonCtcBonus\NonCtcBonusController@dueBonusUser');
        Route::get('bonus-due/{monthId}/{userId}/approve-all', 'NonCtcBonus\NonCtcBonusController@approveAll');

        //Adhoc Payments
        Route::resource('adhoc-payments', 'AdhocPayment\AdhocPaymentsController');
        Route::get('adhoc-payments/{monthId}/status-month', 'AdhocPayment\AdhocPaymentsController@lockMonth');

        //Adhoc Payment Component
        Route::resource('adhoc-payment-component', 'AdhocPayment\AdhocPaymentComponentController');

        //It Saving Month
        Route::get('it-saving-month/', 'ItSaving\ItSavingController@monthUserList');
        Route::get('it-saving-month/{monthId}', 'ItSaving\ItSavingController@monthUserList');
        Route::post('it-saving-month/regenerate', 'ItSaving\ItSavingController@regenerate');
        Route::get('it-saving-month/{monthId}/status-month', 'ItSaving\ItSavingController@lockMonth');

        /* Deductions */
        //Food Deduction
        Route::resource('deduction/food-deduction', 'Deduction\FoodDeductionController');
        Route::get('deduction/food-deduction/{monthId}/delete', 'Deduction\FoodDeductionController@destroy');
        Route::get('deduction/food-deduction/{monthId}/status-month', 'Deduction\FoodDeductionController@lockMonth');
        Route::post('deduction/food-deduction/regenerate', 'Deduction\FoodDeductionController@regenerate');

        //Vpf Deduction
        Route::get('deduction/vpf-deduction', 'Deduction\VpfDeductionController@getVpfDeductions');
        Route::get('deduction/vpf-deduction/{monthId}', 'Deduction\VpfDeductionController@viewDeductions');
        Route::post('deduction/vpf-deduction/regenerate', 'Deduction\VpfDeductionController@regenerate');
        Route::post('deduction/vpf-deduction/update', 'Deduction\VpfDeductionController@storeData');
        Route::get('deduction/vpf-deduction/{vpfId}/delete', 'Deduction\VpfDeductionController@deleteRecord');
        Route::get('deduction/vpf-deduction/{monthId}/status-month', 'Deduction\VpfDeductionController@monthStatus');

        //Insurance Deduction
        Route::resource('deduction/insurance-deduction', 'Deduction\InsuranceDeductionController');
        Route::get('deduction/insurance-deduction/{monthId}/status-month', 'Deduction\InsuranceDeductionController@monthStatusDeduction');
        Route::post('deduction/insurance-deduction/regenerate', 'Deduction\InsuranceDeductionController@regenerate');

        //Loan Deduction
        Route::resource('deduction/loan-deduction', 'Deduction\LoanDeductionController');
        Route::get('deduction/loan-deduction/{monthId}/status-month', 'Deduction\LoanDeductionController@monthStatusDeduction');
        Route::post('deduction/loan-deduction/regenerate', 'Deduction\LoanDeductionController@regenerate');
        Route::get('deduction/loan-deduction/{monthId}/delete', 'Deduction\LoanDeductionController@destroy');

        //Loan Interest Deduction
        Route::get('deduction/loan-interest-income/{monthId}', 'Deduction\LoanInterestController@show');
        Route::get('deduction/loan-interest-income/{monthId}/status-month', 'Deduction\LoanInterestController@monthStatusDeduction');
        Route::post('deduction/loan-interest-income/regenerate', 'Deduction\LoanInterestController@regenerate');
        Route::put('deduction/loan-interest-income/{monthId}/update', 'Deduction\LoanInterestController@update');

        //Monthly Variable Bonus
        Route::resource('deduction/monthly-variable-bonus', 'Deduction\MonthlyVariableBonusController');
        Route::get('deduction/monthly-variable-bonus/{monthId}/status-month', 'Deduction\MonthlyVariableBonusController@lockMonth');
        Route::get('deduction/monthly-variable-bonus/{monthId}/delete', 'Deduction\MonthlyVariableBonusController@destroy');
        Route::get('deduction/monthly-variable-bonus/{monthId}/regenerate', 'Deduction\MonthlyVariableBonusController@regenerate');

        //File
        Route::get('db-file/{file_id}', 'File\FileController@streamFile');

        Route::resource('onsite-allowance', 'Onsite\OnsiteAllowanceController');
    });

    Route::group(['middleware' => ['role:admin|account']], function () {

        //Prep Components compare
        Route::get('prep-salary/paid-comparison/{prepSalId}', 'PrepSalary\PrepSalaryController@compComparison');
        Route::get('prep-salary/paid-comparison/{prepSalId}/download', 'PrepSalary\PrepSalaryController@paidComparisonDownload');
        Route::get('prep-salary/current-gross-comparison/{prepSalId}', 'PrepSalary\PrepSalaryController@compComparison');
        Route::get('prep-salary/current-gross-comparison/{prepSalId}/download', 'PrepSalary\PrepSalaryController@currentGrossComparisonDownload');
        Route::get('prep-salary/future-pay-comparison/{prepSalId}', 'PrepSalary\PrepSalaryController@compComparison');
        Route::get('prep-salary/future-pay-comparison/{prepSalId}/download', 'PrepSalary\PrepSalaryController@futurePayComparisonDownload');
        Route::get('prep-salary/tds-comparison/{prepSalId}', 'PrepSalary\PrepSalaryController@compComparison');
        Route::get('prep-salary/tds-comparison/{prepSalId}/download', 'PrepSalary\PrepSalaryController@tdsComparisonDownload');
        Route::get('prep-salary/it-saving-comparison/{prepSalId}', 'PrepSalary\PrepSalaryController@compComparison');
        Route::get('prep-salary/appraisal-comparison/{prepSalId}', 'PrepSalary\PrepSalaryController@compComparison');

        //Prep Salary
        Route::get('prep-salary/salary-sheet-comparison/{monthId?}', 'PrepSalary\PrepSalaryController@salarySheetCompare');
        Route::get('prep-salary/salary-sheet-comparison/{prepSalId}/download', 'PrepSalary\PrepSalaryController@salarySheetCompareDownload');

        Route::get('prep-salary/{id}/discard', 'PrepSalary\PrepSalaryController@discardPrepSalary');
        Route::get('prep-salary/{id}/close', 'PrepSalary\PrepSalaryController@closePrepSalary');
        Route::get('prep-salary/{id}/salary-sheet', 'PrepSalary\PrepSalaryController@prepSalarySheet');
        Route::get('prep-salary/{id}/finalize', 'PrepSalary\PrepSalaryController@finalize');
        Route::get('prep-salary/{id}/payslip/{userId?}', 'PrepSalary\PrepSalaryController@generatePayslip');
        Route::get('prep-salary/{id}/salary/{userId}', 'PrepSalary\PrepSalaryController@salaryStore');
        Route::get('prep-salary/{id}/lock', 'PrepSalary\PrepSalaryController@lockMonth');
        Route::post('prep-salary/create', 'PrepSalary\PrepSalaryController@storeSalary');
        Route::resource('prep-salary', 'PrepSalary\PrepSalaryController');
        Route::resource('prep-salary-component-type', 'PrepSalary\PrepSalaryComponentTypeController');
        Route::get('prep-salary/{id}/generate', 'PrepSalary\PrepSalaryController@generate');
        Route::get('prep-salary/{id}/view', 'PrepSalary\PrepSalaryController@getView');
        Route::get('prep-salary/{id}/download', 'PrepSalary\PrepSalaryController@downloadCSV');
        Route::get('prep-salary/{id}/tds', 'PrepSalary\PrepSalaryController@viewTds');
        Route::get('prep-salary/{id}/tds/download', 'PrepSalary\PrepSalaryController@downloadCSVTds');
        Route::get('prep-salary/{id}/salary-sheet/download', 'PrepSalary\PrepSalaryController@prepSalarySheetDownload');
        Route::get('prep-salary/{id}/generate-all', 'PrepSalary\PrepSalaryController@generateAll');
        Route::get('prep-salary/{prepSalaryId}/{prepComponentId}/working-day', 'PrepSalary\PrepWorkingDayController@show');
        Route::get('prep-users/{id}', 'PrepSalary\PrepSalaryController@getUsers');
        Route::post('prep-users/{id}', 'PrepSalary\PrepSalaryController@setUsers');
        Route::get('prep-users/{prepSalaryId}/{monthId}/status-month', 'PrepSalary\PrepSalaryController@statusMonth');
        Route::get('prep-salary/salary-sheet-compare/{monthId?}', 'PrepSalary\PrepSalaryController@salarySheetCompare');
        Route::get('prep-salary/user-generate/{salaryId}', 'PrepSalary\PrepSalaryController@userGenerate');
        Route::get('prep-salary/user-generate-all/{salaryId}/{userId}', 'PrepSalary\PrepSalaryController@userGenerateAll');
        Route::get('prep-salary/user-generate-single/{salaryExecutionId}', 'PrepSalary\PrepSalaryController@userGenerateExecution');
        Route::get('prep-salary/user-components-view/{prepSalaryId}/{userId}', 'PrepSalary\PrepSalaryController@userComponentsView');
        Route::get('prep-salary/user-generate/compare/{prepSalaryId}/{userId}', 'PrepSalary\PrepSalaryController@userCompare');

        //Bank
        Route::get('bank/bank-transaction/{accountType?}', 'Bank\BankTransactionController@index');
        Route::post('bank/bank-transaction/store', 'Bank\BankTransactionController@store');
        Route::get('bank/payment-transfers', 'Bank\PaymentTransferController@index');
        Route::get('bank/payment-transfers/{id}', 'Bank\PaymentTransferController@show');
        Route::put('bank/payment-transfers/{id}', 'Bank\PaymentTransferController@update');
        Route::post('bank/payment-transfers/{id}/upload', 'Bank\PaymentTransferController@upload');

        Route::get('bank/due-payments', 'Bank\BankTransactionController@showPaymentIndex');
        Route::post('bank/due-payments/{id}/preview', 'Bank\BankTransactionController@showTransactions');
        Route::get('bank/due-payments/{id}', 'Bank\BankTransactionController@showPendingAccountTransactions');
        Route::put('bank/due-payments/{id}', 'Bank\BankTransactionController@storePendingTransactions');
        Route::get('bank/due-payments/{accountTypeId}/payment-transfers', 'Bank\PaymentTransferController@index');

        //Salary Transfer
        Route::get('salary-transfer/{monthId?}', 'Salary\SalaryTransferController@index');
        Route::post('salary-transfer/preview', 'Salary\SalaryTransferController@preview');
        //Route::post('salary-transfer/{monthId}/preview', 'Salary\SalaryTransferController@preview');
        Route::post('salary-transfer/finalise', 'Salary\SalaryTransferController@finaliseTransfer');
        Route::get('salary-transfer/{monthId}/bank-data', 'Salary\SalaryTransferController@getBankData');

        //Bank Transfer
        Route::get('bank-transfer/{monthId?}', 'BankTransfer\BankTransferController@index');
        Route::get('bank-transfer/{bankTransferUserId}/complete', 'BankTransfer\BankTransferController@completeTransfer');
        Route::get('bank-transfer/{bankTransferUserId}/reject', 'BankTransfer\BankTransferController@rejectTransfer');
        Route::get('bank-transfer/{bankTransferId}/view-complete', 'BankTransfer\BankTransferController@complete');
        Route::post('bank-transfer/{bankTransferId}/view-complete/upload', 'BankTransfer\BankTransferController@upload');
        Route::get('bank-transfer/{id}/salary-sheet-compare', 'BankTransfer\BankTransferController@showSalarySheetCompare');
        Route::get('bank-transfer/{id}/salary-sheet', 'BankTransfer\BankTransferController@showSalarySheet');
        Route::get('bank-transfer/{id}/salary-sheet/download', 'BankTransfer\BankTransferController@downloadSalarySheet');
        Route::get('bank-transfer/{bankTransferId}/complete-bank-transfer', 'BankTransfer\BankTransferController@completeBankTransfer');

        //Bank Transfer Hold
        Route::get('bank-transfer/{bankTransferId}/hold', 'BankTransfer\BankTransferController@getHolds');
        Route::get('bank-transfer/{bankTransferId}/hold/{bankHoldId}', 'BankTransfer\BankTransferController@release');

        //File
        Route::get('db-file/{file_id}', 'File\FileController@streamFile');

        //VPF master
        Route::resource('vpf', 'Vpf\VpfController');

        Route::get('vpf/status-toggle/{id}', 'Vpf\VpfController@statusToggle');

        //Leaves
        Route::get('calendar-leave-credit/{calendarYearId?}', 'CalendarLeaveCredit\CalendarLeaveCreditController@index');
        Route::post('calendar-leave-credit', 'CalendarLeaveCredit\CalendarLeaveCreditController@store');
        Route::get('calendar-leave-credit/{calendarYearId}/lock-toggle', 'CalendarLeaveCredit\CalendarLeaveCreditController@lockToggle');

        //system Setting
        Route::resource('system-setting', 'SystemSetting\SystemSettingController');
    });

});

Auth::routes();
