@extends('layouts.dashboard')
@section('title')
Add OnSite Allowance
@endsection
@section('main-content')
<div class="page-title-box" >
    <div class="row align-items-center">
        <div class="col-sm-12">
            <h1 class="page-title">Add OnSite Allowance</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active"><a href="{{ url('onsite-allowance') }}">Add OnSite Allowance</a></li>
            </ol>
        </div>
    </div>
    @include('flash')
    <div class="row">
		<div class="col-md-12">
			<div class="card">
					<div class="panel-body">
                        <br>
                        {{ Form::open(array('url' => "onsite-allowance")) }}
                        {{ Form::hidden('approver_id', Auth::id()) }}
                        <div class="form-group row">
                            {{ Form::Label('user', 'User Name:', array('class' =>'col-md-4' ,'align' => 'right')) }}
                            <div class="col-md-4">
                                {{ Form::select('user_id', $users->pluck('name','id'),null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select User'))}}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::Label('Start Date', 'Start Date:', array('class' =>'col-md-4' ,'align' => 'right')) }}
                            <div class="col-md-4 input-group date" id="startdate">
                                {{Form::date("start_date", null, ['class' => 'form-control'])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::Label('End Date', 'End Date:', array('class' =>'col-md-4' ,'align' => 'right')) }}
                            <div class="col-md-4 input-group date" id="enddate">
                                {{Form::date("end_date", null, ['class' => 'form-control'])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::Label('project', 'Project Name:', array('class' =>'col-md-4' ,'align' => 'right')) }}
                            <div class="col-md-4">
                                {{ Form::select('project_id', $projects->pluck('project_name','id'),null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Project'))}}
                            </div>
                        </div>


                        <div class="form-group row">
                            {{ Form::Label('status', 'Status:', array('class' =>'col-md-4' ,'align' => 'right')) }}
                            <div class="col-md-4">
                                {{ Form::select('status', ['pending' => 'pending','approved' => 'approved','rejected' => 'rejected'],null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Status'))}}
                            </div>
                        </div>


                        <div class="form-group row">
                            {{ Form::Label('type', 'Type:', array('class' =>'col-md-4' ,'align' => 'right')) }}
                            <div class="col-md-4">
                                {{ Form::select('type', ['local' => 'local','domestic' => 'domestic','international' => 'international'],null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Status'))}}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::Label('amount', 'Amount(Per Diem):',array('class' =>'col-md-4', 'align' => 'right')) }}
                            <div class="col-md-4">
                                {{ Form::number('amount', null, ['class' => 'form-control']) }}
                            </div>
                        </div>


                        <div class="form-group row">
                            {{ Form::Label('noted', 'Notes:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
                            <div class="col-md-4">

                                    {{ Form::textarea('notes',null,['class'=>'form-control', 'rows' => 5] ) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
                        </div>
                    	{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
    	
	</div>
    </div>
</div>

@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        $(".userSelect1").select2();
    });
</script>
@endsection
