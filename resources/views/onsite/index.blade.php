@extends('layouts.dashboard')
@section('title')
OnsiteAllowance | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="page-title">Onsite Allowance </h1>
                <ol class="breadcrumb">
        		  	<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        		  	<li class="breadcrumb-item" active>Onsite Allowance</li>
        		
        		</ol>
            </div>
            <div class="col-sm-6 text-right">
                <a href="onsite-allowance/create" class="btn btn-success"><i class="fa fa-plus fa-fw"></i>Add New</a>
			</div>
    </div>

	@include('flash')
      
        <div class="user-list-view">
            <div class="card">
                <table class="table table-striped table-sm" id="onsite-table">
                    <thead>
                        <th class="text-enter">#</th>
                        <th class="text-left">EmpId</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Start Date</th>
                        <th class="text-left">End Date</th>
                        <th class="text-left">Project</th>
                        <th class="text-left">Status</th>
                        <th class="text-left">Type</th>
                        <th class="text-left">Amount</th>
                        <th class="text-left">Approved By</th>
                        <th class="text-right">Action</th>
                    </thead>
                    <tbody>
                    @if(count($onsiteAllowances) > 0)
                        @foreach($onsiteAllowances as $index => $onsiteAllowance)
                            <tr>
                                <td class="text-left">{{ $index + 1  }}</td>
                                <td class="text-left">{{$onsiteAllowance->user->employee_id}}</td>
                                <td class="text-left">{{$onsiteAllowance->user->name}}</td>
                                <td class="text-left">{{date_in_view($onsiteAllowance->start_date)}}</td>
                                <td class="text-left">{{date_in_view($onsiteAllowance->end_date)}}</td>
                                <td class="text-left">{{$onsiteAllowance->project->project_name}}</td>
                                <td class="text-left">{{$onsiteAllowance->status}}</td>
                                <td class="text-left">{{$onsiteAllowance->type}}</td>
                                <td class="text-left">{{$onsiteAllowance->amount}}</td>
                                <td class="text-left">{{$onsiteAllowance->approver->name}}</td>
                                
                                <td class="text-right">
                                    <a href="onsite-allowance/{{$onsiteAllowance->id}}/edit" type="button" data-toggle="tooltip" data-placement="top" title="Edit" class="btn btn-info btn-sm">Edit</a>
                                    {{ Form::open(['url' => '/onsite-allowance/'.$onsiteAllowance->id, 'method' => 'delete']) }}
                                    {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                    {{ Form::close() }}
                                     
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                    <tbody>
                </table>
            
</div>
@endsection
@section('js')
@parent
<script>
	$(document).ready(function() {
		$('#onsite-table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            pageLength:500,
        });
    });
   
</script>

@endsection
