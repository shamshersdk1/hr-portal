@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="main-content">
    <div class="page-title-box">
        <div class="row">
            <div class="col-sm-8">
                <h1 class="page-title">IT Saving Month</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/it-saving-month">IT Saving Month</a></li>
                    <li class="active">{{$monthObj ? $monthObj->formatMonth() : " "}}</li>
                </ol>
            </div>

            <div class="col-sm-4 text-right">
                @if(isset($months))
                    <select id="selectid2" name="user"  placeholder= "{{$monthObj ? $monthObj->formatMonth() : "Select Month"}}">
                        <option value=""></option>
                        @foreach($months as $x)
                            <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                        @endforeach
                    </select>
                @endif
            </div>
        </div>
    </div>
     <div class="col-sm-12">
    @if(!$monthObj->itSavingMonthSetting || ($monthObj->itSavingMonthSetting &&  $monthObj->itSavingMonthSetting->value == 'open'))
        <div class="text-right">
            <span>
                {{ Form::open(['url' => '/it-saving-month/regenerate', 'method' => 'post']) }}
                {{ Form::hidden('month_id', $monthObj->id) }}
                {{ Form::submit('Regenerate',['class' => 'btn btn-primary']) }}
                {{ Form::close() }}
            </span>
        </div>
        <div class="text-right">
            <span>
                <a href="/it-saving-month/{{$monthObj->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
            </span>
        </div>
    @elseif(($monthObj->getMonth(date("Y/m/d")) == $monthObj->id ) && $monthObj->itSavingMonthSetting && $monthObj->itSavingMonthSetting->value == 'locked')
        <div class="text-right">
            <span>
                <a href="/it-saving-month/{{$monthObj->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
            </span>
        </div>
    @endif
    @if($monthObj && $monthObj->itSavingMonthSetting &&  $monthObj->itSavingMonthSetting->value == 'locked')
            <div class="pull pull-right">
                <span class="badge badge-primary">Locked at {{ $monthObj->itSavingMonthSetting ? datetime_in_view($monthObj->itSavingMonthSetting->created_at) : ''}}</span>
            </div>
        @endif
    </div>

    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
    <div class="user-list-view">
        <div class="card">
            <table class="table table-striped table-sm" id="it-saving-table">
                <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">User Name</th>
                    <th class="text-center">Employee ID</th>
                    <th class="text-center">Email</th>
                    @if(count($ItSavingObjMonths) > 0)
                    <th class="text-center">Total Investments</th>
                    <th class="text-center">Total Deductions</th>
                        @if($columns)
                            @foreach($columns as $column)
                            <th class="text-center">{{ucfirst(str_replace("_"," ",$column->COLUMN_NAME))}}</th>
                            @endforeach
                        @endif
                    @endif
                </thead>
                <tbody>
                @if(count($ItSavingObjMonths) > 0)
                    @foreach($ItSavingObjMonths as $index => $item)
                        <tr>
                            <td class="text-center">{{$index+1}}</td>
                             <td class="text-left">{{$item->user->name}}</td>
                             <td class="text-center">{{$item->user->employee_id}}</td>
                             <td class="text-left">{{$item->user->email}}</td>
                            @if(count($columns)>0)
                            <td class="text-center">{{round($item->total_investments,2)}}</td>
                              <td class="text-center">{{round($item->total_deductions,2)}}</td>
                                @foreach($columns as $column)
                                @if($column->COLUMN_NAME == 'other_multiple_investments')
                                    <td class="text-center">{{$item->others->where('type',"other_multiple_investments")->sum('value')}}</td>
                                @elseif($column->COLUMN_NAME == 'other_multiple_deductions')
                                    <td class="text-center">{{$item->others->where('type',"other_multiple_deductions")->sum('value')}}</td>
                                @elseif($column->COLUMN_NAME == 'agree')
                                    @if($item[$column->COLUMN_NAME] == 1)
                                      <td class="text-center">  <span class="label label-success">Yes</span></td>
                                    @else
                                       <td class="text-center">  <span class="label label-danger">No</span></td>
                                    @endif
                                @else
                                <td class="text-center">{{$item[$column->COLUMN_NAME] ? $item[$column->COLUMN_NAME] : '-'}}</td>
                                @endif
                                @endforeach
                            @endif

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center">No Records</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
	</div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#it-saving-table').DataTable( {
            pageLength:500,
			fixedHeader: true,
			"columnDefs": [ {
				"searchable": true,
                "orderable": true,
            } ],
			scrollY:        true,
            scrollX:        true,
            paging:         false,
            scrollCollapse: true,
            fixedColumns:   {
                leftColumns: 3,
            },
            "order": [[ 2, 'asc' ]],
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

$('#selectid2').change(function(){
        console.log('here');
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) {
            window.location = "/it-saving-month/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$monthObj ? $monthObj->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>
@endsection
