
@extends('layouts.dashboard')
@section('title')
Roles
@endsection
@section('main-content')

    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">List of Roles</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"> <a href="/role">list of Roles</a></li>
                    <li class="breadcrumb-item active"> Add Role</li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                {{Form::open(['url'=>'role/create' ,'method'=>'get'])}}
                {{Form::button('<i class="fa fa-plus fa-fw"></i> Add Role',
                array('type' => 'submit', 'class' => 'btn btn-success'))}}
                {{Form::close()}}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="card border">
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-md-6 mt-3">
                    {{ Form::open(array('url' => "role/store")) }}
                        {{ Form::Label('name', 'Role Name:') }}
                        <div class="row">
                            <div class="col-md-9">
                                {{ Form::text('role_name', '',['class' => 'form-control']) }}
                            </div>
                            <div class="form-group col-md-2">
                                {{Form::submit('Save',array('class' => 'btn btn-success '))}}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
</script>
@endsection
