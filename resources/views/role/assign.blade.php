
@extends('layouts.dashboard')
@section('title')
Roles
@endsection
@section('main-content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">List of Roles</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"> <a href="/role">list of Roles</a></li>
                    <li class="breadcrumb-item active"> Assign Permissions</li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
               
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>

    
    <div class="card border">
        <h6 class="card-header no-margin">Give permissions to {{$role->name}} role.</h6>
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-md-6 mt-3">
                    {{ Form::open(array('url' => "role/". $role->id. "/assign-permissions")) }}
                        <div class="row">
                            <div class="col-md-9">
                                @if(isset($permissions))
                                    {{ Form::label('permissions', 'Permissions:')}}
                                    <div class="multipicker">
                                        <select class="form-control js-example-basic-multiple22 " multiple="multiple" name="permissions[]">
                                            @foreach($permissions as $permission)
                                                <option value="{{$permission->id}}">{{$permission->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>	
                                @endif
                            </div>
                            <div class="col-md-3 mt-4">
                                {{Form::submit('Save',array('class' => 'btn btn-success mt-4'))}}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <div class="card card-default">
        <div class="table-responsive">
            <table class="table table-striped table-outer-border no-margin">
                <thead>
                    <tr>
                        <th width="10%">#</th>
                        <th width="10%">Name</th>
                        <th width="25%" class="text-right">Actions</th>
                    </tr>
                </thead>
				@if(isset($assignedPermissions))
                    @if(count($assignedPermissions) > 0)
                        @foreach($assignedPermissions as $index => $permission)
                            <tr>
                                <td>{{$index+1}}</td>
                                <td> {{$permission->name}}</td>
                                <td class="text-right">
                                    <div class="deleteform">
                                    {{ Form::open(['url' => 'role/'.$role->id.'/delete-permission/'.$permission->id, 'method' => 'get']) }}
                                    {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                    {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" class="text-center">No Assigned permissions added.</td>
                        </tr>
                    @endif
				@endif
			</table>
        </div>
    </div>

<script type="text/javascript">
    
</script>
@endsection
