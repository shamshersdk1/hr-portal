
@extends('layouts.dashboard')
@section('title')
Roles
@endsection
@section('main-content')
	<div class="page-title-box">
	    <div class="row align-items-center">
	        <div class="col-sm-6">
	            <h4 class="page-title">List of Roles</h4>
	            <ol class="breadcrumb">
	                <li class="breadcrumb-item">
	                	<a href="/dashboard">Dashboard</a>
	                </li>
	                <li class="breadcrumb-item active">List of Roles</li>
	            </ol>
	        </div>
	        <div class="col-sm-6 text-right">
	            {{Form::open(['url'=>'role/create' ,'method'=>'get'])}}
				{{Form::button('<i class="fa fa-plus fa-fw"></i> Add Role',
				array('type' => 'submit', 'class' => 'btn btn-success'))}}
				{{Form::close()}}
	        </div>
	    </div>
	</div>


	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="user-list-view">
		<div class="">
			<div class="row table-header-section">
				<div class="col-sm-6 d-flex align-items-center"> 
					
				</div>
				  <div class="col-sm-6 table-search">
				  </div>
				</div>
				<div class="table-responsive mt-2">
					<table class="table table-striped table-outer-border no-margin table-sm " id="role-table">
						<thead>
							<th>#</th>
							<th>Role Name</th>
							<th>Code</th>
							<th>Description</th>
							<th class="text-right">Actions</th>
						</thead>
						<tbody>
						@if(count($roles) > 0)
				    		@foreach($roles as $index => $role)
								<tr>
									<td>{{$index + 1}}</td>
									<td>
										{{$role->name}}
									</td>
									<td>
										{{$role->code}}
									</td>
									<td>
										{{$role->description}}
									</td>
									<td class="text-right">
										<a href="role/{{$role->id}}/permissions" class="btn btn-secondary btn-sm">Assign Permissions</a>
										<a href="role/{{$role->id}}/edit" class="btn btn-success btn-sm">Edit</a>
										<a href="role/{{$role->id}}/delete" class="btn btn-danger btn-sm">Delete</a>
									</td>
								</tr>
							@endforeach
				    	@else
				    		<tr>
				    			<td colspan="5">No results found.</td>
				    		</tr>
				    	@endif
						</tbody>
		   			</table>
				   </div>
			</div>
		   	</div>
		</div>
	</div>
@endsection
@section('js')
@parent
<script>
	$(document).ready(function() {
        var t = $('#role-table').DataTable({
			pageLength:500, 
			"order": false,
            "paging": false,
            "info": false,
            "bLengthChange": false,
		});
		$('.table-header-section .table-search').append($('.dataTables_filter'))

    });

</script>
@endsection
