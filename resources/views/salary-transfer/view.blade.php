@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-6">
                <h1 class="admin-page-title">Salary Transfer</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
					<li><a href="/admin/salary-transfer">Month List</a></li>
        		  	<li class="active">{{$month ? $month->formatMonth() : ""}}</li>
        		</ol>
            </div>
		</div>
    </div>
    {{Form::open(['url' => '/admin/salary-transfer/' . $month->id . '/preview', 'method' => 'POST'])}} 
	<div class="row">
        <div class="col-md-3">
            <h4>Transaction Summary for {{$month ? $month->formatMonth() : ""}}</h4>
        </div>
		<div class="col-md-2 pull-right">
			{{Form::button('<i class="fa fa-eye" aria-hidden="true"></i><span style="padding-left:5px">Preview</span>', array('type' => 'submit', 'class' => 'btn btn-warning'));}}		
            {{Form::button('<i class="fa fa-caret-left fa-fw" aria-hidden="true"></i><span style="padding-left:5px">Back</span>', array('type' => 'button', 'class' => 'btn btn-default', 'onClick' => 'window.history.back();'));}}		
		</div>
    </div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="summary-table">
                <thead>
					<th class="check text-center" width="7%"><input type="checkbox" id="flowcheckall" value="" />&nbsp;<small>Bank Transfer</small></th>
                    <th class="text-center" width="5%">#</th>
                    <th class="" width="7%">Employee Id</th>
                    <th class="" width="7%">Name</th>
                    <th class="text-center">Total Deduction</th>
                    <th class="text-center">Total Allowance</th>
                    <th class="text-center">Amount Payable</th>
                </thead>
                <tbody>
                @if(count($users) > 0)
                    @foreach($users as $user)
                    @if(isset($userArray[$user->id]))
                        <tr>
							<td class="check text-center"><input type="checkbox" id={{$user->id}} name="options[{{$user->id}}]" value={{$user->id}} />&nbsp;</td>
                            <td class="text-center"></td>
                            <td class="">{{ $user->employee_id}}</td>
                            <td class="">{{ $user->name}}</td>
                            <td class="text-center">{{ $data[$user->id]['debit'] ?? 0}}</td>
                            <td class="text-center">{{ $data[$user->id]['credit'] ?? 0}}</td>
                            <td class="text-center">{{ $data[$user->id]['total'] ?? 0}}</td>
                        </tr>
                    @endif
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
		</div>
    </div>
{{Form::close()}}
</div>
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
<script>
	$("#flowcheckall").click(function () {
        $('#summary-table tbody input[type="checkbox"]').prop('checked', this.checked);
    });
	$('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if(optionValue == 0) {
            window.location = "/admin/transaction-summary"; 
        }
        else if (optionValue) { 
            window.location = "/admin/transaction-summary/"+optionValue; 
        }
    });
</script>

@endsection
