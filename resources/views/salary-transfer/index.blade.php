@extends('layouts.dashboard')
@section('title')
Salary Transfer
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center ">
            <div class="col-sm-6">
				<h1 class="page-title">Salary Transfer</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
					<li class="active">Salary Transfer</li>
                </ol>
            </div>
            <div class=" col-sm-6">
                <div class="float-right">
                    {{Form::open(['url' => '/transaction-summary/create', 'method' => 'get'])}}
                        {{Form::button('<i class="fa fa-plus fa-fw"></i>  Add Transaction', array('type' => 'submit', 'class' => 'btn btn-success'))}}
                    {{Form::close()}}
                </div>
            </div>

	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
<div class="">


            {{Form::open(['url' => '/salary-transfer/' . ($month ? $month->id.'/preview' : 'preview'), 'method' => 'POST'])}}
            <div class="row">
                <div class="col-md-6">
                </div>
                @if($transactions->sum('amountPayable') > 0)
                <div class="col-md-6 text-right">
                    {{Form::button('<i class="fa fa-eye" aria-hidden="true"></i><span style="padding-left:5px">Preview</span>', array('type' => 'submit', 'class' => 'btn btn-warning'))}}
                </div>
                @endif
            </div>
            <div class="row table-header-section mt-4">
                <div class="col-sm-6">
                    <h4 class="m-0 p-0 header-title text-capitalize">Transaction Summary for {{$month ? $month->formatMonth() : "All Months"}}</h4>


                </div>
                  <div class="col-sm-6 table-search">
                  </div>
                </div>

            <div class="table-respondive mt-2">
            @foreach($holds as $hold)
                <div class="alert alert-warning">
                <a href="bank-transfer/{{$hold->bank_transfer_id}}/hold">{{$hold->user->name}} Salary on hold for month {{$hold->bankTransfer->month->formatmonth()}}</a>
                </div>
            @endforeach
            <div class="user-list-view">
                <div class="panel panel-default">
                    <table class="table table-striped table-sm table-outer-border" id="summary-table">
                        <thead>
                            <th class="check text-center" width="100px"><input type="checkbox" id="flowcheckall" value="" />&nbsp;<small><span id="text-check">Bank Transfer</span></small></th>
                            <th class="text-center" >#</th>
                            <th class="" width="100px">Employee Id</th>
                            <th class="" width="7%">Name</th>
                            <th class="">PAN Number</th>
                            <th class="">Bank Account Number</th>
                            <th class="">Bank IFSC Code</th>
                            <th class="text-center">Prev Net Payable</th>
                            <th class="text-center">Net Payable</th>
                            <th class="text-center">DIFF</th>
                        </thead>
                        <tbody>
                        @if(count($transactions) > 0)
                            @foreach($transactions as $transaction)
                                @if($transaction->amountPayable != 0)
                                    <tr>
                                        <td class="check text-center"><input type="checkbox" id={{$transaction->user->id}} name="options[{{$transaction->user->id}}]" value={{$transaction->user->id}} />&nbsp;</td>
                                        <td class="text-center"></td>
                                        <td class="">{{ $transaction->user->employee_id}}</td>
                                        <td class="">{{ $transaction->user->name}}</td>
                                        <td class="text-center">{{ $transaction->user->panAccount ? $transaction->user->panAccount->account_number : '-'}}</td>
                                        <td class="text-center">{{ $transaction->user->savingAccount ? $transaction->user->savingAccount->account_number : '-'}}</td>
                                        <td class="text-center">{{ $transaction->user->accountMetaValue('ifsc')->value ?? '-'}}</td>
                                        <td class="text-center">{{ $transaction->user->prevAmountPayable() ?? 0}}</td>
                                        <td class="text-center">{{ $transaction->amountPayable ?? 0}}</td>
                                        <td class="text-center">{{ $transaction->amountPayable - $transaction->user->prevAmountPayable()}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9" class="text-center">
                                    No Records found
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
            </div>
            {{Form::close()}}
</div>

@endsection
@section('js')
@parent
<script>
	$('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) {
            window.location = "/salary-transfer/"+optionValue;
        }
    });

	$(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500,
            fixedHeader: {
                header: true
            },
           dom: 'Bfrtip',
            buttons: [
                'csv'
            ]
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        $('.table-header-section .table-search').append($('.dataTables_filter'))

    });

	$("#flowcheckall").click(function () {
        $('#summary-table tbody input[type="checkbox"]').prop('checked', this.checked);
    });

    $("#text-check").click(function () {
        source = document.getElementById('flowcheckall');
        $('#summary-table tbody input[type="checkbox"]').prop('checked', !source.checked);
        $('#flowcheckall').prop('checked', !source.checked);
    });

</script>
@endsection
