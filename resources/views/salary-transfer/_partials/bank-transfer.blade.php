<div class="user-list-view">
    <h6 style="padding-left:10px">BANK TRANSFER</h6>
        <div class="panel panel-default">
            <table class="table table-striped" id="transfer-table">
                <thead>
                    <th class="text-center" >#</th>
					<th class="text-left" >Employee Id</th>
                    <th class="text-left" >Name</th>
                    <th class="text-center">Net Payable</th>
                </thead>
                <tbody>
                @if(count($transactionsTransferUsers) > 0)
					@foreach($transactionsTransferUsers as $transactionsTransferUser)
                        @if($transactionsTransferUser->amountPayable != 0)
							<tr>
                                <input type="hidden" id="custId" name="process[{{$transactionsTransferUser->user->id}}]" value="process">
								<td class="text-center"></td>
								<td class="text-left">{{ $transactionsTransferUser->user->employee_id}}</td>
								<td class="text-left">{{ $transactionsTransferUser->user->name}}</td>
								<td class="text-center">{{ $transactionsTransferUser->amountPayable ?? 0}}</td>
							</tr>
                        @endif
					@endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
		</div>
    </div>
    