
@component('mail::message')

@slot('header')
@component('mail::header', ['url' => config('app.url')])
    {{ config('app.name') }}
@endcomponent
@endslot
<p>List of Adhoc payment for the month of {{ date_format(date_create($date),"M, Y") }}</p>
<table border="1" cellpadding="10">
    <thead>
        @foreach(array_keys($data[array_key_first($data)]) as $key)
        <th>{{ucwords(str_replace("_"," ",$key))}}</th>
        @endforeach
    </thead>
    <tbody>
        @foreach($data as $key => $value)
        <tr>
            <td>{{$value['user_name'].""}}</td>
            <td>{{$value['employee_id'].""}}</td>
            <td>{{$value['type'].""}}</td>
            <td>{{$value['comment'].""}}</td>
            <td>&#8377;{{custom_money($value['amount']).""}}</td>
        </tr>   
        @endforeach
    </tbody>
</table><br>
<span>Regards,</span><br>
<span><a href="{{config('app.url')}}">HR Team<a><span>
@slot('footer')
@component('mail::footer')
    © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
    
    