@component('mail::message')

@slot('header')
@component('mail::header', ['url' => config('app.url')])
    {{ config('app.name') }}
@endcomponent
@endslot
<p>List of Non Ctc Bonus for the month of {{ date_format(date_create($date),"M, Y") }}(Bonuses of last month).</p>    
<table border="1" cellpadding="10">
<thead>
    @foreach(array_keys($data[array_key_first($data)]) as $key)
    <th>{{ucwords(str_replace("_"," ",$key))}}</th>
    @endforeach
</thead>
<tbody>
    @foreach($data as $key => $value)
    <tr>
        <td>{{$value['user_name'].""}}</td>
        <td>{{$value['employee_id'].""}}</td>
        <td>@foreach($value['all_bonuses'] as $bonus_key => $bonus_value)
                @if($bonus_key == 'App\\Models\\Admin\\TechTalkBonusesUser')
                    {{"TechTalk[".$bonus_value['count']."]: "}}&#8377;{{custom_money($bonus_value['amount'])}}
                @elseif($bonus_key == 'App\\Models\\Admin\\OnSiteBonus')
                    {{"On Site[".$bonus_value['count']."]: "}}&#8377;{{custom_money($bonus_value['amount'])}}    
                @elseif($bonus_key == 'App\\Models\\User\\UserTimesheetExtra')
                    {{"Extra[".$bonus_value['count']."]: "}}&#8377;{{custom_money($bonus_value['amount'])}}    
                @elseif($bonus_key == 'App\\Models\\Admin\\PerformanceBonus')
                    {{"Performance[".$bonus_value['count']."]: "}}&#8377;{{custom_money($bonus_value['amount'])}}    
                @elseif($bonus_key == 'App\\Models\\Admin\\AdditionalWorkDaysBonus')
                    {{"Additional[".$bonus_value['count']."]: "}}&#8377;{{custom_money($bonus_value['amount'])}}    
                @elseif($bonus_key == 'App\\Models\\Admin\\ReferralBonus')
                    {{"Referral[".$bonus_value['count']."]: "}}&#8377;{{custom_money($bonus_value['amount'])}}
                @endif
            @endforeach</td>
        <td>&#8377;{{custom_money($value['amount']).""}}</td>
    </tr>
    @endforeach
</tbody>
</table><br>
<span>Regards,</span><br>
<span><a href="{{ config('app.url') }}">HR Team</a></span>
@slot('footer')
@component('mail::footer')
    © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent

