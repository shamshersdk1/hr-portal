
@component('mail::message')

{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
    {{ config('app.name') }}
@endcomponent
@endslot


<p>The following information has been updated on GeekyAnts HR Portal on {{ $date }}</p>
    
<table border="1" cellpadding="10">
    <thead>
        @foreach(array_keys($data[array_key_first($data)]) as $key)
            <th>{{ucwords(str_replace("_"," ",$key))}}</th>
        @endforeach
    </thead>
    <tbody>
        @foreach($data as $key => $value)
                <tr>
                    @foreach(array_keys($data[array_key_first($data)]) as $key)
                        <td>{{$value[$key]}}</td>
                    @endforeach
                </tr>
        @endforeach
    </tbody>
</table>
<br>
<p>In case of any change please contact the HR.</p><br>

<span>Regards,</span><br>
<span><a href="portal.geekyants.com">HR Team<a><span>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
