
@component('mail::message')

{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
    {{ config('app.name') }}
@endcomponent
@endslot

Dear {{ $user->name ?? ''}},

<p>The following information has been updated on GeekyAnts Portal on {{ $date }}</p>
    
<table border="1" cellpadding="10">
    <thead>
        <th></th>
        <th>Old Value</th>
        <th>New Value</th>
    </thead>
    <tbody>
        @foreach($audit as $key => $values )
            @foreach($values['new_values'] as $index => $item)
                <tr>
                    <td>{{ucfirst(str_replace('_',' ',$index))}}</td>
                    <td>
                        @if(!empty($values['old_values'][$index]))
                            {{$values['old_values'][$index]}}
                        @endif
                    </td>
                    <td>
                        @if(!empty($values['new_values'][$index]))
                            {{$values['new_values'][$index]}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>
<br>
<p>In case of any change please contact the HR.</p><br>

<span>Regards,</span><br>
<span><a href="portal.geekyants.com">HR Team<a><span>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    © {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
