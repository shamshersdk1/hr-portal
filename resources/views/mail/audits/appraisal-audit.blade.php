<html>
   <link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">
   <style>
      *, ::after, ::before {
      box-sizing: border-box;
      }
      body{
      font-family: 'Roboto', sans-serif;
      color: #354558;
      font-size: 13px;
      max-width:1084px;
      background:#fff;
      }
      .header{
         padding:15px;
         text-align: center;
         background: #f0f4f7;
         margin-bottom:15px;
      }
      .footer{
         padding:30px;
         background: #f0f4f7;
      }
      .appraisal-accordition .card {
      background: #f0f4f7;
      }
      .text-muted {
      color: #9ca8b3 !important;
      }
      .font-14 {
      font-size: 14px;
      }
      .font-500 {
      font-weight: 500;
      }
      .text-primary {
      color: #30419b !important;
      }
      .col-sm-6 {
      -ms-flex: 0 0 50%;
      flex: 0 0 50%;
      max-width: 50%;
      }
      .table {
      margin-bottom: 10px;
      width: 100%;
      color: #212529;
      border-collapse: collapse;
      background:#fff;
      }
      .table-bordered {
      border: 1px solid #dee2e6;
      }
      .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
      position: relative;
      width: 100%;
      padding-right: 15px;
      padding-left: 15px;
      }
      .table-bordered thead td, .table-bordered thead th {
      border-bottom-width: 2px;
      }
      .table thead th {
      vertical-align: bottom;
      border-bottom: 2px solid #dee2e6;
      }
      .table th {
      font-size: 13px !important;
      }
      .table-bordered td, .table-bordered th {
      border: 1px solid #dee2e6;
      font-size: 13px;
      }
      .table td, .table th {
      padding: 12px !important;
      vertical-align: top;
      border-top: 1px solid #dee2e6;
      font-size: 13px !important;
      }
      .text-center {
      text-align: center!important;
      }
      .card{
      box-shadow: 0px 0px 13px 0px rgba(236, 236, 241, 0.44);
      margin-bottom: 30px;
      background: #f0f4f7;
      position: relative;
      
      min-width: 0;
      word-wrap: break-word;
      background-clip: border-box;
      border: 1px solid rgba(0,0,0,.125);
      border-radius: .25rem;
      }
      .card-header {
      padding: .75rem 1.25rem;
      margin-bottom: 0;
      background: #f0f4f7;
      border-bottom: 1px solid rgba(0,0,0,.125);
      }
      .card-body {
      -ms-flex: 1 1 auto;
      flex: 1 1 auto;
      padding: 1.25rem;
      }
      .d-flex {
      display:-webkit-flex;
      display:-ms-flexbox;
      display:flex;
      }
      .justify-content-between {
      -ms-flex-pack: justify!important;
      justify-content: space-between!important;
      }
      {{--  .btn{
      background-color: #616f80;
      border: 1px solid #616f80;
      border-radius: 0px;
      font-size: 13px;
      font-weight: 500;
      font-size: 11.8181818182px;
      padding: .25rem .5rem;
      font-size: .875rem;
      line-height: 1.5;
      color: #fff;
      text-align: center;
      vertical-align: middle;
      }  --}}
      .row {
      display: -ms-flexbox;
      display: flex;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      margin-right: -15px;
      margin-left: -15px;
      }
      .col-sm-4 {
      -ms-flex: 0 0 33.333333%;
      flex: 0 0 33.333333%;
      max-width: 33.333333%;
      }
      label {
      display: inline-block;
      margin-bottom: .5rem;
      font-weight: 500;
      font-size:13px !important
      }
      .card-title{
          width:100%;
      }
      .tabel-cell{
          display:table-cell;
      }
      .d-table{
          display:table;
      }
   </style>
   <body style=" margin:50px auto">
      <div class="header">
         <img src="https://portal.geekyants.com/images/logo-dark.png" height="32px" />
      </div>
      <div >
         <h4 style="margin-top:50px;">Dear {{ $user->name }},</h4>
         <p style="margin-bottom:50px; font-size:15px">Your appraisal data has been updated on GeekyAnts Portal on {{ $date }}</p>


      </div>
      <div class="card mb-2">
         <div class="card-header  collapsed"  >
            <a class="card-title d-flex justify-content-between mr-5 align-items-center pb-0 mb-0">
               <div class="d-table " style="width:100%">
                  <div class="tabel-cell">
                     <span class="text-muted">Types</span> <br/>
                     <span class="font-14">{{$appraisal['type']['name']}}</span>
                  </div>
                  <div class="text-center tabel-cell">
                     <span class="text-muted">Effective Date</span> <br/>
                     <span class="font-14">{{date_in_view($appraisal->effective_date)}}</span>
                  </div>
                  <div class="text-center tabel-cell">
                     <span class="text-muted">Monthly Gross Salary</span> <br/>
                     <span class="font-14">&#8377;{{number_format(round($appraisal->getMonthlyGrossSalary(),2))}}</span>
                  </div>
                  <div class="text-center tabel-cell">
                     <span class="text-muted">Annual CTC</span> <br/>
                     <span class="font-14">&#8377;{{number_format($appraisal->getAnnualCtc())}}</span>
                  </div>
               </div>
               {{--  <div class="text-right d-flex align-items-center">
                  {{Form::open(['url'=>'appraisal/'.$appraisal->id.'/edit' ,'method'=>'get','class'=>'pb-0 mb-0 position-relative', 'style'=>'z-index:9999'])}}
                  {{Form::button('Edit Appraisal', array('type' => 'submit', 'class' => 'btn btn-secondary btn-sm '))}}
                  {{Form::close()}}
               </div>  --}}
            </a>
         </div>
         <div id="collapseExample{{$appraisal->id}}" class="card-body collapse " data-parent="#accordion">
            <div class="row">
               <div class="col-sm-6">
                  <table class="table table-bordered bg-white">
                     <caption><strong>Part A : Fixed Salary Details</strong></caption>
                     <thead>
                        <tr class="active">
                           <th width="60%" class="text-center"><strong>Particulares</strong></th>
                           <th width="20%" class="text-center"><strong>Annual</strong></th>
                           <th width="20%" class="text-center"><strong>Monthly</strong></th>
                        </tr>
                     </thead>
                     <tbody>
                        @if($appraisal['appraisalComponent'])
                        @php
                        $fixedSalary = 0;
                        @endphp
                        @foreach($appraisal->appraisalComponent as $component)
                        @if($component->value > 0 && $component->appraisalComponentType->is_computed==0)
                        <tr>
                           <td>{{$component->appraisalComponentType->name}}  </td>
                           <td>&#8377;{{number_format($component->value * 12)}}</td>
                           <td>&#8377;{{number_format(round($component->value,2))}}</td>
                        </tr>
                        @php
                        $fixedSalary += ($component->value * 12);
                        @endphp
                        @endif
                        @endforeach
                        <tr class="active">
                           <td class="text-right"><strong>Gross Salary </strong></td>
                           <td>&#8377;{{number_format($fixedSalary)}}</td>
                           <td>&#8377;{{number_format(round($fixedSalary/12,2))}}</td>
                        </tr>
                        <tr>
                           <td colspan="3"><strong>Other Salary Details: </strong></td>
                        </tr>
                        @php
                        $otherBenefits = 0;
                        @endphp
                        @foreach($appraisal->appraisalComponent as $component)
                        @if($component->value < 0)
                        <tr>
                           <td>{{$component->appraisalComponentType->name}}  </td>
                           <td>&#8377;{{number_format(abs($component->value * 12))}}</td>
                           <td>&#8377;{{number_format(abs(round($component->value,2)) )}}</td>
                        </tr>
                        @php
                        $otherBenefits += abs($component->value * 12);
                        @endphp
                        @endif
                        @endforeach
                        <tr class="active">
                           <td class="text-right">Total Other Benefits </td>
                           <td>&#8377;{{number_format($otherBenefits)}}</td>
                           <td>&#8377;{{number_format(round($otherBenefits/12,2))}}</td>
                        </tr>
                        <tr class="active">
                           <td class="text-right">Total Fixed CTC - (A) </td>
                           <td>&#8377;{{number_format($fixedSalary + $otherBenefits)}}</td>
                           <td>&#8377;{{number_format(($fixedSalary+ $otherBenefits)/12)}}</td>
                        </tr>
                        @endif
                     </tbody>
                  </table>
               </div>
               <div class="col-sm-6">
                  <table class="table table-bordered bg-white">
                     <caption><strong>Part B : Variable Bonus Details</strong></caption>
                     <thead>
                        <tr class="active">
                           <th width="40%" class="text-center"><strong>Particulares</strong></th>
                           <th width="20%" class="text-center"><strong>Annual</strong></th>
                           <th width="20%" class="text-center"><strong>Monthly</strong></th>
                           <th width="20%" class="text-center"><strong>Date</strong></th>
                        </tr>
                     </thead>
                     <tbody>
                        @if($bonusTypes)
                        @php
                        $variableBonus = 0;
                        @endphp
                        @foreach($bonusTypes as $index => $bonusType)
                        @php $status = false; @endphp
                        <tr>
                           <td><label>{{$bonusType->description}} </label></td>
                           @foreach ($appraisal->appraisalBonus as $bonus)
                           @if($bonus->appraisal_bonus_type_id == $bonusType->id)
                           @php $status = true; @endphp
                           <td>
                              <p>&#8377;{{ number_format($bonus->value) }}</p>
                           </td>
                           <td>
                              <p>&#8377;{{ number_format(round($bonus->value/12,2)) }}</p>
                           </td>
                           <td>
                              <p>{{ date_in_view($bonus->value_date) }}</p>
                           </td>
                           @php $variableBonus += (isset($bonus->value)?$bonus->value:0); @endphp
                           @endif
                           @endforeach
                           @if($status === false)
                           <td></td>
                           <td></td>
                           <td></td>
                           @endif
                        </tr>
                        @endforeach
                        <tr class="active">
                           <td class="text-right"><label><strong>Total Variabe CTC -(B)</strong></label></td>
                           <td>&#8377;{{ number_format($variableBonus)}}</td>
                           <td>&#8377;{{ number_format(round($variableBonus/12,2))}}</td>
                           <td></td>
                        </tr>
                        <tr class="active">
                           <td class="text-right"><label><strong>Total CTC for the Year - (A+B)</strong></label></td>
                           <td>&#8377;{{number_format($fixedSalary + $otherBenefits + $variableBonus)}}</td>
                           <td>&#8377;{{number_format(round(($fixedSalary + $otherBenefits + $variableBonus)/12, 2))}}</td>
                           <td></td>
                        </tr>
                        @endif
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <p style="font-size:16px">In case of any change please contact the HR.</p>
      <div style="margin-top:30px; margin-bottom:30px">
         <h4 style="margin:0; line-height:20px;">Regards,</h4>
         <p style="margin:0;padding:0">HR Team</p>
      </div>




      <div class="footer text-center">
         © 2020 GeekyAnts. All rights reserved.
      </div>
   </body>
</html>