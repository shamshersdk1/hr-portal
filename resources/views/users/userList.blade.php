@extends('layouts.dashboard')
@section('title')
User List
@endsection

@section('main-content')
	<div class="page-title-box">
	    <div class="row align-items-center">
	        <div class="col-sm-6">
	            <h4 class="page-title">List of Users</h4>
	            <ol class="breadcrumb">
	                <li class="breadcrumb-item">
	                	<a href="/dashboard">Dashboard</a>
	                </li>
	                <li class="breadcrumb-item active">List of Users</li>
	            </ol>
			</div>
			<div class="text-right col-sm-6">
				<a href="user/create" class="btn btn-success waves-effect waves-light"><i class="fa fa-plus"></i> Add User</a>
			</div>
	    </div>
	</div>

	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="">
					<div class="row table-header-section">
						<div class="col-sm-6">

						</div>
						  <div class="col-sm-6 table-search">
						  </div>
						</div>
				<div class="table-responsive mt-2">
					<table class="table table-striped table-outer-border no-margin user-list-table table-sm" id="user-table" style="width:100%">

						<thead>
							<th>#</th>
							<th>Emp</th>
							<th>Name of Employee</th>
							<th>Role</th>
							<th>E-Mail</th>
							<th>Status</th>
							<th>Joining Date</th>
							<th>Total Experience </th>
							<th class="text-right">Actions</th>
						</thead>
						@if(count($usersList) > 0)
				    		@foreach($usersList as $index => $user)
								<tr>
									<td class="td-text">
										{{ $index + 1 }}
									</td>
									<td class="td-text">
										{{!empty($user->employee_id) ? $user->employee_id : "--"}}
									</td>
									<td class="td-text" width="100px">
										{{$user->name}}
									</td>
									<td class="td-text" >
                                        @foreach($user->getRoleNames() as $role)
                                            {{ucfirst($role)}},
                                        @endforeach
									</td>
									<td class="td-text">
										{{$user->email}}
									</td>
									<td class="td-text" width="150px">
										@if($user->is_active == 1)
										<span class="badge badge-success custom-label">Active</span>
										@else
										<span class="badge badge-danger custom-label">Deactivated</span>
										@endif
									</td>
									<td>@if($user->is_active == 1 )
											@if(!empty($user->joining_date))<small>{{date_in_view($user->joining_date)}}</small>@endif
										@else
											@if(!empty($user->release_date))<small>{{date_in_view($user->release_date)}}</small>@endif
										@endif
									</td>
									@if($user->confirmation_date )
									<td class="td-text" width="150px">
										<small>{{$user->getExperience()}}</small>
									</td>
									@elseif(!$user->confirmation_date)
									<td class="td-text">
										<span class="badge badge-primary custom-label">Not yet confirmed</span>
									</td>
									@endif
									<td class="text-right" width="300px">
                                        <a href="appraisal/{{$user->id}}" class="btn btn-primary waves-effect waves-light"  data-toggle="tooltip" data-placement="top" title="View" target="_blank"><i class="fas fa-eye"></i></a>
										<a href="user/{{$user->id}}/assign-roles" class="btn btn-primary waves-effect waves-light "  data-toggle="tooltip" data-placement="top" title="Assign Role"><i class="fas fa-id-card"></i></a>
										<a href="user/{{$user->id}}/assign-permissions" class="btn btn-dark waves-effect waves-light" style="display: inline-block;" data-toggle="tooltip" data-placement="top" title="Permissions"><i class="fas fa-universal-access"></i></a>
										<a href="user/{{$user->id}}/edit" class="btn btn-warning waves-effect waves-light  " style="display: inline-block;" data-toggle="tooltip" data-placement="top" title="Setting"><i class="fas fa-cog"></i></a>
										{{-- <a href="user/{{$user->id}}/financial-transactions" class="btn btn-info waves-effect waves-light btn-sm" style="display: inline-block;"><i class="fa fa-eye"></i> Financial Transactions</a> --}}
										<a href="user/{{$user->id}}/tds-summary" class="btn btn-success waves-effect waves-light " style="display: inline-block;" data-toggle="tooltip" data-placement="top" title="View TDS"><i class="fas fa-file-invoice-dollar"></i> </a>
									</td>
								</tr>
							@endforeach
				    	@else
			    		<tr>
			    			<td>No results found.</td>
			    			<td></td>
			    			<td></td>
							<td></td>
			    			<td></td>
							<td></td>
			    			<td></td>
			    		</tr>
			    		@endif
			   		</table>
				   </div>
			</div>
		</div>
	</div>
@endsection

@section('js')
@parent
<script>
    $(document).ready(function() {
		var t = $('#user-table').DataTable({
			fixedHeader: true,
			scrollY:        '70vh',
			paging:         false,
			scrollCollapse: true,
			"drawCallback": function( settings ) {

				$('[data-toggle="tooltip"]').tooltip();
			}
		});
		$('.table-header-section .table-search').append($('.dataTables_filter'))
	})
</script>
@endsection

