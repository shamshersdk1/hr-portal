@extends('layouts.dashboard')
@section('title')
Users
@endsection
@section('main-content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-12">
                <h1 class="page-title">User Setting</h1>
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ url('/user') }}">Users</a></li>
                <li class="breadcrumb-item active"><a class="">Add User Account</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="card border">
        <div class="card-body">
            <div class="row" >
                <div class="col-md-12">
                    @if(!empty($errors->all()))
                        @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span>{{ $error }}</span><br/>
                        </div>
                        @endforeach
                    @endif
                    @if (session('message'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ session('message') }}</span><br/>
                        </div>
                    @endif
                    @if(session('alert-class'))
                        <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span>{{ session('alert-class') }}</span><br/>
                        </div>
                    @endif
                </div>
            </div>
            {{ Form::open(['url' => 'user', 'method' => 'post']) }}


            <div class="row">
                <div class="col-sm-4">
                    <div class="row mb-3">
                        {{ Form::Label("User Name", "User Name * :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                        {{ Form::text("name", old('name'), ['class' => 'form-control','placeholder' => 'User Name'])}}
                        </div>
                    </div>
                    <div class="row mb-3">
                        {{ Form::Label("Name", "Name * :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::text("first_name", old('first_name'), ['class' => 'form-control','placeholder' => 'First Name'])}}
                        </div>
                    </div>
                    

                    <div class="row mb-3">
                        {{ Form::Label("Employee Id", "Employee Id * :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::text("employee_id", old('employee_id'), ['class' => 'form-control','placeholder' => 'Employee Id'])}}
                        </div>
                    </div>

                    <div class="row mb-3">
                        {{ Form::Label("Reporting Manager ", "Reporting Manager :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::select('parent_id', $users->pluck('name','id'), old('parent_id'), ['class' => 'form-control','placeholder' => 'Reporting Manager']) }}
                        </div>
                    </div>
                    
                    <div class="row mb-3">
                        {{ Form::Label("Gender", "Gender :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::select('gender', ['male' => 'male', 'female' => 'female'], old('gender'),['id'=>'selectid2','class' => 'form-control','placeholder' => 'Gender'])}}
                        </div>
                    </div>
                    

                </div>

                <div class="col-sm-4">
                    <div class="row mb-3">
                        {{ Form::Label("Email", "Email * :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::text("email", old('email'), ['class' => 'form-control','placeholder' => 'Email'])}}
                        </div>
                    </div>
                    <div class="row mb-3">
                        {{ Form::Label("Middle Name", "Middle Name :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::text("middle_name", old('middle_name'), ['class' => 'form-control','placeholder' => 'Middle Name'])}}
                        </div>
                    </div>

                    <div class="row mb-3">
                        {{ Form::Label("Date of Birth", "Date of Birth * :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::date('dob', old('dob'), ['class' => 'form-control','placeholder' => 'Date of Birth']) }}
                        </div>
                    </div>
                    <div class="row mb-3">
                        {{ Form::Label("Designation", "Designation :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::select('designation', $designations->pluck('designation','id'), old('designation'), ['class' => 'form-control','placeholder' => 'Designation']) }}
                        </div>
                    </div>
                    <div class="row mb-3">
                        {{ Form::Label("Confirmation Date", "Confirmation Date :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::date('confirmation_date', old('confirmation_date'), ['class' => 'form-control','placeholder' => 'Confirmation Date']) }}
                        </div>
                        
                    </div>
                   

                  

                </div>

                <div class="col-sm-4">
                    <div class="row mb-3">
                        {{ Form::Label("Joining Date", "Joining Date * :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::date('joining_date', old('joining_date'), ['class' => 'form-control','placeholder' => 'Joining Date']) }}
                        </div>
                    </div>

                    <div class="row mb-3">
                        {{ Form::Label("Last Name", "Last Name :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::text("last_name", old('last_name'), ['class' => 'form-control','placeholder' => 'Last Name'])}}
                        </div>
                    </div>
                    
                    <div class="row mb-3">
                        {{ Form::Label("Previous Experience", "Previous Experience :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Month</label>
                                    <select class="form-control" name="months">
                                    @for ($i = 0; $i <= 12 ; $i++)

                                    <option value="{{ $i }}" @if($i == old('months')) selected @endif>{{ $i }}</option>

                                    @endfor
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Year</label>
                                    <select class="form-control" name="years">
                                            @for ($i = 0; $i <= 15 ; $i++)
        
                                                    <option value="{{ $i }}" @if($i == old('years')) selected @endif>{{ $i }}</option>
        
                                            @endfor
                                        </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        {{ Form::Label("Aadhar No", "Aadhar No :", array('class' =>'col-md-4' , 'align' => 'left')) }}
                        <div class="col-md-8">
                            {{ Form::text("aadhar_no", old('aadhar_no'), ['class' => 'form-control','placeholder' => 'Aadhar No'])}}
                        </div>
                    </div>

                </div>
            </div>
            
            <div class="col-md-12 text-center">
                <div class="row text-center">
                    <div class="col-md-12">
                        {{Form::submit('Create',array('class' => 'btn btn-success'))}}

                    </div>
                </div>
            </div>
        </div>  
    </div>
    {{ Form::close() }}
    @endsection


