@extends('layouts.dashboard')
@section('title')
Users
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
            <div class="col-sm-12">
            <h1 class="page-title">User Setting</h1>
                <ol class="breadcrumb">
        		  	<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/user') }}">Users</a></li>
                    <li class="breadcrumb-item active"><a class="">User Settings</a></li>
        		</ol>
            </div>
		</div>
    </div>
    <div>
        @if(!$user->gender)
            <div class="alert alert-secondary alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Warning! No Gender selected.</strong>
            </div>
            @endif
            @if(!$user->designation)
            <div class="alert alert-secondary">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Warning! No Designation specified</strong>
            </div>
            @endif
            @if(!$user->parent_id)
            <div class="alert alert-secondary alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Warning! No Reporting Manager assigned. </strong>
            </div>
            @endif
            @if(!$user->confirmation_date)
            <div class="alert alert-secondary alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Warning: Employee is not yet confirmed.</strong>
            </div>
        @endif
    </div>
    <div class="card border">
        <div class="card-body">
            <div class="row" >
                <div class="col-md-12">
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span>{{ $error }}</span><br/>
                            @endforeach
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ session('message') }}</span><br/>
                        </div>
                    @endif
                    @if(session('alert-class'))
                        <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span>{{ session('alert-class') }}</span><br/>
                        </div>
                    @endif

                    
                    
                </div>
            </div>
            {{ Form::open(['url' => 'user/'.$user->id, 'method' => 'put']) }}
            <div class="row">
                <div class="col-md-6">

                    {{ Form::text("name", $user->name, ['class' => 'form-control','placeholder' => 'Full Name'])}}
                </div>
                <div class="col-md-6" >
                    {{ Form::text("email", $user->email, ['class' => 'form-control','placeholder' => 'Email','disabled'])}}
                </div>
            </div>
            <br>
            <div class="row">
                {{ Form::Label("Name", "Name * :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::text("first_name", $user->first_name, ['class' => 'form-control','placeholder' => 'First Name'])}}
                </div>
                <div class="col-md-3">
                    {{ Form::text("middle_name", $user->middle_name, ['class' => 'form-control','placeholder' => 'Middle Name'])}}
                </div>
                <div class="col-md-3">
                    {{ Form::text("last_name", $user->last_name, ['class' => 'form-control','placeholder' => 'Last Name'])}}
                </div>
            </div>
            <br>
            <div class="row">
                {{ Form::Label("Joining Date", "Joining Date * :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::date('joining_date', $user->joining_date, ['class' => 'form-control','placeholder' => 'Joining Date']) }}
                </div>
                {{ Form::Label("Confirmation Date", "Confirmation Date :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::date('confirmation_date', $user->confirmation_date, ['class' => 'form-control','placeholder' => 'Confirmation Date']) }}
                </div>
            </div>
            <br>
            <div class="row">
                {{ Form::Label("Date of Birth", "Date of Birth * :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::date('dob', $user->dob, ['class' => 'form-control','placeholder' => 'Date of Birth']) }}
                </div>
                {{ Form::Label("Employee Id", "Employee Id * :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::text("employee_id", $user->employee_id, ['class' => 'form-control','placeholder' => 'Employee Id'])}}
                </div>
            </div>
            <br>
            <div class="row">
                {{ Form::Label("Release Date", "Release Date :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::date('release_date', $user->release_date, ['class' => 'form-control','placeholder' => 'Release Date']) }}
                </div>
                {{ Form::Label("Reporting Manager Name", "Reporting Manager Name :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::select('parent_id', $users->pluck('name','id'), $user->parent_id, ['class' => 'form-control','placeholder' => 'Reporting Manager']) }}
                </div>
            </div>
            <br>
            <div class="row">
                {{ Form::Label("Aadhar No", "Aadhar No :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::text("aadhar_no", $user->userDetails->aadhar_no ?? '', ['class' => 'form-control','placeholder' => 'Aadhar No'])}}
                </div>
                {{ Form::Label("Previous Experience", "Previous Experience :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    <div class="row">
                    <div class="col-md-6">
                                <label>Month</label>
                                <select class="form-control" name="months">
                                    @for ($i = 0; $i <= 12 ; $i++)
                                        @if ( $i == $user->months )
                                            <option value="{{ $i }}" selected>{{ $i }}</option>
                                        @else
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endif
                                    @endfor
                                </select>
                    </div>
                    <div class="col-md-6">
                            <label>Year</label>
                            <select class="form-control" name="years">
                                    @for ($i = 0; $i <= 15 ; $i++)
                                        @if ( $i == $user->years )
                                            <option value="{{ $i }}" selected>{{ $i }}</option>
                                        @else
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endif
                                    @endfor
                                </select>
                            </div>
                    </div>

                </div>
            </div>
            <br>
            <div class="row">
                {{ Form::Label("Gender", "Gender :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::select('gender', ['male' => 'male', 'female' => 'female'], $user->gender,['id'=>'selectid2','class' => 'form-control','placeholder' => 'Gender'])}}
                </div>
                {{ Form::Label("Designation", "Designation :", array('class' =>'col-md-3' , 'align' => 'left')) }}
                <div class="col-md-3">
                    {{ Form::select('designation', $designations->pluck('designation','id'), $user->designation, ['class' => 'form-control','placeholder' => 'Designation']) }}
                </div>
            </div>
            <br>


            <div class="col-md-12 text-center">
                <div class="row text-center">
                    <div class="col-md-12">
                        {{Form::submit('Update',array('class' => 'btn btn-success'))}}
                        @if($user->is_active)
                            <a href="/user/{{$user->id}}/active-toggle" class="btn btn-danger" >De-Activate</a>
                        @else
                        <a href="/user/{{$user->id}}/active-toggle" class="btn btn-success" >Activate</a>
                        @endif
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <div>
        <h4>Add Account Details</h4>
    </div>

<div>
    <div class="row">
    @if(count($accountTypes) > 0)
        @foreach ($accountTypes as $type)
            <div class="col-sm-6">
                <div class="card border mb-2">
                    <div class="card-body">
                        <h6 class=" mb-2 p-0">{{$type->name}}</h6>
                        {{ Form::open(array('url' => "user/updateOrCreate/". $user->id))}}
                            <div class="row">
                                {{Form::hidden('account_type_id', $type->id)}}
                                @if(!empty($type->userAccount($user->id)))
                                    {{Form::hidden('account_id', $type->userAccount($user->id)->id)}}
                                @endif
                                {{ Form::Label("account_number", "Account Number :", array('class' =>'col-md-4' , )) }}
                                {{ Form::text("account", !empty($type->userAccount($user->id)) ? $type->userAccount($user->id)->account_number : null, ['class' => 'form-control col-sm-7', ($type->is_editable != 1)? 'disabled' :''])}}

                            </div>
                            @if($type->metaKeys)
                                @foreach($type->metaKeys as $key)
                                    <div class="row mt-2">
                                        {{ Form::Label($key->key, $key->key." :", array('class' =>'col-md-4' )) }}
                                            {{ Form::text("keys[".$key->key."]", !empty($type->userAccount($user->id)) ? $type->userAccount($user->id)->metaValue($key->id) : null, ['class' => 'form-control col-sm-7',($type->is_editable != 1)? 'disabled' :''])}}
                                    </div>
                                @endforeach
                            @endif
                            @if($type->is_editable)
                            <div>
                                {{Form::submit('Save',array('class' => 'btn btn-success float-right mt-4'))}}
                            </div>
                            @endif
                        {{Form::close()}}
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="col-md-12" style="padding:0">
            <span>No Account Type Found</span>
        </div>
    @endif
    </div>
</div>



@endsection


