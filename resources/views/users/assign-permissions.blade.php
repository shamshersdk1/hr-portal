
@extends('layouts.dashboard')
@section('title')
Users
@endsection
@section('main-content')
<div class="page-title-box">
    	<div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Assign Permissions</h1>
                <ol class="breadcrumb">
        		  	<li class="breadcrumb-item"> <a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"> <a href="/user">list of Users</a></li>
        		  	<li class="breadcrumb-item active"> Add Permission</li>
        		</ol>
            </div>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card border">
                <div class="card-body pt-2">
                    <label>Assign permissions to {{$user->name}}.</label>
                    <div class="d-flex">
                       
                        {{ Form::open(array('url' => "user/". $user->id. "/save-permissions")) }}
                        @if(isset($permissions))
                                <div class="d-flex" >
                                    @foreach($permissions as $permission)
                                    <div class="custom-control custom-checkbox mr-4">
                                        <input type="checkbox" class="custom-control-input" value="{{$permission->id}}" name="permissions[]" id="{{$permission->id}}" 
                                          {{  $user->hasPermissionTo($permission->name) ? 'checked="checked" ' : ''  }} />
                                        <label class="custom-control-label" for="{{$permission->id}}">{{ $permission->name }}</label>
                                        </div>	
                                        @endforeach				      		
                                   
                                </div>	
                    </div>
                        @endif
                        {{Form::submit('Save',array('class' => 'btn btn-success float-right'))}}
                        {{ Form::close() }}



                    </div>
                </div>
            </div>
        </div>
    </div>
 

    {{-- <div class="row">
        <div class="col-md-12">
				<table class="table table-striped">
                    <thead>
                        <th width="10%">#</th>
                        <th width="10%">Name</th>
                        <th width="25%" class="text-right">Actions</th>
                    </thead>
				@if(isset($assignedPermissions))
                    @if(count($assignedPermissions) > 0)
                        @foreach($assignedPermissions as $index => $permission)
                            <tr>
                                <td class="td-text">{{$index+1}}</td>
                                <td class="td-text"> {{$permission->name}}</td>
                                <td class="text-right">
                                    <div style="display:inline-block;" class="deleteform">
                                    {{ Form::open(['url' => 'user/'.$user->id.'/delete-permission/'.$permission->id, 'method' => 'get']) }}
                                    {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                    {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" class="text-center">No Assigned permissions.</td>
                        </tr>
                    @endif
				@endif
			</table>
        </div>
    </div>
</div> --}}

@endsection
