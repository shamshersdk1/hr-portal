
@extends('layouts.dashboard')
@section('title')
Users
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-12">
            <h1 class="page-title">Assign Roles</h1>
            
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"> <a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"> <a href="/user">List of Users</a></li>
                    <li class="breadcrumb-item active"> Add Role</li>
                </ol>
                
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card border">
                <div class="card-body pt-2">
                    <h6>Assign Roles to {{$user->name}}.</h6>

                    <div class="d-flex">
                        {{ Form::open(array('url' => "user/". $user->id. "/save-roles")) }}
                        @if(isset($roles))
                            <div class="d-flex">
                                        @foreach($roles as $role)
                                        <div class="custom-control custom-checkbox mr-4">
                                        <input type="checkbox" class="custom-control-input" value="{{$role->id}}" name="roles[]" id="{{$role->id}}">
                                        <label class="custom-control-label" for="{{$role->id}}">{{ $role->name }}</label>
                                        </div>

                                        @endforeach

                                
                                            {{-- <select class="js-example-basic-multiple22 " multiple="multiple" name="roles[]" style="width:100%">
                                                @foreach($roles as $role)
                                                    <option value="{{$role->id}}">{{ $role->name }}</option>
                                                @endforeach
                                            </select>
                                    --}}
                                </div>	
                            </div>
                        @endif
                        {{Form::submit('Save',array('class' => 'btn btn-success float-right '))}}
                        {{ Form::close() }}


                    </div>
                </div>
            </div>
    </div>


    {{-- <div class="card border">
     <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <th width="10%">#</th>
                            <th width="10%">Name</th>
                            <th width="25%" class="text-right">Actions</th>
                        </thead>
                    @if(isset($assignedRoles))
                        @if(count($assignedRoles) > 0)
                            @foreach($assignedRoles as $index => $role)
                                <tr>
                                    <td class="td-text">{{$index+1}}</td>
                                    <td class="td-text"> {{$role->name}}</td>
                                    <td class="text-right">
                                        <div style="display:inline-block;" class="deleteform">
                                        {{ Form::open(['url' => 'user/'.$user->id.'/delete-role/'.$role->id, 'method' => 'get']) }}
                                        {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                        {{ Form::close() }}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4" class="text-center">No Assigned roles.</td>
                            </tr>
                        @endif
                    @endif
                </table>
            </div>
        </div>
     </div>
    </div> --}}
       
</div>
<script type="text/javascript">
    
</script>
@endsection
