@extends('layouts.dashboard')
@section('title')
Calendar Leave Credits
@endsection
@section('main-content')

	<div class="page-title-box">
	    <div class="row align-items-center">
	        <div class="col-sm-6">
	            <h4 class="page-title">Calendar Leave Credits</h4>
	            <ol class="breadcrumb">
	                <li class="breadcrumb-item">
	                	<a href="/dashboard">Dashboard</a>
	                </li>
	                <li class="breadcrumb-item active">List of leave credits</li>
	            </ol>
	        </div>
        </div>
	</div>


    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger p-1 m-1">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                </div>
                @endforeach
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="panel-body">
                    <br>
                    {{ Form::open(array('url' => "calendar-leave-credit")) }}
                        <div class="form-group row">
                            {{ Form::Label('Code', 'Leave Type:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
                            <div class="col-md-4" style="padding:0">
                                {{ Form::select('leave_type', $leaveTypes->pluck('title','id'), null, ['class' => 'form-control','placeholder' => 'Leave Type']) }}
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::Label('description', 'Max Leave:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
                            <div class="col-md-4" style="padding:0">
                                {{ Form::text('max_leave',null,['class'=>'form-control']) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                            {{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

		<div class="col-sm-12">
			<div class="card border">
                <div class="card-body">
                    {{ Form::open(['url' => 'calendar-leave-credit', 'method' => 'post','class' => 'form-inline mt-3']) }}
                    <div class="table-responsive mt-3">
                        <table class="table table-striped table-outer-border table-condensed table-sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="text-left">Year</th>
                                    <th>Leave Type</th>
                                    <th class="text-left">Leave</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($calendarLeaveCredits as $index => $calendarLeaveCredit)
                               <tr>
                               <td>{{$index+1}}</td>
                               <td>{{$calendarLeaveCredit->calendarYear->year}}</td>
                               <td>{{$calendarLeaveCredit->leaveType->title}}</td>
                               <td>{{$calendarLeaveCredit->max_allowed_leave}}</td>
                               </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{Form::close()}}                             
                </div>
			</div>
		</div>
	</div>

@endsection
