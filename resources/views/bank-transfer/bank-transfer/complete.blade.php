@extends('layouts.dashboard')
@section('title')
Bank Transfer Complete
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
                <h1 class="page-title">Bank Transfer</h1>
                <ol class="breadcrumb">
                      <li><a href="/dashboard">Dashboard</a></li>
                      <li><a href="/bank-transfer">Bank Transfer</a></li>
                     <li><a href="/bank-transfer">{{$bankTransfers->id}}</a></li>
        		</ol>
            </div>

    </div>

    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
            @endif
            @if(session('alert-class'))
                <div class="alert alert-danger">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ session('alert-class') }}</span><br/>
		        </div>
            @endif
	    </div>
    </div>

    <div class="card border">
				<div class="card-body">
					<div class="row">
						<div class="col-sm-4">
							<span>Month: <label>{{$bankTransfers->month->formatMonth()}}</label></span><br>
							<span>Status: <label>
								@if($bankTransfers->status == 'open')
								<span class="badge badge-warning">Open</span>
								@elseif($bankTransfers->status == 'completed')
								<span class="badge badge-success">Completed</span>
								@elseif($bankTransfers->status == 'rejected')
								<span class="badge badge-danger">Rejected</span>
								@else
								@endif
							</label></span></br>
                            <span>Created At: <label>{{datetime_in_view($bankTransfers->created_at)}}</label></span><br>
                        </div>
                        <div class="text-right  col-sm-8">
                            <a class="btn btn-danger btn-sm crude-btn" href="/bank-transfer/{{$bankTransfers->id}}/complete-bank-transfer"> Complete Bank Transfer</a>
                        </div>
            </div>
        </div>
    </div>

    <div class="row">
		<div class="col-md-12">

			<div class="card border">
                <div class="card-body">
                    <table id="bank-table" class=" table table-striped table-sm table-outer-border"  style="width:100%">
                        <thead>
                            <tr>
                                <th class="">#</th>
                                <th class=""  width="100px">Employee Id</th>
                                <th class="" width="150px">Employee Name</th>
                                <th class="" width="150px">Bank Account Number</th>
                                <th class="">Amount</th>
                                <th class="">Transaction Amount</th>
                                <th class="">Mode of Transfer</th>
                                <th class="" width="150px">Bank Transaction ID</th>
                                <th class="" width="150px">Comment</th>
                                <th class="">Status</th>
                                <th class="" width="120px">Txn Posted Date</th>

                            </tr>

                        </thead>
                        <tbody>
                    @if($bankTransfers)
                        @foreach($bankTransfers->transferUsers as $index=>$bankTransfer)

                            <tr>
                                <td>{{$index+1}}</td>
                                <td class="" width="100px">{{$bankTransfer->user->employee_id}}</td>
                                <td class="" width="150px">{{$bankTransfer->user->name}}</td>
                                <td class="" width="150px">{{$bankTransfer->bank_acct_number}}</td>
                                <td class="">{{$bankTransfer->amount}} </td>

                                    <td class="">
                                        {{$bankTransfer->transaction_amount}}
                                    </td>
                                    <td class="">
                                        {{$bankTransfer->mode_of_transfer}}
                                    </td>
                                    <td class="">
                                        {{$bankTransfer->bank_transaction_id}}
                                    </td>
                                    <td class="">
                                        {{$bankTransfer->comment}}
                                    </td>
                                    <td>
                                        @if($bankTransfer->status == 'completed')
                                            <span class="label label-warning">Completed<span>
                                        @elseif($bankTransfer->status == 'rejected')
                                            <span class="label label-danger">Rejected<span>
                                        @endif
                                    </td>
                                    <td class="">{{$bankTransfer->txn_posted_date}} </td>


                            </tr>
                        @endforeach
                    @else
                        <tr >
                            <td colspan="8" class="text-center">No Record Found For Selected Month.</td>
                        </tr>
                    @endif
                        </tbody>
                    </table>
            </div>
			</div>
		</div>
        {{ Form::close() }}
	</div>
</div>

@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#bank-table').DataTable( {
            scrollY:        true,
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            stateSave: true,
            "bDestroy": true,
            fixedColumns:   {
                leftColumns: 3,
            }
        } );
    } );
</script>
@endsection

