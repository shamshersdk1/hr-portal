@extends('layouts.dashboard')
@section('title')
Bank Transfer
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="page-title">Bank Transfer</h1>
                <ol class="breadcrumb">
                      <li><a href="/dashboard">Dashboard</a></li>
                      <li><a href="/bank-transfer">Bank Transfer</a></li>
                     <li><a href="/bank-transfer">{{$currMonth ? $currMonth->formatMonth() : ' '}}</a></li>
        		</ol>
            </div>
            <div class="col-sm-6">
                <div class="d-flex justify-content-end align-items-center">
                    
                    <div style="display:inline-block;" class="lockform ml-2">
                        {{ Form::open(['url' => '/payslips', 'method' => 'get', 'class' => 'm-0']) }}
                        {{ Form::submit('View Payslip',['class' => 'btn btn-dark crude-btn']) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
    </div>
</div>

        <div class="row">
                <div class="col-md-12">
                    @if(!empty($errors->all()))
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span>{{ $error }}</span><br/>
                            @endforeach
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ session('message') }}</span><br/>
                        </div>
                    @endif
                    @if(session('alert-class'))
                        <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <span>{{ session('alert-class') }}</span><br/>
                        </div>
                    @endif
                </div>
            </div>

           

            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        <div class="">
                            <div class="row table-header-section">
                                <div class="col-sm-6 d-flex align-items-center"> 
                                    @if(isset($monthList))
                                    <label class="pb-0 mb-0 mr-1">Select Month</label>
                                    <select id="selectid2" name="month"  placeholder= "{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}">
                                        <option value=""></option>
                                        @foreach($monthList as $x)
                                        <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                                        @endforeach
                                    </select>
                                @endif
                                </div>
                                  <div class="col-sm-6 table-search">
                                  </div>
                                </div>
                            <div class="float-right mb-2">
                             
                            </div>
                        <table class="table table-striped table-sm table-outer-border" id="bank-table">
                        <thead>
                            <tr>
                            <th class="td-text">#</th>
                            <th class="td-text">Created At</th>
                            <th class="td-text">Hold Users</th>
                            <th class="td-text">Processed Users</th>
                            <th class="td-text">Status</th>
                            <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($bankTransfers) > 0)
                            @foreach($bankTransfers as $index=>$bankTransfer)
                                <tr>
                                    <td class="td-text">{{$index + 1}}</td>
                                    <td class="td-text">{{datetime_in_view($bankTransfer->created_at)}}</td>
                                    <td class="td-text">{{$bankTransfer->holdUsers->count()}} </td>
                                    <td class="td-text">{{$bankTransfer->transferUsers->count()}} </td>
                                    <td class="td-text">{{$bankTransfer->status}}</td>
                                    <td class="text-right">
                                        <!-- <a class="btn btn-primary btn-sm crude-btn" href="bank-transfer/{{$bankTransfer->id}}/download"><i class="fa fa-download"></i> &nbsp;Download CSV</a> -->
                                        <a class="btn btn-danger btn-sm crude-btn" href="/bank-transfer/{{$bankTransfer->id}}/complete-bank-transfer"> Complete Bank Transfer</a>
                                        <a class="btn btn-warning btn-sm crude-btn" href="/bank-transfer/{{$bankTransfer->id}}/salary-sheet-compare"> &nbsp;Salary Sheet Compare</a>
                                        <a class="btn btn-primary btn-sm crude-btn" href="/bank-transfer/{{$bankTransfer->id}}/salary-sheet"> &nbsp;Salary Sheet</a>
                                        <a class="btn btn-success btn-sm crude-btn" href="/bank-transfer/{{$bankTransfer->id}}/view-complete"> &nbsp;View</a>
                                        @if(count($bankTransfer->holdUsers) > 0)
                                            <a class="btn btn-danger btn-sm crude-btn" href="/bank-transfer/{{$bankTransfer->id}}/hold"> &nbsp;View Hold(s)</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr >
                                <td colspan="8" class="text-center">No Record Found For Selected Month.</td>
                            </tr>
                        @endif
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {

        var t = $('#bank-table').DataTable( {
            "columnDefs": [ {
                "orderable": false,
                "targets": 0,
            } ],
            "order": false,
            "paging": false,
            "info": false,
            "bLengthChange": false,
            fixedHeader: true,
            drawCallback: function() {
                $('.dt-select2').select2();
             }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        $('.table-header-section .table-search').append($('.dataTables_filter'))

    });



    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/bank-transfer/"+optionValue; 
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}',
			allowClear:true
        });
 
</script>
@endsection

