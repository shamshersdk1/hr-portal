@extends('layouts.dashboard')
@section('title')
Bank Transfer Compare
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h1 class="page-title">Bank Transfer</h1>
            <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/bank-transfer">Bank Transfer</a></li>
                    <li><a href="/bank-transfer">Bank Transfer Compare for {{$bankTransferObj->id}}</a></li>
            </ol>
        </div>
    </div>

    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
            @endif
            @if(session('alert-class'))
                <div class="alert alert-danger">
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ session('alert-class') }}</span><br/>
		        </div>
            @endif
	    </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-8">
                    <span>Month: <label>{{$bankTransferObj->month->formatMonth()}}</label></span><br>
                    <span>Status: <label>
                        @if($bankTransferObj->status == 'open')
                        <span class="label label-warning">Open</span>
                        @elseif($bankTransferObj->status == 'completed')
                        <span class="label label-success">Completed</span>
                        @elseif($bankTransferObj->status == 'rejected')
                        <span class="label label-danger">Rejected</span>
                        @else
                        @endif    
                    </label></span></br>
                    <span>Created At: <label>{{datetime_in_view($bankTransferObj->created_at)}}</label></span><br>
                </div>
            </div>
        </div>
    </div>
    
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="summary-table">
                <thead>
                    <th class="text-center">#</th>
                    <th class="text-center">Employee Id</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Bank</th>
                    @foreach($apprCompType as $type)
                        <th></th>
                        <th class="text-center">{{$type->name}}</th>
                        <th></th>
                    @endforeach
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        @foreach($apprCompType as $type)
                            <th class="text-center">Prep Salary</th>
                            <th class="text-center">Transaction</th>
                            <th class="text-center">Diff</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @if(count($users) > 0)
                    @foreach($users as $user)
                    @if(isset($data[$user->id]))
                        <tr>
                            <td class="text-center"></td>
                            <td class="text-center">{{ $user->employee_id}}</td>
                            <td class="text-center">{{ $user->name}}</td>
                            <td class="text-center">{{$data[$user->id]['Bank'] ?? 0}}</td>
                            @foreach($apprCompType as $type) 
                                <td class="text-center">{{$data[$user->id]['previous'][$type->code] ?? 0}}</td>
                                <td class="text-center">{{$data[$user->id]['current'][$type->code] ?? 0}}</td>
                                @if(!isset($data[$user->id]['previous'][$type->code]) && isset($data[$user->id]['current'][$type->code]))
                                    @if($data[$user->id]['current'][$type->code] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{(-1) * $data[$user->id]['current'][$type->code]}}</td>
                                    @endif
                                @elseif(isset($data[$user->id]['previous'][$type->code]) && !isset($data[$user->id]['current'][$type->code]))
                                    @if($data[$user->id]['previous'][$type->code] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{$data[$user->id]['previous'][$type->code]}}</td>
                                    @endif
                                @elseif(isset($data[$user->id]['previous'][$type->code]) && isset($data[$user->id]['current'][$type->code]))
                                    @if($data[$user->id]['previous'][$type->code] - $data[$user->id]['current'][$type->code] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{$data[$user->id]['previous'][$type->code] - $data[$user->id]['current'][$type->code]}}</td>
                                    @endif
                                @else
                                    <td>0</td>
                                @endif
                            @endforeach
                        </tr> 
                    @endif
                    @endforeach
                @else
                    <tr>
                        <td class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500,
            fixedHeader: true,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            scrollY:        true,
            scrollX:        true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection


