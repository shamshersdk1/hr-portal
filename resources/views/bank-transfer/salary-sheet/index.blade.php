@extends('layouts.dashboard')
@section('title')
Salary Transaction
@endsection
@section('main-content')
<div class="page-title-box">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="page-title">Salary Transaction</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/prep-salary">Prep Salary</a></li>
                    <li class="active">Salary Sheet for {{$prepSalObj ??'' ? $prepSalObj->month->formatMonth() : ""}}</li>
                </ol>
            </div>
            <div class="col-sm-6">
                <div class="float-right">
                    <a class="btn btn-primary btn-sm crude-btn pull-right" href="salary-sheet/download"><i class="fa fa-download"></i> &nbsp;Download CSV</a>
                </div>

            </div>
        </div>
</div>
 <div class="card border">
     <div class="card-body">
        <div class="user-list-view">
            <div class="table-responsive">
                <table class="table table-striped table-outer-border  table-sm" id="summary-table">
                    <thead>
                        <th class="text-center">Sl.No</th>
                        <th class="text-center">Employee Id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Bank</th>
                        <th class="text-center">Working Days</th>
                        <th class="text-center">Days Worked</th>
                        <th class="text-center">Monthly Gross Salary</th>
                        <th class="text-center">Basic</th>
                        <th class="text-center">HRA</th>
                        <th class="text-center">Car Allowance</th>
                        <th class="text-center">Food Allowance</th>
                        <th class="text-center">Leave Travel Allowance</th>
                        <th class="text-center">Special Allowance</th>
                        <th class="text-center">Stipend</th>
                        <th class="text-center">PF Employee</th>
                        <th class="text-center">PF Employeer</th>
                        <th class="text-center">PF Other</th>
                        <th class="text-center">ESI</th>
                        <th class="text-center">ESI Employeer</th>
                        <th class="text-center">Professional Tax</th>
                        <th class="text-center">VPF Deduction</th>
                        <th class="text-center">Medical Insurance</th>
                        <th class="text-center">Food Deduction</th>
                        <th class="text-center">Loan Deduction</th>
                        <th class="text-center">Adhoc Amount</th>
                        <th class="text-center">TDS</th>
                        <th class="text-center">Onsite Bonus</th>
                        <th class="text-center">AdditionalWorkDay Bonus</th>
                        <th class="text-center">Extra Hour Bonus</th>
                        <th class="text-center">TechTalk Bonus</th>
                        <th class="text-center">Performance Bonus</th>
                        <th class="text-center">Referral Bonus</th>
                        <th class="text-center">CTC Bonus</th>
                        <th class="text-center">NON CTC Bonus</th>
                        <th class="text-center">Total Deduction</th>
                        <th class="text-center">Net Payable</th>
                    </thead>
                    <tbody>
                    @if(count($data) > 0)
                        @foreach($data as $user)
                            <tr>
                                <td class="text-center">{{ $user['sl_no']}}</td>
                                <td class="text-center">{{ $user['employee_id']}}</td>
                                <td class="text-center">{{ $user['name']}}</td>
                                <td class="text-center">{{ $user['bank_ac_no'] ?? ''}}</td>
                                <td class="text-center">{{ $user['working_days'] ?? ''}}</td>
                                <td class="text-center">{{ $user['days_worked'] ?? ''}}</td>
                                <td class="text-center">{{ $user['monthly_gross_salary'] ?? 0}}</td>
                                <td class="text-center">{{ $user['basic']}}</td>
                                <td class="text-center">{{ $user['hra']}}</td>
                                <td class="text-center">{{ $user['car_allowance']}}</td>
                                <td class="text-center">{{ $user['food_allowance']}}</td>
                                <td class="text-center">{{ $user['lta']}}</td>
                                <td class="text-center">{{ $user['special_allowance']}}</td>
                                <td class="text-center">{{ $user['stipend']}}</td>
                                <td class="text-center">{{ $user['pf_employee']}}</td>
                                <td class="text-center">{{ $user['pf_employeer']}}</td>
                                <td class="text-center">{{ $user['pf_other']}}</td>
                                <td class="text-center">{{ $user['esi']}}</td>
                                <td class="text-center">{{ $user['esi_employer']}}</td>
                                <td class="text-center">{{ $user['professional_tax']}}</td>
                                <td class="text-center">{{ $user['vpf'] ?? 0}}</td>
                                <td class="text-center">{{ $user['medical_insurance'] ?? 0}}</td>
                                <td class="text-center">{{ $user['food_deduction']}}</td>
                                <td class="text-center">{{ $user['loan_deduction']}}</td>
                                <td class="text-center">{{ $user['adhoc_amount']}}</td>
                                <td class="text-center">{{ $user['tds']}}</td>
                                <td class="text-center">{{ $user['onsite_bonus'] ?? 0}}</td>
                                <td class="text-center">{{ $user['additional_work_day_bonus'] ?? 0}}</td>
                                <td class="text-center">{{ $user['extra_hour_bonus'] ?? 0}}</td>
                                <td class="text-center">{{ $user['tech_talk_bonus'] ?? 0}}</td>
                                <td class="text-center">{{ $user['performance_bonus'] ?? 0}}</td>
                                <td class="text-center">{{ $user['referral_bonus'] ?? 0}}</td>
                                <td class="text-center">{{ $user['ctc_bonus']}}</td>
                                <td class="text-center">{{ $user['non_ctc_bonus']}}</td>
                                <td class="text-center">{{ $user['total_deduction']}}</td>
                                <td class="text-center">{{ $user['net_payable']}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="20" class="text-center">
                                No Records found
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
     </div>
 </div>
 @endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500,
            fixedHeader: true,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            scrollY:        500,
            scrollX:        true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );

    });
</script>
@endsection
