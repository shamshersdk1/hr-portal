@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-centers">
            <div class="col-sm-6">
                <h1 class="page-title">Non Ctc Bonuses</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li><a href="{{ url('/non-ctc-bonus') }}">Non Ctc Bonus</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6 text-right">

                @if(!$month->nonCtcBonusSetting || ($month->nonCtcBonusSetting &&  $month->nonCtcBonusSetting->value == 'open'))
                        <span>
                            <a href="/non-ctc-bonus/{{$month->id}}/status-month" class="btn btn-warning"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>

                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->nonCtcBonusSetting && $month->nonCtcBonusSetting->value == 'locked')
                        <span>
                            <a href="/non-ctc-bonus/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                @endif
                <span>
                    <a href="/non-ctc-bonus/due" target="_blank" class="btn btn-success">Due Bonuses</a>
                    <a href="/non-ctc-bonus/confirmed" target="_blank" class="btn btn-primary">Confirmed Bonuses</a>
                </span>
            </div>
    </div>
</div>
	@include('flash')
        <div class=" ">
                <div class="row table-header-section">
                <div class="col-sm-6">
                    @if(isset($monthList))
                    <label>Select Month</label>
                        <select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                                <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                     @endif
                </div>
                  <div class="col-sm-6 table-search">
                  </div>
                </div>

                @if($month && $month->nonCtcBonusSetting &&  $month->nonCtcBonusSetting->value == 'locked')
                <div class="">
                    <span class="btn btn-primary btn-sm">Locked at {{ $month->nonCtcBonusSetting ? datetime_in_view($month->nonCtcBonusSetting->created_at) : ''}}</span>
                </div>
                @endif
                <table class="table table-striped table-sm table-outer-border" id="bonus-table">
                    <thead>
                        <tr>
                            <th class="td-text">#</th>
                            <th class="td-text">Employee Id</th>
                            <th class="td-text">Name</th>
                            <th class="td-text">All Bonuses</th>
                            <th class="td-text">Due for Payment</th>
                            <th class="td-text">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $index=0 ?>
                        @foreach($bonuses as $bonus)
                        <tr>
                            <td class="td-text"></td>
                            <td class="td-text">{{$bonus['user']['employee_id']}}</td>
                            <td class="td-text">{{$bonus['user']['name']}}</td>
                            <td class="td-text">
                                @if(isset($bonus['types']))
                                    @foreach($bonus['types'] as $type)
                                        @if($type['type'] == 'App\Models\Admin\AdditionalWorkDaysBonus')
                                            <span class="badge badge-dark">{{$type['count']}} Days Additional : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\OnSiteBonus')
                                            <span class="badge badge-primary">{{$type['count']}} Days Onsite : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\User\UserTimesheetExtra')
                                            <span class="badge badge-danger"> Extra Hours : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\ReferralBonus')
                                            <span class="badge badge-info">{{$type['count']}} Referral Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\PerformanceBonus')
                                            <span class="badge badge-success">{{$type['count']}} Performance Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\TechTalkBonusesUser')
                                            <span class="badge badge-warning">{{$type['count']}} TechTalk Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @else

                                        @endif
                                    @endforeach
                                @endif
                            </td>
                            <td class="td-text">
                                @if(isset($bonus['due_types']))
                                    @foreach($bonus['due_types'] as $type)
                                        @if($type['type'] == 'App\Models\Admin\AdditionalWorkDaysBonus')
                                            <span class="badge badge-dark">{{$type['count']}} Days Additional : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\OnSiteBonus')
                                            <span class="badge badge-primary">{{$type['count']}} Days Onsite : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\User\UserTimesheetExtra')
                                            <span class="badge badge-danger"> Extra Hours : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\ReferralBonus')
                                            <span class="badge badge-info">{{$type['count']}} Referral Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\PerformanceBonus')
                                            <span class="badge badge-success">{{$type['count']}} Performance Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\TechTalkBonusesUser')
                                            <span class="badge badge-warning">{{$type['count']}} TechTalk Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @else

                                        @endif
                                    @endforeach
                                @endif
                            </td>
                        <td class="td-text">{{$bonus['amount']}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
</div>
@endsection
@section('js')
@parent
<script>
    var t = $('#bonus-table').DataTable( {
        pageLength:500,
        dom: 'Bfrtip',
        buttons: [
            'csv'
        ],
        "order": [[ 1,'asc' ]]
    } );
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();


    $(document).ready(function() {
        $('.table-header-section .table-search').append($('.dataTables_filter'))


    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/non-ctc-bonus/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
        });
    });
</script>
@endsection
