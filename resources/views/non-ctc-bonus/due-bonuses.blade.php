@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-centers">
            <div class="col-sm-6">
                <h1 class="page-title">Due Bonuses</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li><a href="{{ url('/non-ctc-bonus') }}">Non CTC Bonuses</a></li>
        		  	<li>Due Bonus</li>
        		</ol>
            </div>
    </div>
</div>
@include('flash')
<div id="accordion">
    @foreach($data as $month)
        <div class="card">
        <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#{{'collapseTwo'.$month['month']->id}}" aria-expanded="false" aria-controls="{{'collapseTwo'.$month['month']->id}}">
                {{ $month['month']->formatMonth() }} | Total Due : {{ count($month['bonuses']) }}
            </button>
        </h5>
        </div>
        <div id="{{'collapseTwo'.$month['month']->id}}" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
        <div class="card-body">
                <div class=" ">
                    <div class="row table-header-section">
                        <table class="table table-striped table-sm table-outer-border" id="bonus-table-1">
                            <thead>
                                <tr>
                                    <th class="td-text">#</th>
                                    <th class="td-text">Employee Id</th>
                                    <th class="td-text">Name</th>
                                    <th class="td-text">Bonus Type</th>
                                    <th class="td-text">Amount</th>
                                    <th class="td-text">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index=0 ?>
                                @foreach($month['bonuses'] as $bonus)
            
                                <tr>
                                    <td class="td-text"></td>
                                    <td class="td-text">{{$bonus['user']['employee_id']}}</td>
                                    <td class="td-text">{{$bonus['user']['name']}}</td>
                                    <td class="td-text">
                                        @if(isset($bonus['types']))
                                            @foreach($bonus['types'] as $type)
                                                @if($type['type'] == 'App\Models\Admin\AdditionalWorkDaysBonus')
                                                    <span class="badge badge-dark">{{$type['count']}} Days Additional : &#x20B9 {{$type['sum']}}</span>
                                                @elseif($type['type'] == 'App\Models\Admin\OnSiteBonus')
                                                    <span class="badge badge-primary">{{$type['count']}} Days Onsite : &#x20B9 {{$type['sum']}}</span>
                                                @elseif($type['type'] == 'App\Models\User\UserTimesheetExtra')
                                                    <span class="badge badge-danger"> Extra Hours : &#x20B9 {{$type['sum']}}</span>
                                                @elseif($type['type'] == 'App\Models\Admin\ReferralBonus')
                                                    <span class="badge badge-info">{{$type['count']}} Referral Bonuses : &#x20B9 {{$type['sum']}}</span>
                                                @elseif($type['type'] == 'App\Models\Admin\PerformanceBonus')
                                                    <span class="badge badge-success">{{$type['count']}} Performance Bonuses : &#x20B9 {{$type['sum']}}</span>
                                                @elseif($type['type'] == 'App\Models\Admin\TechTalkBonusesUser')
                                                    <span class="badge badge-warning">{{$type['count']}} TechTalk Bonuses : &#x20B9 {{$type['sum']}}</span>
                                                @else

                                                @endif
                                            @endforeach
                                        @endif
                                    </td>
                                <td class="td-text">{{$bonus['amount']}}</td>
                                <td class="td-text">
                                    <a href="/bonus-due/{{$month['month']['id']}}/{{$bonus['user']['id']}}" class="btn btn-success btn-sm"><i class="fa fa-eye fa-fw"></i> View</a>
                                </td>
                                <td class="td-text">
                                    <a href="/bonus-due/{{$month['month']['id']}}/{{$bonus['user']['id']}}/approve-all" class="btn btn-success btn-sm"><i class="fa fa-tick fa-fw"></i> Approve All</a>
                                </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
        </div>
            
        </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
