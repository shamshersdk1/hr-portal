@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-centers">
            <div class="col-sm-6">
                <h1 class="page-title">Confirmed Bonuses</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li><a href="{{ url('/non-ctc-bonus') }}">Non CTC Bonus</a></li>
        		  	<li>Confirmed Bonus</li>
        		</ol>
            </div>
            @if(count($bonuses))
                <div class="col-sm-6 text-right">
                    <span>
                        <a href="/non-ctc-bonus/confirmed/delete-all" onClick="return confirm('All records will be deleted?')" class="btn btn-danger">Delete Bonuses</a>
                    </span>
                </div>
            @endif
    </div>
</div>
	@include('flash')
        <div class="">
            <div class="row table-header-section">
                <table class="table table-striped table-sm table-outer-border" id="bonus-table-1">
                    <thead>
                        <tr>
                            <th class="td-text">#</th>
                            <th class="td-text">Employee Id</th>
                            <th class="td-text">Name</th>
                            <th class="td-text">Bonus Type</th>
                            <th class="td-text">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $index=0 ?>
                        @foreach($bonuses as $bonus)
                        <tr>
                            <td class="td-text"></td>
                            <td class="td-text">{{$bonus['user']['employee_id']}}</td>
                            <td class="td-text">{{$bonus['user']['name']}}</td>
                            <td class="td-text">
                                @if(isset($bonus['types']))
                                    @foreach($bonus['types'] as $type)
                                        @if($type['type'] == 'App\Models\Admin\AdditionalWorkDaysBonus')
                                            <span class="badge badge-dark">{{$type['count']}} Days Additional : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\OnSiteBonus')
                                            <span class="badge badge-primary">{{$type['count']}} Days Onsite : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\User\UserTimesheetExtra')
                                            <span class="badge badge-danger"> Extra Hours : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\ReferralBonus')
                                            <span class="badge badge-info">{{$type['count']}} Referral Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\PerformanceBonus')
                                            <span class="badge badge-success">{{$type['count']}} Performance Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @elseif($type['type'] == 'App\Models\Admin\TechTalkBonusesUser')
                                            <span class="badge badge-warning">{{$type['count']}} TechTalk Bonuses : &#x20B9 {{$type['sum']}}</span>
                                        @else

                                        @endif
                                    @endforeach
                                @endif
                            </td>
                        <td class="td-text">{{$bonus['amount']}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
</div>
@endsection
@section('js')
@parent
<script>
    var t = $('#bonus-table-1').DataTable( {
        pageLength:500,
        dom: 'Bfrtip',
        buttons: [
            'csv'
        ],
        "order": [[ 1,'asc' ]]
    } );
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();


    $(document).ready(function() {
        $('.table-header-section .table-search').append($('.dataTables_filter'))

    }
</script>
@endsection
