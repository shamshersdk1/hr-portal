@extends('layouts.dashboard')
@section('title')
Bank | Account Meta Key
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-12">
            <h1 class="page-title">Account</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/bank/bank-account-meta-key">Account Meta Key</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card border">
                <div class="card-body">
                    {{ Form::open(array('url' => "bank/bank-account-meta-key/".$bankAccount->id, 'method' => "put")) }}
                    <div class="row">
                        {{ Form::Label('bank_type', 'Bank Type:',array('class' =>'col-md-4')) }}
                        <div class="col-md-6" >
                            {{ Form::select('bank_account_type_id', $bankTypes->pluck('name','id'), $bankAccount->bank_account_type_id, array('class' => 'userSelect1'  ,'placeholder' => 'Select Account Type','id' => 'selectid2'))}}
                        </div>
                    </div>
                    <div class="row mt-2">
                        {{ Form::label('key', 'Key Code :',['class' => 'col-md-4'])}}
                        <div class="col-md-6" >
                            {{ Form::text('key', $bankAccount->key,['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row mt-2">
                        {{ Form::label('is_required', 'Is Required :',['class' => 'col-md-4'])}}
                        <div class="col-md-6" >
                            {{ Form::radio('is_required', '1' , $bankAccount->is_required == 1) }} YES
                            {{ Form::radio('is_required', '0' , $bankAccount->is_required == 0) }} NO
                        </div>
                    </div>
                    <div class="row mt-2">
                        {{ Form::label('field_type', 'field_type :',['class' => 'col-md-4'])}}
                        <div class="col-md-6" >
                            {{ Form::text('field_type', $bankAccount->field_type,['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="form-group col-md-10 text-right">
                            {{Form::submit('Save',array('class' => 'btn btn-success '))}}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

@endsection
@section('js')
@parent
<script>
    $('#selectid2').select2({
			allowClear:true
		});
</script>
@endsection
