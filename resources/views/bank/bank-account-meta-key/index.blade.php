@extends('layouts.dashboard')
@section('title')
Bank | Account Meta Keys
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h1 class="page-title">Bank</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Account Meta Keys</li>
            </ol>
        </div>
        <div class="col-sm-6 text-right">
            {{Form::open(['url'=>'bank/bank-account-meta-key/create' ,'method'=>'get'])}}
            {{Form::button('<i class="fa fa-plus fa-fw"></i> Add Account Meta Key',
            array('type' => 'submit', 'class' => 'btn btn-success'))}}
            {{Form::close()}}
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-sm table-outer-border" id="bank-account">
                <thead>
                    <th width="5%">#</th>
                    <th widht="10%">Account Type</th>
                    <th width="10%">Key</th>
                    <th width="10%">Is Required</th>
                    <th width="10%">Field Type</th>
                    <th width="" class="text-right">Actions</th>
                </thead>
                <tbody>
                    @if($bankAccounts && count($bankAccounts)>0)
                    @foreach($bankAccounts as $index=> $bankAccount)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>
                            {{$bankAccount->accountType ? $bankAccount->accountType->name : ''}}
                        </td>
                        <td>
                            {{$bankAccount->key}}
                        </td>
                        <td>
                            @if($bankAccount->is_required == 1)
                                YES
                            @else
                                NO
                            @endif
                        </td>
                        <td>
                            {{$bankAccount->field_type}}
                        </td>
                        <td class="text-right">
                            <a
                                href="bank-account-meta-key/{{$bankAccount->id}}/edit"
                                class="btn btn-info btn-sm"
                                ><i
                                    class="fa fa-pencil btn-icon-space"
                                    aria-hidden="true"
                                ></i
                                >Edit</a
                            >
                            <div
                                style="display:inline-block;"
                                class="deleteform"
                            >
                                {{ Form::open(['url' => 'bank/bank-account-meta-key/'.$bankAccount->id, 'method' => 'delete']) }}
                                {{Form::button('<i
                                    class="fa fa-trash fa-fw btn-icon-space"
                                ></i>
                                Delete', array('type' => 'submit', 'class' =>
                                'btn btn-danger btn-sm', 'onclick' => 'return
                                confirm("Are you sure you want to delete this
                                item?")'))}}
                                {{ Form::close() }}
                            </div>
                        </td>
                    </tr>
                    @endforeach @else
                    <td colspan="8" style="text-align:center;">
                        <span class="align-center"
                            ><big>No Account Meta Key Found</big></span
                        >
                    </td>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

