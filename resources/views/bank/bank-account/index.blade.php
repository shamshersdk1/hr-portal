@extends('layouts.dashboard')
@section('title')
Bank | Account
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row breadcrumb-wrap">

            <div class="col-sm-6">
                <h1 class="page-title">Bank</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active">Account</li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                {{Form::open(['url'=>'bank/bank-account/create' ,'method'=>'get'])}}
                {{Form::button('<i class="fa fa-plus fa-fw"></i> Add Account',
                array('type' => 'submit', 'class' => 'btn btn-success'))}}
                {{Form::close()}}
            </div>

    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="card border">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-sm table-outer-border" id="bank-account">
                        <thead>
                            <th width="5%">#</th>
                            <th widht="10%">Account Type</th>
                            <th width="10%">Account Number</th>
                            <th width="10%">User Name</th>
                            <th width="10%">Created At</th>
                            <th width="10%">Updated At</th>
                            <th width="" class="text-right">Actions</th>
                        </thead>
                        <tbody>
                            @if($bankAccounts && count($bankAccounts)>0)
                            @foreach($bankAccounts as $index=> $bankAccount)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>
                                    {{$bankAccount->accountType ? $bankAccount->accountType->name : ''}}
                                </td>
                                <td>
                                    {{$bankAccount->account_number}}
                                </td>
                                <td>
                                    {{$bankAccount->reference ? $bankAccount->reference->employee_id . ' - ' . $bankAccount->reference->name : ''}}
                                </td>
                                <td>
                                    {{datetime_in_view($bankAccount->created_at) ?? null}}
                                </td>
                                <td>
                                    {{datetime_in_view($bankAccount->updated_at) ?? null}}
                                </td>
                                <td class="text-right">
                                    <a
                                        href="bank-account/{{$bankAccount->id}}"
                                        class="btn btn-success btn-sm"
                                        >View</a
                                    >
                                    <a
                                        href="bank-account/{{$bankAccount->id}}/edit"
                                        class="btn btn-info btn-sm"
                                        >Edit</a
                                    >
                                    <div
                                        style="display:inline-block;"
                                        class="deleteform"
                                    >
                                        {{ Form::open(['url' => 'bank/bank-account/'.$bankAccount->id, 'method' => 'delete', 'class' => 'm-0']) }}
                                            {{Form::button('Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'))}}
                                        {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                            @endforeach @else
                            <td colspan="8" style="text-align:center;">
                                <span class="align-center"
                                    ><big>No Account Found</big></span
                                >
                            </td>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@parent
<script>

$(document).ready(function() {
		var t = $('#bank-account').DataTable({
			pageLength:500,
		});
	})
</script>
@endsection

