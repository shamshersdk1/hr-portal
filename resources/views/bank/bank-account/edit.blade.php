@extends('layouts.dashboard')
@section('title')
Bank | Account
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row breadcrumb-wrap">
        <div class="col-sm-12">
            <h1 class="page-title">Account</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/bank/bank-account">Account</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="card  border">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                   
                            {{ Form::open(array('url' => "bank/bank-account/".$bankAccount->id, 'method' => "put")) }}
                            <div class="row">
                                {{ Form::Label('bank_type', 'Account Type:',array('class' =>'col-md-4' )) }}
                                <div class="col-md-8">
                                    {{$bankAccount->accountType->name}}
                                </div>
                            </div>
                            <div class="row mt-2">
                                {{ Form::label('reference_id', 'User Name:',['class' => 'col-md-4'])}}
                                <div class="col-md-8" >
                                    {{ Form::select('reference_id', $referArray->pluck('user_id_name', 'id'), $bankAccount->reference_id, ['class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="row mt-2">
                                {{ Form::label('account_number', 'Account Number :',['class' => 'col-md-4'])}}
                                <div class="col-md-8" >
                                    {{ Form::text('account_number', $bankAccount->account_number,['class' => 'form-control']) }}
                                </div>
                            </div>
                            @foreach ($keys as $key)
                                <div class="row mt-2">
                                    {{ Form::label($key->key, $key->key, ['class' => 'col-md-4'])}}
                                    <div class="col-md-8" >
                                        {{ Form::text("meta[".$key->key."]", $bankAccount->metaValue($key->id),['class' => 'form-control', 'id' => 'refer', 'required' => "required"]) }}
                                    </div>
                                </div>
                            @endforeach
                            <div class="row mt-2">
                                <div class="form-group col-md-12 mt-4">
                                    {{Form::submit('Update',array('class' => 'btn btn-success float-right',))}}
                                </div>
                            </div>
                            {{ Form::close() }}
                      
                </div>
            </div>
        </div>

<script type="text/javascript">

</script>
@endsection
