@extends('layouts.dashboard')
@section('title')
Bank | Account
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row breadcrumb-wrap">
        <div class="col-sm-12">
            <h1 class="page-title">Account</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/bank/bank-account">Account</a></li>
                <li class="breadcrumb-item active">Add</li>
            </ol>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="card border">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                 
                            {{ Form::open(array('url' => "bank/bank-account")) }}

                            @if($accountTypeId == null && $referId == null)
                                <div class="row">
                                    {{ Form::Label('bank_account_type', 'Enter Account Type:',array('class' =>'col-md-4' )) }}
                                    <div class="col-md-8" >
                                        {{ Form::select('bank_account_type_id', $bankTypes->pluck('name','id'),null, array('class' => 'userSelect1'  ,'placeholder' => 'Select Account Type', 'id' => 'selectid3', 'style' =>'width:100%'))}}
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    {{ Form::label('reference_type', 'Account For:',['class' => 'col-md-4'])}}
                                    <div class="col-md-8" >
                                        {{ Form::select('reference_type', $referenceFor, null, array('class' => 'userSelect1'  ,'placeholder' => 'Select Category', 'id' => 'selectid2','style' =>'width:100%'))}}
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    {{ Form::Label('bank_account_type', 'Account Type:',array('class' =>'col-md-4' )) }}
                                    <div class="col-md-8" >
                                        {{Form::hidden('bank_account_type_id', $bankAccountObj->name)}}
                                        {{Form::hidden('reference_type', $referType)}}
                                        {{$bankAccountObj->name}}
                                    </div>
                                </div>
                                @if($referId != null)
                                    <div class="row">
                                        {{ Form::label('reference_id', 'User Name:',['class' => 'col-md-4'])}}
                                        <div class="col-md-8" >
                                            {{ Form::select('reference_id', $referArray->pluck('user_id_name', 'id'), null, ['class' => 'form-control userSelect1', 'id' => 'refer', 'placeholder' => 'Select Name', 'style' => 'width:100%']) }}
                                        </div>
                                    </div>
                                @endif

                                <div class="row mt-2">
                                    {{ Form::label('account_number', 'Account Number :',['class' => 'col-md-4'])}}
                                    <div class="col-md-8" >
                                        {{ Form::text('account_number', '',['class' => 'form-control']) }}
                                    </div>
                                </div>
                            @endif

                            @if($accountTypeId != null)
                                @foreach ($keys as $key)
                                    <div class="row mt-2">
                                        {{ Form::label($key->key, $key->key, ['class' => 'col-md-4'])}}
                                        <div class="col-md-8" >
                                            {{ Form::text("meta[".$key->key."]", "",['class' => 'form-control', 'id' => 'refer', 'required' => "required"]) }}
                                        </div>
                                    </div>
                                @endforeach
                            @endif


                            @if($accountTypeId != null && $referId != null)
                                <div class="row mt-4">
                                    <div class="form-group col-md-12">
                                        {{Form::submit('Save',array('class' => 'btn btn-success float-right'))}}
                                    </div>
                                </div>
                            @else
                                <div class="row mt-4">
                                    <div class="form-group col-md-12">
                                        {{Form::button('Next->',array('class' => 'btn btn-success float-right', 'onclick' => 'myfunction()'))}}
                                    </div>
                                </div>
                            @endif
                            {{ Form::close() }}
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>

    var optionValue = null;
    var secondValue = null;

    $('#selectid3').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
    });
    $('#selectid2').change(function(){
		var Selected = $("option:selected", this);
        secondValue = this.value;
    });

    function myfunction() {
        if (optionValue != null && secondValue != null) {
            window.location = "/bank/bank-account/create/"+optionValue+"/"+secondValue;
        }
        else if(secondValue == null) {
            window.location = "/bank/bank-account/create/"+optionValue;
        }
        else {
            window.location = "/bank/bank-account/create";
        }
    }
    $(document).ready(function() {
        $('.userSelect1').select2();
    });
</script>
@endsection
