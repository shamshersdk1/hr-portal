
@extends('layouts.dashboard')
@section('title')
Bank | Account
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row breadcrumb-wrap">
        <div class="col-sm-12">
            <h1 class="page-title">Account</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/bank/bank-account">Account</a></li>
                <li class="breadcrumb-item active">View</li>
            </ol>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="card border">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-heading">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4 text-info"  align="left">
                                    Account Type:
                                </div>
                                <div class="col-md-8" >
                                    {{$bankAccount->accountType->name}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 text-info" >
                                User Name:
                                </div>
                                <div class="col-md-8" >
                                    {{$bankAccount->reference ? $bankAccount->reference->employee_id . ' - ' . $bankAccount->reference->name : ''}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 text-info" >
                                Account Number:
                                </div>
                                <div class="col-md-8" >
                                    {{$bankAccount->account_number}}
                                </div>
                            </div>
                            @foreach ($keys as $key)
                                <div class="row">
                                    <div class="col-md-4 text-info" >
                                        {{$key->key}}
                                    </div>
                                    <div class="col-md-8" >
                                        {{ $bankAccount->metaValue($key->id)}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
@endsection
