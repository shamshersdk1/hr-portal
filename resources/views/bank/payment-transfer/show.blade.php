@extends('layouts.dashboard')
@section('title')
Payments
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-12">
                <h1 class="page-title">Payment Transfers</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="/bank/payment-transfers">Payment Transfers</a></li>
                    <li class="breadcrumb-item active">Payment Transfer Accounts #{{$id}}</li>
                </ol>
            </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8" style="padding:1%" align="left">
                            Month:
                        </div>
                        <div class="col-md-4" style="padding:0">
                            {{$paymentsTransfer->month->formatMonth()}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8" style="padding:1%" align="left">
                           Account Type:
                        </div>
                        <div class="col-md-4" style="padding:0">
                            {{$paymentsTransfer->accountType ? $paymentsTransfer->accountType->name : ''}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8" style="padding:1%" align="left">
                           Status:
                        </div>
                        <div class="col-md-4" style="padding:0">
                            {{$paymentsTransfer->status}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8" style="padding:1%" align="left">
                           Created At:
                        </div>
                        <div class="col-md-4" style="padding:0">
                            {{datetime_in_view($paymentsTransfer->created_at)}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8" style="padding:1%" align="left">
                        </div>
                        <div class="col-md-4" style="padding:0">
                        </div>
                    </div>

                    <form action="/bank/payment-transfers/{{$paymentsTransfer->id}}/upload" method="post" style="display:inline" enctype="multipart/form-data" class="d-flex m-0">
                        @csrf
                        <input id="fileupload" type="file" name="file" required  style="border:0">
                        {{ Form::button('<i class="fa fa-upload fa-fw"></i>Upload', ['type'=> 'submit','class' => 'btn btn-success btn-sm'] )  }}
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form name="editForm" method="post" action="/bank/payment-transfers/{{$paymentsTransfer->id}}">
            @csrf
            @method('PUT')
            <div class="text-right">
            {{ Form::button('Save All', ['type'=> 'submit','class' => 'btn btn-success'] )  }}
            </div>
            <table class="table table-striped" id="payment-transfer-accounts">
                <thead>
                    <th class="text-left">#</th>
                    <th class="text-left">Emp Id</th>
                    <th class="text-left">User</th>
                    <th class="text-left">Account Number</th>
                    <th class="text-left">IFSC code</th>
                    <th class="text-left">Amount</th>
                    <th class="text-left">Transaction Amount</th>
                    <th class="text-left">Mode of transfer</th>
                    <th class="text-left">Transaction Id</th>
                </thead>
                <tbody>
                    @if($paymentsTransfer && $paymentsTransfer->transferAccounts)
                    @foreach($paymentsTransfer->transferAccounts as $transferAccount)
                    <tr>
                        <td></td>
                        <td>{{$transferAccount->account->user->employee_id}}</td>
                        <td>{{$transferAccount->account->user->name}}</td>
                        <td><input name="data[{{$transferAccount->id}}][account_number]" value="{{$transferAccount->account ? $transferAccount->account->account_number : null}}"/></td>
                        <td><input name="data[{{$transferAccount->id}}][ifsc]" value="{{$transferAccount->ifsc_code ?? 0}}"/></td>
                        <td><input name="data[{{$transferAccount->id}}][amount]" value="{{$transferAccount->amount ?? 0}}"/></td>
                        <td><input name="data[{{$transferAccount->id}}][transaction_amount]" value="{{$transferAccount->transaction_amount ?? 0}}"/></td>
                        <td><input name="data[{{$transferAccount->id}}][mode_of_transfer]" value="{{$transferAccount->mode_of_transfer ?? null}}"/></td>
                        <td><input name="data[{{$transferAccount->id}}][transaction_id]" value="{{$transferAccount->transaction_id ?? null}}"/></td>
                    </tr>
                    @endforeach @else
                    <td colspan="8" style="text-align:center;">
                        <span class="align-center"><big>No bank transaction found</big></span>
                    </td>
                    @endif
                </tbody>
            </table>
        </form>
        </div>
    </div>
</div>

@endsection
@section('js')
@parent
<script>
    $('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue == 'all') {
            window.location = "/bank/bank-transaction";
        }
        else if(optionValue) {
            window.location = "/bank/bank-transaction/"+optionValue;
        }
    });

	$(document).ready(function() {
        var t = $('#payment-transfer-accounts').DataTable( {
            pageLength:500,
            fixedHeader: {
                header: true
            },
           dom: 'Bfrtip',
            buttons: [
                'csv'
            ]
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection

