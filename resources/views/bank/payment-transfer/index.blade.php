@extends('layouts.dashboard')
@section('title')
Payments
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
            <div class="col-sm-12">
                <h1 class="page-title">Payment Transfers</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    @if($accountTypeId != null)
                        <li class="breadcrumb-item active"><a href="/bank/due-payments">Account Types</a></li>
                    @endif
                    <li class="breadcrumb-item active">Payment Transfers</li>
                </ol>
            </div>

    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="">
            <table class="table table-striped table-sm table-outer-border" id="payment-transaction">
                <thead>
                    <th width="5%">#</th>
                    <th widht="20%">Created At</th>
                    <th width="20%">Account Type</th>
                    <th width="20%">Status</th>
                    <th width="10%">Actions</th>
                </thead>
                <tbody>
                    @if($paymentsTransfers && count($paymentsTransfers)>0)
                    @foreach($paymentsTransfers as $transfer)
                    <tr>
                        <td></td>
                        <td>{{date_in_view($transfer->created_at) ?? null}}</td>
                        <td>{{$transfer->accountType ? $transfer->accountType->name : null}}</td>
                        <td>{{$transfer->status ?? 0}}</td>
                        <td><a href="/bank/payment-transfers/{{$transfer->id}}" class="btn btn-success btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>Show Accounts</a></td>
                    </tr>
                    @endforeach @else
                    <td colspan="8" style="text-align:center;">
                        <span class="align-center"><big>No bank transaction found</big></span>
                    </td>
                    @endif
                </tbody>
            </table>
    </div>
    {{ Form::close() }}
</div>

@endsection
@section('js')
@parent
<script>
    $('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue == 'all') {
            window.location = "/bank/bank-transaction";
        }
        else if(optionValue) {
            window.location = "/bank/bank-transaction/"+optionValue;
        }
    });

	$(document).ready(function() {
        var t = $('#payment-transaction').DataTable( {
            pageLength:500,
            fixedHeader: {
                header: true
            },
           dom: 'Bfrtip',
            buttons: [
                'csv'
            ]
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection

