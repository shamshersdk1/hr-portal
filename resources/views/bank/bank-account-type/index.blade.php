@extends('layouts.dashboard')
@section('title')
Bank | Account Type
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-12">
            <h1 class="page-title">Bank</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Account Type</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
				<div class="panel-body" align="center">
					{{ Form::open(['url' => '/bank/bank-account-type', 'method' => 'post']) }}
                        <div class="row form-group col-md-12">
                            {{ Form::label('name', 'Name :',['class' => 'col-md-4 text-right','style' => 'padding-top:1%', 'align' => 'left'])}}
                            <div class="col-md-4" style="padding:0">
                                {{ Form::text('name', '',['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="row form-group col-md-12">
                            {{ Form::label('code', 'Code :',['class' => 'col-md-4 text-right','style' => 'padding-top:1%', 'align' => 'left'])}}
                            <div class="col-md-4" style="padding:0">
                                {{ Form::text('code', '',['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="row form-group col-md-12">
                            {{ Form::label('is_editable', 'Is Editable :',['class' => 'col-md-4','align' => 'right'])}}
                                <div class="" style="padding:0">
                                    {{Form::radio('is_editable', true)}} Yes
                                    {{Form::radio('is_editable', false)}} No
                                </div>
                            </div>
                        <div class="col-md-12">
                            {{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
                        </div>
                    {{ Form::close() }}
				</div>
			</div>
			<div class="col-md-12">
				<table class="table table-striped table-sm table-outer-border">
                    <thead>
                        <th width="10%">#</th>
                        <th width="20%">Name</th>
                        <th width="20%">Code</th>
                        <th width="20%">Editable</th>
                        <th width="" class="text-right">Actions</th>
                    </thead>
				@if(isset($bankAccountTypes))
                    @if(count($bankAccountTypes) > 0)
                        @foreach($bankAccountTypes as $index => $bankType)
                            <tr>
                                <td class="td-text">{{$index+1}}</td>
                                <td class="td-text"> {{$bankType->name}}</td>
                                <td class="td-text"> {{$bankType->code}}</td>
                                <td class="td-text"> {{$bankType->is_editable ? 'Yes' : 'No'}}</td>
                                <td class="text-right">
                                    <a href="/bank/bank-account-type/{{$bankType->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
                                    <div style="display:inline-block;" class="deleteform">
                                    {{ Form::open(['url' => '/bank/bank-account-type/'.$bankType->id, 'method' => 'delete']) }}
                                    {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                    {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" class="text-center">No account types added.</td>
                        </tr>
                    @endif
				@endif
			</table>
        </div>
    </div>
</div>
@endsection
