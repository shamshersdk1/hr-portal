@extends('layouts.dashboard')
@section('title')
Bank | Account Type
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-12">
            <h1 class="page-title">Bank</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/bank/bank-account-type">Account Type</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
				<div class="panel-body" align="center">
				{{ Form::open(['url' => '/bank/bank-account-type/'.$bankType->id, 'method' => 'put']) }}
				{{ Form::token() }}
				<div class="row form-group col-md-12">
                    {{ Form::label('name', 'Name :',['class' => 'col-md-4 text-right','style' => 'padding-top:1%','align' => 'left'])}}
					<div class="col-md-4" style="padding:0">
                    	{{ Form::text('name', $bankType->name,['class' => 'form-control']) }}
					</div>
				</div>
				<div class="row form-group col-md-12">
                    {{ Form::label('code', 'Code :',['class' => 'col-md-4 text-right','style' => 'padding-top:1%','align' => 'left'])}}
					<div class="col-md-4" style="padding:0">
                    	{{ Form::text('code', $bankType->code,['class' => 'form-control']) }}
					</div>
				</div>
				<div class="col-md-12">
					{{ Form::submit('Update',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
				</div>
				{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
@parent
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
