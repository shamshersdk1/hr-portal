@extends('layouts.dashboard')
@section('title')
Due Payments
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-12">
            <h1 class="page-title">Due Payments</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item active">Account Types</li>
            </ol>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="">
        <div class="">
            <div class="row table-header-section">
                <div class="col-sm-6 d-flex align-items-center"> 
                  
                </div>
                  <div class="col-sm-6 table-search">
                  </div>
                </div>
            <table class="table table-striped table-sm table-outer-border mt-2" id="bank-account">
                <thead>
                    <th width="5%">#</th>
                    <th widht="20%">Account Type</th>
                    <th width="10%">Outstanding Amount</th>
                    <th width="" class="text-right">Actions</th>
                </thead>
                <tbody>
                    @if($accountTypeList && count($accountTypeList)>0)
                        @foreach($accountTypeList as $index => $type)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$type->name ?? null}}</td>
                            <td>{{$type->bankTransactions($type->id) ?? 0}}</td>
                            <td class="text-right">
                                @if($type->bankTransactions($type->id) != 0)
                                    <a href="due-payments/{{$type->id}}" class="btn btn-warning btn-sm " style="display: inline-block;">Pending Transactions</a>
                                @endif
                                <a href="due-payments/{{$type->id}}/payment-transfers" class="btn btn-dark btn-sm " style="display: inline-block;">Payment Transfers</a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                    <td colspan="8" style="text-align:center;">
                        <span class="align-center"><big>No Account Types found</big></span>
                    </td>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('js')
@parent
<script>

	$(document).ready(function() {
        $('.table-header-section .table-search').append($('.dataTables_filter'))
    });


        var t = $('#bank-account').DataTable( {
            pageLength:500,
            "order": false,
            "paging": false,
            "info": false,
            "bLengthChange": false,

        } );
</script>
@endsection

