@extends('layouts.dashboard')
@section('title')
Due Payments
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-12">
            <h1 class="page-title">Due Payments for {{$accountTypeObj->name}}</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="/bank/due-payments">Account Types</a></li>
                <li class="breadcrumb-item active">Pending Payments</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    {{ Form::open(array('url' => "bank/due-payments/".$accountTypeObj->id."/preview")) }}
    <div class="row">
        <div class="col-sm-10">
        </div>
        <div class="col-sm-2">
            {{Form::hidden('account_type_id', $accountTypeObj->id)}}
            {{Form::submit('Preview',array('class' => 'btn btn-success ','style'=>'color:white;margin:auto'))}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="bank_account">
                <thead>
                    <th class="check text-center" width="7%">
                        {{Form::checkbox('', '', false, ['id' => 'flowcheck'])}}
                        &nbsp;<small><span id="text-check">Bank Transfer</span></small>
                    </th>
                    <th width="5%">#</th>
                    <th widht="10%">Account Number</th>
                    <th width="10%">User</th>
                    <th width="10%">Amount Payable</th>
                    <th width="10%">Total Allowance</th>
                    <th width="10%">Total Deduction</th>
                </thead>

                <tbody>
                    @if($transactions && count($transactions)>0)
                        @foreach($transactions as $index => $transaction)
                            <tr>
                                @if($transaction->amountPayable != 0)
                                <td class="check text-center">
                                    {{Form::checkbox('options['.$transaction->account_id.']', $transaction->amountPayable, false, ['id' => $transaction->account_id])}}
                                    &nbsp;
                                </td>
                                <td>{{$index + 1}}</td>
                                <td>{{$transaction->account ? $transaction->account->account_number : null}}</td>
                                <td>{{$transaction->account ? $transaction->account->user ? $transaction->account->user->name : null : null}}</td>
                                <td>{{$transaction->amountPayable ?? 0}}</td>
                                <td>{{$transaction->totalAllowance ?? 0}}</td>
                                <td>{{$transaction->totalDeduction ?? 0}}</td>
                                @endif
                            </tr>
                        @endforeach
                    @else
                    <td colspan="7" style="text-align:center;">
                        <span class="align-center"><big>No bank transaction found</big></span>
                    </td>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    {{ Form::close() }}
</div>

@endsection
@section('js')
@parent
<script>

    $(document).ready(function() {

	    $("#flowcheck").click(function () {
            $('#bank_account tbody input[type="checkbox"]').prop('checked', this.checked);
        });

        $("#text-check").click(function () {
            source = document.getElementById('flowcheck');
            $('#bank_account tbody input[type="checkbox"]').prop('checked', !source.checked);
            $('#flowcheck').prop('checked', !source.checked);
        });

        var t = $('#bank_account').DataTable( {
            pageLength:500,
        } );

    });
</script>
@endsection

