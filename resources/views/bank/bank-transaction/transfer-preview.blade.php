@extends('layouts.dashboard') 
@section('title')
Due Payments
@endsection
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="admin-page-title">Payment Preview for {{$accountTypeObj->name}}</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="/bank/due-payments">Account Types</a></li>
                    <li class="breadcrumb-item"><a href="/bank/due-payments/{{$accountTypeObj->id}}">Pending Payments</a></li>
                    <li class="breadcrumb-item active">Preview</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    {{ Form::open(array('url' => "bank/due-payments/".$accountTypeObj->id)) }}
    @method('PUT')
    <div class="row">
        <div class="col-sm-10">
        </div>
        <div class="col-sm-2">
            {{Form::hidden('account_type_id', $accountTypeObj->id)}}
            {{Form::submit('Transfer',array('class' => 'btn btn-success ','style'=>'color:white;margin:auto'))}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="bank-account">
                <thead>
                    <th width="5%">#</th>
                    <th widht="40%">Account Number</th>
                    <th width="30%">User</th>
                    <th width="30%">Amount Payable</th>
                </thead>
                
                <tbody>
                    @if($transactions && count($transactions)>0)
                    @foreach($transactions as $index => $transaction)
                    <tr>
                        @if(isset($data['options'][$transaction->account_id]))
                            {{Form::hidden('options['.$transaction->account_id.']', $transaction->amountPayable)}}
                            <td>{{$index + 1}}</td>
                            <td>{{$transaction->account ? $transaction->account->account_number : null}}</td>
                            <td>{{$transaction->account ? $transaction->account->user ? $transaction->account->user->name : null : null}}</td>
                            <td>{{$transaction->amountPayable ?? 0}}</td>
                        @endif
                    </tr>
                    @endforeach @else
                    <td colspan="8" style="text-align:center;">
                        <span class="align-center"><big>No bank transaction found</big></span>
                    </td>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    {{ Form::close() }}
</div>

@endsection
@section('js')
@parent
<script>
	$(document).ready(function() {
        var t = $('#bank-account').DataTable( {
            pageLength:500, 
        } );
    });
</script>
@endsection

