@extends('layouts.dashboard') 
@section('title')
Bank Transaction
@endsection
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="admin-page-title">Bank Transactions</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active">Bank Transaction</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <h3 class="text-danger">Pending Payment Transfers for {{$accountTypeObj->name}}</h3>
        </div>
        <div class="col-sm-2">
            @if(isset($accountTypeList))
                <select id="selectid2" name="accountType"  placeholder="Select Account Type">
                    <option value=""></option>
                    @foreach($accountTypeList as $x)
                        <option value="{{$x->id}}" >{{$x->name}}</option>
                    @endforeach
                </select>
            @endif
        </div>
    </div>
    {{-- {{ Form::open(array('url' => "bank/bank-transaction/store")) }} --}}
    {{ Form::open(array('url' => "bank/due-payments/".$accountType."/preview")) }}
    <div class="row">
        <div class="col-sm-10">
        </div>
        <div class="col-sm-2">
            {{-- {{Form::hidden('account_type_id', $accountType)}} --}}
            {{-- {{Form::submit('Transfer',array('class' => 'btn btn-success ','style'=>'color:white;margin:auto'))}} --}}
            {{Form::hidden('account_type_id', $accountType)}}
            {{Form::submit('Preview',array('class' => 'btn btn-success ','style'=>'color:white;margin:auto'))}}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped" id="bank-account">
                <thead>
                    <th class="check text-center" width="7%">
                        {{Form::checkbox('', '', false, ['id' => 'flowcheckall'])}}
                        &nbsp;<small><span id="text-check">Bank Transfer</span></small>
                    </th>
                    <th width="5%">#</th>
                    <th widht="10%">Account Number</th>
                    <th width="10%">User</th>
                    <th width="10%">Amount Payable</th>
                    <th width="10%">Total Allowance</th>
                    <th width="10%">Total Deduction</th>
                </thead>
                
                <tbody>
                    @if($transactions && count($transactions)>0)
                    @foreach($transactions as $index=> $transaction)
                    <tr>
                        @if($transaction->amountPayable != 0)
                        <td class="check text-center">
                            {{Form::checkbox('options['.$transaction->account_id.']', $transaction->amountPayable, false, ['id' => $transaction->account_id])}}
                            &nbsp;
                        </td>
                        <td></td>
                        <td>{{$transaction->account ? $transaction->account->account_number : null}}</td>
                        <td>{{$transaction->account ? $transaction->account->user ? $transaction->account->user->name : null : null}}</td>
                        <td>{{$transaction->amountPayable ?? 0}}</td>
                        <td>{{$transaction->totalAllowance ?? 0}}</td>
                        <td>{{$transaction->totalDeduction ?? 0}}</td>
                        @endif
                    </tr>
                    @endforeach @else
                    <td colspan="8" style="text-align:center;">
                        <span class="align-center"><big>No bank transaction found</big></span>
                    </td>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    {{ Form::close() }}
</div>

@endsection
@section('js')
@parent
<script>
    $('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if(optionValue) {
            window.location = "/bank/bank-transaction/"+optionValue; 
        }
    });

	$(document).ready(function() {
        var t = $('#bank-account').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            },
           dom: 'Bfrtip',
            buttons: [
                'csv'
            ]
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    
	    $("#flowcheckall").click(function () {
            $('#bank-account tbody input[type="checkbox"]').prop('checked', this.checked);
        });
        
        $("#text-check").click(function () {
            source = document.getElementById('flowcheckall');
            $('#bank-account tbody input[type="checkbox"]').prop('checked', !source.checked);
            $('#flowcheckall').prop('checked', !source.checked);
        });
    });
</script>
@endsection

