<div class="card border">
    <div class="card-body">
        <div class="row justify-content-md-center">
            <div class="col-sm-4">
                <div class="info-table">
                    <label>Effective Date </label>
                    <span>{{date_in_view($appraisal->effective_date)}}</span>
                </div>
                <div class="info-table">
                    <label>Appraisal Type </label>
                    <span>{{$appraisal->type->name}}</span>
                </div>
                <div class="info-table">
                    <label>Special Allowance </label>
                    <span>&#8377;{{number_format($appraisal->getSpecialAllowance())}}</span>
                </div>
                <div class="info-table">
                    <label>Special Allowance Internal</label>
                    <span>&#8377;{{number_format($appraisal->getSpecialAllowanceInternal())}}</span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="info-table">
                    <label>Monthly Gross Salary</label>
                    <span>&#8377;{{number_format(round($appraisal->getMonthlyGrossSalary(),2))}}</span>
                </div>
                <div class="info-table">
                    <label>Monthly CTC </label>
                    <span>&#8377;{{number_format($appraisal->getMonthlyCtc())}}</span>
                </div>
                <div class="info-table">
                    <label>Monthly Gross Earnings </label>
                    <span>&#8377;{{number_format($appraisal->getMonthlyGrossEarnings())}}</span>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="info-table">
                    <label>Annual Gross Salary</label>
                    <span>&#8377;{{number_format($appraisal->getAnnualGrossSalary())}}</span>
                </div>
                <div class="info-table">
                    <label>Annual CTC</label>
                    <span>&#8377;{{number_format($appraisal->getAnnualCtc())}}</span>
                </div>
                <div class="info-table">
                    <label>Annual Gross Earnings </label>
                    <span>&#8377;{{number_format($appraisal->getAnnualGrossEarnings())}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <table class="table table-bordered bg-white">
            <caption><strong>Part A : Fixed Salary Details</strong></caption>
            <thead>
                <tr class="active">
                    <th width="60%" class="text-center"><strong>Particulares</strong></th>
                    <th width="20%" class="text-center"><strong>Annual</strong></th>
                    <th width="20%" class="text-center"><strong>Monthly</strong></th>
                </tr>
            </thead>
            <tbody>
                @if($appraisal['appraisalComponent'])
                @php
                $fixedSalary = 0;
                @endphp
                @foreach($appraisal->appraisalComponent as $component)
                @if($component->value > 0 && $component->appraisalComponentType->is_computed==0)
                    <tr>
                        <td>{{$component->appraisalComponentType->name}}  </td>
                        <td>&#8377;{{number_format($component->value * 12)}}</td>
                        <td>&#8377;{{number_format(round($component->value,2))}}</td>
                    </tr>
                    @php
                    $fixedSalary += ($component->value * 12);
                    @endphp
                @endif
                @endforeach
                <tr class="active">
                    <td class="text-right"><strong>Gross Salary </strong></td>
                    <td>&#8377;{{number_format($fixedSalary)}}</td>
                    <td>&#8377;{{number_format(round($fixedSalary/12,2))}}</td>
                </tr>

                <tr>
                    <td colspan="3"><strong>Other Salary Details: </strong></td>

                </tr>
                @php
                $otherBenefits = 0;
                @endphp
                @foreach($appraisal->appraisalComponent as $component)
                    @if($component->value < 0)
                    <tr>
                        <td>{{$component->appraisalComponentType->name}}  </td>
                        <td>&#8377;{{number_format(abs($component->value * 12))}}</td>
                        <td>&#8377;{{number_format(abs(round($component->value,2)) )}}</td>
                    </tr>
                    @php
                    $otherBenefits += abs($component->value * 12);
                    @endphp
                    @endif
                @endforeach
                <tr class="active">
                    <td class="text-right">Total Other Benefits </td>
                    <td>&#8377;{{number_format($otherBenefits)}}</td>
                    <td>&#8377;{{number_format(round($otherBenefits/12,2))}}</td>
                </tr>
                <tr class="active">
                    <td class="text-right">Total Fixed CTC - (A) </td>
                    <td>&#8377;{{number_format($fixedSalary + $otherBenefits)}}</td>
                    <td>&#8377;{{number_format(($fixedSalary+ $otherBenefits)/12)}}</td>

                </tr>
                @endif
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-bordered bg-white">
            <caption><strong>Part B : Variable Bonus Details</strong></caption>
            <thead>
                <tr class="active">
                    <th width="40%" class="text-center"><strong>Particulares</strong></th>
                    <th width="20%" class="text-center"><strong>Annual</strong></th>
                    <th width="20%" class="text-center"><strong>Monthly</strong></th>
                    <th width="20%" class="text-center"><strong>Date</strong></th>
                </tr>
            </thead>
            <tbody>
                @if($bonusTypes)
                @php
                $variableBonus = 0;
                @endphp
                @foreach($bonusTypes as $index => $bonusType)
                    @php $status = false; @endphp
                    <tr>
                        <td><label>{{$bonusType->description}} </label></td>
                        @foreach ($appraisal->appraisalBonus as $bonus)

                            @if($bonus->appraisal_bonus_type_id == $bonusType->id)
                                @php $status = true; @endphp
                                <td><p>&#8377;{{ number_format($bonus->value) }}</p></td>
                                <td><p>&#8377;{{ number_format(round($bonus->value/12,2)) }}</p></td>
                                <td><p>{{ date_in_view($bonus->value_date) }}</p></td>
                                @php $variableBonus += (isset($bonus->value)?$bonus->value:0); @endphp
                            @endif
                        @endforeach
                        @if($status === false)
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                    </tr>

                @endforeach
                         <tr class="active">
                            <td class="text-right"><label><strong>Total Variabe CTC -(B)</strong></label></td>
                            <td>&#8377;{{ number_format($variableBonus)}}</td>
                            <td>&#8377;{{ number_format(round($variableBonus/12,2))}}</td>
                            <td></td>
                        </tr>
                        <tr class="active">
                            <td class="text-right"><label><strong>Total CTC for the Year - (A+B)</strong></label></td>
                            <td>&#8377;{{number_format($fixedSalary + $otherBenefits + $variableBonus)}}</td>
                            <td>&#8377;{{number_format(round(($fixedSalary + $otherBenefits + $variableBonus)/12, 2))}}</td>
                            <td></td>
                        </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
