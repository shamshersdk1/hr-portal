
@extends('layouts.dashboard')
@section('title')
Permissions
@endsection
@section('main-content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">List of Permissions</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"> <a href="/permission">List of Permissions</a></li>
                    <li class="breadcrumb-item active"> Add Permission</li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>

    <div class="card border">
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-md-6 mt-3">
                    {{ Form::open(array('url' => "permission")) }}
                        {{ Form::Label('name', 'Permission Name:') }}
                        <div class="row">
                            <div class="col-md-9">
                                {{ Form::text('permission_name', '',['class' => 'form-control']) }}
                            </div>
                            <div class="col-md-3">
                                 {{Form::submit('Save',array('class' => 'btn btn-success'))}}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    
</script>
@endsection
