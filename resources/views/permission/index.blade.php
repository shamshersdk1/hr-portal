
@extends('layouts.dashboard')
@section('title')
Permissions
@endsection
@section('main-content')
	<div class="page-title-box">
	    <div class="row align-items-center">
	        <div class="col-sm-6">
	            <h4 class="page-title">List of Permissions</h4>
	            <ol class="breadcrumb">
	                <li class="breadcrumb-item">
	                	<a href="/dashboard">Dashboard</a>
	                </li>
	                <li class="breadcrumb-item active">List of Permissions</li>
	            </ol>
	        </div>
	        <div class="col-sm-6 text-right">
	            {{Form::open(['url'=>'permission/create' ,'method'=>'get'])}}
				{{Form::button('<i class="fa fa-plus fa-fw"></i> Add Permission',
				array('type' => 'submit', 'class' => 'btn btn-success'))}}
				{{Form::close()}}
	        </div>
	    </div>
	</div>

	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="user-list-view">
		<div class="card card-default">
				<div class="table-responsive">
					<table class="table table-striped table-outer-border no-margin" id="permission-table">
						<thead>
							<th>#</th>
							<th>Permission Name</th>
							<th class="text-right">Actions</th>
						</thead>
						<tbody>
						@if(count($permissions) > 0)
				    		@foreach($permissions as $index => $permission)
								<tr>
									<td>{{$index + 1}}</td>
									<td>
										{{$permission->name}}
									</td>
									<td class="text-right">
										<a href="permission/{{$permission->id}}/edit" class="btn btn-success btn-sm ">Edit</a>
										<a href="permission/{{$permission->id}}/roles" class="btn btn-warning btn-sm ">Assign Roles</a>
										<a href="permission/{{$permission->id}}/delete" class="btn btn-danger btn-sm ">Delete</a>
									</td>
								</tr>
							@endforeach
				    	@else
				    		<tr>
				    			<td colspan="3">No results found.</td>
				    		</tr>
				    	@endif
						</tbody>
				    </table>
				</div>
			</div>
		</div>
	</div>

@endsection
@section('js')
@parent
<script>
	$(document).ready(function() {
        var t = $('#permission-table').DataTable( {
            pageLength:500, 
        } );
    });

</script>
@endsection
