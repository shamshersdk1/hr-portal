@extends('layouts.dashboard')
@section('title')
System Setting | @parent
@endsection
@section('main-content')
<div class="page-title-box" >
    <div class="row align-items-center">

            <div class="col-sm-12">
                <h1 class="page-title">System Settings</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item active"><a href="{{ url('system-setting') }}">Notification</a></li>
                </ol>
            </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
    <div class="row">
		<div class="col-md-12">
			<div class="card">
					<div class="panel-body">
                        <br>
						{{ Form::open(array('url' => "system-setting")) }}
                            <div class="form-group row">
                                {{ Form::Label('Key', 'Key:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
                                <div class="col-md-4" style="padding:0">
                                    {{ Form::text('key', null, ['class' => 'form-control']) }}
                                </div>
                            </div>


							<div class="form-group row">
								{{ Form::Label('Value', 'Value:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
								<div class="col-md-4" style="padding:0">

										{{ Form::textarea('value',null,['class'=>'form-control', 'rows' => 5] ) }}
                                </div>
                            </div>
							<div class="col-md-12">
								{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
							</div>
                    		{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
    	<div class="card">
			<table class="table table-striped">
				<thead>
					<th class="td-text">#</th>
					<th class="td-text">Key</th>
                    <th class="td-text">Value</th>
                    <th class="text-right">Actions</th>
				</thead>
				<tbody>
					@if(count($systemSettings)>0)
						@foreach($systemSettings as $index=> $systemSetting)
						<tr>
							<td class="td-text">{{$index + 1}}</td>
							<td class="td-text">{{$systemSetting->key ?? ''}}</td>
							<td class="td-text">
                            @if($systemSetting->value == '1')
                                <span class="badge badge-success" style="font-size:12px">Yes</span>
                            @elseif($systemSetting->value == '0')
                                <span class="badge badge-danger" style="font-size:12px">No</span>
                            @else
                                {{$systemSetting->value ?? ''}}
                            @endif
                            </td>


							<td class="text-right">
							<a href="system-setting/{{$systemSetting->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true" style="margin-bottom:10"></i>Edit</a>
							<div style="display:inline-block;" class="deleteform">
                                {{ Form::open(['url' => '/system-setting/'.$systemSetting->id, 'method' => 'delete']) }}
                                {{ Form::submit('Delete',['class' => 'btn btn-danger crud-btn btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                {{ Form::close() }}
                                </div>
							</td>
						</tr>
						@endforeach
					@else
						<td colspan="4" style="text-align:center;"><span class="align-center"><big>No System Setting Found</big></span></td>
					@endif
				</tbody>
			</table>
		</div>
	</div>
    </div>
</div>

@endsection
