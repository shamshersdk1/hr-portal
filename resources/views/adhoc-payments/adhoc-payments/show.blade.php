@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
				<h1 class="page-title">Adhoc Payment</h1>
                <ol class="breadcrumb">
					<li><a href="/dashboard">Dashboard</a></li>
					<li><a href="/adhoc-payments">{{$month->formatMonth()}}</a></li>
					<li class="active">Adhoc Payment</li>
                </ol>
			</div>
			<div class="col-sm-6">
				@if(!$month->adhocPaymentSetting || ($month->adhocPaymentSetting &&  $month->adhocPaymentSetting->value == 'open'))

					<div class="text-right m-t-10">
                        <span>
                            <a href="/adhoc-payments/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->adhocPaymentSetting && $month->adhocPaymentSetting->value == 'locked')
                    <div class="text-right m-t-10">
                        <span>
                            <a href="/adhoc-payments/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
				@if(isset($monthList))
					<div class="text-right">
						<select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
							<option value=""></option>
							@foreach($monthList as $x)
								<option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
							@endforeach
						</select>
					</div>
				@endif
			</div>
            @if($month && $month->adhocPaymentSetting &&  $month->adhocPaymentSetting->value == 'locked')
            <div class="pull pull-right">
                <span class="badge badge-primary">Locked at {{ $month->adhocPaymentSetting ? datetime_in_view($month->adhocPaymentSetting->created_at) : ''}}</span>
            </div>
        @endif




	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			@if($month->adhocPaymentSetting && $month->adhocPaymentSetting->value == "locked" )
			@else
			<div class="panel panel-default">
				<div class="panel-body">
                        {{ Form::open(['url' => '/adhoc-payments/', 'method' => 'post']) }}
                        {{Form::hidden('month_id', $month->id)}}
                        <div class="row">
							<div class="form-group col-md-6">
								{{ Form::Label('user', 'User Name:',array('class' =>'col-md-6')) }}
								<div class="col-md-4" style="padding:0">
									{{ Form::select('user_id', $users->pluck('name','id'),null, array('class' => 'userSelect1'  ,'placeholder' => 'Select User'))}}
								</div>
							</div>
							<div class="form-group col-md-6">
								{{ Form::label('adhocPaymentComponent', 'Adhoc Payment Component:', ['class' => 'col-md-4', 'align' => 'left']) }}
								<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0px">
									{{ Form::select('adhoc_payment_component_id', $adhocPaymentsComponents->pluck('key','id'),null, array('class' => 'userSelect1'  ,'placeholder' => 'Select Appraisal Type'))}}
								</div>
							</div>
						</div>
					<div class="row">
							<div class="form-group col-md-6">
								{{ Form::Label('amount', 'Amount:',array('class' =>'col-md-4')) }}
								<div class='col-md-8 col-sm-12 col-xs-12' style="padding:0px">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-inr"></i></span>
										{{ Form::text('amount', '',['class' => 'form-control']) }}
									</div>
								</div>
                            </div>
                            <div class="form-group col-md-6">
								{{ Form::Label('comment', 'Comment :',array('class' =>'col-md-4')) }}
								<div class="col-md-8 col-sm-12 col-xs-12" style="padding:0">
										{{ Form::textarea('comment',null,['class'=>'form-control', 'rows' => 5] ) }}
								</div>
							</div>
                        </div>
					<div class="col-md-12">
					{{Form::button('<i class="fa fa-floppy-o btn-icon"></i> Save', array('type' => 'submit','class' => 'btn btn-success','style' => 'display: block; margin: 0 auto'))}}
					</div>
                    {{ Form::close() }}
				</div>
				@endif
				</div>
				<div class="panel panel-default">
				<table class="table table-striped">
				<thead>
                    <th class="td-text">#</th>
                    <th class="td-text">Emp Id</th>
					<th class="td-text">Adhoc Payment</th>
                    <th class="td-text">User</th>
                    <th class="td-text">Amount</th>
                    <th class="td-text">Comment</th>
					<th class="text-right">Actions</th>
				</thead>
				@if(isset($adhocPayments))
				@if(count($adhocPayments) > 0)
					@foreach($adhocPayments as $index=>$adhocPayment)
						<tr>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text"> {{$adhocPayment->user->employee_id}}</td>
							<td class="td-text"> {{$adhocPayment->component->description}}</td>
                            <td class="td-text"> {{$adhocPayment->user->name}}</td>
                            <td class="td-text"> {{$adhocPayment->amount}}</td>
                            <td class="td-text"> {{$adhocPayment->comment}}</td>
							<td class="text-right">
								@if($month->adhocPaymentSetting && $month->adhocPaymentSetting->value == "locked" )
								@else
								<a href="/adhoc-payments/{{$adhocPayment->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
								<div style="display:inline-block;" class="deleteform">

								{{ Form::open(['url' => '/adhoc-payments/'.$adhocPayment->id, 'method' => 'delete']) }}
                                {{Form::button('<i class="fa fa-trash btn-icon"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger btn-sm', 'onclick' => 'return confirm("Are you sure you want to delete this item?")'))}}
								{{ Form::close() }}
								@endif
								</div>
							</td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="6" class="text-center">No adhoc payment values added.</td>
					</tr>
				@endif
				@endif
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
@parent
<script type="text/javascript">
 $(document).ready(function() {
		$('.userSelect1').select2({
			placeholder: 'Select a user',
			allowClear:true
		});
    });
	$('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/adhoc-payments/"+optionValue;
        }
    });
	$('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
