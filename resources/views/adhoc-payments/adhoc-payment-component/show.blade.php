@extends('layouts.dashboard')
@section('title')
Adhoc Component Edit
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
            <div class="col-sm-8">
				<h1 class="page-title">Adhoc Payment Component</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
					<li><a href="{{ url('/adhoc-payment-component') }}">Adhoc Payment Component</a></li>
					<li class="active">{{$adhocPaymentComponentObj->description}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
				{{ Form::open(['url' => '/adhoc-payment-component/'.$adhocPaymentComponentObj->id, 'method' => 'put']) }}
				{{ Form::token() }}
				<div class="row form-group col-md-12">
                    {{ Form::label('key', 'Key :',['class' => 'col-md-4','align' => 'right'])}}
					<div class="col-md-4" style="padding:0">
                    	{{ Form::text('key', $adhocPaymentComponentObj->key,['class' => 'form-control']) }}
					</div>
				</div>
				<div class="row form-group col-md-12">
                    {{ Form::label('description', 'Description :',['class' => 'col-md-4','align' => 'right'])}}
					<div class="col-md-4" style="padding:0">
                    	{{ Form::text('description', $adhocPaymentComponentObj->description,['class' => 'form-control']) }}
					</div>
				</div>
				<div class="row form-group col-md-12">
                    {{ Form::label('type', 'Type :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{Form::radio('type', 'credit',$adhocPaymentComponentObj->type=="credit" ? true : false)}} Credit
							{{Form::radio('type', 'debit',$adhocPaymentComponentObj->type=="debit" ? true : false)}} Debit
						</div>
					</div>
				<div class="col-md-12">
					{{ Form::submit('Update',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
				</div>
				{{ Form::close() }}
				</div>
				</div>
				<div class="panel panel-default">
			</div>
		</div>
	</div>
</div>
<script>
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
