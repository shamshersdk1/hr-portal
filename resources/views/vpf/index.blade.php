@extends('layouts.dashboard')
@section('title')
VPF
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
                <h1 class="page-title">VPF</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li>VPF</li>
        		</ol>
            </div>
            <div class="col-sm-6 ">
                <a href="/vpf/create" class="btn btn-success float-right"> Add User</a>
            </div>

    </div>
</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>



    {{--  <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="/vpf" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <input type="hidden" name="from_admin" value="1">
                    <div class="row form-group">
                        <label for="" class="col-sm-6 control-label text-right">Employee Name:</label>
                            <div class="input-group col-sm-6">
                                @if(isset($userList))
                                    <select id="selectid2" name="user_id" placeholder= "Select name" required>
                                        <option value=""></option>
                                        @foreach($userList as $x)
                                            <option value="{{$x->id}}" >{{$x->name}}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                    </div>
                    <div class="row form-group">
                        <label for="name" class="col-sm-6 control-label text-right">Voluntary Provident Fund:</label>
                        <div class="input-group col-sm-3">
                        <input type="number" class="form-control" value="" name="amount" placeholder="" min="1" max="999999" required/>
                    </div>
                    </div>
                    <div class="text-center" style="margin-top: 30px;">
                        <button type="submit" class="btn btn-primary btn-sm crude-btn "> SAVE</button>
                    </div>
            </form>
        </div>
    </div>  --}}

    <div class="user-list-view">
        <div class="">
                <div class="row table-header-section">
                    <div class="col-sm-6 d-flex align-items-center"> 
                       
                    </div>
                      <div class="col-sm-6 table-search">
                      </div>
                    </div>
                <table class="table table-striped table-sm table-outer-border mt-2" id="vpf-table">
                    <thead>
                    <tr>
                        <th class="text-left">#</th>
                        <th class="text-left">Emp Id</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Amount</th>
                        <th class="text-left">Created At</th>
                        <th class="text-left">Active</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
                    @if( empty($vpfList) || count($vpfList) == 0 )
                        <tr>
                            <td>
                                <td class="text-center">No Records found</td>
                                </td>
                            </tr>
                    @endif
                    @if(isset($vpfList) > 0)
                        @foreach($vpfList as $vpf)
                            <tr>
                                <td class="text-left"></td>
                                <td class="text-left"> {{ $vpf->user ? $vpf->user->employee_id : null }} </td>
                                <td class="text-left"> {{ $vpf->user ? $vpf->user->name : null }} </td>
                                <td class="text-left">&#8377; {{ $vpf->amount }} </td>
                            

                                <td class="text-left"> {{ datetime_in_view($vpf->created_at) }} </td>
                                <td>
                                    <div class="col-md-4" >
                                    <div class="onoffswitch">
                                        <input type="checkbox" value="{{$vpf->id}}" name="onoffswitch[]" onchange="toggle(this)"
                                        class="onoffswitch-checkbox" id="{{$vpf->id}}"
                                        <?php echo (!empty($vpf->status) && $vpf->status == 'enable') ? 'checked' : ''; ?>>
                                        <label class="onoffswitch-label" for= {{$vpf->id}} >
                                            <div class="onoffswitch-inner"></div>
                                            <div class="onoffswitch-switch"></div>
                                        </label>
                                    </div>
                                </div>
                            </td>
                                <td class="text-right">
                                @if($vpf->status == 'pending')
                                        <span>
                                        <form name="showForm" method="get" action="/vpf/approve/{{$vpf->id}}"  style="display: inline-block;" class="m-0">
                                                <button style="display:none;" type="submit" class="btn btn-success btn-sm crude-btn">Approve</button>
                                        </form>
                                        <span>
                                        <span>
                                            <form name="editForm" method="get" action="/vpf/rejected/{{$vpf->id}}"  style="display: inline-block;" class="m-0">
                                                <button style="display:none;" type="submit" class="btn btn-danger btn-sm crude-btn">Reject</button>
                                            </form>
                                        </span>
                                    @endif
                                    <span>
                                    <form name="editForm" method="get" action="/vpf/{{$vpf->id}}/edit"  style="display: inline-block;" class="m-0">
                                        <button type="submit" class="btn btn-warning btn-sm crude-btn"><i class="fa fa-edit fa-fw"></i>Edit</button>
                                    </form>
                                    </span>

                                <span style="display: inline-block;">
                                    {{ Form::open(['url' => '/vpf/'.$vpf->id, 'method' => 'delete', 'class' => 'm-0']) }}
                                    {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm']) }}
                                    {{ Form::close() }}
                                </span>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </table>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(function(){
        toggle = function (item) {
            window.location = "/vpf/status-toggle/"+item.value;
		}
	});
    $('#selectid2').select2({
        placeholder: 'Select User',
        allowClear:true
    });

    $(document).ready(function() {
		var t = $('#vpf-table').DataTable( {
            pageLength:500,
            "order": false,
            "paging": false,
            "info": false,
            "bLengthChange": false,
            order: [[ 1, 'asc' ]],
        } );

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        $('.table-header-section .table-search').append($('.dataTables_filter'))


    });
</script>
@endsection

