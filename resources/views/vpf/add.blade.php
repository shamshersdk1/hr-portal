@extends('layouts.dashboard')
@section('title')
User List
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
                <h1 class="page-title">Add Voluntary Provident Fund</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li><a href="{{ url('vpf') }}">VPF</a></li>
        		</ol>
            </div>
            <div class="col-sm-6">
                <a href="{{ url('vpf') }}" class="btn btn-dark float-right">Back</a>
            </div>
    </div>
</div>
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="card border">
                <div class="card-body">
                    <form class="form-horizontal" method="post" action="/vpf" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        <input type="hidden" name="from_admin" value="1">
                            <div class="row form-group">
                                <label for="" class="col-sm-6 control-label text-right">Employee Name:</label>
                                    <div class=" col-sm-6">
                                        @if(isset($userList))
                                            <select id="selectid2" name="user_id" placeholder= "Select name" required style="width:100%">
                                                <option value=""></option>
                                                @foreach($userList as $x)
                                                    <option value="{{$x->id}}" >{{$x->name}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </div>
                            </div>
                            <div class="row form-group">
                                <label for="name" class="col-sm-6 control-label text-right">Voluntary Provident Fund:</label>
                                <div class="input-group col-sm-6">
                                <input type="number" class="form-control" value="" name="amount" placeholder="" min="1" max="999999" required/>
                            </div>
                            </div>
                            <div class="text-center" style="margin-top: 30px;">
                                <button type="submit" class="btn btn-primary btn-sm crude-btn "> SAVE</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endsection
@section('js')
@parent
<script>
    $(function(){
        toggle = function (item) {
            window.location = "/vpf/status-toggle/"+item.value;
		}
	});
    $('#selectid2').select2({
        placeholder: 'Select User',
        allowClear:true
    });

    $(document).ready(function() {
		var t = $('#vpf-table').DataTable( {
            pageLength:500,
            order: [[ 1, 'asc' ]],
        } );

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

    });
</script>
@endsection
