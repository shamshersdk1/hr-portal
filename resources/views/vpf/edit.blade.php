@extends('layouts.dashboard')
@section('title')
User List
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
                <h1 class="page-title">Edit Voluntary Provident Fund</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li><a href="{{ url('vpf') }}">VPF</a></li>
        		</ol>
            </div>
            <div class="col-sm-6">
                 <a class="btn btn-dark float-right" href="{{ url('vpf') }}">Back</a>
            </div>
    </div>
</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="card border">
                <div class="card-body">
                    <form class="form-horizontal" method="post" action="/vpf/{{$vpf->id}}" enctype="multipart/form-data">
                        @csrf
                        <input name="_method" type="hidden" value="PUT" />
                            <div class="form-group row">
                                <label for="name" class="col-sm-4  control-label">User : </label>
                                <div class="col-sm-6">
                                    <label class="form-control" name="user">{{$vpf->user->name}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 control-label">Voluntary Provident Fund:</label>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control" value="{{$vpf->amount}}" name="amount" placeholder="" min="1" max="999999" required/>
                                </div>
                            </div>
                            <div class="text-center" style="margin-top: 30px;">
                                <button type="submit" class="btn btn-primary btn-sm crude-btn float-right "> Edit</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

