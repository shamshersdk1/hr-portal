@extends('layouts.dashboard')
@section('title')
Salary Dependency
@endsection

@section('main-content')
	<div class="page-title-box">
	    <div class="row align-items-center">
	        <div class="col-sm-6">
	            <h4 class="page-title">Salary Dependencies</h4>
	            <ol class="breadcrumb">
	                <li class="breadcrumb-item">
	                	<a href="/dashboard">Dashboard</a>
	                </li>
	                <li class="breadcrumb-item active">List of components</li>
	            </ol>
	        </div>


        </div>
	</div>



	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="">
                    <div class="row align-items-center">
                        <div class="col-sm-6">
                            <h4 class="m-0 p-0  header-title text-capitalize">{{$currMonth ? $currMonth->formatMonth() : 'Month not selected'}}</h4>
                        </div>
                        <div class="col-sm-6">
                            <div class="d-flex justify-content-end align-items-center">
                                <div>
                                    <label class="m-0 mr-1">Select Month: </label>
                                        @if(isset($monthList))
                                            <select id="selectid2" name="month"  placeholder= "{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}">
                                                <option value=""></option>
                                                @foreach($monthList as $x)
                                                    <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                </div>
                                <h6 class=" ml-3">No. of Working Days : {{$currMonth ? $currMonth->working_days : 'Month not selected'}}</h6>

                            </div>
                        </div>

                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-striped table-outer-border  table-condensed table-sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Component Name</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Users</td>
                                    <td class="text-center">
                                    @switch($currMonth->workingDaySetting ? $currMonth->workingDaySetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open')
                                        @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px">{{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px">{{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px">Open</span>
                                    @endswitch
                                    </td>
                                    <td class="text-right">
                                        <a href="/user-working-day/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Due Bonuses<br>(Onsite, Performance, Extra-working, Additional, Referral, TechTalk)</td>
                                    <td class="text-center">
                                    @switch($currMonth->nonCtcBonusSetting ? $currMonth->nonCtcBonusSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open'  )
                                        @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px"> {{ datetime_in_view($currMonth->nonCtcBonusSetting->updated_at??'') }}</span>

                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{ datetime_in_view($currMonth->nonCtcBonusSetting->updated_at??'') }}</span>

                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px">Open</span>
                                    @endswitch

                                    </td>

                                    <td class="text-right">
                                        <a href="/non-ctc-bonus/due" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Fixed Bonuses</td>
                                    <td class="text-center">
                                    @switch($currMonth->fixedBonusSetting ? $currMonth->fixedBonusSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open'  )
                                        @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px"> {{ datetime_in_view($currMonth->workingDaySetting->updated_at??'') }}</span>

                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{ datetime_in_view($currMonth->workingDaySetting->updated_at??'') }}</span>

                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px"> Open</span>
                                    @endswitch

                                    </td>

                                    <td class="text-right">
                                        <a href="/deduction/fixed-bonus/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Variable Pay</td>
                                    <td class="text-center">
                                    @switch($currMonth->variablePaySetting ? $currMonth->variablePaySetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open'  )
                                        @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px">{{ datetime_in_view($currMonth->workingDaySetting->updated_at??'') }}</span>
                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>

                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px"> Open</span>
                                    @endswitch
                                    </td>
                                    <td class="text-right">
                                        <a href="/deduction/variable-pay/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Loan Deductions</td>
                                    <td class="text-center">
                                    @switch($currMonth->loanSetting ? $currMonth->loanSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open')
                                        @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>

                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px"> Open</span>
                                    @endswitch
                                    </td>
                                    <td class="text-right">
                                        <a href="/deduction/loan-deduction/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Loan Interest Income</td>
                                    <td class="text-center">
                                    @switch($currMonth->loanInterestSetting ? $currMonth->loanInterestSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open')
                                        @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px">{{ datetime_in_view($currMonth->workingDaySetting->updated_at??'') }}</span>

                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px"> Open</span>
                                    @endswitch
                                    </td>
                                    <td class="text-right">
                                        <a href="/deduction/loan-interest-income/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>VPF Deductions</td>
                                    <td class="text-center">
                                    @switch($currMonth->vpfSetting ? $currMonth->vpfSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open' )
                                    @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px"> {{ datetime_in_view($currMonth->workingDaySetting->updated_at??'') }}</span>
                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px">  Open</span>
                                    @endswitch</td>
                                    <td class="text-right">
                                        <a href="/deduction/vpf-deduction/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>It Saving Month</td>
                                    <th class="text-center">
                                    @switch($currMonth->itSavingMonthSetting ? $currMonth->itSavingMonthSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open' )
                                        @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>

                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{ datetime_in_view($currMonth->workingDaySetting->updated_at??'') }}</span>

                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px"> Open</span>
                                    @endswitch
                                    </td>
                                    <td class="text-right">
                                        <a href="/it-saving-month/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Food Deductions</td>
                                    <td class="text-center">
                                    @switch($currMonth->monthlyFoodDeductionSetting ? $currMonth->monthlyFoodDeductionSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open')
                                        @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px"> {{ datetime_in_view($currMonth->workingDaySetting->updated_at??'') }}</span>
                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px">  {{ datetime_in_view($currMonth->workingDaySetting->updated_at??'') }}</span>
                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px"> Open</span>
                                    @endswitch
                                    </td>
                                    <td class="text-right">
                                        <a href="/deduction/food-deduction/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>Insurance Deductions</td>
                                    <td class="text-center">
                                    @switch($currMonth->insuranceSetting ? $currMonth->insuranceSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open')
                                    @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px"> Open</span>
                                    @endswitch</td>
                                    <td class="text-right">
                                        <a href="/deduction/insurance-deduction/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn" >View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>11</td>
                                    <td>Adhoc Payment</td>
                                    <td class="text-center">
                                    @switch($currMonth->adhocPaymentSetting ? $currMonth->adhocPaymentSetting->value == 'locked' ? 'Locked' : 'UnLocked' : 'Open')
                                    @case('Locked')
                                            <span class="badge badge-danger" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @case('UnLocked')
                                            <span class="badge badge-success" style="font-size:12px"> {{datetime_in_view($currMonth->workingDaySetting->updated_at ?? '')}}</span>
                                            @break
                                        @default
                                            <span class="badge badge-warning" style="font-size:12px"> Open</span>
                                    @endswitch</td>
                                    <td class="text-right">
                                        <a href="/adhoc-payments/{{$currMonth->id}}" target="_blank" class="btn btn-primary btn-sm crude-btn">View</a>
                                    </td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
			</div>
		</div>
	</div>
@endsection

@section('js')
@parent
<script>
$('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/salary-dependency/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}',
			allowClear:true
		});

</script>
@endsection

