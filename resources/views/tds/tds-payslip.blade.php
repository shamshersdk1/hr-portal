@extends('layouts.dashboard')
@section('title')
Prep Salary
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="page-title">TDS Breakdown</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li class="active">TDS Breakdown for {{$monthObj->formatMonth()}}</li>
                </ol>
            </div>
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">@include('tds._partials.tds-monthly-view')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection