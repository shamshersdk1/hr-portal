<style type="text/css">
    * {
        margin:0;
        padding:0;
    }
    body {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        background: #fff;
    }
    table {
        width: 100%;
        border: solid 1px #ddd;
        border-collapse: collapse;
        margin-bottom: 20px;
    }
    table tr td, table tr th {
        border-bottom: solid 1px #ddd;
        border-right: solid 1px #ddd;
        padding: 5px;
    }
    table tr th {
        text-align: left;
    }
    p {
        font-size: 12px;
        margin-bottom: 15px;
    }
    @page {
      size: A4;
      margin: 0;
    }
    @media print {
      html, body {
        width: 210mm;
        height: 297mm;
      }
    }
</style>
<div class="main">
    <div style="text-align: right;">
        <img src="https://geekyants.com/images/logo-dark.png" width="150px" />
    </div>
    <div>
        <h1 style="font-size: 18px; text-align: center; padding: 15px 0; text-transform: uppercase; font-weight: bold;">TDS BREAKDOWN</h1>
    </div>
    <table>
        <tr>
            <th width="20%">Employee Name</th>
            <td width="40%">{{$user->name}}</td>
            <th width="20%">Employee Code</th>
            <td width="20%">{{$user->employee_id}}</td>
        </tr>
        <tr>
            <th>Designation</th>
            <td>{{$user->userDesignation ? $user->userDesignation->designation : null}}</td>
            <th>Period</th>
            <td>{{$monthObj->formatMonth()}}</td>
        </tr>
        <tr>
            <th>PAN</th>
            <td>{{$user->panAccount ? $user->panAccount->account_number : '-'}}</td>
            <th>DOJ</th>
            <td>{{$user->joining_date}}</td>
        </tr>
        <tr>
            <th>PF UAN</th>
            <td>{{$user->uanAccount ? $user->uanAccount->account_number : '-'}}</td>
            <th>ESI No.</th>
            <td>{{$user->esiAccount ? $user->esiAccount->account_number : '-'}}</td>
        </tr>
        <tr>
            <th>Bank A/c. No</th>
            <td>{{$user->savingAccount ? $user->savingAccount->account_number : '-'}}</td>
            <th>Bank IFSC</th>
            <td>{{$user->accountMetaValue('ifsc')->value ?? '-'}}</td>
        </tr>
        <tr>
            <th>No. of Working Days</th>
            <td>{{$workingDays ?? ''}}</td>
            <th>No. of Days Worked</th>
            <td>{{$days_worked ?? ''}}</td>
        </tr>
    </table>
    <table style="border:none;">
        <tr>
            <td style="border:none; vertical-align: top; padding:0 5px 0 0; width: 50%">
                <table>
                    <tr>
                        <th colspan="2" style="text-align: center;">TDS Summary</th>
                    </tr>
                    <tr>
                        <th colspan="2"><h5>INCOME</h5></th>
                    </tr>
                    <tr>
                        <th>Gross Annual Salary</th>
                        <td>{{custom_format($tdsSummary['gross-annual-salary'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Previous Employment Salary</th>
                        @if(isset($tdsSummary['previous-employment-salary']))
                            <td>{{custom_format($tdsSummary['previous-employment-salary'] ?? 0)}}</td>
                        @elseif(isset($tdsSummary['income-paid']))
                            <td>{{custom_format($tdsSummary['income-paid'] ?? 0)}}</td>
                        @else 
                            <td>0</td>
                        @endif
                    </tr>
                    <tr>
                        <th>CTC Bonus</th>
                        @if(isset($tdsSummary['ctc-bonus']))
                            <td>{{custom_format($tdsSummary['ctc-bonus'] ?? 0)}}</td>
                        @elseif(isset($tdsSummary['annual-appraisal-bonus']))
                            <td>{{custom_format($tdsSummary['annual-appraisal-bonus'] ?? 0)}}</td>
                        @else 
                            <td>0</td>
                        @endif
                    </tr>
                    <tr>
                        <th>Other Bonus</th>
                        @if(isset($tdsSummary['other-bonus']))
                            <td>{{custom_format($tdsSummary['other-bonus'] ?? 0)}}</td>
                        @elseif(isset($tdsSummary['gross-other-bonus']))
                            <td>{{custom_format($tdsSummary['gross-other-bonus'] ?? 0)}}</td>
                        @else 
                            <td>0</td>
                        @endif
                    </tr>
                    <tr>
                        <th>Total Income for the Year</th>
                        
                        <td>{{custom_format(($tdsSummary['gross-annual-salary'] ?? 0) + ($tdsSummary['income-paid'] ?? 0) + ($tdsSummary['annual-appraisal-bonus'] ?? 0) + ($tdsSummary['gross-other-bonus'] ?? 0))}}</td>
                    </tr>

                    <tr><th colspan="13"><h5>DEDUCTION</h5></th></tr>
                    <tr>
                        <th>HRA Exemption</th>
                        
                        <td>{{custom_format($tdsSummary['hra-exemption'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Standard Deduction</th>
                        
                            <td>{{custom_format($tdsSummary['standard-deduction'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Employee IT Declared u/s. 80C, limited to Rs.1.5L</th>
                        
                        <td>{{custom_format($tdsSummary['it-saving-investment'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Other Employee IT Declared</th>
                        
                        <td>{{custom_format($tdsSummary['it-saving-deduction'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Gross Professional Tax</th>
                        
                        <td>{{custom_format($tdsSummary['gross-professional-tax'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Total Deduction</th>
                        
                        <td>{{custom_format(($tdsSummary['hra-exemption']??0) + ($tdsSummary['gross-professional-tax']??0) + ($tdsSummary['it-saving-deduction']??0) + ($tdsSummary['it-saving-investment']??0) + ($tdsSummary['standard-deduction'] ?? 0))}}</td>
                    </tr>

                    <tr><th colspan="13"><h5>TAX CALCULATION</h5></th></tr>
                    <tr>
                        <th>Net Taxable Income for the Year</th>
                        
                        @if(isset($tdsSummary['net-taxable-income']))
                            <td>{{custom_format($tdsSummary['net-taxable-income'] ?? 0)}}</td>
                        @elseif(isset($tdsSummary['net-taxable-salary']))
                            <td>{{custom_format($tdsSummary['net-taxable-salary'] ?? 0)}}</td>
                        @else 
                            <td>0</td>
                        @endif
                    </tr>
                    <tr>
                        <th>Rebate if Net Taxable Income upto Rs. 5 Lakhs</th>
                        
                        <td>{{custom_format($tdsSummary['rebate'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Annual Income Tax</th>
                        
                        @if(isset($tdsSummary['annual-income-tax']))
                            <td>{{custom_format($tdsSummary['annual-income-tax'] ?? 0)}}</td>
                        @elseif(isset($tdsSummary['gross-tds']))
                            <td>{{custom_format($tdsSummary['gross-tds'] ?? 0)}}</td>
                        @else 
                            <td>0</td>
                        @endif
                    </tr>
                    <tr>
                        <th>Taxable Income after Rebate</th>
                        
                        @if(isset($tdsSummary['annual-income-tax-after-rebate']))
                            <td>{{custom_format($tdsSummary['annual-income-tax-after-rebate'] ?? 0)}}</td>
                        @elseif(isset($tdsSummary['taxable-income-after-rebate']))
                            <td>{{custom_format($tdsSummary['taxable-income-after-rebate'] ?? 0)}}</td>
                        @else 
                            <td>0</td>
                        @endif
                    </tr>
                    <tr>
                        <th>Annual Edu Cess Payable</th>
                        
                        <td>{{custom_format($tdsSummary['edu-cess'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Total Annual Income Tax Payable</th>
                        
                        @if(isset($tdsSummary['total-annual-income-tax-payable']))
                            <td>{{custom_format($tdsSummary['total-annual-income-tax-payable'] ?? 0)}}</td>
                        @elseif(isset($tdsSummary['taxable-payable']))
                            <td>{{custom_format($tdsSummary['taxable-payable'] ?? 0)}}</td>
                        @else 
                            <td>0</td>
                        @endif
                    </tr>
                    <tr>
                        <th>TDS Already deducted till date</th>
                        
                        <td>{{custom_format($tdsSummary['tds-paid-till-date'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>Balance Tax Payable</th>
                        
                        <td>{{custom_format($tdsSummary['balance-tax-payable'] ?? 0)}}</td>
                    </tr>
                    <tr>
                        <th>TDS(for the month)</th>
                        
                            @if(isset($tdsSummary['tds-for-the-month']))
                                <td>{{custom_format($tdsSummary['tds-for-the-month'] ?? 0)}}</td>
                            @elseif(isset($tdsSummary['tds']))
                                <td>{{custom_format($tdsSummary['tds'] ?? 0)}}</td>
                            @else 
                                <td>0</td>
                            @endif
                    </tr>                            
                </table>
            </td>
        </tr>
    </table>

    <div style="text-align: center; margin-top:40px;">
        <p>This is a computer generated payslip. Hence no signature is required.</p>
        @if(substr($user->employee_id,0,1) == 'B')
        <div>
            <p>
                <b>SAHU SOFT INDIA PRIVATE LIMITED</b>, CIN:  U72200BR2006PTC011902
                <br />
                Regd. Office: Amgola Road, Muzaffarpur, P.S: Kaji Mohammadpur, Bihar 842002 India
                <br />
                Corp. Office: #18, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru - 76, India
            </p>
            <p> 
                +91 80 409 74929 | 
                <a href="mailto:info@geekyants.com">info@geekyants.com</a> | 
                <a href="https://geekyants.com/" target="_blank">www.geekyants.com</a>
            </p>
        </div>

        <!-- GeekyAnts Academy -->
        @elseif(substr($user->employee_id,0,1) == 'A')
        <div>
            <p>
                <b>GEEKYANTS ACADEMY PRIVATE LIMITED</b>, CIN: U80302KA2019PTC127401
                <br />
                Corp. Office: #18, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru - 76, India
            </p>
            <p> 
                +91 80 409 74929 | 
                <a href="mailto:reachout@geekyants.academy">reachout@geekyants.academy</a> | 
                <a href="https://geekyants.academy/" target="_blank">geekyants.academy</a>
            </p>
        </div>

        @elseif(substr($user->employee_id,0,1) == 'S')
         <!-- MunshiApp -->
        <div>
            <p>
                <b>MUNSHIAPP INDIA PRIVATE LIMITED</b>, CIN: U72200KA2015PTC079406 
                <br />
                Corp. Office: #18, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru - 76, India
            </p>
            <p> 
                +91 80 409 74929 | 
                <a href="mailto:info@geekyants.com">info@geekyants.com</a>
            </p>
        </div>

        

        @elseif(substr($user->employee_id,0,1) == 'G')
         <!-- GeekyAnts -->
        <div>
            <p>
                <b>GEEKYANTS SOFTWARE PRIVATE LIMITED</b>, CIN:  U72900KA2015PTC083390
                <br />
                Corp. Office: #18, 2nd Cross Road, N S Palya, BTM Layout Stage 2, Bengaluru - 76, India
            </p>
            <p> 
                +91 80 409 74929 | 
                <a href="mailto:info@geekyants.com">info@geekyants.com</a> | 
                <a href="https://geekyants.com/" target="_blank">www.geekyants.com</a>
            </p>
        </div>
        @endif
    </div>
</div>

