<div class="user-list-view">
        <div class="panel panel-default ">
            <table class="table-striped">
                @if(count($monthList) > 0)
                    @foreach($monthList as $month)
                        <th style="padding:20px" class="text-center">{{$month->formatMonth()}}</th>
                    @endforeach
                    <tr>
                        @foreach($monthList as $month)
                            @if(count($data[$month->id]) > 0)
                                <td valign = "top" style="padding-top: 25px;">
                                    No. Of Days -> {{$data[$month->id]['no_of_working']}}
                                    <table class="table table-striped">
                                        
                                    @foreach($appraisalCompType as $item)
                                        <tr>
                                            <td>{{$item->name}}</td>
                                            <td><span style="font-weight:300; margin-left:20px">{{$data[$month->id][$item->code] ?? 0}}</span></td>
                                        </tr>
                                    @endforeach
                                    </table>
                                    <span style="padding:10px">Monthly Gross Salary:</span>
                                    <span style="font-weight:300; margin-left:20px">{{$data[$month->id]['monthly_gross_salary']}}</span>
                                </td>
                            @elseif(count($futureData[$month->id]) > 0)
                                <td valign = "top" style="padding-top: 25px;">
                                    No. Of Days -> {{$futureData['no_of_working'][$month->id]}} 
                                    <table class="table table-striped no-margin">
                                        @foreach ($appraisalCompType as $item)
                                            <tr>
                                                <td>{{$item->name}} : </td>
                                                <td><span style="margin-left:20px">{{$futureData[$month->id][$futureAppraisal->id][$item->code] ?? 0}}</span></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    <?php echo "<br>"; ?> 
                                    <span>Monthly Gross Salary:</span>
                                    <span style="font-weight:300; margin-left:20px">{{$futureData[$month->id]['monthly_gross_salary']}}</span>
                                </td>
                            @else 
                                <td valign = "top" style="padding-top: 50px;">No Data Found</td>
                            @endif
                        @endforeach
                    </tr>
                @endif
            </table>
        </div>
    </div>