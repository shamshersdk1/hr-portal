@extends('layouts.dashboard')
@section('title')
TDS Summary
@endsection

@section('main-content')
<div class="page-title-box">
    	<div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="page-title">List of Users</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/admin">Admin</a></li>
                    <li><a href="{{ url('admin/users') }}">List of Users</a></li>
                    <li><a class="active">TDS Summary for {{$userObj->name}}</a></li>                      
        		</ol>
            </div>
		</div>
    </div>
   
  
    <div class="card border">
        <div class="card-body">
        <div class="row">
            <div class="col-sm-4 pull-left">
                <label>Processing salary for {{$monthObj ? $monthObj->formatMonth() : null}}</label>
            </div>
            <div class="col-sm-8">
               
                <div class="float-right">
                    @if(isset($financialYearList))
                       Select Year: <select id="selectid3" name="year"  placeholder= "{{$financialYearObj->year}} - {{$financialYearObj->year + 1}}">
                            <option value=""></option>
                            @foreach($financialYearList as $x)
                                <option value="{{$x->id}}" >{{$x->year}} - {{$x->year + 1}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
                <div class=" float-right mr-2">
                    @if(isset($userList))
                        Select User: <select id="selectid2" name="user"  placeholder= "{{$userObj->name}}">
                            <option value=""></option>
                            @foreach($userList as $x)
                                <option value="{{$x->id}}" >{{$x->employee_id}} - {{$x->name}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(!empty($errors->all()))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ $error }}</span><br/>
                        @endforeach
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ session('message') }}</span><br/>
                    </div>
                @endif
                @if(session('alert-class'))
                    <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span>{{ session('alert-class') }}</span><br/>
                    </div>
                @endif
            </div>
        </div>
            <div class="bg-white mt-4">
                <ul class="nav nav-tabs nav-tabs-user" role="tablist">
                    <li role="presentation" class="nav-item">
                        <a href="#salary-details" class="nav-link active" id="salary" role="tab" data-toggle="tab" aria-selected="true">Salary Breakdown</a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#tds" id="pay" role="tab" data-toggle="tab" class="nav-link "aria-selected="false" >TDS Breakdown</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="salary-details" role="tabpanel" class="tab-pane active" aria-labelledby="salary">
                        @include('tds/_partials.monthly-salary-summary')
                    </div>
                    <div id="tds" role="tabpanel" class="tab-pane" aria-labelledby="tds" >
                        @include('tds/_partials.tds-summary')
                    </div>
                </div>
            </div>
    </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $('#selectid2').select2();
    $('#selectid3').select2();

    $('#selectid3').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/user/"+{{$userObj->id}}+"/tds-summary/"+optionValue; 
        }
    });
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) { 
            window.location = "/user/"+optionValue+"/tds-summary/"+{{$financialYearObj->id}}; 
        }
    });

</script>
@endsection

