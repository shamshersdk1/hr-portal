@extends('layouts.dashboard')
@section('title')
Prep Salary
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-centers">
        <div class="col-sm-8">
            <h1 class="page-title">Prep Components Comparison</h1>
            <ol class="breadcrumb">
                <li><a href="/dashboard">Dashboard</a></li>
                <li><a href="/prep-salary">Prep Salary</a></li>
                <li class="active">
                    @if(Request::is("prep-salary/paid-comparison/*"))
                    Gross Paid
                    @elseif(Request::is("prep-salary/future-pay-comparison/*"))
                    Gross To Be Paid
                    @elseif(Request::is("prep-salary/current-gross-comparison/*"))
                    Current Gross
                    @elseif(Request::is("prep-salary/tds-comparison/*"))
                    TDS
                    @endif
                    Compare for
                {{$prevPrepSal->month ? $prevPrepSal->month->formatMonth() : ""}} - {{$prepSalObj->month ? $prepSalObj->month->formatMonth() : ""}}</li>
            </ol>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="card border">
        <div class="card-body">
            <div class="position-absolute">
                <div class="d-flex align-items-center">
                    <label class="label">Comparision for {{ucfirst(str_replace('/{prepSalId}','',str_replace('prep-salary/','',$url)))  }} </label>
                </div>
            </div>
            <table class="table table-striped table-sm table-outer-border" id="summary-table">
                <thead>
                    <th class="text-left">#</th>
                    <th class="text-left">Emp ID</th>
                    <th class="text-left">Name</th>
                    @foreach($data['keys'] as $key => $value)
                        <th class="text-center"><span class="badge badge-primary">{{nameFromCode($key)}}</span> <br/> {{ date('M', mktime(0, 0, 0, $prevPrepSal->month->month, 10))}}, {{date('y', mktime(0, 0, 0, 0, 0,$prevPrepSal->month->year))}}</th>
                        <th class="text-center"><span class="badge badge-primary">{{nameFromCode($key)}}</span> <br/> {{date('M', mktime(0, 0, 0, $prepSalObj->month->month, 10))}}, {{date('y', mktime(0, 0, 0, 0, 0,$prepSalObj->month->year))}}</th>
                        <th class="text-center"><span class="badge badge-primary">{{nameFromCode($key)}}</span> <br/> Diff</th>
                    @endforeach
                </thead>
                <tbody>
                    @if(count($users) > 0)
                    @foreach($users as $user)
                    @if(isset($data[$user->id]))
                        <tr>
                            <td class="text-left"></td>
                            <td class="text-left">{{ $user->employee_id}}</td>
                            <td class="text-left">{{ $user->name}}</td>
                            @foreach($data['keys'] as $key => $value)
                                <td class="text-center">{{$data[$user->id][$key]['prev'] ?? 0}}</td>
                                <td class="text-center">{{$data[$user->id][$key]['curr'] ?? 0}}</td>
                                @if(!isset($data[$user->id][$key]['prev']) && isset($data[$user->id][$key]['curr']))
                                    @if($data[$user->id][$key]['curr'] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{(-1) * $data[$user->id][$key]['curr']}}</td>
                                    @endif
                                @elseif(isset($data[$user->id][$key]['prev']) && !isset($data[$user->id][$key]['curr']))
                                    @if($data[$user->id][$key]['prev'] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{$data[$user->id][$key]['prev']}}</td>
                                    @endif
                                @elseif(isset($data[$user->id][$key]['prev']) && isset($data[$user->id][$key]['curr']))
                                    @if($data[$user->id][$key]['prev'] - $data[$user->id][$key]['curr'] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{$data[$user->id][$key]['prev'] - $data[$user->id][$key]['curr']}}</td>
                                    @endif
                                @else
                                    <td>0</td>
                                @endif
                            @endforeach
                        </tr>
                    @endif
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="6">
                            No Records found
                        </td>
                    </tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500,
			fixedHeader: true,
            dom: 'Bfrtip',
			buttons: [
                'csv'
            ],
			
            scrollCollapse: true,
            paging:         false,
            
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection
