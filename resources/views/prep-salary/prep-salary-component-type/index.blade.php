@extends('layouts.dashboard')
@section('title')
Prep Salary Component Type | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="page-title">Prepare Salary Component Type</h1>
                <ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="/dashboard">Dashboard</a>
					</li>
					<li class="breadcrumb-item active">Prepare Salary Component Type</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">

                <button type="button" onclick="window.history.back();" class="btn btn-secondary"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card border">
				<div class="card-body">
					<h6>Add user</h6>
                    <div class="row mt-4">
						<div class="col-sm-6">
							{{ Form::open(['url' => '/prep-salary/prep-salary-component-type', 'method' => 'post']) }}
							<div class="row">
								{{ Form::label('name', 'Name :',['class' => 'col-md-4','align' => 'right'])}}
								<div class="form-group col-md-8" >
									{{ Form::text('name', '',['class' => 'form-control']) }}
								</div>
							</div>
							<div class="row">
								{{ Form::label('service_class', 'Service Class :',['class' => 'col-md-4','align' => 'right'])}}
								<div class="col-md-8">
									<div class="form-group input-group">
									<span class="input-group-addon">App\Services\SalaryService\</span>
									{{ Form::text('service_class', '',['class' => 'form-control','style' => 'left-margin:0','align' => 'left']) }}
									</div>
									</div>
								</div>
	

						
						</div>

						<div class="col-sm-6">
							<div class="row">
								{{ Form::label('code', 'Code :',['class' => 'col-md-4','align' => 'right'])}}
								<div class="form-group col-md-8" >
									{{ Form::text('code', '',['class' => 'form-control']) }}
								</div>
							</div>

							
						@if(isset($prepSalaryComponentTypes))
									<div class="row">
											{{ Form::label('service_class', 'Service Class Dependencies:',['class' => 'col-md-4','align' => 'right'])}}
										<div class="form-group col-md-8" >
											<div class="multipicker">
												<div class="">
													<select class="js-example-basic-multiple22 " multiple="multiple" name="dependencies[]">
														@foreach($prepSalaryComponentTypes as $prepSalaryComponentType)
															<?php
																$saved = false;
																foreach($selected_classes as $selected_class){
																	if($selected_class == $prepSalaryComponentType->id){
																		$saved = true;
																	}
																}
															?>
															@if(isset($selected_classes))
																@if($saved)
																	<option value="{{$prepSalaryComponentType->id}}" selected >{{substr($prepSalaryComponentType->service_class,27)}}</option>
																@else
																	<option value="{{$prepSalaryComponentType->id}}">{{substr($prepSalaryComponentType->service_class,27)}}</option>
																@endif
															@else
																<option value="{{$prepSalaryComponentType->id}}">{{substr($prepSalaryComponentType->service_class,27)}}</option>
															@endif
														@endforeach
													</select>
												</div>
											</div>
										</div>
									</div>
									@endif
						</div>
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success float-right','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
                </div>
            </div>
				<div class="card">
				<table class="table table-striped table-sm table-outer-border">
				<thead>
					<th class="td-text">#</th>
					<th class="td-text">Name</th>
					<th class="td-text">Code</th>
                    <th class="td-text">Service Class</th>
					<th class="td-text">Dependency</th>
					<th  class="text-right">Actions</th>
                </thead>
                <tbody>
				@if(isset($prepSalaryComponentTypes))
				@if(count($prepSalaryComponentTypes) > 0)
					@foreach($prepSalaryComponentTypes as $index=>$prepSalaryComponentType)
						<tr>
							<td class="td-text">{{$index+1}}</td>
							<td class="td-text"> {{$prepSalaryComponentType->name}}</td>
							<td class="td-text"> {{$prepSalaryComponentType->code}}</td>
                            <td class="td-text"> {{$prepSalaryComponentType->service_class}}</td>
							<td class="td-text align-middle">
								@foreach($prepSalaryComponentType->dependsOn as $dependent)
									{{$dependent->dependentOn ? $dependent->dependentOn->name : ''}},
								@endforeach
								</td>
							<td class="text-right" width="150px">

								<a href="/prep-salary/prep-salary-component-type/{{$prepSalaryComponentType->id}}" class="btn btn-info crud-btn btn-sm">Edit</a>
                                <div style="display:inline-block;" >
								{{ Form::open(['url' => '/prep-salary/prep-salary-component-type/'.$prepSalaryComponentType->id, 'method' => 'delete', 'class' => 'm-0']) }}
								{{ Form::submit('Delete',['class' => 'btn btn-danger crud-btn btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
								{{ Form::close() }}
								</div>
							</td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="6" class="text-center">No prepare salary component type types added.</td>
					</tr>
				@endif
                @endif
                </tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>

</script>
@endsection
