<div class="container-fluid">
    <div class="row">
		<table class="table">
            <thead>
                <tr>
                    <td>Total Amount</td>
                    <td>Remaining Amount</td>
                    <td>EMI</td>
                </tr>
            </thead>
            @foreach($data as $key=> $loan)
                <tr>
                    <td>{{$loan['amount']  ? $loan['amount'] : '-'}}</td>
                    <td>{{$loan['remaining_amount']  ? $loan['remaining_amount'] : '-'}}</td>
                    <td>{{$loan['emi']  ? $loan['emi'] : '-'}}</td>
                </tr>
            @endforeach                                     
        </table>

	</div>
</div>

