<div class="container-fluid">
    <div class="row">
		<table class="table">
            <thead>
                <tr>
                    @foreach($data as $component)
                        <td>{{$component['key'] ? $component['key'] : '-'}}</td>
                    @endforeach
                </tr>
            </thead>
            <tr>
                @foreach($data as $component)
                    <td>{{$component['value'] ? $component['value'] : '-'}}</td>  
                @endforeach
            </tr>
        </table>
	</div>
</div>

