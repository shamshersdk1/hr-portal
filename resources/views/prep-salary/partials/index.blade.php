@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-8">
				<h1 class="page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
					<li><a href="{{ url('prep-salary') }}">Prepare Salary</a></li>
					<li class="active">View</li>
                </ol>
			</div>

	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="user-list-view">
		<table class="table table-striped" id="prep-salary-table">
			<thead>
				<th>#</th>
				<th>Employee Code</th>
				<th> Name</th>
				<th>Appraisal</th>
				<th>Appraisal Bonus</th>
				<th>Working Days</th>
				<th>Bonus</th>
				<th>Loan</th>
				<th>IT Saving</th>
				<th>VPF</th>
				<th>Food Deduction</th>
				<th>Insurance</th>
				<th>Adhoc Payment</th>
				<th>Gross Salary Paid</th>
				<th>Current Gross</th>
				<th>Gross Salary  To Be Paid</th>
				<th>TDS</th>





			</thead>
			<tbody>

				@if(count($prepUsers)>0)
					@foreach($prepUsers as  $index => $prepUser)
						<tr>
							<td>{{$index+1}}</td>
							<td>{{$prepUser['user']['employee_id']}}</td>
                            <td>{{$prepUser['user']['name']}}</td>
                            <td>
								@include('prep-salary.partials.appraisal',array('data'=>$response[$prepUser->user_id]['appraisal']))
							</td>
							<td>
								@include('prep-salary.partials.appraisal-bonus',array('data'=>$response[$prepUser->user_id]['appraisal-bonus']))
							</td>
							<td>
								@include('prep-salary.partials.working-day',array('data'=>$response[$prepUser->user_id]['working-day']))
							</td>
							<td>
								@include('prep-salary.partials.bonus',array('data'=>$response[$prepUser->user_id]['bonus']))
							</td>
							<td>
								@include('prep-salary.partials.loan',array('data'=>$response[$prepUser->user_id]['loan']))
							</td>
							<td>
								@include('prep-salary.partials.it-saving',array('data'=>$response[$prepUser->user_id]['it-saving']))
							</td>
							<td>
								@include('prep-salary.partials.vpf',array('data'=>$response[$prepUser->user_id]['vpf']))
							</td>
							<td>
								@include('prep-salary.partials.food-deduction',array('data'=>$response[$prepUser->user_id]['food-deduction']))
							</td>
							<td>
								@include('prep-salary.partials.insurance',array('data'=>$response[$prepUser->user_id]['insurance']))
							</td>
							<td>
								@include('prep-salary.partials.adhoc-payment',array('data'=>$response[$prepUser->user_id]['adhoc-payment']))
							</td>
							<td>
								@include('prep-salary.partials.gross-salary-paid',array('data'=>$response[$prepUser->user_id]['gross-paid-till-now']))
							</td>
							<td>
								@include('prep-salary.partials.current-gross',array('data'=>$response[$prepUser->user_id]['current-gross']))
							</td>
							<td>
								@include('prep-salary.partials.gross-salary-to-be-paid',array('data'=>$response[$prepUser->user_id]['gross-to-be-paid']))
							</td>
							<td>
								@include('prep-salary.partials.tds',array('data'=>$response[$prepUser->user_id]['tds']))
							</td>

						</tr>
					@endforeach
				@endif
			</tbody>
		</table>

	</div>
</div>
@endsection
@section('js')
@parent
<script>
   $(document).ready(function() {
		$tableHeight = $( window ).height();
        var t = $('#prep-salary-table').DataTable( {
            pageLength:500,
			fixedHeader: true,
			"columnDefs": [ {
				"searchable": true,
                "orderable": true,
            } ],
			scrollY: $tableHeight - 200,
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
    });
</script>
@endsection
