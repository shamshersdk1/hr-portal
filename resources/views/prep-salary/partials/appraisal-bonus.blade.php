<div class="container-fluid">
    <div class="row">
		<table class="table">
            <thead>
                <tr>
                    <td> Bonus Type</td>
                    <td>Amount</td>
                </tr>
            </thead>
            @foreach($data as $key=> $bonus)
                    @if(count($bonus)>0)
                    <tr>
                        <td>{{$bonus['bonus_type']  ? $bonus['bonus_type']  : '-'}}</td>
                        <td>{{$bonus['amount']  ? $bonus['amount']  : '-'}}</td>
                    </tr>
                    @else
                        <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    @endif
            @endforeach                                     
        </table>

	</div>
</div>
