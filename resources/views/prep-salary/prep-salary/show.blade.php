@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-8">
				<h1 class="page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li><a href="{{ url('prep-salary') }}">Prepare Salary</a></li>
					<li class="active">{{$prepSalarie->month->formatMonth()}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-dark"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>

	</div>
</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            @foreach ($errors->all() as $error)
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class=" p-2 d-flex justify-center border mb-3 align-items-center" style="justify-content: space-between;  position: -webkit-sticky; position: sticky; top:50px; z-index: 999; background: #fff;">
		<div class="d-flex w-100 align-items-center">
			<div class="col-sm-6 d-flex justify-content-between">
				<div>
					<div class="text-muted">Month</div>
					<div class="font-14 font-500 text-center text-primary">{{$prepSalarie->month->formatMonth()}}</div>
				</div>
				<div>
					<div class="text-muted">Created At</div>
					<div class="font-14 font-500 text-center">{{datetime_in_view($prepSalarie->created_at)}}</div>
				</div>
				
				
				<div>
					<div class="text-muted">Status
					</div>
					<div class="font-16 font-500 text-center">
						@if($prepSalarie->status == 'open')
						<span class="badge badge-primary">Open</span>
						@elseif($prepSalarie->status == 'in_progress')
						<span class="badge badge-primary">In Progress</span>
						@elseif($prepSalarie->status == 'processed')
						<span class="badge badge-primary">Processed</span>
						@elseif($prepSalarie->status == 'finalizing')
						<span class="badge badge-primary">Finalizing</span>
						@else
						<span class="badge badge-danger">Closed</span>
						@endif					  
				</div>
				</div>
			</div>
			<div class="col-sm-6 d-flex justify-content-end">
				<div class="d-flex justify-content-end align-items-center">
					@if($prepSalarie->status != 'open' && $prepSalarie->status != 'discarded')
						{{ Form::open(['url' => '/prep-salary/salary-sheet-comparison/'.$prepSalarie->month_id, 'method' => 'get', 'class'=>'ml-2 mb-0']) }}
						{{ Form::submit('Salary Sheet Compare',['class' => 'btn btn-warning']) }}
						{{ Form::close() }}
					@endif
					{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/salary-sheet', 'method' => 'get', 'class'=>'ml-2  mb-0']) }}
					{{ Form::submit('Salary Sheet',['class' => 'btn btn-info']) }}
					{{ Form::close() }}
					@if($prepSalarie->checkAll() == true && $prepSalarie->status === 'processed')
						{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/finalize', 'method' => 'get' , 'class'=>'ml-2  mb-0']) }}
						{{ Form::submit('Finalize',['class' => 'btn btn-success']) }}
						{{ Form::close() }}
					@endif
					@if($prepSalarie->status === 'open')
						{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/generate-all', 'method' => 'get', 'class'=>'ml-2  mb-0' ]) }}
						{{ Form::submit('Generate All',['class' => 'btn btn-primary','onClick'=> "return confirm('This will generate all components?')"]) }}
						{{ Form::close() }}
					@endif
					@if($prepSalarie->status === 'processed')
						{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/generate-all', 'method' => 'get']) }}
						{{ Form::submit('Regenerate All',['class' => 'btn btn-primary','onClick'=> "return confirm('This will regenerate all components?')"]) }}
						{{ Form::close() }}
					@endif
					@if($prepSalarie->status == 'finalizing')
						{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/close', 'method' => 'get']) }}
						{{ Form::submit('Close',['class' => 'btn btn-danger','onclick' => 'return confirm("Are you sure you want to close this item?")']) }}
						{{ Form::close() }}
						@endif
				</div>
			</div>
		</div>
	   
	</div>

	<div class="row">
		<div class="col-md-12">
			@if($prepSalarie->status === 'in_progress')
					<div class="alert alert-warning">
	    				<b><i class="fa fa-spinner fa-spin"></i> Please wait! Preparing the salary data. This may take sometime.</b>
    				</div>
				@endif
			
				<div class="card border table-sm">
				<table class="table table-striped">
				<thead>
					<th class="td-text">#</th>
                    <th class="td-text">Name</th>
                    <th class="td-text">Status</th>
					<th class="td-text">Is Generated</th>
					<th class="td-text">Execution Status</th>
					<th class="text-right" class="text-right">Actions</th>
				</thead>
				@if(isset($prepSalarie))
				@if($prepSalarie->components && count($prepSalarie->components) > 0)
					@foreach($prepSalarie->components as $index=>$prepSalaryComponent)
						<tr>
							<td class="td-text">{{$index+1}}</td>
                            <td class="td-text"> {{$prepSalaryComponent->type->name}}</td>
                            <td class="td-text">
                                    @if($prepSalaryComponent->status == 'open')
                                    <span class="badge badge-primary">Open<span>
                                    @else
                                    <span class="badge badge-danger">Close<span>
                                    @endif
                            </td>
							<td class="td-text">
								@if($prepSalaryComponent->is_generated == 1)
									<span class="badge badge-primary">Yes<span>
								@else
                                    <span class="badge badge-danger">No<span>
                                @endif
							</td>
							<td class="td-text">
								@if($prepSalarie->status != 'open')
									@if($prepSalaryComponent->totalExecutions() > 0)
										<span class="badge badge-default">
											Total : {{$prepSalaryComponent->totalExecutions() ?? 0}}
										</span>
									@endif
									@if($prepSalaryComponent->countExecutions('completed') > 0)
										<span class="badge badge-success">
											Completed : {{$prepSalaryComponent->countExecutions('completed') ?? 0}}
										</span>
									@endif
									@if($prepSalaryComponent->countExecutions('failed') > 0)
										<span class="badge badge-danger">
											Failed : {{$prepSalaryComponent->countExecutions('failed') ?? 0}}
										</span>
									@endif
								@endif

							</td>
							<td class="text-right">
                                @if($prepSalaryComponent->type->code === 'user' && $prepSalaryComponent->is_generated == '1' )
                                <a target="_blank" href="/prep-salary/user-generate/{{$prepSalarie->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i> Users Generate</a>
                                <a target="_blank" href="/prep-salary/{{$prepSalaryComponent->id}}/view" class="btn btn-warning crud-btn btn-sm"><i class="fa fa-eye btn-icon-space" aria-hidden="true"></i>View</a>
								@endif
								@if($prepSalaryComponent->type->code === 'user' && $prepSalarie->status == 'open')
								<a target="_blank" href="/prep-users/{{$prepSalarie->id}}" class="btn btn-success btn-sm">Add Users</a>
								@endif
								@if($prepSalaryComponent->type->code === 'appraisal-bonus')
								<a target="_blank" href="/deduction/fixed-bonus/{{$prepSalarie->month->id}}" class="btn btn-primary btn-sm">View Fixed Bonuses</a>
								@endif
								@if($prepSalaryComponent->type->code === 'appraisal-bonus')
								<a target="_blank" href="/deduction/variable-pay/{{$prepSalarie->month->id}}" class="btn btn-dark btn-sm">View Variable Pay</a>
								@endif
								@if($prepSalaryComponent->type->code == 'loan')
								<a target="_blank" href="/deduction/loan-deduction/{{$prepSalarie->month->id}}" class="btn btn-primary btn-sm">View Loan Deductions</a>
								@endif
								@if($prepSalaryComponent->type->code == 'loan')
								<a target="_blank" href="/deduction/loan-interest-income/{{$prepSalarie->month->id}}" class="btn btn-dark btn-sm">View Loan Interest Income</a>
								@endif
								@if($prepSalaryComponent->type->code == 'vpf')
								<a target="_blank" href="/deduction/vpf-deduction/{{$prepSalarie->month->id}}" class="btn btn-primary btn-sm">View VPF Deductions</a>
								@endif
								@if($prepSalaryComponent->type->code == 'food-deduction')
								<a target="_blank" href="/deduction/food-deduction/{{$prepSalarie->month->id}}" class="btn btn-primary btn-sm">View Food Deductions</a>
								@endif
								@if($prepSalaryComponent->type->code == 'insurance')
								<a target="_blank" href="/deduction/insurance-deduction/{{$prepSalarie->month->id}}" class="btn btn-primary btn-sm">View Insurance Deductions</a>
								@endif
								@if($prepSalaryComponent->type->code == 'it-saving')
								<a target="_blank" href="/it-saving-month/{{$prepSalarie->month->id}}" class="btn btn-primary btn-sm">View Saving Month</a>
								@endif
								@if($prepSalaryComponent->type->code == 'working-day')
								<a target="_blank" href="/user-working-day/{{$prepSalarie->month->id}}" class="btn btn-primary btn-sm">View Working Day</a>
								@endif
								@if($prepSalaryComponent->type->code == 'gross-paid-till-now')
								<a target="_blank" href="paid-comparison/{{$prepSalarie->id}}" class="btn btn-info btn-sm">Compare</a>
								@endif
								@if($prepSalaryComponent->type->code == 'current-gross')
								<a target="_blank" href="current-gross-comparison/{{$prepSalarie->id}}" class="btn btn-info btn-sm">Compare</a>
								@endif
								@if($prepSalaryComponent->type->code == 'gross-to-be-paid')
								<a target="_blank" href="future-pay-comparison/{{$prepSalarie->id}}" class="btn btn-info btn-sm">Compare</a>
								@endif
								@if($prepSalaryComponent->type->code === 'tds')
								<a target="_blank" href="tds-comparison/{{$prepSalarie->id}}" class="btn btn-info btn-sm">Compare</a>
                                @endif
                                @if($prepSalaryComponent->type->code === 'it-saving')
								<a target="_blank" href="it-saving-comparison/{{$prepSalarie->id}}" class="btn btn-info btn-sm">Compare</a>
                                @endif
                                @if($prepSalaryComponent->type->code === 'appraisal')
								<a target="_blank" href="appraisal-comparison/{{$prepSalarie->id}}" class="btn btn-info btn-sm">Compare</a>
								@endif
                                @if(($prepSalarie->status == 'open' || $prepSalarie->status == 'processed') && !($prepSalaryComponent->type->code === 'user'))
								    <a href="/prep-salary/{{$prepSalaryComponent->id}}/generate" onClick="return confirm('This will regenerate dependent components?')" class="btn btn-secondary crud-btn btn-sm" ><i class="fa fa-gear btn-icon-space" aria-hidden="true"></i>Generate</a>
								@endif

                            </td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="6" class="text-center">No prepare salary component type types added.</td>
					</tr>
				@endif
				@endif
				</table>

			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
@parent
<script>

	function generate() {
		window.location = '/prep-salary/'+{{$prepSalarie->id}}+'/generate-all';
    }
	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
