@extends('layouts.admin-dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
					<li class="breadcrumb-item"><a href="{{ url('admin/prep-salary') }}">Prepare Salary</a></li>
					<li class="breadcrumb-item active">{{$month->formatMonth()}}</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="panel-heading">
                    <span>Month: <label>{{$month->formatMonth()}}</label></span><br>
                    <span>Status: <label>
                        <span class="badge badge-info">Open<span> 
                    </label></span>
                    <div class="row">
                <div class="row col-sm-4">
			    <div class="card">
				    <table class="table table-striped">
                        <thead>
                            <th width="10%">#</th>
                            <th width="10%">Name</th>
                        </thead>
                        @if(isset($prepSalaryComponents))
                            @if(count($prepSalaryComponents) > 0)
                                @foreach($prepSalaryComponents as $prepSalaryComponent)
                                    <tr>
                                        <td class="td-text">{{$prepSalaryComponent->id}}</td>
                                        <td class="td-text"> {{$prepSalaryComponent->name}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr >
                                    <td colspan="6" class="text-center">No prepare salary component type types added.</td>
                                </tr>
                            @endif
                        @endif
				    </table>
            </div>
            </div>
            <div class="col-sm-12"> 
					{{ Form::open(['url' => '/admin/prep-salary/store/'.$month->id, 'method' => 'get']) }}
                    <div class="col-md-12">
                        {{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto; align: center']) }}
                        {{ Form::close() }}
					</div>
                </div>
            </div>
            </div>            
          </div>
    </div>
</div>  
</div>
</div>
@endsection
@section('js')
@parent
<script>

	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
