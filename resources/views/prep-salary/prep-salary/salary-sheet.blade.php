@extends('layouts.dashboard')
@section('title')
Prep Salary Payslip
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
                <h1 class="page-title">Salary Transaction</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/prep-salary">Prep Salary</a></li>
                    <li class="active">Salary Sheet for {{$prepSalObj->month ? $prepSalObj->month->formatMonth() : ""}}</li>
                </ol>
            </div>
            <div class="col-sm-6">
                <a class="btn btn-primary btn-sm crude-btn float-right" href="salary-sheet/download"><i class="fa fa-download"></i> &nbsp;Download CSV</a>
                </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>

    <div class="mt-4">
           
        <div class="table-responsive">

            <table class="table table-striped table-outer-border" id="summary-table">
                <thead>
                    <th class="text-left">#</th>
                    <th class="text-left">Emp ID</th>
                    <th class="text-left">Name</th>
                    <th class="text-center">Bank</th>
                    <th class="text-center">Monthly Gross Salary</th>
                    <th class="text-center">Basic</th>
                    <th class="text-center">HRA</th>
                    <th class="text-center">Car Allowance</th>
                    <th class="text-center">Food Allowance</th>
                    <th class="text-center">Leave Travel Allowance</th>
                    <th class="text-center">Special Allowance</th>
                    <th class="text-center">Stipend</th>
                    <th class="text-center">PF Employee</th>
                    <th class="text-center">PF Employeer</th>
                    <th class="text-center">PF Other</th>
                    <th class="text-center">ESI</th>
                    <th class="text-center">Professional Tax</th>
                    <th class="text-center">VPF Deduction</th>
                    <th class="text-center">Medical Insurance</th>
                    <th class="text-center">Food Deduction</th>
                    <th class="text-center">Loan Deduction</th>
                    <th class="text-center">Adhoc Amount</th>
                    <th class="text-center">TDS</th>
                    <th class="text-center">Appraisal Bonus</th>
                    <th class="text-center">Onsite Bonus</th>
                    <th class="text-center">Referral Bonus</th>
                    <th class="text-center">Tech Talk Bonus</th>
                    <th class="text-center">Additional Work Day Bonus</th>
                    <th class="text-center">Performance Bonus</th>
                    <th class="text-center">User Timesheet Extra</th>
                    <th class="text-center">Total Other Bonus</th>
                    <th class="text-center">Total Deduction</th>
                    <th class="text-center">Net Payable</th>
                </thead>
                <tbody>
                @if(count($users) > 0)
                    @foreach($users as $user)
                    @if(isset($data[$user->id]))
                        <tr>
                            <td class="text-left"></td>
                            <td class="text-left">{{ $user->employee_id}}</td>
                            <td class="text-left">{{ $user->name}}</td>
                            <td class="text-center">{{$data[$user->id]['Bank'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Monthly_Gross_Salary'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Basic'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['HRA'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Car_Allowance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Food_Allowance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Leave_Travel_Allowance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Special_Allowance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Stipend'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['PF_Employee'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['PF_Employeer'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['PF_Other'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['ESI'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Professional_Tax'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['VPF_Deduction'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Medical_Insurance'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Food_Deduction'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Loan_Deduction'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Adhoc_Amount'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['TDS'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Appraisal_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Onsite_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Referral_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['TechTalk_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Additional_Work_Day_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Performance_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['User_Timesheet_Extra'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Total_Other_Bonus'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Total_Deduction'] ?? 0}}</td>
                            <td class="text-center">{{$data[$user->id]['Net_Payable'] ?? 0}}</td>
                        </tr>
                    @endif
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500,
            fixedHeader: true,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ],
            scrollY: '60vh',
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

    });
</script>
@endsection
