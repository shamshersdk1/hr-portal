@extends('layouts.dashboard')
@section('title')
Salary Dependency
@endsection
@section('main-content')
	<div class="page-title-box">
	    <div class="row align-items-center">
	        <div class="col-sm-6">
	            <h4 class="page-title">User Salary Compare</h4>
	            <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/prep-salary/{{$prepSalObj->id}}">Prep Salary</a></li>
                    <li class="active">{{$prepSalObj ? $prepSalObj->month->formatMonth() : " "}}</li>
                </ol>
	        </div>
        </div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card border">
                <div class="card-body">
                    <div class="col-sm-12 text-right d-flex align-items-center justify-content-end" >
                        <label class="m-0 pr-1">Select User: </label>
                        @if(isset($prepSalObj->prepUsers))
                            <select id="selectid2" name="month"  placeholder= "{{$user ? $user->name : 'Select User'}}">
                                <option value=""></option>
                                @foreach($prepSalObj->prepUsers as $x)
                                    <option value="{{$x->user_id}}" >{{$x->user->name}}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-outer-border  table-condensed table-sm" id="user-table">
                            <thead>
                                <tr>
                                    <th class="text-left">#</th>
                                    <th class="text-left">Key</th>
                                    <th class="text-left">{{$prevMonthObj->formatMonth()}}</th>
                                    <th class="text-left">{{$prepSalObj->month->formatMonth()}}</th>
                                    <th class="text-left">Difference</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index=1 ?>
                                @foreach($appraisalData['current'] as $key => $data)
                                    @if($key != 'Employee_ID' && $key != 'Name' && $key != 'Bank')
                                    <tr>
                                        <td class="text-left">{{$index++}}</td>
                                        <td class="text-left">{{str_replace("_"," ",$key)}}</td>
                                        <td class="text-left">{{$appraisalData['previous'][$key] ?? ''}}</td>
                                        <td class="text-left">{{$data ?? ''}}</td>
                                        <td class="text-left">{{($data ?? 0) - ($appraisalData['previous'][$key]??0)}}</td>
                                    </tr>
                                    @endif
                                @endforeach
                                <tr>
                                    <td class="text-center" colspan="3"> TDS component</td>
                                </tr>
                                <?php $index=1 ?>
                                @foreach($tdsData as $key => $data)
                                <tr>
                                    <td class="text-left">{{$index++}}</td>
                                    <td class="text-left">{{ucwords(str_replace("-"," ",$key))}}</td>
                                    <td class="text-left">{{$data['prev'] ?? ''}}</td>
                                    <td class="text-left">{{$data['curr'] ?? ''}}</td>
                                    <td class="text-left">{{$data['curr'] - ($data['prev']?? 0)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
		</div>
	</div>
@endsection
@section('js')
@parent
<script>
    $('#selectid2').change(function(){
            var optionSelected = $("option:selected", this);
            optionValue = this.value;
            if (optionValue) {
                window.location = "/prep-salary/user-generate/compare/{{$prepSalObj->id}}/"+optionValue;
            }
        });
        $('#selectid2').select2({
                placeholder: '{{$user ? $user->name : 'Select User'}}',
                allowClear:true
            });

    </script>
@endsection
