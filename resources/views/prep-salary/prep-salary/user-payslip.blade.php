@extends('layouts.dashboard')
@section('title')
Payslip
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        
            <div class="col-sm-6">
                <h1 class="page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
	                	<a href="/dashboard">Dashboard</a>
	                </li>
                    <li class="breadcrumb-item active">Payslips Salary</li>
                    <li class="breadcrumb-item active">{{$month->formatMonth()}}</li>
                </ol>
            </div>
      
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">@include('prep-salary.prep-salary.partial.payslip-view')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection