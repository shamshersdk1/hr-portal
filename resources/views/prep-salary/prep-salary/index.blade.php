@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
		<div class="row align-items-center">
            <div class="col-sm-6">
				<h1 class="page-title">Prepare Salary</h1>

                <ol class="breadcrumb">
					<li class="breadcrumb-item">
	                	<a href="/dashboard">Dashboard</a>
	                </li>
	                <li class="breadcrumb-item active">Prepare Salary</li>
                </ol>
			</div>	
			<div class="col-sm-6">
				
			</div>

					
		</div>
	</div>
	<div class="">
		<div class="">
		
		<div class="row">
			<div class="col-md-12">
				@if(!empty($errors->all()))
					<div class="alert alert-danger">
						@foreach ($errors->all() as $error)
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<span>{{ $error }}</span><br/>
						@endforeach
					</div>
				@endif
				@if (session('message'))
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<span>{{ session('message') }}</span><br/>
					</div>
				@endif
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
			
				<div class="">
					<div class="row table-header-section">
						<div class="col-sm-6">
							{{ Form::open(['url' => '/prep-salary/create', 'method' => 'post']) }}
							<div class=" d-flex align-items-center">
								{{ Form::select('id', $monthList,'Select Month', array('class' => 'userSelect'  ,'placeholder' => 'Select Month'),['align' => 'left'])}}
							{{ Form::submit('Add',['class' => 'float-right btn btn-success btn-sm ml-2']) }}
							</div>
							{{ Form::close() }}

						
						</div>
						  <div class="col-sm-6 table-search">
						  </div>
						</div>
					<div class="table-responsive mt-2">
					<table class="table table-striped table-outer-border no-margin user-list-table table-sm" id="salary-table">
					<thead>
						<th >#</th>
						<th >Month</th>
						<th >Status</th>
						<th >Created At</th>
						<th  class="text-right">Actions</th>
					</thead>
					@if(isset($prepSalaries))
					@if(count($prepSalaries) > 0)
						@foreach($prepSalaries as $prepSalarie)
							<tr>
								<td class="td-text">{{$prepSalarie->id}}</td>
								<td class="td-text"> {{$prepSalarie->month->formatMonth()}}</td>
								<td class="td-text"> 
									@if($prepSalarie->status == 'open')
									<span class="label label-primary">Open<span>
									@elseif($prepSalarie->status == 'in_progress')
									<span class="label label-danger">In Progress<span>
									@elseif($prepSalarie->status == 'processed')
									<span class="label label-danger">Processed<span>
									@elseif($prepSalarie->status == 'finalizing')
									<span class="label label-danger">Finalizing<span>
									@elseif($prepSalarie->status == 'discarded')
									<span class="label label-danger">Discarded<span>
									@else
									<span class="label label-warning">{{$prepSalarie->status}}<span>
									@endif
									</td>
								<td class="td-text"> {{datetime_in_view($prepSalarie->created_at)}}</td>
								<td class="text-right">
									@if($prepSalarie->status != 'open' && $prepSalarie->status != 'discarded')
										<div style="display:inline-block;" class="lockform">
											{{ Form::open(['url' => '/prep-salary/salary-sheet-comparison/'.$prepSalarie->month_id, 'method' => 'get', 'class' => 'm-0']) }}
											{{ Form::submit('Salary Sheet Compare',['class' => 'btn btn-warning btn-sm crude-btn']) }}
											{{ Form::close() }}
										</div>
									@endif
									<div style="display:inline-block;" class="lockform">
										{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/salary-sheet', 'method' => 'get', 'class' => 'm-0']) }}
										{{ Form::submit('Salary Sheet',['class' => 'btn btn-info btn-sm crude-btn']) }}
										{{ Form::close() }}
									</div>
									@if($prepSalarie->status == 'open')
									<div style="display:inline-block;" class="lockform">
										{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/lock', 'method' => 'get', 'class' => 'm-0']) }}
										{{ Form::submit('Lock',['class' => 'btn btn-warning btn-sm crude-btn','onclick' => 'return confirm("Are you sure you want to Lock this item?")']) }}
										{{ Form::close() }}
									</div>
									@endif
									<a href="/prep-salary/{{$prepSalarie->id}}" class="btn btn-primary btn-sm">View</a>
									@if($prepSalarie->status == 'open')
									<div style="display:inline-block;" class="deleteform">
									{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id, 'method' => 'delete', 'class' => 'm-0']) }}
									{{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm crude-btn','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
									{{ Form::close() }}
									</div>
									@endif
									@if($prepSalarie->status == 'open' || $prepSalarie->status == 'in_progress')
									<div style="display:inline-block;" class="deleteform">
									{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/discard', 'method' => 'get', 'class' => 'm-0']) }}
									{{ Form::submit('Discard',['class' => 'btn btn-primary btn-sm crude-btn','onclick' => 'return confirm("Are you sure you want to discard this item?")']) }}
									{{ Form::close() }}
									</div>
									@endif
									@if($prepSalarie->status == 'finalizing')
									<div style="display:inline-block;" class="deleteform">
									{{ Form::open(['url' => '/prep-salary/'.$prepSalarie->id.'/close', 'method' => 'get', 'class' => 'm-0']) }}
									{{ Form::submit('Close',['class' => 'btn btn-danger btn-sm crude-btn','onclick' => 'return confirm("Are you sure you want to close this item?")']) }}
									{{ Form::close() }}
									</div>
									@endif
								</td>
							</tr>
						@endforeach
					@else
						<tr >
							<td colspan="6" class="text-center">No prepare salary added.</td>
						</tr>
					@endif
					@endif
					</table>
				</div>
			</div>
		</div>
	</div>
		</div>
	</div>

@endsection
@section('js')	
@parent
<script>

 $(document).ready(function() {
	var t = $('#salary-table').DataTable( {
		pageLength:500, 
		paging:false,

		"ordering": false,

		"columnDefs": [ {
			"searchable": false,
			"targets": 0
		} ],
		fixedHeader: true
	} );
        $('#effectiveDate').datetimepicker({
            format: 'YYYY-MM-DD'
        });
		$('.userSelect').select2({
			placeholder: 'Select Month',
			allowClear:true
		});
		$('.table-header-section .table-search').append($('.dataTables_filter'))

    });
</script>
@endsection
