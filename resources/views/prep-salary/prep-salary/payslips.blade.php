@extends('layouts.dashboard')
@section('title')
Prep Salary
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row">
        <div class="col-sm-6">
            <h1 class="page-title">Payslip</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li class="breadcrumb-item"><a href="/bank-transfer">Bank Transfer</a></li>
                <li class="breadcrumb-item active">Salary Payslips, {{ $monthObj->formatMonth()}}</li>
            </ol>
        </div>
        <div class="col-sm-6 text-right">
           

        </div>
    </div>
</div>
<div class="">
    <div class="d-flex justify-content-between align-items-center mb-3">
      <h4 class="m-0 p-0  header-title text-capitalize ">List of payslip for the month of {{ $monthObj->formatMonth()}}</h5>
        @if(isset($monthList))
        <div class="d-flex align-items-center">
            <label class="mr-1 mb-0">Select Month:</label>
            <select id="selectid4" name="month"  placeholder= "{{ $monthObj->formatMonth()}}">
                <option value=""></option>
                @foreach($monthList as $x)
                    <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                @endforeach
            </select>
        </div>
        @endif
    </div>
            <div class="table-responsive">
                    <table class="table table-striped table-sm table-outer-border" id="payslip-table">
                        <thead>
                        <tr>
                            <td>#</td>
                            <td>EMP ID</td>
                            <td>Name</td>
                            <td class="text-right">Actions</td>
                        </tr>
                        </thead>
                    @if(count($transactions) > 0)
                        @foreach($transactions as $user_transction)
                            <tr>
                                <td></td>
                                <td>{{$user_transction->user->employee_id}}</td>
                                <td>{{$user_transction->user->name}}</td>
                                <td class="text-right">
                                    <a href="/payslips/{{$monthId}}/{{$user_transction->user->id}}" class="btn btn-primary waves-effect waves-light btn-sm" target="_blank">View Payslip</a>
                                    <a href="/payslips/{{$monthId}}/{{$user_transction->user->id}}/download" class="btn btn-danger waves-effect waves-light btn-sm">Download Payslip</a>
                                    <a href="/tds-breakdown/{{$monthId}}/{{$user_transction->user->id}}" class="btn btn-warning waves-effect waves-light btn-sm" target="_blank">View TDS Breakdown</a>
                                    <a href="/tds-breakdown/{{$monthId}}/{{$user_transction->user->id}}/download" class="btn btn-success waves-effect waves-light btn-sm">Download TDS Breakdown</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8" class="text-center">
                                No Records found
                            </td>
                        </tr>
                    @endif
                </table>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#payslip-table').DataTable( {
            pageLength:500,
            scrollY:        true,
            scrollCollapse: true,
            paging:         false,
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
    $('#selectid4').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/payslips/"+optionValue;
        }
    });
    $('#selectid4').select2({
			placeholder: '{{$monthObj ? $monthObj->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>
@endsection
