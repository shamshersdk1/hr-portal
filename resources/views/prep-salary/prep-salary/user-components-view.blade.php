@extends('layouts.dashboard')
@section('title')
Salary Dependency
@endsection
@section('main-content')
	<div class="page-title-box">
	    <div class="row align-items-center">
	        <div class="col-sm-6">
	            <h4 class="page-title">User Components View</h4>
	            <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li class="active"><a href="/prep-salary/{{$prepObj->id}}">PrepSalary</a></li>
                    <li class="active">{{$prepObj->month->formatMonth()}}</li>

                </ol>
	        </div>
        </div>
    </div>

	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <div class="card border mb-4">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <span>Month: <label>{{$prepObj->month->formatMonth()}}</label></span><br>
                    <span>User: <label>{{$user->name}}</label></span><br>
                    <span>Employee Id: <label> {{$user->employee_id}}</label></span></br>
                </div>
                <div class="col-sm-6 text-right d-flex align-items-center justify-content-end" >
                    <label class="m-0 pr-1">Select User: </label>
                    @if(isset($prepUsers))
                        <select id="selectid2" name="month"  placeholder= "{{$user ? $user->name : 'Select User'}}">
                            <option value=""></option>
                            @foreach($prepUsers as $x)
                                <option value="{{$x->user->id}}" >{{$x->user->name}}</option>
                            @endforeach
                        </select>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card card-body table-responsive">
        <caption class="text-left">Appraisal</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['appraisal']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['appraisal']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped ">
            <tr><td>@include('prep-salary.partials.appraisal',array('data'=>$data['appraisal']))</td></tr>
        </table>
    </div>

    <div class="card card-body table-responsive">
        <caption class="text-left">Appraisal Bonus</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['appraisal-bonus']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['appraisal-bonus']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped">
            <tr><td>@include('prep-salary.partials.appraisal-bonus',array('data'=>$data['appraisal-bonus']))</td></tr>
        </table>
    </div>

    <div class="card card-body table-responsive">
        <label class="label">Working Day</label>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['working-day']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['working-day']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped">
            <tr><td>@include('prep-salary.partials.working-day',array('data'=>$data['working-day']))</td></tr>
        </table>
    </div>
    <div class="card card-body table-responsive">
        <caption class="text-left">Bonus</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['bonus']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['bonus']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.bonus',array('data'=>$data['bonus']))</td></tr>
        </table>
    </div>
    <div class="card card-body table-responsive">
        <caption class="text-left">Loan</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['loan']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['loan']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.loan',array('data'=>$data['loan']))</td></tr>
        </table>
    </div>
    <div class="card card-body table-responsive">
        <caption class="text-left">It Saving</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['it-saving']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['it-saving']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.it-saving',array('data'=>$data['it-saving']))</td></tr>
        </table>
    </div>
    <div class="card card-body table-responsive">
        <caption class="text-left">VPF</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['vpf']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['vpf']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.vpf',array('data'=>$data['vpf']))</td></tr>
        </table>
    </div>
    <tr class="card card-body table-responsive">
        <caption class="text-left">Food Deduction</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['food-deduction']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['food-deduction']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.food-deduction',array('data'=>$data['food-deduction']))</td></tr>
        </tr>
    </table>
    <div class="card card-body table-responsive">
        <caption class="text-left">Insurance</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['insurance']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['insurance']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.insurance',array('data'=>$data['insurance']))</td></tr>
        </table>
    </div>
    <div class="card card-body table-responsive">
        <caption class="text-left">Adhoc Payment</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['adhoc-payment']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['adhoc-payment']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.adhoc-payment',array('data'=>$data['adhoc-payment']))</td></tr>
        <div class="card card-body table-responsive"></table>
            </div>
        <caption class="text-left">Gross Paid Till Date</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['gross-paid-till-now']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['gross-paid-till-now']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.gross-salary-paid',array('data'=>$data['gross-paid-till-now']))</td></tr>
        </table>
    </div>
    <div class="card card-body table-responsive">
        <caption class="text-left">Gross Current</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['current-gross']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['current-gross']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.current-gross',array('data'=>$data['current-gross']))</td></tr>
        </table>
    </div>
    <div class="card card-body table-responsive">
        <caption class="text-left">Gross Current To Be Paid</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['gross-to-be-paid']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['gross-to-be-paid']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.gross-salary-to-be-paid',array('data'=>$data['gross-to-be-paid']))</td></tr>
        </table>
    </div>
    <div class="card card-body table-responsive">
        <caption class="text-left">TDS</caption>
        <div class="text-right">
            <span class="badge badge-primary">{{$executions['tds']['status']}}</span>
            <a href="/prep-salary/user-generate-single/{{$executions['tds']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate</a>
        </div>
        <table class="table table-striped table-responsive">
            <tr><td>@include('prep-salary.partials.tds',array('data'=>$data['tds']))</td></tr>
        </table>
    </div>





@endsection
@section('js')
@parent
<script>
    $('#selectid2').change(function(){
            var optionSelected = $("option:selected", this);
            optionValue = this.value;
            if (optionValue) {
                window.location = "/prep-salary/user-components-view/{{$prepObj->id}}/"+optionValue;
            }
        });
        $('#selectid2').select2({
                placeholder: '{{$user ? $user->name : 'Select User'}}',
                allowClear:true
            });

    </script>
@endsection
