@extends('layouts.dashboard')
@section('title')
Prep Salary
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
				<h1 class="page-title">Select Users</h1>
                <ol class="breadcrumb">
					<li class="active">Month List</li>
                </ol>
            </div>
            <div class="col-sm-6 right-btn-wrap d-flex justify-content-end">
                @if((!$prepSalCompObj->status || ($prepSalCompObj->status == 'open')) && !empty($prepUsers) && count($prepUsers) > 0)

                    <div class="text-right mr-1">
                        <span>
                            <a href="/prep-users/{{$prepSalaryObj->id}}/{{$prepSalaryObj->month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif($prepSalCompObj->status == 'closed')
                    <div class="text-right mr-1">
                        <span>
                            <a href="/prep-users/{{$prepSalaryObj->id}}/{{$prepSalaryObj->month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif

			</div>
            @if($prepSalCompObj->status == 'closed')
            <div class="pull text-right">
                <span class="label label-primary">Locked at {{ $prepSalaryObj->month->prepUserSetting ? datetime_in_view($prepSalaryObj->month->prepUserSetting->created_at) : ''}}</span>
            </div>
            @endif


        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>


		{{Form::open(['url' => '/prep-users/' .$prepSalaryObj->id, 'method' => 'POST'])}}
	<div class="row mb-2 align-items-center">
        <div class="col-sm-6">
            <label>Processing Salary For {{$prepSalaryObj->month->formatMonth()}}</label>

        </div>

        @if(count($users) > 0)
        @if($prepSalCompObj->status == 'closed')
        @else
		<div class=" col-sm-6 text-right btn-save">
			{{Form::button('<i class="fa fa-save fa-fw" aria-hidden="true"></i><span style="padding-left:5px">Save</span>', array('type' => 'submit', 'class' => 'btn btn-success'))}}
        </div>
        @endif
		@endif
	</div>

	<div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped table-outer-border" id="summary-table">
                <thead>
                    @if($prepSalCompObj->status == 'closed')
                        <th class="text-left" width="5%">#</th>
                    @else
					    <th class="check text-center" width="7%"><input type="checkbox" id="flowcheckall" value="" />&nbsp;<small>Select All Users</small></th>
                    @endif
                    <th class="text-left">Employee Id</th>
                    <th class="text-left">Name</th>
                    <th class="text-left">Joining Date</th>
                    <th class="text-left">Email</th>
                    <th class="text-left">Status</th>

                </thead>
                <tbody>
                @if($prepSalCompObj->status == 'closed')
                    @if(count($prepUsers) > 0)
                        @foreach($prepUsers as $index=>$user)
                        <tr>
                            <td>{{$index+1}}</td>
                            <td class="text-left">{{ $user->user->employee_id}}</td>
                            <td class="text-left">{{ $user->user->name}}</td>
                            <td class="text-left">{{ date_in_view($user->user->joining_date)}}</td>
                            <td class="text-left">{{ $user->user->email}}</td>
                            <td class="td-text">
                            @if($user->user->is_active)
                            <span class="label label-primary">Active<span>
                            @else
                            <span class="label label-danger">Inactive<span>
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    @else
                    <tr>
                        <td colspan="7" class="text-center">
                            No Records found
                        </td>
                    </tr>
                    @endif
                @else
                    @if(count($users) > 0)
                        @foreach($users as $user)
                                <tr>
                                    @if($prepSalaryObj->prepUsers->where('user_id',$user->user_id)->first())
                                    <td class="check text-center"><input type="checkbox" id={{$user->id}} name="options[{{$user->user_id}}]" value={{$user->id}} checked />&nbsp;</td>
                                    @else
                                        @if(!count($prepSalaryObj->prepUsers) > 0 && $user->is_active === 1)
                                        <td class="check text-center"><input type="checkbox" id={{$user->user_id}} name="options[{{$user->user_id}}]" value={{$user->user_id}} checked />&nbsp;</td>
                                        @else
                                        <td class="check text-center"><input type="checkbox" id={{$user->user_id}} name="options[{{$user->user_id}}]" value={{$user->user_id}}/>&nbsp;</td>
                                        @endif
                                    @endif
                                    <td class="text-left">{{ $user->user->employee_id}}</td>
                                    <td class="text-left">{{ $user->user->name}}</td>
                                    <td class="text-left">{{ date_in_view($user->user->joining_date)}}</td>
                                    <td class="text-left">{{ $user->user->email}}</td>
                                    <td class="td-text">
                                    @if($user->user->is_active)
                                    <span class="label label-primary">Active<span>
                                    @else
                                    <span class="label label-danger">Inactive<span>
                                    @endif
                                    </td>
                                </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7" class="text-center">
                                No Records found
                            </td>
                        </tr>
                    @endif
                @endif
                </tbody>
            </table>
		</div>
	</div>
	{{Form::close()}}
</div>
@endsection
@section('js')
@parent
<script>
$('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) {
            window.location = "/salary-transfer/"+optionValue;
        }
    });
    $.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('input', td).prop('checked') ? '0' : '1';
        } );
    }
	$(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500,
            fixedHeader: {
                header: true
            },
            columnDefs: [
            {
                targets: 0,
                orderDataType: 'dom-checkbox',
                orderable: false,
            }
            ]
        } );
        var data = t.column(0).data().sort().reverse();
        $(':checkbox').on('change', function(e) {
        var row = $(this).closest('tr');
        var hmc = row.find(':checkbox:checked').length;
        var kluj = parseInt(hmc);
        row.find('td.counter').text(kluj);
        table.row(row).invalidate('dom');
        });
    });
	$("#flowcheckall").click(function () {
        $('#summary-table tbody input[type="checkbox"]').prop('checked', this.checked);
    });
            {{-- $('.right-btn-wrap').append($('.btn-save')) --}}

</script>
@endsection
