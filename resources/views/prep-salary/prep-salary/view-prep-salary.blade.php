@extends('layouts.admin-dashboard')
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
		<div class="row">
            <div class="col-sm-8">
				<h1 class="admin-page-title">Prepare Salary</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/admin">Admin</a></li>
					<li class="breadcrumb-item"><a href="{{ url('admin/prep-salary') }}">Prepare Salary</a></li>
					<li class="breadcrumb-item active">View</li>
                </ol>
			</div>
        </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<table class="table table-striped">
			<thead>
				<th width="8%"> Employee Id</th>
				<th width="15%">Employee Code</th>
				<th>Appraisal</th>

			</thead>
			<tbody>
				
				@if(count($data)>0)
					@foreach($data as $index=> $userData)
						<tr>
							<td>{{$index}}</td>
							<td>{{$userData['employee_id']}}</td>
							<td>
								<table class="table table-striped">
									

								@if(count($userData['appraisal'])>0)
									@foreach($userData['appraisal'] as $key => $appraisal)
									
										@if(isset($appraisal['component']))
											@foreach($appraisal['component'] as $index => $component)
												<tr>
													{{$index}}
												</tr>
												<tr>
													{{$component}}
												</tr>
												<br/>
											@endforeach
										@endif	
										<br/>							
								@endforeach
								@endif
								</table>
							</td>

						</tr>
					@endforeach
				@endif
			</tbody>
		</table>

	</div>
</div>

@endsection
