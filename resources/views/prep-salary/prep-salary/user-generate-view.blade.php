@extends('layouts.dashboard')
@section('title')
Salary Dependency
@endsection
@section('main-content')
	<div class="page-title-box">
	    <div class="row align-items-center">
	        <div class="col-sm-6">
	            <h4 class="page-title">User Salary Generate</h4>
	            <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/prep-salary/{{$prepSalary->id}}">Prep Salary</a></li>
                    <li class="active">{{$prepSalary ? $prepSalary->month->formatMonth() : " "}}</li>
                </ol>
	        </div>
        </div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card border">
                <div class="card-body">
                    <div class="row mb-3">
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-outer-border  table-condensed table-sm" id="user-table">
                            <thead>
                                <tr>
                                    <th class="text-left">#</th>
                                    <th class="text-left">Emp Id:</th>
                                    <th class="text-left">Emp Name:</th>
                                    @foreach($components as $component)
                                        <th class="text-left">{{$component['name']}}</th>
                                    @endforeach
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($userExecutions as $userExecution)
                                <tr>
                                    <td class="text-left"></td>
                                    <td class="text-left">{{$userExecution['user']['employee_id']}}</td>
                                    <td class="text-left">{{$userExecution['user']['name']}}</td>
                                    @foreach($userExecution['executions'] as $execution)
                                        @if($execution['status'] == 'init')
                                            <td class="text-center"><i class="fa fa-info-circle"></i> </td>
                                        @elseif($execution['status'] == 'completed')
                                            <td class="text-center"><i class="fa fa-check"></i> </td>
                                        @elseif($execution['status'] == 'in_progress')
                                            <td class="text-center"><i class="fa fa-spinner"></i> </td>
                                        @else
                                            <td> <a href="/prep-salary/user-generate-single/{{$execution['id']}}" class="btn btn-primary btn-sm crude-btn" >Generate</a></td>
                                        @endif
                                    @endforeach
                                    <td class="text-right">
                                        <a href="/prep-salary/user-generate/compare/{{$prepSalary->id}}/{{$userExecution['user']['id']}}" class="btn btn-success btn-sm crude-btn" >Compare</a>
                                        <a href="/prep-salary/user-components-view/{{$prepSalary->id}}/{{$userExecution['user']['id']}}" class="btn btn-warning btn-sm crude-btn" >View</a>
                                    @if($userExecution['is_all_generated'])
                                        <a href="/prep-salary/user-generate-all/{{$prepSalary->id}}/{{$userExecution['user']['id']}}" class="btn btn-success btn-sm crude-btn" >Generate All</a>
                                    @else
                                        <a href="/prep-salary/user-generate-all/{{$prepSalary->id}}/{{$userExecution['user']['id']}}" class="btn btn-primary btn-sm crude-btn" >Generate All</a>
                                    @endif
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                        </table>
                    </div>
                </div>
			</div>
		</div>
	</div>
@endsection
@section('js')
@parent
<script>
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/salary-dependency/"+optionValue;
        }
    });
    $('#selectid2').select2({
		allowClear:true
	});
    $(document).ready(function() {
		var t = $('#user-table').DataTable({
            fixedHeader: true,
			scrollY:        '70vh',
            scrollX:        'true',
			paging:         false,
			scrollCollapse: true,

			fixedColumns:   {
				leftColumns: 3,
				rightColumns:1,
            },
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
@endsection
