@extends('layouts.dashboard')
@section('title')
Prep Salary Payslip
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-centers">
        <div class="col-sm-8">
            <h1 class="page-title">Salary Compare</h1>
            <ol class="breadcrumb">
                <li><a href="/dashboard">Dashboard</a></li>
                <li><a href="/prep-salary">Prep Salary</a></li>
                <li class="active">Salary Sheet Compare for {{$prevMonthObj->formatMonth()}} - {{$prepSalObj->month ? $prepSalObj->month->formatMonth() : ""}}</li>
            </ol>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
        </div>
    </div>
    <div class="card border">
        <div class="card-body">
            <div class="top-search">
                <label>Select Month:</label>
                @if(isset($monthList))
                <select id="selectid2" name="month"  placeholder= "{{$prepSalObj->month ? $prepSalObj->month->formatMonth() : 'Select Month'}}">
                    <option value=""></option>
                    @foreach($monthList as $x)
                        <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                    @endforeach
                </select>
            @endif
            </div>
            <table class="table table-striped table-sm table-outer-border" id="summary-table">
                <thead>
                    <th class="text-left">#</th>
                    <th class="text-left">Emp ID</th>
                    <th class="text-left">Name</th>
                    <th class="text-center">Bank</th>
                    @foreach($apprCompType as $type)
                        <th class="text-center">{{$type->name}} <br/> {{ date('M', mktime(0, 0, 0, $prevMonthObj->month, 10))}}, {{date('y', mktime(0, 0, 0, 0, 0,$prevMonthObj->year+1))}}</th>
                        <th class="text-center">{{$type->name}} <br/> {{date('M', mktime(0, 0, 0, $prepSalObj->month->month, 10))}}, {{date('y', mktime(0, 0, 0, 0, 0,$prepSalObj->month->year+1))}}</th>
                        <th class="text-center">{{$type->name}} <br/> Diff</th>
                    @endforeach
                        <th class="text-center">Net Salary ExBonus<br/> {{ date('M', mktime(0, 0, 0, $prevMonthObj->month, 10))}}, {{date('y', mktime(0, 0, 0, 0, 0,$prevMonthObj->year+1))}}</th>
                        <th class="text-center">Net Salary ExBonus<br/> {{  date('M', mktime(0, 0, 0, $prepSalObj->month->month, 10))}}, {{date('y', mktime(0, 0, 0, 0, 0,$prepSalObj->month->year+1))}}</th>
                        <th class="text-center">Net Salary<br/> Diff</th>
                        <th class="text-center">Net Salary<br/> {{ date('M', mktime(0, 0, 0, $prevMonthObj->month, 10))}}, {{date('y', mktime(0, 0, 0, 0, 0,$prevMonthObj->year+1))}}</th>
                        <th class="text-center">Net Salary<br/> {{date('M', mktime(0, 0, 0, $prepSalObj->month->month, 10))}}, {{date('y', mktime(0, 0, 0, 0, 0,$prepSalObj->month->year+1))}}</th>
                        <th class="text-center">Net Salary<br/> Diff</th>
                </thead>
                <tbody>
                    @if(count($users) > 0)
                    @foreach($users as $user)
                    @if(isset($data[$user->id]))
                        <tr>
                            <td class="text-left"></td>
                            <td class="text-left">{{ $user->employee_id}}</td>
                            <td class="text-left">{{ $user->name}}</td>
                            <td class="text-center">{{$data[$user->id]['Bank'] ?? 0}}</td>
                            @foreach($apprCompType as $type)
                                <td class="text-center">{{$data[$user->id]['previous'][$type->code] ?? 0}}</td>
                                <td class="text-center">{{$data[$user->id]['current'][$type->code] ?? 0}}</td>
                                @if(!isset($data[$user->id]['previous'][$type->code]) && isset($data[$user->id]['current'][$type->code]))
                                    @if($data[$user->id]['current'][$type->code] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{(-1) * $data[$user->id]['current'][$type->code]}}</td>
                                    @endif
                                @elseif(isset($data[$user->id]['previous'][$type->code]) && !isset($data[$user->id]['current'][$type->code]))
                                    @if($data[$user->id]['previous'][$type->code] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{$data[$user->id]['previous'][$type->code]}}</td>
                                    @endif
                                @elseif(isset($data[$user->id]['previous'][$type->code]) && isset($data[$user->id]['current'][$type->code]))
                                    @if($data[$user->id]['previous'][$type->code] - $data[$user->id]['current'][$type->code] == 0)
                                        <td>0</td>
                                    @else
                                        <td class="text-center text-white bg-dark">{{$data[$user->id]['current'][$type->code] - $data[$user->id]['previous'][$type->code]}}</td>
                                    @endif
                                @else
                                    <td>0</td>
                                @endif
                            @endforeach
                            <td>{{$data[$user->id]['previous']['net_sal_compare_ex_bonus'] ?? 0}}</td>
                            <td>{{$data[$user->id]['current']['net_sal_compare_ex_bonus'] ?? 0}}</td>
                            @if(!isset($data[$user->id]['previous']['net_sal_compare_ex_bonus']) && isset($data[$user->id]['current']['net_sal_compare_ex_bonus']))
                                @if($data[$user->id]['current']['net_sal_compare_ex_bonus'] == 0)
                                    <td>0</td>
                                @else
                                    <td class="text-center text-white bg-dark">{{(-1) * $data[$user->id]['current']['net_sal_compare_ex_bonus']}}</td>
                                @endif
                            @elseif(isset($data[$user->id]['previous']['net_sal_compare_ex_bonus']) && !isset($data[$user->id]['current']['net_sal_compare_ex_bonus']))
                                @if($data[$user->id]['previous']['net_sal_compare_ex_bonus'] == 0)
                                    <td>0</td>
                                @else
                                    <td class="text-center text-white bg-dark">{{$data[$user->id]['previous']['net_sal_compare_ex_bonus']}}</td>
                                @endif
                            @elseif(isset($data[$user->id]['previous']['net_sal_compare_ex_bonus']) && isset($data[$user->id]['current']['net_sal_compare_ex_bonus']))
                                @if($data[$user->id]['previous']['net_sal_compare_ex_bonus'] - $data[$user->id]['current']['net_sal_compare_ex_bonus'] == 0)
                                    <td>0</td>
                                @else
                                    <td class="text-center text-white bg-dark">{{$data[$user->id]['current']['net_sal_compare_ex_bonus'] - $data[$user->id]['previous']['net_sal_compare_ex_bonus']}}</td>
                                @endif
                            @else
                                <td>0</td>
                            @endif
                            <td>{{$data[$user->id]['previous']['net_sal_compare'] ?? 0}}</td>
                            <td>{{$data[$user->id]['current']['net_sal_compare'] ?? 0}}</td>

                            @if(!isset($data[$user->id]['previous']['net_sal_compare']) && isset($data[$user->id]['current']['net_sal_compare']))
                                @if($data[$user->id]['current']['net_sal_compare'] == 0)
                                    <td>0</td>
                                @else
                                    <td class="text-center text-white bg-dark">{{(-1) * $data[$user->id]['current']['net_sal_compare']}}</td>
                                @endif
                            @elseif(isset($data[$user->id]['previous']['net_sal_compare']) && !isset($data[$user->id]['current']['net_sal_compare']))
                                @if($data[$user->id]['previous']['net_sal_compare'] == 0)
                                    <td>0</td>
                                @else
                                    <td class="text-center text-white bg-dark">{{$data[$user->id]['previous']['net_sal_compare']}}</td>
                                @endif
                            @elseif(isset($data[$user->id]['previous']['net_sal_compare']) && isset($data[$user->id]['current']['net_sal_compare']))
                                @if($data[$user->id]['previous']['net_sal_compare'] - $data[$user->id]['current']['net_sal_compare'] == 0)
                                    <td>0</td>
                                @else
                                    <td class="text-center text-white bg-dark">{{$data[$user->id]['current']['net_sal_compare'] - $data[$user->id]['previous']['net_sal_compare'] }}</td>
                                @endif
                            @else
                                <td>0</td>
                            @endif
                        </tr>
                    @endif
                    @endforeach
                @else
                    <tr>
                        <td class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500,
			fixedHeader: true,
            dom: 'Bfrtip',
			"columnDefs": [ {
				"searchable": true,
                "orderable": true,
            } ],
            buttons: [
                'csv'
            ],
			scrollY: '60vh',
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            fixedColumns:   {
                leftColumns: 3,
            },
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        $('#selectid2').change(function(){
            var optionSelected = $("option:selected", this);
            optionValue = this.value;
            if (optionValue) {
                window.location = "/prep-salary/salary-sheet-comparison/"+optionValue;
            }
         });
        $('#selectid2').select2();
    });
    $('#selectid2').select2({
        placeholder: '{{$prepSalObj->month ? $prepSalObj->month->formatMonth() : 'Select Month'}}',
        allowClear:true
	});
</script>
@endsection
