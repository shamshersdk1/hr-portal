@extends('layouts.dashboard')
@section('title')
Transaction | 
@endsection
@section('main-content')

<section class="new-project-section">
	<div class="page-title-box">
		<div class="row align-items-center">
	            <div class="col-sm-8">
	                <h1 class="page-title">Add Transaction</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/dashboard">Dashboard</a></li>
			  			<li><a href="{{ url('transaction-summary') }}">Transaction Summary</a></li>
			  			<li class="active">Add</li>
	        		</ol>
	            </div>
	            <div class="col-sm-4 text-right m-t-10">
	                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
	            </div>
	        
	    </div>

	    @if(!empty($errors->all()))
	        <div class="alert alert-danger">
	            @foreach ($errors->all() as $error)
	                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	                <span>{{ $error }}</span><br/>
	              @endforeach
	        </div>
	    @endif
	    @if (session('message'))
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	            <span>{{ session('message') }}</span><br/>
	        </div>
	    @endif

		<form class="form-horizontal" method="post" action="/transaction-summary/store" enctype="multipart/form-data">
		@csrf
		
			<div class="panel panel-default">
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-md-12">
						  	<div class="form-group">
						    	<label for="user_id" class="col-sm-2 control-label">Employee Name</label>
						    	<div class="col-sm-5">
                                        @if($users && isset($users))
										<select id="selectid2" name="user_id"  style="width=35%;" placeholder= "Select user" required>
											<option value=""></option>
											@foreach($users as $x)
									        	<option value="{{$x->id}}" >{{$x->name}}</option>
										    @endforeach
										</select>
									@endif
                                </div>
						  	</div>
						  	<div class="form-group">
                                <label for="month_id" class="col-sm-2 control-label">Month - Year</label>
                                <div class="col-sm-5">
                                        @if(isset($monthList))
										<select id="selectid3" name="month_id"  style="width=35%;" placeholder= "Select month and year" required>
											<option value=""></option>
											@foreach($monthList as $x)
									        	<option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
										    @endforeach
										</select>
									@endif
                                </div>
                            </div>
						  	<div class="form-group">
						    	<label for="amount" class="col-sm-2 control-label">Amount</label>
						    	<div class="col-sm-5">
						    		<input type="number" id="amount" class="form-control" placeholder="0.00" name="amount" required/>
						    	</div>
							</div>
							  

							<div class="form-group">
                                <label for="reference_type" class="col-sm-2 control-label">Reference Type</label>
                                <div class="col-sm-5">
										<select id="selectid4" name="reference_type"  style="width=35%;" placeholder= "Select month and year" required>
											<option value=""></option>
											@foreach ($appraisalComponentsTypes as $appraisalComponentsType)
												<option value="App\Models\Appraisal\AppraisalComponentType" >{{$appraisalComponentsType->name}}</option>
											@endforeach
											@foreach ($appraisalBonuses as $appraisalBonus)
												<option value="App\Models\Appraisal\AppraisalBonus" >{{$appraisalBonus->description}}</option>
											@endforeach
											<option value="App\Models\Audited\AdhocPayments\AdhocPayment" >Adhoc Payment</option>
											<option value="App\Models\Admin\Bonus" >Bonus</option>
											<option value="App\Models\Audited\FoodDeduction\FoodDeduction" >Food Deduction</option>
											<option value="App\Models\Admin\VpfDeduction" >VPF Deduction</option>
											<option value="App\Models\Admin\Insurance\InsuranceDeduction" >Insurance Deduction</option>
											<option value="App\Models\LoanEmi" >Loan Deduction</option>											
										</select>
                                </div>
                            </div>
							  
                            <div class="form-group">
						    	<label for="reference_id" class="col-sm-2 control-label">Reference Id</label>
						    	<div class="col-sm-5">
						      		<input type="number" class="form-control" placeholder="0" name="reference_id"/>
						    	</div>
						  	</div>
					  	</div>
					</div>
				</div>
			</div>
  	  		<div class="text-center">
		  		<button type="reset" class="btn btn-default">Clear</button>
		  		<button type="submit" class="btn btn-success">Save</button>
		  	</div>
		</form>
	</div>
</section>
@endsection
@section('js')
@parent
	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
				format: 'YYYY-MM-DD'
			});
            $('#datetimepicker2').datetimepicker({
				format: 'YYYY-MM-DD'
            });
        });
		$('#selectid2').select2({
			placeholder: 'Select User',
			allowClear:true
		});
		$('#selectid3').select2({
			placeholder: 'Select Month-Year',
			allowClear:true
		});
		$('#selectid4').select2({
			placeholder: 'Select Reference Type',
			allowClear:true
		});
	</script> 
@endsection