@extends('layouts.dashboard')
@section('title')
Transaction Summary
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
            <div class="col-sm-6">
                <h1 class="page-title">Transaction Summary </h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li>Transaction Summary</a></li>
        		</ol>
            </div>
    </div>
    <div class="row">
            <div class="col-md-4">
                {{Form::open(['url' => '/transaction-summary/create', 'method' => 'get'])}}
                    {{Form::button('<i class="fa fa-plus fa-fw"></i>  Add Transaction', array('type' => 'submit', 'class' => 'btn btn-success'))}}
                {{Form::close()}}
            </div>
            <div class="col-md-8 text-right">
                    <div class="form-group m-b-10">
                        <div  style="width:100%">
                            @if(isset($monthArray))
                                <select id="selectid2" name="month"  placeholder= "{{$monthObj ? $monthObj->formatMonth() : "Select Month"}}">
                                    <option value=""></option>
                                    @foreach($monthArray as $x)
                                        <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>                
            </div> 
        </div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <div class="user-list-view">
        <div class="panel panel-default">
            <table class="table table-striped" id="summary-table">
                <thead>
                    <th class="text-center" width="5%">#</th>
                    <th class="" width="7%">Employee Id</th>
                    <th class="" width="7%">Name</th>
                    <th class="text-center">Total Deduction</th>
                    <th class="text-center">Total Earnings</th>
                    <th class="text-center">Amount Payable</th>
                </thead>
                @if(count($users) > 0)
                    @foreach($users as $user)
                        @if(isset($userArray) && isset($userArray[$user->id]) && isset($data[$user->id]) && ($data[$user->id]['total'] != 0))
                        <tr>
                            <td class="text-center"></td>
                            <td class="">{{ $user->employee_id}}</td>
                            <td class="">{{ $user->name}}</td>
                            <td class="text-center">{{ $data[$user->id]['debit'] ?? 0}}</td>
                            <td class="text-center">{{ $data[$user->id]['credit'] ?? 0}}</td>
                            <td class="text-center">{{ $data[$user->id]['total'] ?? 0}}</td>
                        </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">
                            No Records found
                        </td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#summary-table').DataTable( {
            pageLength:500, 
            fixedHeader: {
                header: true
            }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
<script>
	$('#selectid2').change(function(){
		var optionSelected = $("option:selected", this);
        optionValue = this.value;
        console.log(optionValue);
        if (optionValue) { 
            window.location = "/transaction-summary/"+optionValue; 
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$monthObj ? $monthObj->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>

@endsection
