
@extends('layouts.dashboard')
@section('title')
Dashboard
@endsection
@section('main-content')

    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>

    <div class="card border">
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-md-6 mt-3">
                    <h2>{{ date('D d F, Y ') }}</h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection