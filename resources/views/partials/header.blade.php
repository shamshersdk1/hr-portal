
<nav class="navbar fixed-top navbar-light ">
    <a class="navbar-brand admin-dashboard-logo" href="/dashboard">
        <img src="/images/logo-dark.png" style="width:140px; " alt="GeekyAnts Portal">
    </a>
    <a href="/logout" class="btn btn-primary">Log Out</a>
</nav>
        