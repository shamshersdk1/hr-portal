@extends('layouts.plane') 
@section('body')


@php
    $userLoggedIn = Auth::check();
    $user = Auth::user();
    $access = ($user->role == "admin" || $user->role == "account" || $user->role == "human-resources") ? true : false;
@endphp
<div id="wrapper">
    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a class="logo" href="/dashboard">
                <span class="logo-light">
                    <img src="/images/logo-dark.png" style="width:140px;" alt="GeekyAnts HR Portal"> 
                    <span class="badge badge-primary badge-pill">HR</span>
                </span>
                <span class="logo-sm">
                    <img src="/images/logo-icon.png" style="width:30px" alt="GeekyAnts HR Portal">
                </span>
            </a>
        </div>

        <nav class="navbar-custom">
            <ul class="navbar-right list-inline float-right mb-0">
                <!-- full screen -->
                <li class="notification-list list-inline-item d-none d-md-inline-block">
                    <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                        <i class="mdi mdi-arrow-expand-all noti-icon"></i>
                    </a>
                </li>
                <li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
                    <a href="/logout" class="nav-link waves-effect">Log out</a>
                </li>

            </ul>

            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left waves-effect">
                        <i class="fa fa-bars"></i>
                    </button>
                </li>
            </ul>
        </nav>

    </div>
    <!-- Top Bar End -->
    @include('layouts.sidebar')
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
                @yield('main-content')
                <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop
