<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                @foreach(config("sidebar") as $index => $value)
                    @if(is_array($value)) <!--If value is an array and contains roles_access key-->
                        @if(isset($value['section_name']) && isset($value['roles_access'])) <!--If array has section name and is a nested menu-->
                        @if(!empty(array_intersect(auth()->user()->getRoleNames()->toArray(), $value['roles_access'])) || (isset($value['permissions']) && auth()->user()->hasPermissionTo($value['permissions']) ))
                            <li> 
                                <a href="javascript:void(0);" class="waves-effect"> <!--Prints the section name and section icon, if present-->
                                    @if(isset($value['section_icon']))
                                        <i class="icon-{{$value['section_icon']}}"></i>
                                    @endif
                                    <span> {{$value['section_name']}}</span>
                                    <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span>
                                </a>
                                <ul class="submenu" aria-labelledby="">
                                    @foreach($value as $key => $val)
                                        @if($key != 'section_name' && $key != 'roles_access' && $key != 'section_icon')
                                            @if($key == $index || $index == 'nested')  <!--If it is a master-menu or if it is a group of non-related, non-nested routes-->
                                                <li>
                                                    <a href="/{{$key}}">
                                                        <span>{{$val}}</span>
                                                    </a>
                                                </li>
                                            @else <!--If nested routes-->
                                                <li>
                                                    <a href="/{{$index}}/{{$key}}">
                                                        <span>{{$val}}</span>
                                                    </a>
                                                </li>
                                            @endif
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                        @elseif(isset($value['roles_access'])) <!--If it is a single url but with section_name and roles_access-->
                            @if(!empty(array_intersect(auth()->user()->getRoleNames()->toArray(), $value['roles_access'])) || (isset($value['permissions']) && auth()->user()->hasPermissionTo($value['permissions']) )) <!--If user has access to this route-->
                                @if(isset($value['section_icon']))
                                    <li class="nav-item "><a href="/{{$index}}"><i class="icon-{{$value['section_icon']}}"></i><span>{{$value[$index]}}</span></a></li>
                                @else 
                                    <li class="nav-item "><a href="/{{$index}}"><span>{{$value[$index]}}</span></a></li>
                                @endif
                            @endif
                        @endif
                    @else <!--If simple url => Section Name -->
                        <li>
                            <a class="{{$index}}" href="/{{$index}}">
                                <i class="icon-graph"></i>
                                <span>{{$value}}</span>
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>
    </div>
</div>
@section('js')
@parent
<script>
    $(document).ready(function() {

        $('.fa-bars').click(function(){
            $('.sidenav').toggleClass('show');
            $('.main-container').toggleClass('slide');
        })
        $("#search").on("keyup", function () {
            if (this.value.length > 0) {   
              $("li").hide().filter(function () {
                return $(this).text().toLowerCase().indexOf($("#search").val().toLowerCase()) != -1;
              }).show(); 
            }  
            else { 
              $("li").show();
            }
        }); 

        var path = window.location.href; 
        $('ul a').each(function() {
            if (this.href === path) {
                $(this).addClass('active');
            }
        });
   });
</script> 
@endsection



