<html>
    <head>
        <link rel="stylesheet" href="{{ mix('assets/styles/dashboard.css') }}" />
        <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
        <title>@yield('title', config("app.name"))</title>
        @livewireStyles
     
    </head>
    <body>
        @yield('body')

        <script src="{{ mix('assets/scripts/admin.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>
        @livewireScripts
        @yield('js')
    </body>

</html>     

