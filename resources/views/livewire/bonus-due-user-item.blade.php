<tr wire:poll="$refresh">
    


    <td>
    {{date_in_view($item['date']) ?? '-'}}
    <br />

    </td>
        <!-- <td>
            <label class="label label-primary">%%bonus.type%%</label>

        </td> -->
    <td>
        
        @if($index == 'onsite')
            <small>Project: {{$item['project'] ?? $project ?? ''}}</small><br/>
    @endif
        <small>Rate: {{$item['rate'] ?? $rate}}</small><br/>
        <small>Details : {{$item['description'] ?? $details}}</small><br/>
        @if($index != 'onsite')
        <small>Annual Gross Salary: {{$item['annual_gross_salary'] ?? $annualGross}}</small>
        @endif
    </td>
    <td>{{$item['draft_amount'] ?? 0}}</td>
    <td>
    <div class="input-group input-group-sm">
        <span class="input-group-btn">
           
        <input @if($item['paid_at']) disabled @endif type='text' class="form-control" wire:model="amount">
            <!-- <button ng-disabled="bonus.loading || bonus.payment_approved"  ng-click="form.update(bonus)" class="btn btn-primary">Update
            </button> -->
        </span>
        <span  class="input-group-addon">
        <!-- <i ng-if="bonus.succuss" class="fa fa-check text-success"></i>
        <i ng-if="bonus.error" class="fa fa-close text-danger"></i> -->
        </span>
        </div>
    </div>
    </td>
    <td >
    @if($index == 'onsite' || $index == 'extra' || $index == 'additional' || $index == 'performance' || $index == 'techtalk' || $index == 'referral')
    <div>
        {{$item['approver']['name'] ?? ''}}
    </div>
    @endif
    </td>
    <td><small> {{datetime_in_view($item['created_at'])}}</small></td>
    @if(!$item['bonus_confirm'] && !$item['salary_process'])
    <td>Bonus Approved</td>
    @endif
    @if(!$item['salary_process'] && $item['bonus_confirmed'])
    <td >Bonus Confirmed</td>
    @endif
    @if($item['salary_process'])
    <td >Salary Process</td>
    @endif
    <td class="text-right">
        @if(!$item['paid_at'])
        <button class="btn btn-sm btn-primary" wire:click="approveBonus()">Approve for Payment</button>
        @else
        <span >Approved at {{datetime_in_view($item['paid_at'])}}</span>
        <button class="btn btn-sm btn-danger" wire:click="rejectBonus()">Rejct for Payment</button>
        @endif
    </td>
</tr>
