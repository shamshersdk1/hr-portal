<div>
   <div class="row m-b-10">
         <div class="col-sm-6">
         <h4 class="no-m-b m-t-5">{{$user->name}}</h4>
         </div>
   </div>
   @foreach($dueBonuses as $index => $bonus)

   <div class=" p-2 d-flex justify-center border mb-3 collapsed" data-toggle="collapse" data-target="#{{$index}}" style="justify-content: space-between;  position: -webkit-sticky; position: sticky; top:50px; z-index: 999; background: #fff;">
      <div>
         <div class="text-muted">Bonus Type</div>
         <div class="font-14 font-500 text-center text-primary">{{ucfirst($index)}}</div>
      </div>
      <div>
         <div class="text-muted">@if($index == 'extra') Hour(s) @else Day(s) @endif</div>
         <div class="font-14 font-500 text-center">@if($index == 'extra') {{$bonus['hours']}} @else {{$bonus['days'] ?? 0}} @endif</div>
      </div>
      <div>
         <div class="text-muted">Amount</div>
         <div class="font-14 font-500 text-center">{{$bonus['amount']}}</div>
      </div>
   </div>
   
   <div id="{{$index}}" class="panel-collapse collapse">
   <div>
         <button @if(!$bonus['bonuses']) style="display:none" @endif class="float-right btn btn-sm btn-primary" wire:click="approveAllBonuses('{{ $index }}')" onclick = "return confirm('Are you sure you want to approve all items?') ">Approve All</button>
   </div>
               <div class="panel-body" style="background: #D4DCE6;">
                  <table class="table table-condensed bg-white no-margin">
                     <tr>
                     <th class="text-left">Date</th>
                     <!-- <th width="10%">Requested For</th> -->
                     <th class="text-left">Description</th>
                     <th class="text-left">Draft Amount</th>
                     <th class="text-left">Amount</th>
                     <th class="text-left">Approved By</th>
                     <th class="text-left">Approved At</th>
                     <th class="text-left">Status</th>
                     <th class="text-right">Action</th>

                     </tr>
                     
                     @if($bonus['bonuses'])
                     @foreach($bonus['bonuses'] as $item)
               
                        @livewire('bonus-due-user-item',['item' => $item,'index' => $index,'bonus' => $bonus],key($item['id']))
                   
                     @endforeach
                     @else
                     
                     
                     <tr>
                     <td class="text-center" colspan="8">No Bonuses due this Month</td>
                     </tr>
                     @endif
                  </table>
               </div>
               </div>

   @endforeach
</div>