@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

        <div class="col-sm-6">
                <h1 class="page-title">Users Salary</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/">Dashboard</a></li>
        		  	<li><a href="{{ url('user-working-day') }}">User Salary</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>


            <div class="col-sm-6">
                <div class="flaot-right d-flex justify-content-end">


                @if(!$month->workingDaySetting || ($month->workingDaySetting &&  $month->workingDaySetting->value == 'open'))


                        <span>
                            <a href="/user-working-day/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->workingDaySetting && $month->workingDaySetting->value == 'locked')
                        <span>
                            <a href="/user-working-day/{{$month->id}}/status-month" class="btn btn-warning"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>

                @endif
            </div>



                  </div>

			</div>




	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    </div>
                @endforeach
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    </div>
    @if((!$month->workingDaySetting || ($month->workingDaySetting &&  $month->workingDaySetting->value == 'open')) && (count($userObjs->where('status','pending')) > 0 ))


    <div class="card">
        <div class="card-body pb-2">
            <div class="d-flex justify-content-start">

        {{ Form::open(array('url' => "user-working-day/$month->id/addUser")) }}
        {{ Form::hidden('month_id', $month->id ?? null) }}
        {{ Form::hidden('month_id', $month->id ?? null) }}
            <div class="d-flex align-items-center justify-content-between">
                <div class='d-flex align-items-center '>
                <div class=" d-flex align-items-center mr-1 ml-1">
                    <select id="selectid3" name="user_id"  placeholder= "Select an option" required>
                        <option value=""></option>
                            @foreach($allUsers as $x)
                                <option value="{{$x->id}}" >{{$x->name." | ".$x->employee_id}}</option>
                            @endforeach
                    </select>
                </div>


                    {{ Form::submit('Add User',['class' => 'btn btn-info','style' => 'display: block; margin: 0 auto']) }}

            </div>

            </div>

            </div>



        {{ Form::close() }}
        </div>
    </div>

</div>
        @endif






        <form method="post" action="{{$month->id}}" class="col-sm-12">
            {{ csrf_field() }}
            @method('PUT')
            {{ Form::hidden('month_id', $month->id) }}





        <div class="">
            <div class="row table-header-section mb-2">
                <div class="col-sm-6">
                    @if(isset($monthList))
                    <div class="d-flex align-items-center">
                        <label class="mb-0 pb-0 mr-1">Select Month:</label>

                        <select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                                <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                </div>
                <div class="col-sm-6 table-search d-flex justify-content-end align-items-center">
                    @if(!$month->workingDaySetting ||  $month->workingDaySetting->value == 'open')
                        <div class=" text-right ml-1">
                            <button type="submit" class="btn btn-success ">Save All</button>
                        </div>
                     @endif
                </div>
            </div>

                <div class="position-absolute">
                    @if($month && $month->workingDaySetting &&  $month->workingDaySetting->value == 'locked')
                    <div class="d-flex align-items-center">
                        <span class="btn btn-success btn-sm">Locked at {{ $month->workingDaySetting ? datetime_in_view($month->workingDaySetting->created_at) : ''}}</span>
                    </div>
                    @endif
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-outer-border no-margin user-list-table table-sm" id="deduction-table" width="100%">
                        <thead>
                            @if(count($userObjs->where('status','pending')) > 0 )
                                <th class="check text-center" width="7%">#</th>
                            @else
                                <th class="check text-center" width="7%"><input type="checkbox" id="flowcheckall" value="" />&nbsp;<small>Select All Users</small></th>
                            @endif
                            <th class="td-text">Employee Id</th>
                            <th class="td-text">Name</th>
                            <th class="td-text">Working Days</th>
                            <th class="td-text">Loss Of Pay</th>
                            <th class="td-text">Comment</th>
                            <th class="td-text">Status</th>
                            <th class="text-right">Action</th>
                        </thead>
                        @if(isset($userObjs) && count($userObjs) > 0)
                            @foreach($userObjs as $index=>$userObj)
                                <tr>
                                    @if(!isset($userObj['status']))
                                        <td class="check text-center"><input type="checkbox" id={{$userObj['id']}} name="options[{{$userObj['id']}}]" value={{$userObj['id']}} @if($userObj['is_active']) checked @endif />&nbsp;</td>
                                    @else
                                        <td>{{$index+1}}</td>
                                    @endif
                                    <td class="td-text">{{ $userObj['employee_id'] ?? $userObj['user']['employee_id']}}</td>
                                    <td class="text-left">{{ $userObj['name'] ?? $userObj['user']['name']}}</td>

                                    <td class="td-text">
                                        @if($month->workingDaySetting && $month->workingDaySetting->value == 'locked')
                                            {{$userObj['working_day']}}
                                        @else
                                            <input name="working_days[{{$userObj['id']}}]" type="number" value="{{$userObj['working_day']}}"/>
                                        @endif
                                    </td>
                                    <td class="td-text">
                                        @if($month->workingDaySetting && $month->workingDaySetting->value == 'locked')
                                            {{$userObj->lop}}
                                        @else
                                            <input name="lops[{{$userObj['id']}}]" type="number" value="{{$userObj['lop'] ?? 0}}"/>
                                        @endif
                                    </td>


                                    <td class="td-text">
                                        @if($month->workingDaySetting && $month->workingDaySetting->value == 'locked')
                                            {{$userObj->comment}}
                                        @else
                                            <input name="comment[{{$userObj['id']}}]" type="text" value="{{$userObj['comment'] ?? ''}}"/>
                                        @endif
                                    </td>

                                    <td class="td-text">
                                        @if($userObj['status'] == 'pending')
                                            <span class="badge badge-warning">Pending</span>
                                        @elseif($userObj['status'] == 'completed')
                                            <span class="badge badge-success">Completed</span>
                                        @else
                                            {{$userObj['status']}}
                                        @endif

                                    </td>


                                    @if(isset($userObj['status']))
                                    <td class="text-right">
                                        <div style="display:inline-block;" class="deleteform">
                                            <a href="/user-working-day/{{$userObj['id']}}/delete" class="btn btn-danger btn-sm" onclick = "return confirm('Are you sure you want to delete this item?') "> Delete</a>
                                        </div>
                                    </td>
                                    @else
                                    <td class="text-right">-</td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8" class="text-center">
                                    No Records found
                                </td>
                            </tr>

                        @endif
                    </table>
            </div>
    </form>
@endsection
@section('js')
@parent
<script>


$.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
    {
        return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
            return $('input', td).prop('checked') ? '0' : '1';
        } );
    }

    $(document).ready(function() {
        var t = $('#deduction-table').DataTable( {
            pageLength:500,
            fixedHeader: {
                header: true
            },
            columnDefs: [
            {
                targets: 0,
                orderDataType: 'dom-checkbox',
                orderable: false,
            }
            ]
        } );
        var data = t.column(0).data().sort().reverse();
        $(':checkbox').on('change', function(e) {
        var row = $(this).closest('tr');
        var hmc = row.find(':checkbox:checked').length;
        var kluj = parseInt(hmc);
        row.find('td.counter').text(kluj);
        table.row(row).invalidate('dom');
        });
    });

    $("#flowcheckall").click(function () {
        $('#deduction-table tbody input[type="checkbox"]').prop('checked', this.checked);
    });

   $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/user-working-day/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
    $('#selectid3').select2({
			placeholder: 'Select User',
			allowClear:true
		});

</script>

@endsection
