@extends('layouts.dashboard')
@section('title')
Loan Show
@endsection
@section('main-content')
	<div class="page-title-box">
		<div class="row align-items-center">

	        	<div class="col-sm-6">
	                <h1 class="page-title">Loans</h1>
	                <ol class="breadcrumb">
	        		  	<li><a href="/dashboard">Dashboard</a></li>
				  		<li><a href="{{ url('loans') }}">Loans</a></li>
				  		<li class="active">{{$loan->id}}</li>
		        	</ol>
				</div>



				<div class="col-sm-6">

					@if($file ?? null)
						<div class="float-right">
						<a href="{{$file->url()}}" target="_blank" class="btn btn-info crud-btn btn-sm">View Agreement</a>
						</div>
					@endif

					@if( $loan->status == "in_progress")
						<div class="float-right pr-2">
							<a href="{{url('loans/'.$loan->id.'/custom-payment')}}" class="btn btn-warning crud-btn btn-sm">Custom Payment</a>
						</div>
					@endif

					<div class="float-right pr-2">
						<form action="/loans/{{$loan->id}}/complete" method="post" enctype="multipart/form-data" class="float-right">
							@csrf
							{{method_field('PUT')}}
							@if ( $loan->status != 'completed'  && $remainingAmount<=0)
								<div class="upload-btn-wrapper">
									<input id="agreement" type="file" name="file" required >
									<button type="submit" name="approve" value="approve" class="btn btn-success crud-btn btn-sm"> Upload & Complete Loan</button>
								</div>
						</form>
					</div>

                    @else
                        @if($loanCompleteFile)
                        <h6>Closer Agreement </h6>
                            <a href="{{$loanCompleteFile->url()}}" target="_blank" class="btn btn-success btn-sm">View Loan Closer Agreement</a>
                        @endif
                    @endif
				</div>
		</div>

	</div>

	<div class=" p-2 d-flex justify-center border mb-4 flex-column" style="justify-content: space-between;  position: -webkit-sticky; position: sticky; top:50px; z-index: 999; background: #fff;">
		<div id="accordion" class="accordion">

		<div class="d-flex justify-content-between header mr-4 collapsed" data-toggle="collapse" href="#collapseOne">
			<div >
				<div class="text-muted">Loan Id </div>
				<div class="font-14 font-500 text-center text-primary">{{$loan->user->id}}</div>
			</div>
			<div >
				<div class="text-muted">Employee Name</div>
				<div class="font-14 font-500 text-center text-primary">{{$loan->user->name}} ({{$loan->user->employee_id}})  </div>
			</div>
			<div>
				<div class="text-muted">Application Date</div>
				<div class="font-14 font-500 text-center">{{date_in_view($loan->created_at)}}</div>
			</div>
			{{-- <div>
				<div class="text-muted">Loan Against </div>
				<div class="font-14 font-500 text-center">{{$loan->appraisal_bonus_id === NULL ? 'Salary' : $loan->appraisalBonus->appraisalBonusType->description}}</div>
			</div> --}}
			<div>
				<div class="text-muted">EMI </div>
				<div class="font-14 font-500 text-center">
					{{$loan->emi}}
				</div>
			</div>
			
			<div>
				<div class="text-muted">Loan Amount</div>
				<div class="font-14 font-500 text-center">{{ $loan->amount }}</div>
			</div>
		
			<div>
				<div class="text-muted">Loan Statu
				</div>
				<div class="font-16 font-500 text-center">
					<span class="" >
						@if($loan->status != 'completed')
							<span class="badge badge-primary ">Running</span>
						@elseif($loan->status == 'completed')
							<span class="badge badge-success ">Completed</span>
						@endif
					</span>
						
			</div>
			</div>


		</div>
		<div  id="collapseOne" class="card-body collapse flex-column mt-4 p-0" data-parent="#accordion">
			<div class="row ">
				<div class="col-sm-4">
					<div>
						<label class="text-muted">EMI Start Date: </label> <span class="font-14 font-500">{{date_in_view($loan->emi_start_date)}}</span>
						
					</div>
					<div>
						<label class="text-muted">Outstanding Amount: </label> <span class="font-14 font-500">{{$remainingAmount}}</span>
						
					</div>
			</div>
			<div class="col-sm-8">
				<label class="m-0 p-0">Description: </label><br/>
				<span>{{$loan->description}}</span>
			</div>
		</div>
		</div>
	   
	</div>
	</div>

		@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
		@endif

		<div class="row">
			<div class="col-sm-12">
				<h4 class="m-0 p-0 mb-1 header-title text-capitalize">Loan EMI Transaction</h4>

				<div class="card border">
						<div class="table-responsive">
						<table class="table  no-margin user-list-table">
							<thead >
								<th class="td-text">#</th>
								<th class="td-text">Paid On</th>
								<th class="td-text">Amount</th>
								<th class="td-text">Balance</th>
								<th class="td-text">Status</th>
								<th class="td-text">Comment</th>
								<th class="td-text">Created At</th>
							</thead>
							<tbody>
							@if(count($loanTransactions) > 0)
								@foreach($loanTransactions as $index => $loanTransaction)
								<tr @if($loanTransaction->is_manual_payment) class="table-danger" @endif>
									<td class="td-text">{{$index+1}}</td>
									<td class="td-text">{{date_in_view($loanTransaction->paid_on)??'Not Yet Paid'}}</td>
									<td class="td-text">{{$loanTransaction->amount?? '---'}}</td>
									<td class="td-text">{{$loanTransaction->balance?? '---'}}</td>
									<td>
									@if($loanTransaction->status == 'paid')
										<span class="badge badge-success ">Paid</span>
									@elseif($loanTransaction->status == 'pending')
										<span class="badge badge-warning ">Pending</span>
									@elseif($loanTransaction->status == 'processing')
										<span class="badge badge-primary ">Processing</span>
									@elseif($loanTransaction->status == 'rejected')
										<span class="badge badge-danger ">Rejected</span>
									@endif
									@if($loanTransaction->is_manual_payment)
                                    <span class="badge badge-danger">Custom Paid</span>
                              		@endif
									</td>
									<td class="td-text">{{$loanTransaction->comment?? '---'}}</td>
									<td class="td-text">{{datetime_in_view($loanTransaction->created_at)}}</td>
								</tr>
								@endforeach
							@else
								<tr>
									<td colspan="9">No results found.</td>
								</tr>
							@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	@endsection
	@section('js')
@parent
<script>

	$(function(){
		$(".deleteform").submit(function(event){
			return confirm('Are you sure?');
		});


	});

</script>
@endsection

