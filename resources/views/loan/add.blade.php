@extends('layouts.dashboard')
@section('title')
Add Loan
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
            <div class="col-sm-8">
                <h1 class="page-title">Apply Loan</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="/loan">Loan</a></li>
                    <li class="active">Apply</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-dark"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
    	@if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ $error }}</span><br/>
                  @endforeach
            </div>
            
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
        {{ Form::open(array('url' => 'loans', 'method' => 'post')) }}
           @csrf
            <input type="hidden" name="from_admin" value="1">
            <div class="card border" >
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class='row'>
                            <label for="name" class="col-sm-4 text-right" >Loanee:</label>
                            <div class="col-sm-8  form-group">
                                <select id="selectid3" name="user_id" class="form-control" style="width=35%;" placeholder= "Select an option" required>
                                    <option value=""></option>
                                        @foreach($users as $x)
                                            <option value="{{$x->id}}" >{{$x->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                            @if(!empty($users))

                            @endif
                            </div>
                            <div class="row">
                            <label for="name" class="col-sm-4 text-right">EMI:</label>
                                <div class="col-sm-8 form-group">
                                    <input type="text" id="emi" name="emi" class="form-control" placeholder="0.00" required value="{{ old('emi') }}">
                                    <span id="emi-error" style="color:red;display:none"><i class="fa fa-exclamation-circle"></i> Can not be greater than loan amount</span>
                                </div>
                            </div>
                            <div class="row">
                                <label for="application_date" class="col-sm-4 text-right">Application Date:</label>

                                <div class="col-sm-8  form-group">
                                    {{ Form::date('application_date', null, ['class' => 'form-control','placeholder' => 'Application Date']) }}
                                </div>
                            </div>
                            <div class="row">
                                <label for="name" class="col-sm-4 text-right">Reason:</label>
                                <div class="col-sm-8 form-group">
                                    <textarea name="description" class="form-control" placeholder="Enter the description" rows="5">@if(!empty(old('description'))){{old('description')}}@endif</textarea>
                                </div>
                            </div>


                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <label for="name" class="col-sm-4 text-right">Amount:</label>
                                <div class="col-sm-8  form-group">
                                    <input type="text" id="amount" name="amount" class="form-control" placeholder="0.00" required value="{{ old('amount') }}">
                                    <div class="request-loan-note">
                                        Maximum loan amount cannot exceed more than 1 Lakh
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <label for="name" class="col-sm-4 text-right">Loan Type:</label>
                                <div class="col-sm-8  form-group">
                                    <select name="annual_bonus_type_id" id="loan-type-selector" class="form-control" required>
                                        @foreach($loan_types as $loan_type)
                                            <option value="{{$loan_type->id}}">{{$loan_type->description}}</option>
                                        @endforeach
                                        <option value="">Against Salary</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <label for="emi_start_date" class="col-sm-4 text-right">Emi Start Date:</label>
                                <div class="col-sm-8  form-group">
                                    {{ Form::date('emi_start_date', null, ['class' => 'form-control','placeholder' => 'Emi Start Date']) }}
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="reset" class="btn btn-secondary"> Clear</button>
                <button type="submit" class="btn btn-success" id="submit-btn">Save</button>
            </div>
            {{ Form::close() }}
        </div>
	</div>
</div>
<script>
    $(function(){
        $('.payment_date_datepicker').datetimepicker({
			format:'DD-MM-YYYY',
			useCurrent:false,
		});
        $('.datetimepicker').datetimepicker({
			format:'DD-MM-YYYY',
			useCurrent:false,
		});
        var old_type = "{{old('type')}}";
        $("#loan-type-selector option[value='"+old_type+"']").prop('selected', true);
    });
        $('#datetimepicker2').datetimepicker({
            format:'YYYY-MM-DD',
            useCurrent: false,
        });
        $('#datetimepicker1').datetimepicker({
            format:'YYYY-MM-DD',
            useCurrent: false,
            maxDate: new Date()
       });
</script>
@endsection
