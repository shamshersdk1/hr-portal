@extends('layouts.dashboard')
@section('title')
Loan Request
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-10">
                <h1 class="page-title">Loan Request</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="{{ url('loans') }}">Loans</a></li>
                    <li class="active">Loan Request</li>
                </ol>
            </div>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            <div class="card border" >
                <div class="card-body">
                <table class="table table-striped table-sm table-outer-border" id="all-loans-table">
                    <thead >
                        <tr>
	            		<th>#</th>
                        <th width="20%">Name of loanee</th>
                        <th>Total Amount</th>
                        <th>Type</th>
                        <th>Emi</th>
                        <th>Emi Start Date</th>
                        <th>Application Date</th>
                        <th>Status</th>
                        <th>Created On</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($loans) > 0)
                            @foreach($loans as $index => $loan)
                                <tr>
                                    <td class="td-text"><a>{{$index+1}}</a></td>
                                    <td class="td-text">{{$loan->user->name}}</td>
                                    <td class="td-text"><a>{{$loan->amount}}</a></td>
                                    <td>{{($loan->appraisal_bonus_id === null ) ? "Against Salary" : $loan->appraisalBonus->appraisalBonusType->description ?? ''}}</td>
                                    <td>{{$loan->emi}}</td>
                                    <td>{{$loan->emi_start_date}}</td>
                                    <td>{{$loan->application_date}}</td>
                                    <td>{{$loan->status}}</td>
                                    <td class="td-text"><a>{{datetime_in_view($loan->application_date)}}</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">No results found.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                </div>
            </div>
        </div>
	</div>
</div>
@endsection
@section('js')
@parent
<script>
        $(document).ready(function() {
            var t = $('#all-loans-table').DataTable({
                fixedHeader: true,
                scrollY:'70vh',
                paging: false,
            });
        });
</script>
@endsection

