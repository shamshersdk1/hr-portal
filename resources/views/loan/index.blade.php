@extends('layouts.dashboard')
@section('title')
Loans
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-8">
                <h1 class="page-title">Loans</h1>
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li><a href="{{ url('loans') }}">Loans</a></li>
                </ol>
            </div>
            <div class="col-sm-4 text-right">
                <a href="/loans/request" class="btn btn-warning"><i class="fa fa-eye fa-fw"></i> Loan Request</a>
            </div>

    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <span>{{ $error }}</span><br/>
                    @endforeach
                </div>
            @endif
            @if (session('message'))
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span>{{ session('message') }}</span><br/>
                </div>
            @endif
            <div class="row table-header-section mt-4">
                <div class="col-sm-6">

                
                </div>
                  <div class="col-sm-6 table-search">
                  </div>
                </div>
            <div class="table-responsive mt-2 " >
                <table class="table table-striped table-sm table-outer-border" id="all-loans-table">
                    <thead >
                        <tr>
	            		<th>#</th>
                        <th width="20%">Name of loanee</th>
                        <th>Total Amount</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($loans) > 0)
                            @foreach($loans as $index => $loan)
                                <tr>
                                    <td class="td-text"><a>{{$index+1}}</a></td>
                                    <td class="td-text"><a href="/admin/loans/{{$loan->id}}"> {{$loan->user->name}}</a></td>
                                    <td class="td-text"><a>{{$loan->amount}}</a></td>

                                    <td>{{($loan->appraisal_bonus_id === null ) ? "Against Salary" : $loan->appraisalBonus->appraisalBonusType->description ?? ''}}</td>
                                    <td>
                                        @if($loan->status != 'completed')
                                            <span class="label label-primary custom-label">Running</span>
                                        @elseif($loan->status == 'completed')
                                            <span class="label label-success custom-label">Completed</span>
                                        @endif
                                    </td>
                                    <td class="td-text"><a>{{datetime_in_view($loan->application_date)}}</a></td>
                                    <td class="text-right">


                                        <a href="/loans/{{$loan->id}}" class="btn btn-primary crud-btn btn-sm">View</a>
                                        @if( $loan->remaining == 0 && $loan->status != 'completed' )
                                            <a href="/loans/{{$loan->id}}/complete" class="btn btn-info crude-btn btn-sm">Complete</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">No results found.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
@endsection
@section('js')
@parent
<script>
  
		$(function(){
            $(".deleteform").submit(function(event){
                return confirm('Are you sure?');
            });
          
          
        });
        $(document).ready(function() {
            var t = $('#all-loans-table').DataTable({
                fixedHeader: true,
                scrollY:'70vh',
                paging: false,
               
        
            });
            $('.table-header-section .table-search').append($('.dataTables_filter'))


        });
</script>
@endsection

