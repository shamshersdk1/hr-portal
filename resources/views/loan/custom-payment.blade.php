@extends('layouts.dashboard')
@section('title')
Custom Payment | @parent
@endsection
@section('main-content')

<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-8">
            <h1 class="page-title">Custom Payment</h1>
            <ol class="breadcrumb">
                <li><a href="/dashboard">Dashboard</a></li>
                <li><a href="{{ url('loans') }}">Loans</a></li>
                <li><a href="{{ url('loans/'.$loan->id) }}">{{$loan->id}}</a></li>
                <li class="active">Custom Payment</li>
            </ol>
        </div>
        <div class="col-sm-4 text-right m-t-10">
            <button type="button" onclick="window.history.back();" class="btn btn-dark"><i class="fa fa-caret-left fa-fw"></i> Back</button>
        </div>
	</div>
</div>

<div class="row">
    <div class="col-md-12">
        @if(!empty($errors->all()))
            <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                @foreach ($errors->all() as $error)
                    <span>{{ $error }}</span><br/>
                    @endforeach
            </div>
        @endif
        @if (session('message'))
            <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <span>{{ session('message') }}</span><br/>
            </div>
        @endif
    </div>
</div>

<div class=" p-2 d-flex justify-center border mb-3" style="justify-content: space-between;  position: -webkit-sticky; position: sticky; top:50px; z-index: 999; background: #fff;">
    <div>
        <div class="text-muted">Employee Name</div>
        <div class="font-14 font-500 text-center text-primary">{{$loan->user->name}}</div>
    </div>
    <div>
        <div class="text-muted">Employee Id</div>
        <div class="font-14 font-500 text-center">{{$loan->user->employee_id}}</div>

    </div>
    <div>
        <div class="text-muted">Loan Amount</div>
        <div class="font-14 font-500 text-center">{{$loan->amount}}</div>
    </div>
    <div>
        <div class="text-muted">EMI</div>
        <div class="font-14 font-500 text-center">{{$loan->emi}}</div>
    </div>
    <div>
        <div class="text-muted">Application Date</div>
        <div class="font-14 font-500 text-center">
            @if($loan->created_at)
              {{date_in_view($loan->created_at)}}<br>
            @endif
            @if($loan->emi_start_date)
               {{date_in_view($loan->emi_start_date)}}
            @endif
        </div>
    </div>
    <div>
        <div class="text-muted">LoanStatus
        </div>
        <div class="font-12 font-500 text-center">
            @if($loan->status != 'completed')
                 <label class="badge badge-primary p-1">Running</label>
            @elseif($loan->status == 'completed')
                <label class="badge badge-danger p-1">Completed</label>
            @endif
                  
       </div>
    </div>
   
</div>
<div class="row">
    <div class="col-md-4">
        
        <h4 class="m-0 p-0 mb-1 header-title text-capitalize">Add Custom Payment</h4>
        <div class="card border mb-2">
            <div class="card-body pb-2">
                    {{ Form::open(array('url' => "loans/".$loan->id."/custom-payment")) }}
                        {{Form::hidden('loan_id', $loan->id)}}
                        <div class="form-group row">
                            {{ Form::Label('Amount', 'Amount',array('class' =>'col-md-10  ' )) }}
                            <div class="col-md-10  " >
                                {{ Form::text('amount',null,['class'=>'form-control']) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::Label('Date', 'Date',array('class' =>'col-md-10 ' )) }}
                            <div class="col-md-10" >
                                {{Form::date("date", null, ['class' => 'form-control'])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            {{ Form::Label('Comment', 'Comment',array('class' =>'col-md-10' )) }}
                            <div class="col-md-10" >
                                {{ Form::textarea('comment',null,['class'=>'form-control', 'rows' => 3] ) }}
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                {{ Form::submit('Save',['class' => 'btn btn-success ']) }}
                            </div>
                        </div>
                    {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <h4 class="m-0 p-0 mb-1 header-title text-capitalize">Payment Detail</h4>


        <div class="card border ">
            <table class="table  no-margin user-list-table">
                <thead >
                    <th class="td-text">#</th>
                    <th class="td-text">Paid On</th>
                    <th class="td-text">Amount</th>
                    <th class="td-text">Balance</th>
                    <th class="td-text">Status</th>
                    <th class="td-text">Comment</th>
                    <th class="td-text">Created At</th>
                </thead>
                <tbody>
                @if(count($loanTransactions) > 0)
                    @foreach($loanTransactions as $index => $loanTransaction)
                        <tr @if($loanTransaction->is_manual_payment) class="table-danger" @endif>
                            <td class="td-text">{{$index+1}}</td>
                            <td class="td-text">{{date_in_view($loanTransaction->paid_on)??'Not Yet Paid'}}</td>
                            <td class="td-text">{{$loanTransaction->amount?? '---'}}</td>
                            <td class="td-text">{{$loanTransaction->balance?? '---'}}</td>
                            <td>
                                @if($loanTransaction->status == 'paid')
                                    <span class="badge badge-success">Paid</span>
                                @elseif($loanTransaction->status == 'pending')
                                    <span class="badge badge-warning">Pending</span>
                                @elseif($loanTransaction->status == 'processing')
                                    <span class="badge badge-primary">Processing</span>
                                @elseif($loanTransaction->status == 'rejected')
                                    <span class="badge badge-danger">Rejected</span>
                                @endif
                                @if($loanTransaction->is_manual_payment)
                                    <span class="badge badge-danger">Custom Paid</span>
                                @endif
                            </td>
                            <td class="td-text">{{$loanTransaction->comment?? '---'}}</td>
                            <td class="td-text">{{datetime_in_view($loanTransaction->created_at)}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9">No results found.</td>
                    </tr>
                @endif
                </tbody>
            </table>        
        </div>
    </div>
</div>
</div>
@endsection
