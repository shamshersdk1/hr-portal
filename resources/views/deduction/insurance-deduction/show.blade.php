@extends('layouts.dashboard')
@section('title')
Insurance Deduction | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
                <h1 class="page-title">Insurance Deduction</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        		  	<li class="breadcrumb-item"><a href="{{ url('deduction/insurance-deduction') }}">Insurance Deduction</a></li>
        		  	<li class="breadcrumb-item active">{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->insuranceSetting || ($month->insuranceSetting &&  $month->insuranceSetting->value != 'locked'))
                    <div class="text-right m-t-10">
                        <span>
                            {{ Form::open(['url' => 'deduction/insurance-deduction/regenerate', 'method' => 'post']) }}
                            {{ Form::hidden('month_id', $month->id) }}
							{{ Form::submit('Regenerate',['class' => 'btn btn-primary']) }}
							{{ Form::close() }}
                        </span>
                    </div>
                    <div class="text-right m-t-10">
                        <span>
                            <a href="/deduction/insurance-deduction/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->insuranceSetting && $month->insuranceSetting->value == 'locked')
                    <div class="text-right">
                        <span>
                            <a href="/deduction/insurance-deduction/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>

                @endif
                 @if(isset($monthList))
                    <div class="text-right">
                        <select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                                <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
			</div>

    </div>
    @if($month->insuranceSetting &&  $month->insuranceSetting->value == 'locked')
        <div class="pull pull-right">
            <span class="badge badge-info">Locked at {{datetime_in_view($month->insuranceSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>


        @if(!$month->insuranceSetting ||  $month->insuranceSetting->value != 'locked')

        @endif
        <div class="user-list-view">
            <div class="card">
                <table class="table table-striped table-sm" id="insurance-table">
                    <thead>
                    <tr>
                        <th class="text-left">#</th>
                        <th class="text-left">Emp Id</th>
                        <th class="text-left">User Name</th>
                        <th class="text-left">Status</th>
                        <th class="text-left">Insurance Name</th>
                        <th class="text-left">Coverage</th>
                        <th class="text-left">Premium (p/m)</th>
                        <th class="text-right">Action</th>
                    </tr>
                    </thead>
                    @if(count($insuranceDeductions) > 0)
                        @foreach($insuranceDeductions as $index => $insuranceDeduction)
                            @if($insuranceDeduction->user->is_active == 0)
                                <tr style="background-color:red">
                            @else
                                <tr >
                            @endif
                                <td class="text-left"></td>
                                <td class="text-left">{{ $insuranceDeduction->user->employee_id ?? ''}}</td>
                                <td class="text-left">{{ $insuranceDeduction->user->name ?? ''}}</td>
                                <td class="text-left">
                                    @if($insuranceDeduction->user)
                                        {{$insuranceDeduction->user->is_active ? "Active" : 'DeActive'}}
                                    @endif
                                </td>
                                <td class="text-left">{{$insuranceDeduction->insurance->name}}</td>
                                <td class="text-left">{{$insuranceDeduction->insurance->coverage_amount}}</td>
                                <td class="text-left">{{$insuranceDeduction->amount}}</td>
                                <td class="text-right">
                                    @if(!$month->insuranceSetting || $month->insuranceSetting->value != 'locked')
                                    <div style="display:inline-block;" class="deleteform">
                                    {{ Form::open(['url' => '/deduction/insurance-deduction/'.$insuranceDeduction->id, 'method' => 'delete']) }}
                                    {{ Form::submit('Delete',['class' => 'btn btn-danger','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                    {{ Form::close() }}
                                    </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                                <td class="text-center" colspan="5">No Records found</td>
                        </tr>
                    @endif
                </table>
            </div>
        </div>

</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#insurance-table').DataTable( {
            pageLength:500,
            scrollY:        true,
            scrollCollapse: true,
            paging:         false,
            fixedHeader: true,
            order: [[ 2, 'desc' ]],
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
$('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/deduction/insurance-deduction/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
    </script>
@endsection
