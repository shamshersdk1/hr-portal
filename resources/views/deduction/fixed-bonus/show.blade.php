@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-centers">
            <div class="col-sm-6">
                <h1 class="page-title">Fixed Bonuses</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li><a href="{{ url('deduction/fixed-bonus') }}">Fixed Bonuses</a></li>
        		  	<li>{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6 text-right">

                @if(!$month->fixedBonusSetting || ($month->fixedBonusSetting &&  $month->fixedBonusSetting->value == 'open'))

                        <span>
                            {{ Form::open(['url' => 'deduction/fixed-bonus/regenerate', 'method' => 'post']) }}
                            {{ Form::hidden('month_id', $month->id) }}
							{{ Form::submit('Regenerate',['class' => 'btn btn-primary']) }}
							{{ Form::close() }}
                        </span>

                        <span>
                            <a href="/deduction/fixed-bonus/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>

                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->fixedBonusSetting && $month->fixedBonusSetting->value == 'locked')

                        <span>
                            <a href="/deduction/fixed-bonus/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>

                @endif
                @if(isset($monthList))
                        <select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                                <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                    @endif
			</div>
        

    </div>
</div>

	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

  
        <div class="card border">
            <div class="card-body">
                <form name="editForm" method="post" action="/deduction/fixed-bonus/{{$month->id}}">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    @if(!$month->fixedBonusSetting ||  $month->fixedBonusSetting->value == 'open')
                        <div class="position-absolute" style="z-index:99">
                            <div class="pull-right m-t-1 ">
                                <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                            </div>
                        </div>
                    @endif
                @if($month && $month->fixedBonusSetting &&  $month->fixedBonusSetting->value == 'locked')
                <div class="position-absolute">
                    <span class="btn btn-primary btn-sm">Locked at {{ $month->fixedBonusSetting ? datetime_in_view($month->fixedBonusSetting->created_at) : ''}}</span>
                </div>
            @endif
                <table class="table table-striped table-sm table-outer-border" id="deduction-table">
                    <thead>
                        <tr>
                            <th class="td-text">#</th>
                            <th class="td-text">Employee Id</th>
                            <th class="td-text">Name</th>
                            @foreach($appraisalBonusTypes as $appraisalBonusType)
                            <th class="td-text">{{$appraisalBonusType->description}}</th>
                            @endforeach
                            <th class="td-text">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        @if(count($monthlyDeductions) > 0)
                            @foreach($monthlyDeductions as $index => $monthlyDeduction)
                                @if((count($monthlyDeduction->monthlyDeductionComponents)>0)?true:false)
                                <tr>
                                    <td class="td-text">{{ $index + 1}}</td>
                                    <td class="td-text">{{ $monthlyDeduction->user->employee_id}}</td>
                                    <td class="td-text">{{ $monthlyDeduction->user->name}}</td>
                                    @foreach($appraisalBonusTypes as $appraisalBonusType)
                                    <td class="td-text">
                                        @if($monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first() ? $monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first()->value : null)
                                            @if($month && $month->fixedBonusSetting &&  $month->fixedBonusSetting->value == 'locked')
                                            <label>{{$monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first()->value}}</label>
                                            @else
                                            <input name="new_amount[{{$monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first()->id}}][{{$appraisalBonusType->code}}]" type="number" value="{{$monthlyDeduction->monthlyDeductionComponents->where('key',$appraisalBonusType->code)->first()->value}}"/>
                                            @endif
                                        @endif
                                    </td>
                                    @endforeach

                                    <td class="pull-right">
                                        @if(!$month->fixedBonusSetting || ($month->fixedBonusSetting &&  $month->fixedBonusSetting->value == 'open'))
                                            <a href="/deduction/fixed-bonus/{{$monthlyDeduction->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Delete Fixed Bonus?')"><i class="fa fa-trash fa-fw"></i>Delete</a>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6" class="td-text">
                                    No Records found
                                </td>
                            </tr>

                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>
@endsection
@section('js')
@parent
<script>
     $(document).ready(function() {
        $tableHeight = $( window ).height();
        var t = $('#deduction-table').DataTable( {
            "bPaginate": false,
            scrollY: $tableHeight - 200,
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            "bInfo" : false,
            order: [[ 0, 'asc' ]],
            pageLength:500,
            columnDefs: [ {
                "searchable": true,
                "orderable": true,
                "targets": 0
            } ],
            fixedColumns:   {
            leftColumns: 2,
            rightColumns:1
        }
        } );
    });
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/deduction/fixed-bonus/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>
@endsection
