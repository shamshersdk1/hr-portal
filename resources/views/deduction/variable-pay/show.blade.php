@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <h1 class="page-title">Variable Pay</h1>
            <ol class="breadcrumb">
                <li><a href="/dashboard">Dashboard</a></li>
                <li><a href="{{ url('deduction/variable-pay') }}">Variable Pay</a></li>
                <li>{{$month->formatMonth()}}</li>
            </ol>
        </div>
    </div>
</div>

<div class="card border mb-3">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">

                @if($month && $month->variablePaySetting &&  $month->variablePaySetting->value == 'locked')
                    <span class="badge badge-primary">Locked at {{ $month->variablePaySetting ? datetime_in_view($month->variablePaySetting->created_at) : ''}}</span>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        @if(!empty($errors->all()))
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <span>{{ $error }}</span><br/>
                                @endforeach
                            </div>
                            @endif
                                @if (session('message'))
                                    <div class="alert alert-success">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <span>{{ session('message') }}</span><br/>
                                    </div>
                            @endif
                    </div>
                </div>


                @if(!$month->variablePaySetting || ($month->variablePaySetting &&  $month->variablePaySetting->value == 'open'))
                <span class="mr-2">
                    {{ Form::open(['url' => 'deduction/variable-pay/regenerate', 'method' => 'post']) }}
                    {{ Form::hidden('month_id', $month->id) }}
                    {{ Form::submit('Regenerate',['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </span>
                <span>
                    <a href="/deduction/variable-pay/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                </span>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->variablePaySetting && $month->variablePaySetting->value == 'locked')
                    <span>
                    <a href="/deduction/variable-pay/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                    </span>
                @endif



        <form name="editForm" method="post" action="/deduction/variable-pay/{{$month->id}}" class="">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
         <div class="d-flex align-items-center justify-content-between">
            <label class="label label-danger">Note : Variable pay bonuses is for the previous month.</label>
            <div class="d-flex align-items-center ">
                @if(isset($monthList))
                    <div class="mr-2">
                        <select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                            <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                    </div>
                 @endif



                @if(!$month->variablePaySetting ||  $month->variablePaySetting->value == 'open')
                    <div class="float-right ml-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                @endif
            </div>
         </div>

            <div class="user-list-view">
                <div class="card">
                        <table class="table table-striped table-sm" id="deduction-table">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Employee Id</th>
                                    <th class="text-center">Name</th>
                                    @foreach($appraisalBonusTypes as $appraisalBonusType)
                                    <th class="text-center">{{$appraisalBonusType->description}}</th>
                                    @endforeach
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($lastMonthPays) > 0)
                                @foreach($lastMonthPays as $index => $lastMonthPay)
                                @if((count($lastMonthPay->lastMonthPayComponents)>0)?true:false)
                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-center">{{ $lastMonthPay->user->employee_id}}</td>
                                    <td class="text-center">{{ $lastMonthPay->user->name}}</td>
                                        @foreach($appraisalBonusTypes as $appraisalBonusType)
                                    <td class="text-center">
                                        @if($lastMonthPay->lastMonthPayComponents->where('key',$appraisalBonusType->code)->first() ?$lastMonthPay->lastMonthPayComponents->where('key',$appraisalBonusType->code)->first()->value : null)
                                        @if($month && $month->variablePaySetting &&  $month->variablePaySetting->value == 'locked')
                                        <label>{{$lastMonthPay->lastMonthPayComponents->where('key',$appraisalBonusType->code)->first()->value}}</label>
                                        @else
                                        <input name="new_amount[{{$lastMonthPay->lastMonthPayComponents->where('key',$appraisalBonusType->code)->first()->id}}][{{$appraisalBonusType->code}}]" type="number" value="{{$lastMonthPay->lastMonthPayComponents->where('key',$appraisalBonusType->code)->first()->value}}"/>
                                    @endif
                                    @endif
                                    </td>
                                @endforeach

                                    <td class="pull-right">
                                        @if(!$month->variablePaySetting || ($month->variablePaySetting &&  $month->variablePaySetting->value == 'open'))
                                        <a href="/deduction/variable-pay/{{$month->id}}/{{$lastMonthPay->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Delete Variable Pay?')">Delete</a>
                                        @endif
                                    </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                    @else
                                    <tr>
                                    <td colspan="6" class="text-center">
                                    No Records found
                                    </td>
                                </tr>

                                @endif
                            </tbody>
                        </table>
                </div>
            </div>
        </form>
</div>
        </div>

@endsection
@section('js')
@parent
<script>
     $(document).ready(function() {
        $tableHeight = $( window ).height();
        var t = $('#deduction-table').DataTable( {
            "bPaginate": false,
            scrollY: $tableHeight - 200,
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            "bInfo" : false,
            order: [[ 0, 'asc' ]],
            pageLength:500,
            columnDefs: [ {
                "searchable": true,
                "orderable": true,
                "targets": 0
            } ],
            fixedColumns:   {
            leftColumns: 2
        }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/deduction/variable-pay/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>
@endsection
