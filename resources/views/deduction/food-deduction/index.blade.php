@extends('layouts.dashboard')
@section('title')
Food Deduction | @parent
@endsection
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-12">
                <h1 class="admin-page-title">Monthly Food Deduction</h1>
                <ol class="breadcrumb">
        		  	<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        		  	<li class="breadcrumb-item active">Monthly Food Deduction </li>
        		</ol>
            </div>
			
		</div>
	</div>
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>   
    <div class="user-list-view">
        <div class="card">
            <table class="table table-striped table-sm">
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Month</th>
                    <th class="text-center">Status</th>
                   
                    <th class="text-right">Action</th>
                </tr>
                @if( empty($months) || count($months) == 0 )
                    <tr>
                        <td class="text-center" colspan="4">No Records found</td>
                        </tr>
                @endif
                @if(isset($months) > 0)
                    @foreach($months as $month)
                        <tr>
                            <td class="text-center">{{ $month->id}}</td>
                            <td class="text-center"> 
                               {{$month->formatMonth()}}
                            </td>
                            <td class="text-center">
                                @if($month->monthlyFoodDeductionSetting  &&  $month->monthlyFoodDeductionSetting->value == 'locked') 
                                    <span class="badge badge-primary">Locked</span>
                                    <br/><small class="text-primary">at {{datetime_in_view($month->monthlyFoodDeductionSetting->created_at)}}</small>
                                @else
                                    <span class="badge badge-info">OPEN</span>
                                @endif
                            </td>
                            <td class="text-right">
                            
                                <span>
                                <form name="showForm" method="get" action="/deduction/food-deduction/{{$month->id}}"  style="display: inline-block;">
                                    <button type="submit" class="btn btn-secondary btn-sm crude-btn"><i class="fa fa-eye fa-fw"></i>View</button> 
                                </form>
                                </span>
                                
                            <span>
                                 
                                    @if($month->monthlyFoodDeductionSetting  &&  $month->monthlyFoodDeductionSetting->value == 'locked')
                                    
                                    @else
                                    <form name="deleteForm" method="get" action="/deduction/food-deduction/{{$month->id}}/status-month"  style="display: inline-block;">
                                            <button type="submit" class="btn btn-primary btn-sm crude-btn"><i class="fa fa-lock fa-fw"></i>Lock</button> 
                                        </form>
                                        

                                    @endif
                            </span>
                            </td>
                        </tr>
                    @endforeach
                @endif  
             </table>
        </div>
    </div>
</div>	
@endsection
