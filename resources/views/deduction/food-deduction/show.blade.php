@extends('layouts.dashboard')
@section('title')
Food Deduction | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
                <h1 class="page-title">Food Deduction </h1>
                <ol class="breadcrumb">
        		  	<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        		  	<li class="breadcrumb-item"><a href="{{ url('deduction/food-deduction') }}">Food Deduction</a></li>
        		  	<li class="breadcrumb-item active"> {{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->monthlyFoodDeductionSetting || ($month->monthlyFoodDeductionSetting &&  $month->monthlyFoodDeductionSetting->value != 'locked'))
                    <div class="text-right">
                        <span>
                            {{ Form::open(['url' => 'deduction/food-deduction/regenerate', 'method' => 'post']) }}
                            {{ Form::hidden('month_id', $month->id) }}
							{{ Form::submit('Regenerate',['class' => 'btn btn-primary']) }}
							{{ Form::close() }}
                        </span>
                    </div>
                    <div class="text-right">
                        <span>
                            <a href="/deduction/food-deduction/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->monthlyFoodDeductionSetting && $month->monthlyFoodDeductionSetting->value == 'locked')
                    <div class="text-right">
                        <span>
                            <a href="/deduction/food-deduction/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
                @if(isset($monthList))
                    <div class="text-right">
                        <select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                                <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
			</div>
            @if($month && $month->monthlyFoodDeductionSetting &&  $month->monthlyFoodDeductionSetting->value == 'locked')
            <div class="pull pull-right">
                <span class="badge badge-primary">Locked at {{ $month->monthlyFoodDeductionSetting ? datetime_in_view($month->monthlyFoodDeductionSetting->created_at) : ''}}</span>
            </div>
        @endif


    </div>

	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
        <form method="post" action="{{$month->id}}">
            {{ csrf_field() }}
    {{ method_field('PATCH') }}
        @if(!$month->monthlyFoodDeductionSetting ||  $month->monthlyFoodDeductionSetting->value != 'locked')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="card">
                <table class="table table-striped table-sm" id="deduction-table">
                    <thead>
                        <th class="text-enter">#</th>
                        <th class="text-left">Employee Id</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Actual Amount(Monthly)</th>
                        <th class="text-left">Deduct Amount(Monthly)</th>
                        <th class="text-right">Action</th>
                    </thead>
                    @if(count($foodDeductionObjs) > 0)
                        @foreach($foodDeductionObjs as $index => $foodDeductionObj)
                            <tr>
                                <td class="text-left">{{ $index + 1  }}</td>
                                <td class="text-left">{{ $foodDeductionObj->user->employee_id}}</td>
                                <td class="text-left">{{ $foodDeductionObj->user->name}}</td>
                                <td class="text-left">{{ $foodDeductionObj->actual_amount}}</td>
                                <td class="text-left">
                                    @if($month->monthlyFoodDeductionSetting && $month->monthlyFoodDeductionSetting->value == 'locked')
                                        {{$foodDeductionObj->amount}}
                                    @else
                                        <input name="new_amount[{{$foodDeductionObj->id}}]" type="number" value="{{$foodDeductionObj->amount}}" max="0"/>
                                    @endif
                                </td>
                                <td class="text-right">
                                    @if(!$month->monthlyFoodDeductionSetting || ($month->monthlyFoodDeductionSetting &&  $month->monthlyFoodDeductionSetting->value  != 'locked'))
                                        <a href="/deduction/food-deduction/{{$foodDeductionObj->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Are you sure you want to delete this item?')"><i class="fa fa-trash fa-fw"></i>Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                </table>
            </div>
        </div>
    </form>
</div>
@endsection
@section('js')
@parent
<script>
	$(document).ready(function() {
		$('#deduction-table').DataTable({
              pageLength:500,
        });
    });
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/deduction/food-deduction/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>

@endsection
