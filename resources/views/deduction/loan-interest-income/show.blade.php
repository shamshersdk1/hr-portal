@extends('layouts.dashboard')
@section('title')
Loan Interest Income
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-6">
                <h1 class="page-title">Loan Interest Income</h1>
                <ol class="breadcrumb">
        		  	<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        		  	<li class="breadcrumb-item active">{{$monthObj->formatMonth()}}</li>
        		</ol>
            </div>

            <div class="col-sm-6 text-right">

                @if(!$monthObj->loanInterestSetting || ($monthObj->loanInterestSetting &&  $monthObj->loanInterestSetting->value != 'locked'))
                    <div class="text-right">
                        <span>
                            {{ Form::open(['url' => 'deduction/loan-interest-income/regenerate', 'method' => 'post']) }}
                            {{ Form::hidden('month_id', $monthObj->id) }}
							{{ Form::submit('Regenerate',['class' => 'btn btn-primary']) }}
							{{ Form::close() }}
                        </span>
                    </div>
                    <div class="text-right">
                        <span>
                            <a href="/deduction/loan-interest-income/{{$monthObj->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @elseif(($monthObj->getMonth(date("Y/m/d")) == $monthObj->id ) && $monthObj->loanInterestSetting && $monthObj->loanInterestSetting->value == 'locked')
                    <div class="text-right">
                        <span>
                            <a href="/deduction/loan-interest-income/{{$monthObj->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
                @if(isset($monthList))
                    <select id="selectid2" name="month"  placeholder= "{{$month ? $monthObj->formatMonth() : 'Select Month'}}">
                        <option value=""></option>
                        @foreach($monthList as $x)
                            <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                        @endforeach
                    </select>
                @endif
			</div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <h4
             class="text-warning"> Interest as per SBI rate applicable as on 01.04.2018 - 12.65%</h4
            >
        </div>
    </div>
    @if($monthObj->loanInterestSetting &&  $monthObj->loanInterestSetting->value == 'locked')
        <div class="pull pull-right">
            <span class="badge badge-primary">Locked at {{datetime_in_view($monthObj->loanInterestSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>


    <form name="editForm" method="post" action="/deduction/loan-interest-income/{{$monthObj->id}}/update">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        @if(!$monthObj->loanInterestSetting ||  $monthObj->loanInterestSetting->value == 'open')
            <div class="row">
                <div class="pull-right m-t-1 ">
                    <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                </div>
            </div>
        @endif
        <div class="user-list-view">
        <div class="card>
            <div class="table-responsive">
                <table class="table table-striped table-outer-border no-margin user-list-table table-sm" id="interest-table" width="100%">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Employee Id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Total Outstanding Amount</th>
                        <th class="text-center">Interest</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($loanInterests) > 0)
                        @foreach($loanInterests as $loanInterest)
                            <tr>
                                <td class="text-center"></td>
                                <td class="text-center">{{ $loanInterest->user->employee_id}}</td>
                                <td class="text-center">{{ $loanInterest->user->name}}</td>
                                <td class="text-center">{{$loanInterest->total_outstanding_amount}}</td>
                                <td class="text-center">
                                    @if($monthObj->loanInterestSetting && $monthObj->loanInterestSetting->value == 'locked')
                                        {{$loanInterest->interest}}
                                    @else
                                        <input name="interest[{{$loanInterest->id}}]" type="number" value="{{$loanInterest->interest}}"/>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </form>
</div>
@endsection
@section('js')
@parent
<script>

    $(document).ready(function() {
        var t = $('#interest-table').DataTable( {
            pageLength:500,
            scrollY:        true,
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });

    $('#selectid3').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/deduction/loan-interest-income/"+optionValue;
        }
    });

</script>
@endsection
