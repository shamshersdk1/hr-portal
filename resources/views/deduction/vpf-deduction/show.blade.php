@extends('layouts.dashboard')
@section('title')
VPF Deduction | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">
                <div class="col-sm-6">
                    <h1 class="page-title">VPF Deduction</h1>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('deduction/vpf-deduction') }}">VPF Deduction</a></li>
                        <li class="breadcrumb-item active">{{$month->formatMonth()}}</li>
                    </ol>
                </div>
                <div class="col-sm-6">

                    @if(!$month->vpfSetting || ($month->vpfSetting &&  $month->vpfSetting->value == 'open'))
                        <div class="text-right">
                            <span>
                                {{ Form::open(['url' => 'deduction/vpf-deduction/regenerate', 'method' => 'post']) }}
                                {{ Form::hidden('month_id', $month->id) }}
                                {{ Form::submit('Regenerate',['class' => 'btn btn-primary']) }}
                                {{ Form::close() }}
                            </span>
                        </div>
                        <div class="text-right">
                            <span>
                                <a href="/deduction/vpf-deduction/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                            </span>
                        </div>

                    @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->vpfSetting && $month->vpfSetting->value == 'locked')
                        <div class="text-right">
                            <span>
                                <a href="/deduction/vpf-deduction/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                            </span>
                        </div>
                    @endif
                  
                </div>


    </div>
   
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <form name="editForm" method="post" action="/deduction/vpf-deduction/update">
            {{ csrf_field() }}
            {{ Form::hidden('month_id', $month->id) }}


        @if(!$month->vpfSetting ||  $month->vpfSetting->value == 'open')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view mt-4">
            <div class="">
                <div class="row table-header-section">
                    <div class="col-sm-6">
                        @if(isset($monthList))
                        <div class="">
                            <label class="mb-0 pb-0">Select Month:</label>
                            <select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
                                <option value=""></option>
                                @foreach($monthList as $x)
                                    <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif

                    
                    </div>
                      <div class="col-sm-6 table-search">
                        @if($month->vpfSetting &&  $month->vpfSetting->value == 'locked')
                            <div class=" float-right">
                                <span class="badge badge-info">Locked at {{datetime_in_view($month->vpfSetting->created_at)}}</span>
                            </div>
                         @endif
                      </div>
                    </div>
                    <div class="table-responsive mt-2">
                    <table class="table table-striped table-sm table-outer-border" id="vpf-deduction" width="100%">
                        <thead>
                        <tr>
                            <th class="text-center">Emp Id</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">VPF Amount</th>
                            <th class="text-center">Deduct Amount</th>
                            <th class="text-center">Created At</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                        @if(count($vpfDeductions) > 0)
                            @foreach($vpfDeductions as $index => $vpfDeduction)
                                <tr>
                                    <td class="text-center">{{ $vpfDeduction->user->employee_id}}</td>
                                    <td class="text-center">{{ $vpfDeduction->user->name}}</td>
                                    <td class="text-center">{{ $vpfDeduction->vpf->amount ?? ''}}</td>
                                    <td class="text-center">
                                        @if($month->vpfSetting && $month->vpfSetting->value == 'locked')
                                            {{$vpfDeduction->amount}}
                                        @else
                                            <input name="new_amount[{{$vpfDeduction->id}}]" type="number" value="{{$vpfDeduction->amount}}" max="0"/>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ datetime_in_view($vpfDeduction->created_at)}}</td>
                                    <td class="text-right">
                                        @if(!$month->vpfSetting || $month->vpfSetting->value == 'open')
                                        <a href="/deduction/vpf-deduction/{{$vpfDeduction->id}}/delete" class="btn btn-danger btn-sm crude-btn" onclick = "return confirm('Are you sure you want to delete this item?')" ><i class="fa fa-trash fa-fw"></i>Delete</a>

                                        @endif
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    <td class="text-center">No Records found</td>
                                </td>
                            </tr>

                        @endif
                    </table>
                    </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        var t = $('#vpf-deduction').DataTable( {
           
            paging:         false,
            "columnDefs": [ {
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]],
            fixedHeader: true
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        $('.table-header-section .table-search').append($('.dataTables_filter'))

    });
$('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/deduction/vpf-deduction/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>
@endsection

