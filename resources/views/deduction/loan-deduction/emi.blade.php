@extends('layouts.dashboard')
@section('title')
Loan Deduction | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-centers">
            <div class="col-sm-6">
                <h1 class="page-title">Loan Deduction</h1>
                <ol class="breadcrumb">
        		  	<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        		  	<li class="breadcrumb-item"><a href="{{ url('deduction/loan-deduction') }}">Loan Deduction</a></li>
        		  	<li class="breadcrumb-item active">{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6 text-right">

                @if(!$month->loanSetting || ($month->loanSetting &&  $month->loanSetting->value != 'locked'))

                        <span>
                            {{ Form::open(['url' => 'deduction/loan-deduction/regenerate', 'method' => 'post']) }}
                            {{ Form::hidden('month_id', $month->id) }}
							{{ Form::submit('Regenerate',['class' => 'btn btn-primary']) }}
							{{ Form::close() }}
                        </span>

                        <span>
                            <a href="/deduction/loan-deduction/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>

                @elseif(($month->getMonth(date("Y/m/d")) == $month->id ) && $month->loanSetting && $month->loanSetting->value == 'locked')
                    <div class="text-right">
                        <span>
                            <a href="/deduction/loan-deduction/{{$month->id}}/status-month" class="btn btn-success"><i class="fa fa-unlock fa-fw"></i>UnLock</a>
                        </span>
                    </div>
                @endif
                @if(isset($monthList))
                        <select id="selectid2" name="month"  placeholder= "{{$month ? $month->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                                <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                    @endif
			</div>
    </div>
    @if($month->loanSetting &&  $month->loanSetting->value == 'locked')
        <div class="pull pull-right">
            <span class="badge badge-info">Locked at {{datetime_in_view($month->loanSetting->created_at)}}</span>
        </div>
    @endif
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>
    <form method="post" action="{{$month->id}}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        @if(!$month->loanSetting ||  $month->loanSetting->value != 'locked')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="card">
                <table class="table table-striped table-sm" id="loan-table" width="100%">
                    <thead>
                    <tr>
                        <th class="text-left">#</th>
                        <th class="text-left">Emp Id</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Type</th>
                        <th class="text-left">Loan Amount</th>
                        <th class="text-left">Loan Date</th>
                        <th class="text-left">EMI</th>
                        <th class="text-left">Outstanding Amount</th>
                        <th class="text-left">Deduct Amount</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
                    @if(count($loanDeductions) > 0)
                        @foreach($loanDeductions as $index=>$loanDeduction)
                            <tr @if($loanDeduction->balance <= 0) class="table-danger" @endif>
                                <td class="text-left">{{ $index+1 }}</td>
                                <td class="text-left">{{ $loanDeduction->loan->user->employee_id}}</td>
                                <td class="text-left">{{ $loanDeduction->loan->user->name}}</td>
                                <td class="text-left"><a  href="{{url('appraisal/'.(($loanDeduction->loan->appraisal_bonus_id != null) ?  $loanDeduction->loan->appraisalBonus->appraisal->id : '' ))}}">{{ $loanDeduction->loan->appraisalBonus->appraisalBonusType->description ?? 'Against Salary'}}</a></td>
                                <td class="text-left">{{ $loanDeduction->actual_amount}}</td>
                                <td class="text-left">{{ date_in_view($loanDeduction->loan->application_date)}}</td>
                                <td class="text-left">{{ $loanDeduction->loan->emi}}</td>
                                <td class="text-left">{{ $loanDeduction->balance}}</td>
                                <td class="text-left">
                                    @if($month->loanSetting && $month->loanSetting->value == 'locked')
                                        {{$loanDeduction->amount}}
                                    @else
                                        <input name="new_amount[{{$loanDeduction->id}}]" type="number" value="{{min($loanDeduction->amount,$loanDeduction->balance)}}" max="0"/>
                                    @endif
                                </td>
                                <td class="text-right">
                                    @if(!$month->loanSetting || $month->loanSetting->value != 'locked')
                                       <a href="/deduction/loan-deduction/{{$loanDeduction->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Delete user Loan?')"><i class="fa fa-trash fa-fw"></i>Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                </table>
            </div>
        </div>
    </form>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        $tableHeight = $( window ).height();
        var t = $('#loan-table').DataTable( {
            "bPaginate": false,
            scrollY: $tableHeight - 200,
            scrollX: true,
            scrollCollapse: true,
            paging:         false,
            "bInfo" : false,
            order: [[ 1, 'asc' ]],
            pageLength:500,
            columnDefs: [ {
                "searchable": true,
                "orderable": true,
                "targets": 0
            } ],
            fixedColumns:   {
            leftColumns: 2
        }
        } );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
$('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/deduction/loan-deduction/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$month ? $month->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>

@endsection
