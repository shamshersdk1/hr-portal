@extends('layouts.dashboard')
@section('title')
Monthly Variable Bonus | @parent
@endsection
@section('main-content')
<div class="container-fluid">
    <div class="breadcrumb-wrap">
    	<div class="row">
            <div class="col-sm-12">
                <h1 class="admin-page-title">Monthly Variable Bonus</h1>
                <ol class="breadcrumb">
        		  	<li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
        		  	<li class="breadcrumb-item"><a href="{{ url('deduction/monthly-variable-bonus') }}">Monthly Variable Bonus</a></li>
        		  	<li class="breadcrumb-item active">{{$month->formatMonth()}}</li>
        		</ol>
            </div>
            <div class="col-sm-6">

                @if(!$month->monthlyVariableBonusSetting || ($month->monthlyVariableBonusSetting &&  $month->monthlyVariableBonusSetting->value != 'locked'))
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/deduction/monthly-variable-bonus/{{$month->id}}/regenerate" class="btn btn-warning"><i class="fa fa-gear fa-fw"></i>Regenerate</a>
                        </span>
                    </div>
                    <div class="pull-right m-t-10">
                        <span>
                            <a href="/deduction/monthly-variable-bonus/{{$month->id}}/status-month" class="btn btn-danger"><i class="fa fa-lock fa-fw"></i>Lock</a>
                        </span>
                    </div>
                @endif
			</div>
            @if($month && $month->monthlyVariableBonusSetting &&  $month->monthlyVariableBonusSetting->value == 'locked')
            <div class="pull pull-right">
                <span class="badge badge-info">Locked at {{ $month->loanSetting ? datetime_in_view($month->loanSetting->created_at) : ''}}</span>
            </div>
        @endif
		</div>
    </div>
    
	<div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
    </div>

    <form name="editForm" method="post" action="/deduction/monthly-variable-bonus/{{$month->id}}">
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
        @if(!$month->monthlyVariableBonusSetting ||  $month->monthlyVariableBonusSetting->value != 'locked')
                <div class="row">
                    <div class="pull-right m-t-1 ">
                        <button type="submit" class="btn btn-success "><i class="fa fa-plus fa-fw"></i>Save</button>
                    </div>
                </div>
        @endif
        <div class="user-list-view">
            <div class="card">
                <table class="table table-striped table-sm" >
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Percentage</th>
                        <th class="text-center">Actual Amount(Annual)</th>
                        <th class="text-center">Deduct Amount(Monthly)</th>
                        <th class="text-right">Action</th>
                    </tr>
                    @if(count($monthly_variable_bonuses) > 0)
                        @foreach($monthly_variable_bonuses as $monthly_variable_bonus)
                            <tr>
                                <td class="text-center">{{ $monthly_variable_bonus->id }}</td>
                                <td class="text-center">{{ $monthly_variable_bonus->user->name}}</td>
                                <td class="text-center">{{ $monthly_variable_bonus->percentage}}%</td>
                                <td class="text-center">{{ $monthly_variable_bonus->actual_amount}}</td>
                                <td class="text-center">
                                    @if($month->monthlyVariableBonusSetting && $month->monthlyVariableBonusSetting->value == 'locked')
                                        {{$monthly_variable_bonus->amount}}
                                    @else
                                        <input name="new_amount[{{$monthly_variable_bonus->id}}]" type="number" value="{{$monthly_variable_bonus->amount}}" max="0"/>
                                    @endif
                                </td>
                                <td class="text-right">
                                    @if(!$month->monthlyVariableBonusSetting || ($month->monthlyVariableBonusSetting &&  $month->monthlyVariableBonusSetting->value != 'locked'))
                                        <a href="/deduction/monthly-variable-bonus/{{$monthly_variable_bonus->id}}/delete" class="btn btn-danger btn-sm crude-btn" onClick="return confirm('Delete Monthly Variable?')"><i class="fa fa-trash fa-fw"></i>Delete</a>    
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6" class="text-center">
                                No Records found
                            </td>
                        </tr>

                    @endif
                </table>
            </div>
        </div>
    </form>
</div>
@endsection
