@extends('layouts.dashboard')
@section('title')
Prep Salary | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-centers">
            <div class="col-sm-6">
                <h1 class="page-title">Due Bonuses</h1>
                <ol class="breadcrumb">
        		  	<li><a href="/dashboard">Dashboard</a></li>
        		  	<li><a href="{{ url('/non-ctc-bonus') }}">Non CTC Bonuses</a></li>
        		  	<li>Due Bonus</li>
        		</ol>
            </div>
    </div>
</div>
@livewire('bonus-due-user', ['monthId' => $monthId, 'userId' => $userId, 'dueBonuses' => $dueBonuses])
@endsection
