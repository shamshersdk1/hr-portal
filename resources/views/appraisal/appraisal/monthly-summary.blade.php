@extends('layouts.dashboard')
@section('title')
Appraisal
@endsection
@section('main-content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">List of Appraisal</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active"><a href="/appraisal">Appraisal</a></li>
                    <li class="breadcrumb-item active">{{$currMonth->formatMonth()}}</li>
                </ol>
            </div>
            <div class="col-sm-6">
                <div class="float-right mb-2">
                   
                </div>
            </div>

        </div>

    </div>
    

    <div class="row">
        <div class="col-md-12">
            <div class="card border">
                <div class="card-body py-2">
                    <h5 class="p-0 mt-0 mb-2">Total number of employee for the month of {{ $currMonth->formatMonth() }} : {{ count($appraisals) }}</h5>
                   <small class="">Note: The count of active user for a particular month will not match if the appraisal is not created for the selected month</small>
                </div>
                </div>  
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="">
                <div class="row table-header-section">
                    <div class="col-sm-6">
                        @if(isset($monthList))
                        <label class="mb-0 pb-0 mr-1">Select Month: </label>
                        <select id="selectid2" name="month"  placeholder= "{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}">
                            <option value=""></option>
                            @foreach($monthList as $x)
                            <option value="{{$x->id}}" >{{$x->formatMonth()}}</option>
                            @endforeach
                        </select>
                        @endif
                    
                    </div>
                      <div class="col-sm-6 table-search">
                      </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-striped no-margin table-outer-border table-sm" id="all-appraisals">
                            <thead>
                                <tr>
                                    <th class="td-text">#</th>
                                    <th class="td-text">Employee Id</th>
                                    <th class="td-text">User Name</th>
                                    <th class="td-text">Appraisal Type</th>
                                    <th class="td-text">Effective Date</th>
                                    @foreach($appraisalTypes as $type)
                                        <th class="td-text">{{$type->name}}</th>
                                    @endforeach
                                    @foreach($appraisalBonusTypes as $type)
                                        <th class="td-text">{{$type->description}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @if($appraisals && count($appraisals)>0)
                                @foreach($appraisals as $index=> $appraisal)
                                <tr>
                                    <td class="td-text">
                                        {{ $index + 1 }}
                                    </td>
                                    <td class="td-text">
                                        {{$appraisal['user'] ? $appraisal['user']['employee_id'] : ''}}
                                    </td>
                                    <td class="td-text">
                                        {{$appraisal['user'] ? $appraisal['user']['name'] : ''}}
                                    </td>
                                    <td class="td-text">
                                        {{$appraisal['type'] ? $appraisal['type']['name'] : ''}}
                                    </td>
                                    <td class="td-text">{{date_in_view($appraisal['effective_date'])}}</td>
                                    @foreach($appraisalTypes as $type)
                                        <td class="td-text">{{$appraisal->appraisalComponent->where('component_id',$type['id'])->first()->value ?? ''}}</td>
                                    @endforeach
                                    @foreach($appraisalBonusTypes as $type)
                                        <td class="td-text">{{$appraisal->appraisalBonus->where('appraisal_bonus_type_id',$type['id'])->first()->value ?? ''}}</td>
                                    @endforeach
                                </tr>
                                @endforeach
                                @else
                                <td colspan="8" style="text-align:center;">
                                    <span class="align-center"
                                        ><big>No Appraisal Found</big></span
                                    >
                                </td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
		$('#all-appraisals').DataTable( {
            fixedHeader: true,
			scrollY:        '70vh',
            scrollX:        'true',
			paging:         false,
			scrollCollapse: true,
			fixedColumns:   {
				leftColumns: 3,
            },
            pageLength:500,
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ]
        } );
        $('.table-header-section .table-search').append($('.dataTables_filter'))

    });
    $('#selectid2').change(function(){
        var optionSelected = $("option:selected", this);
        optionValue = this.value;
        if (optionValue) {
            window.location = "/appraisal/monthly-summary/"+optionValue;
        }
    });
    $('#selectid2').select2({
			placeholder: '{{$currMonth ? $currMonth->formatMonth() : 'Select Month'}}',
			allowClear:true
		});
</script>
@endsection
