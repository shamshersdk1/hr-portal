@extends('layouts.dashboard')
@section('title')
Appraisal | @parent
@endsection
@section('main-content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-8">
                <h4 class="page-title">List of Appraisal</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"> <a href="/appraisal">list of Appraisal</a></li>
                    <li class="breadcrumb-item active"> Edit {{$appraisal->id}}</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right">
                <a href="/appraisal" class="btn btn-secondary">Back</a>
                {{-- {{Form::open(['url'=>'appraisal/'.$appraisal->id.'/edit' ,'method'=>'get'])}}
                {{Form::button('<i class="fa fa-edit fa-fw btn-icon-space"></i>  Edit Appraisal', array('type' => 'submit', 'class' => 'btn btn-warning'))}}
                {{Form::close()}} --}}
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>

    <div class=" p-2 d-flex justify-center border mb-5" style="justify-content: space-between;  position: -webkit-sticky; position: sticky; top:50px; z-index: 999; background: #fff;">
        <div>
            <div class="text-muted">Employee Name</div>
            <div class="font-14 font-500 text-center text-primary">{{$appraisal->user->name}}</div>
        </div>
        <div>
            <div class="text-muted">Employee Id</div>
            <div class="font-14 font-500 text-center">{{$appraisal->user->employee_id}}</div>
        </div>
        <div>
            <div class="text-muted">Appraisal Type</div>
            <div class="font-14 font-500 text-center"> {{ $appraisal->type->name}}</div>

        </div>
        <div>
            <div class="text-muted">Effective Date</div>
            <div class="font-14 font-500 text-center"> {{ $appraisal->effective_date}}</div>

        </div>
        

        
       
    </div>
   
    
    <div >
            <div class="row justify-content-md-center">
                <div class="col-md-12 ">
                    {{ Form::open(array('url' => 'appraisal/'.$appraisal->id,'method'=>'put')) }}
                    <div class="row">
                            {{Form::hidden('user_id', $appraisal->user->id)}}

                        <div class="justify-content-end d-flex col-sm-8 align-items-center">
                            <div class="d-flex mr-4 align-items-center">
                            {{ Form::label('account', 'Appraisal Type:', ['style' => 'width:150px'] ) }}
                            {{ Form::select('type_id', $appraisalTypes->pluck('name','id'),$appraisal->type->id, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Appraisal Type'))}}
                            </div>
                            <div class="d-flex align-items-center">
                            {{ Form::Label('effectiveDate', 'Effective Date:',  ['style' => 'width:150px']) }}
                            <div class=" input-group date" id="effectiveDate">
                                {{Form::date("effective_date", $appraisal->effective_date, ['class' => 'form-control'])}}
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="card border mt-4">
                        <div class="card-body">

                            <div class="row ">
                                <div class="col-sm-6">
                                    <h6>Set Appraisal Components</h6>
                                    <table class="table no-margin border ">
                                        @if($appraisalComponentObj)
                                        @foreach($appraisalComponentObj as $component)
                                        @if( $component['is_computed'] == 0)
                                        <tr>
                                            <td>{{ Form::label('', $component['name'] )}}
                                            </td>
                                            <td>{{ Form::text('component['.$component['id'].']',$component['value'],['class' => 'form-control']) }}
                                            </td>
                                            @endif @endforeach @endif
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-sm-6">
                                    <h6>Set Appraisal Bonus</h6>
                                    <table class="table no-margin border ">
                                        @if(count($appraisalBonusObj)>0)
                                        @foreach($appraisalBonusObj as $bonusType)
                                        <tr>
                                            <td>{{ Form::label('', $bonusType['name'] )}}</td>

                                            <td>{{ Form::text('bonus['.$bonusType['id'].'][value]', $bonusType['value'],['class' => 'form-control']) }}</td>
                                            <td>{{Form::date('bonus['.$bonusType['id'].'][value_date]',$bonusType['value_date'])}}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4 text-right">
                        {{Form::submit('Update',array('class' => 'btn btn-success '))}}
                        {{ Form::close() }}
                    </div>
                </div>
        </div>
    </div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        $("#effectiveDate").datetimepicker({
            format: "YYYY-MM-DD"
        });
        $(".userSelect1").select2({
            placeholder: "Select a user",
            allowClear: true
        });
        $(".selectid4").select2({
            placeholder: "Select a user",
            allowClear: true
        });
    });
</script>

@endsection
