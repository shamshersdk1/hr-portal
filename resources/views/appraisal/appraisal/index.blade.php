@extends('layouts.dashboard')
@section('title')
Appraisal
@endsection
@section('main-content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-6">
                <h4 class="page-title">List of Appraisal</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active">List of Appraisal</li>
                </ol>
            </div>
            <div class="col-sm-6 text-right">
                <div class="d-flex justify-content-end">
                    {{Form::open(['url'=>'appraisal/create' ,'method'=>'get', 'class'=>'mb-0 p-0 mr-1'] )}}
                    {{Form::button('<i class="fa fa-plus fa-fw"></i>  Appraisal',
                    array('type' => 'submit', 'class' => 'btn btn-success'))}}
                    {{Form::close()}}

                    {{Form::open(['url'=>'appraisal/monthly-summary' ,'method'=>'get', 'class'=>'m-0 p-0'])}}
                    {{Form::button('<i class="fa fa-eye fa-fw"></i> Monthly Summary',
                    array('type' => 'submit', 'class' => 'btn btn-warning'))}}
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    @include('flash')
    <div class="row">
        <div class="col-sm-12">
            <div class="row table-header-section">
                <div class="col-sm-6">

                </div>
                  <div class="col-sm-6 table-search">
                  </div>
                </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-striped no-margin table-outer-border table-sm" id="all-appraisals">
                            <thead>
                                <tr>
                                    <th class="td-text">#</th>
                                    <th class="td-text">Employee Id</th>
                                    <th class="td-text">User Name</th>
                                    <th class="td-text">Joining Date</th>
                                    <th class="td-text">Variable Bonus</th>
                                    <th class="td-text">Fixed Bonus</th>
                                    <th class="td-text">Annual CTC</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($appraisals && count($appraisals)>0)
                                @foreach($appraisals as $index=> $appraisal)
                                <tr>
                                    <td class="td-text"> </td>
                                    <td class="td-text">
                                        {{$appraisal['user'] ? $appraisal['user']['employee_id'] : ''}}
                                    </td>
                                    <td class="td-text">
                                        {{$appraisal['user'] ? $appraisal['user']['name'] : ''}}
                                    </td>
                                    <td class="td-text">
                                        {{$appraisal['user'] ? date_in_view($appraisal['user']['joining_date']) : ''}}
                                    </td>
                                    <td class="td-text">
                                        &#8377;{{number_format($appraisal->getFixedBonuses()) ?? ''}}
                                    </td>
                                    <td class="td-text">
                                        &#8377;{{number_format($appraisal->getVariableBonuses()) ?? ''}}
                                    </td>
                                    <td class="td-text">
                                        &#8377;{{number_format($appraisal->getAnnualCtc()) ?? ''}}
                                    </td>

                                    <td class="text-right">
                                        <a
                                            href="appraisal/{{$appraisal['user_id']}}"
                                            class="btn btn-primary "  data-toggle="tooltip" data-placement="top" title="View" target="_blank"
                                            ><i class="fas fa-eye"></i></a
                                        >
                                        <a
                                            href="appraisal/{{$appraisal['id']}}/edit"
                                            class="btn btn-info " data-toggle="tooltip" data-placement="top" title="Edit" target="_blank"
                                            ><i class="fas fa-edit"></i></a
                                        >                                    
                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <td colspan="8" style="text-align:center;">
                                    <span class="align-center"
                                        ><big>No Appraisal Found</big></span
                                    >
                                </td>
                            </tr>
                            @endif
                            </tbody>
                        </table>
            </div>
        </div>
    </div>

@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
		var t = $('#all-appraisals').DataTable( {
            pageLength:500,
            paging:         false,
           // "order": [[5, 'asc'],[ 1, 'asc' ]],
        } );

        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        $('.table-header-section .table-search').append($('.dataTables_filter'))

    });
</script>
@endsection
