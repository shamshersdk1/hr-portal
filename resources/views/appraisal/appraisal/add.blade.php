@extends('layouts.dashboard')
@section('title')
Appraisal | @parent
@endsection
@section('main-content')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-8">
                <h4 class="page-title">List of Appraisal</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"> <a href="/appraisal">list of Appraisal</a></li>
                    <li class="breadcrumb-item active"> Add Appraisal</li>
                </ol>
            </div>
            <div class="col-sm-4 text-right">

            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            @if(!empty($errors->all()))
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ $error }}</span
                ><br />
                @endforeach
            </div>
            @endif @if (session('message'))
            <div class="alert alert-success">
                <a
                    href="#"
                    class="close"
                    data-dismiss="alert"
                    aria-label="close"
                    >&times;</a
                >
                <span>{{ session("message") }}</span
                ><br />
            </div>
            @endif
        </div>
    </div>

    <div class="card border">
        <div class="card-body">
            <div class="row justify-content-md-center">
                <div class="col-md-12 mt-3">
                    {{ Form::open(array('url' => "appraisal")) }}
                    <div class="row">
                        <div class="col-md-4">
                            {{ Form::Label('user', 'User Name:') }}
                            {{ Form::select('user_id', $users->pluck('name','id'),null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select User'))}}
                        </div>
                        <div class="col-md-4">
                            {{ Form::label('account', 'Appraisal Type:') }}
                            {{ Form::select('type_id', $appraisalTypes->pluck('name','id'),null, array('class' => 'userSelect1 form-control'  ,'placeholder' => 'Select Appraisal Type'))}}
                        </div>
                        <div class="col-md-4">
                            {{ Form::Label('effectiveDate', 'Effective Date:') }}
                            <div class="input-group date" id="effectiveDate">
                                {{Form::date("effective_date", null, ['class' => 'form-control'])}}
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <h6>Set Appraisal Components</h6>
                            <table class="table no-margin border ">
                                @if(count($appraisalComponentTypes)>0)
                                @foreach($appraisalComponentTypes as $component)
                                @if($component->is_computed == 0)
                                <tr>
                                    <td>{{ Form::label(null, $component->name )}}
                                    </td>
                                    <td>{{ Form::text("component[$component->id]", null,['class' => 'form-control']) }}
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                                @endif
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <h6>Set Appraisal Bonus</h6>
                            <table class="table no-margin border ">
                                @if(count($appraisalBonusType)>0)
                                @foreach($appraisalBonusType as $bonusType)
                                <tr>
                                    <td>
                                        {{ Form::label('', $bonusType->description)}}
                                    </td>
                                    <td>
                                        {{ Form::text("bonus[$bonusType->id][value]", '',['class' => 'form-control']) }}
                                    </td>
                                    <td>
                                        {{Form::date("bonus[$bonusType->id][value_date]", null, ['class' => 'form-control'])}}
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </table>
                        </div>
                    </div>
                    <div class="mt-4 text-right">
                        {{Form::submit('Save',array('class' => 'btn btn-lg btn-success '))}}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        $(".userSelect1").select2();
    });
</script>
@endsection
