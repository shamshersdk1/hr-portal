@extends('layouts.dashboard')
@section('title')
Appraisal | @parent
@endsection
@section('main-content')
	 <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-sm-8">
                <h4 class="page-title">User Appraisals</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/dashboard">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item"> <a href="/appraisal">list of Appraisal</a></li>
                    <li class="breadcrumb-item active"> Show </li>
                </ol>
            </div>
            {{-- <div class="col-sm-4 text-right">
                {{Form::open(['url'=>'appraisal/'.$appraisals->first()->id.'/edit' ,'method'=>'get'])}}
				{{Form::button('<i class="fa fa-edit fa-fw btn-icon-space"></i> Appraisal', array('type' => 'submit', 'class' => 'btn btn-secondary'))}}
				{{Form::close()}}
            </div> --}}
        </div>
	</div>
	@include('partials/user-detail')
	@include('flash')


	<div id="accordion" class="accordion appraisal-accordition">
        <?php $index = 0 ?>
		@foreach($appraisals as $appraisal)
        <div class="card mb-2">
			<div class="card-header  collapsed" data-toggle="collapse" href="#collapseExample{{$appraisal->id}}" >
				<a class="card-title d-flex justify-content-between mr-5 align-items-center pb-0 mb-0">
					<div class="d-flex justify-content-between " style="width:60%">
						<div>
						<span class="text-muted">Type</span> <br/>
						<span class="font-14">{{$appraisal['type']['name']}}</span>
						</div>
						<div class="text-center">
							<span class="text-muted">Effective Date</span> <br/>
							<span class="font-14">{{date_in_view($appraisal->effective_date)}}</span>
						</div>
						<div class="text-center">
							<span class="text-muted">Monthly Gross Salary</span> <br/>
							<span class="font-14">&#8377;{{number_format(round($appraisal->getMonthlyGrossSalary(),2))}}</span>
						</div>
						<div class="text-center">
							<span class="text-muted">Annual CTC</span> <br/>
							<span class="font-14">&#8377;{{number_format($appraisal->getAnnualCtc())}}</span>
                        </div>
                       
                    </div>
                    <div class="text-right d-flex align-items-center">
                        @if(!empty($appraisals[$index+1]))
                        <div class="text-center mr-2">
                            <span class="badge badge-info badge-sm font-12"> {{round((($appraisal->getAnnualCtc() - $appraisals[$index+1]->getAnnualCtc())/$appraisals[$index+1]->getAnnualCtc())*100)}}% hike </span>
                        </div>
                    @endif
                        {{Form::open(['url'=>'appraisal/'.$appraisal->id.'/edit' ,'method'=>'get','class'=>'pb-0 mb-0 position-relative', 'style'=>'z-index:9999', 'target'=>'_blank'])}}
                        {{Form::button('Edit Appraisal', array('type' => 'submit', 'class' => 'btn btn-secondary btn-sm '))}}
                        {{Form::close()}}
                        &nbsp;
                        {{ Form::open(['url' => 'appraisal/'.$appraisal['id'], 'method' => 'delete', 'class' =>'m-0']) }}
                        {{Form::button('Delete', array('type' => 'submit', 'class' =>'btn btn-danger btn-sm', 'onClick' => 'return confirm("Are you sure you want to delete this item?")'))}}
                        {{ Form::close() }}

                    </div>
				</a>
			</div>

			<div id="collapseExample{{$appraisal->id}}" class="card-body collapse " data-parent="#accordion">
                @appraisalinfo(['appraisal'=>$appraisal,'bonusTypes' => $bonusTypes]) @endappraisalinfo
			</div>
		</div>

        <?php $index+=1 ?>
			@endforeach






    </div>
    {{-- @foreach($appraisals as $appraisal)
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample{{$appraisal->id}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$appraisal->id}}">
            {{$appraisal['type']['name']}}. Date:{{$appraisal->effective_date}}
        </a>
        <br>
        <div class="collapse" id="collapseExample{{$appraisal->id}}">
            <div class="card card-body">
                @appraisalinfo(['appraisal'=>$appraisal,'bonusTypes' => $bonusTypes]) @endappraisalinfo
            </div>
        </div>
    @endforeach --}}
@endsection

@section('js')
@parent
    <script>
        $(document).ready(function(){
			{{-- $('#accordion .card').first().children().next().addClass('show')
			$('#accordion .card').first().children().removeClass('collapsed') --}}

		});


    </script>
@endsection
