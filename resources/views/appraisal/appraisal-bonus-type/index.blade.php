@extends('layouts.dashboard')
@section('title')
Appraisal Bonus Type | @parent
@endsection
@section('main-content')
<div class="page-title-box" >
    <div class="row align-items-center">

            <div class="col-sm-12">
                <h1 class="page-title">Appraisal Bonus Type</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item active"><a href="{{ url('appraisal/appraisal-bonus-type') }}">Appraisal Bonus Type</a></li>
                </ol>
            </div>
    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
    <div class="row">
		<div class="col-md-12">
			<div class="card">
					<div class="panel-body">
                        <br>
						{{ Form::open(array('url' => "appraisal/appraisal-bonus-type")) }}
                            <div class="form-group row">
                                {{ Form::Label('Code', 'Code:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
                                <div class="col-md-4" style="padding:0">
                                    {{ Form::text('code', null, ['class' => 'form-control']) }}
                                </div>
                            </div>


							<div class="form-group row">
								{{ Form::Label('description', 'Description:',array('class' =>'col-md-4' , 'style' =>'padding:1%', 'align' => 'right')) }}
								<div class="col-md-4" style="padding:0">

										{{ Form::textarea('description',null,['class'=>'form-control', 'rows' => 5] ) }}
                                </div>
                            </div>
							<div class="col-md-12">
								{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
							</div>
                    		{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
    	<div class="card">
			<table class="table table-striped">
				<thead>
					<th class="td-text">#</th>
					<th class="td-text">Code</th>
                    <th class="td-text">Description</th>
                    <th class="text-right">Actions</th>
				</thead>
				<tbody>
					@if(count($appraisalBonusType)>0)
						@foreach($appraisalBonusType as $index=> $bonusType)
						<tr>
							<td class="td-text">{{$index + 1}}</td>
							<td class="td-text">{{$bonusType->code ? $bonusType->code : ''}}</td>
							<td class="td-text">{{$bonusType->description ? $bonusType->description : ''}}</td>


							<td class="text-right">
							<a href="appraisal-bonus-type/{{$bonusType->id}}/edit" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true" style="margin-bottom:10"></i>Edit</a>
							<div style="display:inline-block;" class="deleteform">
                                {{ Form::open(['url' => '/appraisal/appraisal-bonus-type/'.$bonusType->id, 'method' => 'delete']) }}
                                {{ Form::submit('Delete',['class' => 'btn btn-danger crud-btn btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                {{ Form::close() }}
                                </div>
							</td>
						</tr>
						@endforeach
					@else
						<td colspan="4" style="text-align:center;"><span class="align-center"><big>No Appraisal Bonus Type</big></span></td>
					@endif
				</tbody>
			</table>
		</div>
	</div>
    </div>
</div>

@endsection
