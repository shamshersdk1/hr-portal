@extends('layouts.dashboard')
@section('title')
Appraisal Bonus Type | @parent
@endsection
@section('main-content')
<div class="page-title-box" >
    <div class="row align-items-center">

            <div class="col-sm-12">
                <h1 class="page-title">Appraisal Bonus Type</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item"><a href="{{ url('appraisal/appraisal-bonus-type') }}">Appraisal Bonus Type</a></li>
					<li class=" breadcrumb-item active">{{$appraisalBonusType->id}}</li>
                </ol>
            </div>

    </div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		            @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
    <div class="row">
		<div class="col-md-12">
			<div class="card">
					<div class="panel-body">
                        <br>
						{{ Form::open(array('url' => 'appraisal/appraisal-bonus-type/'.$appraisalBonusType->id,'method'=>'put')) }}
                        <div class="form-group row">
                        {{ Form::Label('Code', 'Code:',['class' => 'col-md-4','align' => 'right']) }}
                            <div class="col-md-4" style="padding:0">
                                    {{ Form::text('code', $appraisalBonusType->code, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group row">
                        {{ Form::Label('description', 'Description:',['class' => 'col-md-4','align' => 'right']) }}
                            <div class="col-md-4" style="padding:0">
                                    {{ Form::textarea('description',$appraisalBonusType->description,['class'=>'form-control', 'rows' => 5 ] ) }}
                            </div>
                        </div>
                        <div class="col-md-12">
                        {{Form::submit('Update',array('class' => 'btn btn-success ','style' => 'display: block; margin: 0 auto'))}}
                        </div>
						{{ Form::close() }}
					</div>
				</div>
   			</div>
		</div>
@endsection
