@extends('layouts.dashboard')
@section('title')
Appraisal Component Type | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-8">
				<h1 class="page-title">Appraisal Component Type</h1>
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
				        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="{{ url('appraisal/appraisal-component-type') }}">Appraisal Component Type</a></li>
						<li class="breadcrumb-item active" aria-current="page">{{$appraisalComponentType->name}}</li>
					</ol>
				</nav>
			</div>
			<div class="col-sm-4 text-right m-t-10">
                <button type="button" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-caret-left fa-fw"></i> Back</button>
            </div>

	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
                <br>
				<div class="panel-body">
				{{ Form::open(['url' => '/appraisal/appraisal-component-type/'.$appraisalComponentType->id, 'method' => 'put']) }}
				{{ Form::token() }}
				<div class="form-group row">
                    {{ Form::label('name', 'Name :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
					<div class="col-md-4" style="padding:0">
                    	{{ Form::text('name', $appraisalComponentType->name,['class' => 'form-control']) }}
					</div>
				</div>
				<div class="form-group row">
                    {{ Form::label('code', 'Code :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
					<div class="col-md-4" style="padding:0">
                    	{{ Form::text('code', $appraisalComponentType->code,['class' => 'form-control']) }}
					</div>
				</div>
				<div class="form-group row">
                    {{ Form::label('is_computed', 'Is Computed :',['class' => 'col-md-4','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
							{{Form::radio('is_computed', true,$appraisalComponentType->is_computed ? true : null)}} Yes
							{{Form::radio('is_computed', false,$appraisalComponentType->is_computed ? false : true)}} No
						</div>
					</div>
				<div class="form-group row">
                    {{ Form::label('is_company_expense', 'Is Company Expense:',['class' => 'col-md-4','align' => 'right'])}}
					<div class="col-md-4" style="padding:0">
						{{Form::radio('is_company_expense', true,$appraisalComponentType->is_company_expense ? true : null)}} Yes
						{{Form::radio('is_company_expense', false,$appraisalComponentType->is_company_expense ? false : true)}} No
					</div>
				</div>
				<div class="form-group row">
				{{ Form::label('is_conditional_prorata', 'Is Conditional Prorata:',['class' => 'col-md-4','align' => 'right'])}}
					<div class="col-md-4" style="padding:0">
						{{Form::radio('is_conditional_prorata', true,$appraisalComponentType->is_conditional_prorata ? true : null)}} Yes
						{{Form::radio('is_conditional_prorata', false,$appraisalComponentType->is_conditional_prorata ? false : null)}} No
					</div>
				</div>
				<div class="form-group row">
				{{ Form::label('prorata_function', 'Prorata Function :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
					<div class="col-md-4" style="padding:0">
						{{ Form::text('prorata_function', $appraisalComponentType->prorata_function,['class' => 'form-control']) }}
					</div>
				</div>
				<div class="col-md-12">
					{{ Form::submit('Update',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
				</div>
				{{ Form::close() }}
				</div>
				</div>
				<div class="card">
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
@parent
<script>

	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
