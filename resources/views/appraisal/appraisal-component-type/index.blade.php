@extends('layouts.dashboard')
@section('title')
Appraisal Component Type | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-12">
				<h1 class="page-title">Appraisal Component Type</h1>
                <nav aria-label="breadcrumb">
					<ol class="breadcrumb">
	                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
						<li class="breadcrumb-item active" aria-current="page">Appraisal Component Type</li>
					</ol>
				</nav>
			</div>
			<div class="col-sm-4 text-right m-t-10">
            </div>

    </div>
	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card border">
				<div class="card-body">
					<h6>Component Type</h6>
					<div class="row">
						<div class="col-sm-6">
							{{ Form::open(['url' => '/appraisal/appraisal-component-type', 'method' => 'post']) }}
							<div class="form-group row">
								{{ Form::label('name', 'Name :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
								<div class="col-md-4" style="padding:0">
									{{ Form::text('name', '',['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group row">
							{{ Form::label('code', 'Code :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
								<div class="col-md-4" style="padding:0">
									{{ Form::text('code', '',['class' => 'form-control']) }}
								</div>
							</div>
							<div class="form-group row">
								{{ Form::label('prorata_function', 'Prorata Function :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
									<div class="col-md-4" style="padding:0">
										{{ Form::text('prorata_function', '',['class' => 'form-control']) }}
									</div>
								</div>
						
						</div>
						<div class="col-sm-6">
							<div class="form-group row">
								{{ Form::label('is_computed', 'Is Computed :',['class' => 'col-md-4','align' => 'right'])}}
									<div class="col-md-4" style="padding:0">
										{{Form::radio('is_computed', true)}} Yes
										{{Form::radio('is_computed', false)}} No
									</div>
								</div>
							<div class="form-group row">
							{{ Form::label('is_company_expense', 'Is Company Expense:',['class' => 'col-md-4','align' => 'right'])}}
								<div class="col-md-4" style="padding:0">
									{{Form::radio('is_company_expense', true)}} Yes
									{{Form::radio('is_company_expense', false)}} No
								</div>
							</div>
							<div class="form-group row">
							{{ Form::label('is_conditional_prorata', 'Is Conditional Prorata:',['class' => 'col-md-4','align' => 'right'])}}
								<div class="col-md-4" style="padding:0">
									{{Form::radio('is_conditional_prorata', true)}} Yes
									{{Form::radio('is_conditional_prorata', false)}} No
								</div>
							</div>
							
						</div>

					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
				</div>
			</div>
				<div class="card">
				<table class="table table-striped">
				<thead>
					<th class="td-text">#</th>
					<th class="td-text">Name</th>
					<th class="td-text">Code</th>
					<th class="td-text">Is Computed</th>
					<th class="td-text">Is Company Expense</th>
					<th class="td-text">Is Conditional Prorata</th>
					<th class="td-text">Prorata Function</th>
					<th class="text-right">Actions</th>
				</thead>
				@if(isset($appraisalComponentTypes))
				@if(count($appraisalComponentTypes) > 0)
					@foreach($appraisalComponentTypes as $appraisalComponentType)
						<tr>
							<td class="td-text">{{$appraisalComponentType->id}}</td>
							<td class="td-text"> {{$appraisalComponentType->name}}</td>
							<td class="td-text"> {{$appraisalComponentType->code}}</td>
							<td class="td-text">
								@if($appraisalComponentType->is_computed == 1)
								Yes
								@else
								No
								@endif
							</td>
							<td class="td-text">
								@if($appraisalComponentType->is_company_expense == 1)
								Yes
								@else
								No
								@endif
							</td>
							<td class="td-text">
								@if($appraisalComponentType->is_conditional_prorata == 1)
								Yes
								@else
								No
								@endif
							</td>
							<td class="td-text"> {{$appraisalComponentType->prorata_function}}</td>
							<td class="text-right">
								<a href="/appraisal/appraisal-component-type/{{$appraisalComponentType->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
								<div style="display:inline-block;" class="deleteform">
								{{ Form::open(['url' => '/appraisal/appraisal-component-type/'.$appraisalComponentType->id, 'method' => 'delete']) }}
								{{ Form::submit('Delete',['class' => 'btn btn-danger crud-btn btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
								{{ Form::close() }}
								</div>
							</td>
						</tr>
					@endforeach
				@else
					<tr >
						<td colspan="4" class="text-center">No appraisal component types added.</td>
					</tr>
				@endif
				@endif
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
@parent
<script>

	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
