@extends('layouts.dashboard')
@section('title')
Appraisal Type | @parent
@endsection
@section('main-content')
<div class="page-title-box">
    <div class="row align-items-center">

            <div class="col-sm-12">
				<h1 class="page-title">Appraisal Type</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
					<li class="breadcrumb-item active">Appraisal Type</li>
                </ol>
			</div>
			<div class="col-sm-4 text-right m-t-10">
            </div>

	</div>
    <div class="row">
	    <div class="col-md-12">
	    	@if(!empty($errors->all()))
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		                <span>{{ $error }}</span><br/>
		              @endforeach
		        </div>
		    @endif
		    @if (session('message'))
		        <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		            <span>{{ session('message') }}</span><br/>
		        </div>
		    @endif
	    </div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">

				<div class="panel-body">
                    <br>
					{{ Form::open(['url' => '/appraisal/appraisal-type', 'method' => 'post']) }}
					<div class="form-group row">
                    	{{ Form::label('name', 'Name :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('name', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="form-group row">
                    {{ Form::label('code', 'Code :',['class' => 'col-md-4','style' => 'padding-top:1%','align' => 'right'])}}
						<div class="col-md-4" style="padding:0">
                    		{{ Form::text('code', '',['class' => 'form-control']) }}
						</div>
					</div>
					<div class="col-md-12">
					{{ Form::submit('Save',['class' => 'btn btn-success','style' => 'display: block; margin: 0 auto']) }}
					</div>
                    {{ Form::close() }}
				</div>
                </div>
                <div class="row">
                        <div class="col-sm-12">
				<div class="card card-default">
                    <div class="table-responsive">
                    <table class="table table-striped table-outer-border no-margin user-list-table" id="type-table">
                    <thead>
                        <th width="10%">#</th>
                        <th width="10%">Name</th>
                        <th width="15%">Code</th>
                        <th width="25%" class="text-right">Actions</th>
                    </thead>
                    @if(isset($appraisalTypes))
                    @if(count($appraisalTypes) > 0)
                        @foreach($appraisalTypes as $index => $appraisalType)
                            <tr>
                                <td class="td-text">{{$index+1}}</td>
                                <td class="td-text"> {{$appraisalType->name}}</td>
                                <td class="td-text"> {{$appraisalType->code}}</td>
                                <td class="text-right">
                                    <a href="/appraisal/appraisal-type/{{$appraisalType->id}}" class="btn btn-info crud-btn btn-sm"><i class="fa fa-pencil btn-icon-space" aria-hidden="true"></i>Edit</a>
                                    <div style="display:inline-block;" class="deleteform">
                                    {{ Form::open(['url' => '/appraisal/appraisal-type/'.$appraisalType->id, 'method' => 'delete']) }}
                                    {{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure you want to delete this item?")']) }}
                                    {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr >
                            <td colspan="4" class="text-center">No appraisal types added.</td>
                        </tr>
                    @endif
                    @endif
                    </table>
                </div>
            </div>
                        </div>
                </div>
		</div>
	</div>
</div>
@endsection
@section('js')
@parent
<script>

	$(function () {
		$('#type option[value="{{old('type')}}"]').attr("selected",true);
	});
</script>
@endsection
