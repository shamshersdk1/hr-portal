<?php

//## Rules Or Types

// 1. For group of single, unrelated and different-url routes under one section name, use 'nested' as index
//      -> 'nested' => [
//              'roles_access' => ['admin'],
//              'section_icon' => 'paper-sheet',
//              'section_name' => 'Settings',
//              'role' => 'Role',
//              'prep-salary/prep-salary-component-type' => 'Prepare Salary Component Type',
//          ],
// 2. For single menu, index and menu url should be same and there should be no 'section_name'
//      -> 'user' => [
//              'roles_access' => ['admin', 'hr'],
//              'section_icon' => 'paper-sheet',
//              'user' => 'User',
//          ],
// 3. For group of related routes with same prefix in the url except for one master menu, there should be a section name, and index == url of master menu
//      -> 'appraisal' => [
//                  'roles_access' => ['admin', 'hr'],
//                  'section_name' => 'Appraisal',
//                  'section_icon' => 'paper-sheet',
//                  'appraisal' => 'Appraisal',
//                  'appraisal-type' => 'Appraisal Type',
//          ],
//      -> here, master menu is Appraisal
// 4. Every menu should have 'roles_access' key, which will contain an array of role's code
//    and 'section_icon' key should be present, though it can be empty('').

return [
    'user' => [
        'roles_access' => ['admin', 'hr', 'account'],
        'section_icon' => 'paper-sheet',
        'user' => 'User',
    ],
    'appraisal' => [
        'roles_access' => ['admin', 'hr'],
        'section_icon' => 'paper-sheet',
        'appraisal' => 'Appraisal',
        'permissions' => 'appraisal-access'
    ],
    'salary-dependency' => [
        'roles_access' => ['admin', 'account', 'hr'],
        'section_icon' => 'pencil-ruler',
        'salary-dependency' => 'Salary Dependency',
    ],
    'prep-salary' => [
        'roles_access' => ['admin', 'account'],
        'section_icon' => 'diamond',
        'prep-salary' => 'Prepare Salary',
    ],
    'salary-transfer' => [
        'roles_access' => ['admin', 'account'],
        'section_icon' => 'pencil-ruler',
        'salary-transfer' => 'Salary Transfer',
    ],
    'bank-transfer' => [
        'roles_access' => ['admin', 'account'],
        'section_icon' => 'pencil-ruler',
        'bank-transfer' => 'Bank Transfer',
    ],
    'bank/due-payments' => [
        'roles_access' => ['admin', 'account'],
        'section_icon' => 'paper-sheet',
        'bank/due-payments' => 'Due Payments',
    ],
    'bank/payment-transfers' => [
        'roles_access' => ['admin', 'account'],
        'section_icon' => 'paper-sheet',
        'bank/payment-transfers' => 'Payment Transfer',
    ],
    'payslips' => [
        'roles_access' => ['admin', 'account', 'hr'],
        'section_icon' => 'pencil-ruler',
        'payslips' => 'Payslips',
    ],
    'loans' => [
        'roles_access' => ['admin', 'account', 'hr'],
        'section_icon' => 'pencil-ruler',
        'loans' => 'Loans',
        'permissions' => 'loan-management'
    ],
    'onsite-allowance' => [
        'roles_access' => ['admin', 'account', 'hr'],
        'section_icon' => 'pencil-ruler',
        'onsite-allowance' => 'Onsite Allowance',
    ],
    'vpf' => [
        'roles_access' => ['admin', 'account', 'hr'],
        'section_icon' => 'pencil-ruler',
        'vpf' => 'VPF',
    ],
    'nested' => [
        'roles_access' => ['admin'],
        'section_icon' => 'paper-sheet',
        'section_name' => 'Settings',
        'role' => 'Role',
        'permission' => 'Permission',
        'appraisal/appraisal-type' => 'Appraisal Type',
        'appraisal/appraisal-component-type' => 'Appraisal Component Type',
        'appraisal/appraisal-bonus-type' => 'Appraisal Bonus Type',
        'bank/bank-account-type' => 'Account Type',
        'bank/bank-account-meta-key' => 'Account Meta',
        'prep-salary/prep-salary-component-type' => 'Prepare Salary Component Type',
        'bank/bank-account' => 'User Account',
        'adhoc-payment-component' => 'Adhoc Payment Component',
        'calendar-leave-credit' => 'Calendar Leave Credit',
        'system-setting' => 'System Settings'
    ],
];
