<?php

return [
    /**
     * Loan Request Reminder Slack channel Webhook
     */
    'loan_request_webhook' => env('LOAN_REQUEST_SLACK_CHANNEL'),


    /*
    *Loan Request Reminder Loans base url
    */
    'loan_request_base_url' => 'http://geekyants-portal.local.geekydev.com/admin/',

];
