<?php
namespace App\Services\Bonus;

use App\Models\Appraisal\Appraisal;
use App\Models\Bonus\Bonus;
use App\Models\Bonus\OnSiteAllowance;
use App\Models\DateTime\NonWorkingCalendar;
use App\Models\Leave\Leave;
use App\Models\Users\User;

class BonusService
{
    public static function getDueBonues($userId,$monthId = null)
    {
        if($monthId)
        {
            $duesOverviews = Bonus::with('reference', 'bonusConfirm', 'salaryProcess')->where('month_id', $monthId)->where('user_id', $userId)->where('status', 'approved')->where('transaction_id', null)->orderBy('type')->get();
           
        }
        else
            $duesOverviews = Bonus::with('reference', 'bonusConfirm', 'salaryProcess')->where('user_id', $userId)->where('status', 'approved')->where('transaction_id', null)->orderBy('type')->get();
            // $unpaidBonuses = Bonus::with('reference')->where('user_id', $userId)->where('transaction_id', null)->orderBy('type')->get();
        // $paidBonuses = Bonus::with('reference')->where('user_id', $userId)->where('transaction_id', '!=', null)->whereMonth('paid_at', date('m'))->whereYear('paid_at', date('Y'))->orderBy('type')->get();
        // $duesOverviews = $paidBonuses->merge($unpaidBonuses);
        $requestType = "due";
        
        $dueBonues['onsite']['bonuses'] = [];
        $dueBonues['onsite']['days'] = 0;
        $dueBonues['onsite']['amount'] = 0;

        $dueBonues['additional']['bonuses'] = [];
        $dueBonues['additional']['days'] = 0;
        $dueBonues['additional']['amount'] = 0;

        $dueBonues['extra']['bonuses'] = [];
        $dueBonues['extra']['hours'] = 0;
        $dueBonues['extra']['amount'] = 0;

        $dueBonues['performance']['bonuses'] = [];
        $dueBonues['performance']['days'] = 0;
        $dueBonues['performance']['amount'] = 0;

        $dueBonues['techtalk']['bonuses'] = [];
        $dueBonues['techtalk']['days'] = 0;
        $dueBonues['techtalk']['amount'] = 0;

        $dueBonues['referral']['bonuses'] = [];
        $dueBonues['referral']['days'] = 0;
        $dueBonues['referral']['amount'] = 0;

        foreach ($duesOverviews as $duesOverview) {

            if ($duesOverview->reference_type == 'App\Models\Admin\OnSiteBonus') {

                $onSiteTemp = [];
                $onSiteTemp = $duesOverview;
                if ($duesOverview->reference && $duesOverview->reference->onsite_allowance_id) {$onSiteObj = OnSiteAllowance::with('project', 'approver')->find($duesOverview->reference->onsite_allowance_id);
                    $onSiteTemp['rate'] = $onSiteObj->amount;
                    $onSiteTemp['project'] = $onSiteObj->project->project_name;
                    $onSiteTemp['approver'] = $onSiteObj->approver->name;
                    $onSiteTemp['description'] = "From " . date('d M,Y', strtotime($onSiteObj->start_date)) . ' to ' . date('d M,Y', strtotime($onSiteObj->end_date));
                    if (NonWorkingCalendar::isWeekend($duesOverview->date)) {
                        $onSiteTemp['weekend'] = true;
                    } elseif (NonworkingCalendar::is_holiday($duesOverview->date)) {
                        $onSiteTemp['holiday'] = true;
                    } else {
                        $extraTemp['on_leave'] = Leave::isOnLeaveToday($duesOverview->user_id, $duesOverview->date);
                    }
                    $dueBonues['onsite']['amount'] += $duesOverview->amount;
                    $dueBonues['onsite']['draft_amount'] = $onSiteObj->amount;
                }
                $dueBonues['onsite']['days']++;
                $dueBonues['onsite']['bonuses'][] = $onSiteTemp;
            } elseif ($duesOverview->reference_type == 'App\Models\Admin\AdditionalWorkDaysBonus') {
                $additionalTemp = [];
                $additionalTemp = $duesOverview;
                $additionalAppraisal = Appraisal::getBonusAmount($duesOverview->user_id, $duesOverview->date);
                $additionalTemp['rate'] = $additionalAppraisal;

                $additionalTemp['approver'] = $duesOverview->reference->approved_by;
                $appraisalObj = Appraisal::getAppraisalByDate($duesOverview->user_id, $duesOverview->date);
                $additionalTemp['annual_gross_salary'] = null;
                if ($appraisalObj) {

                    $additionalTemp['annual_gross_salary'] = $appraisalObj->getAnnualGrossSalary();
                }

                $reference = $duesOverview->reference;
                if ($reference && $reference->redeem_type == 'encash') {
                    $additionalTemp['description'] = 'Encash | Hours : ' . $reference->duration;
                    $dueBonues['additional']['amount'] += $duesOverview->amount;
                } elseif ($reference && $reference->redeem_type == 'comp-off') {
                    $additionalTemp['description'] = 'Paid Leave';
                }
                $additionalTemp['weekend'] = false;
                $additionalTemp['holiday'] = false;
                $additionalTemp['on_leave'] = false;
                if (NonworkingCalendar::isWeekend($duesOverview->date)) {
                    $additionalTemp['weekend'] = true;
                } elseif (NonworkingCalendar::is_holiday($duesOverview->date)) {
                    $additionalTemp['holiday'] = true;
                } else {
                    $extraTemp['on_leave'] = Leave::isOnLeaveToday($duesOverview->user_id, $duesOverview->date);
                }
                // }

                $dueBonues['additional']['draft_amount'] = $reference->amount;

                $dueBonues['additional']['days']++;
                $dueBonues['additional']['bonuses'][] = $additionalTemp;
            } elseif ($duesOverview->reference_type == 'App\Models\User\UserTimesheetExtra') {
                $extraTemp = [];
                $extraTemp = $duesOverview;
                $extraRate = Appraisal::getBonusAmount($duesOverview->user_id, $duesOverview->date);
                $extraTemp['rate'] = round($extraRate / 8);
                // $extraTemp['approver'] = $duesOverview->referance->approved_by;
                $appraisalObj = Appraisal::getAppraisalByDate($duesOverview->user_id, $duesOverview->date);
                $extraTemp['annual_gross_salary'] = null;
                if ($appraisalObj) {

                    $extraTemp['annual_gross_salary'] = $appraisalObj->getAnnualGrossSalary();
                }
                if (NonworkingCalendar::isWeekend($duesOverview->date)) {
                    $extraTemp['weekend'] = true;
                } elseif (NonworkingCalendar::is_holiday($duesOverview->date)) {
                    $extraTemp['holiday'] = true;
                } else {
                    $extraTemp['on_leave'] = Leave::isOnLeaveToday($duesOverview->user_id, $duesOverview->date);
                }
                $reference = $duesOverview->reference;

                if ($reference) {
                    if ($reference->redeem_type == 'encash') {
                        $additionalTemp['description'] = 'Encash | Hours : ' . $reference->duration;
                        $dueBonues['additional']['amount'] += $duesOverview->amount;
                    } elseif ($reference->redeem_type == 'comp-off') {
                        $additionalTemp['description'] = 'Paid Leave';
                    }

                    $approver = User::find($reference->approved_by);
                    if ($approver) {
                        $dueBonues['extra']['approver'] = $approver->name;
                    }
                    $extraTemp['redeem_type'] = $reference->extra_hours;
                    $extraTemp['description'] = 'Extra Hours ' . $reference->extra_hours;
                    $dueBonues['extra']['draft_amount'] = $reference->amount;

                    if ($reference->extra_hours) {
                        $dueBonues['extra']['hours'] += $reference->extra_hours;

                    }
                }
                
                $dueBonues['extra']['amount'] += $duesOverview->amount;

                $dueBonues['extra']['bonuses'][] = $extraTemp;
            } elseif ($duesOverview->reference_type == 'App\Models\Admin\PerformanceBonus') {
                $performanceTemp = [];
                $performanceTemp = $duesOverview;
                //$extraRate = Appraisal::getOneDaySalary($duesOverview->user_id, $duesOverview->date);
                $performanceTemp['rate'] = 'NA';
                $performanceTemp['approver'] = $duesOverview->approver?? '';
                $appraisalObj = Appraisal::getAppraisalByDate($duesOverview->user_id, $duesOverview->date);
                $performanceTemp['annual_gross_salary'] = null;
                if ($appraisalObj) {
                    $performanceTemp['annual_gross_salary'] = $appraisalObj->getAnnualGrossSalary();
                }
                if (NonworkingCalendar::isWeekend($duesOverview->date)) {
                    $performanceTemp['weekend'] = true;
                } elseif (NonworkingCalendar::is_holiday($duesOverview->date)) {
                    $performanceTemp['holiday'] = true;
                } else {
                    $performanceTemp['on_leave'] = Leave::isOnLeaveToday($duesOverview->user_id, $duesOverview->date);
                }
                if ($duesOverview->bonusRequest != null && !empty($duesOverview->bonusRequest)) {
                    $redeem_type = json_decode($duesOverview->bonusRequest->redeem_type);

                    if ($redeem_type != null && isset($redeem_type->amount)) {
                        //$performanceTemp['redeem_type'] = $redeem_type->hours;
                        $performanceTemp['description'] = 'Performance Bonus' . $redeem_type->amount;
                        $dueBonues['performance']['amount'] += $redeem_type->amount;
                    }
                }
                $dueBonues['performance']['days']++;
                $dueBonues['performance']['bonuses'][] = $performanceTemp;

            }
            elseif ($duesOverview->reference_type == 'App\Models\Admin\TechTalkBonusesUser') {
                $dueBonues['techtalk']['draft_amount'] = $duesOverview->amount;
                $dueBonues['techtalk']['days']++;
                $dueBonues['techtalk']['bonuses'][] = $duesOverview;
                $dueBonues['techtalk']['amount'] = $duesOverview->amount;
            }
            elseif ($duesOverview->reference_type == 'App\Models\Admin\ReferralBonus') {
                $dueBonues['referral']['draft_amount'] = $duesOverview->amount;
                $dueBonues['referral']['bonuses'][] = $duesOverview;
                $dueBonues['referral']['amount'] = $duesOverview->amount;
            }

        }
        return $dueBonues;
    }
}
