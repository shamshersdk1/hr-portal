<?php
namespace App\Services\DateTime;

use App\Models\DateTime\Calendar;

class CalendarService
{
    public static function calculateWorkingDays($startDate, $endDate)
    {
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $count = 0;
        $nonWorkingDays = [6, 7];
        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('type', 'Holiday')->pluck('date')->toArray();
        for ($date = $startDate; strtotime($date) <= strtotime($endDate); $date = date('Y-m-d', strtotime($date . '+1 day'))) {
            if (in_array(date('N', strtotime($date)), $nonWorkingDays) || in_array(date('Y-m-d', strtotime($date)), $holidays)) {
                continue;
            } else {
                $count++;
            }
        }
        return $count;
    }
    public static function getWorkingDays($month, $year)
    {

        if (null == ($year)) {
            $year = date("Y", time());
        }

        if (null == ($month)) {
            $month = date("m", time());
        }

        $startDate = date('Y-m-d', strtotime($year . '-' . $month . '-01'));
        $endDate = date("Y-m-t", strtotime($startDate));

        $count = self::calculateWorkingDays($startDate, $endDate);

        return $count;
    }
}
