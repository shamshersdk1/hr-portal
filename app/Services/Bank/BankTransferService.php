<?php namespace App\Services\Bank;

use App\Models\File\File;
use App\Models\File\FileContent;
use App\Models\BankTransfer\BankTransferUser;
use App\Services\File\FileService;
use Excel;
use App\Exports\ExcelImport;
use Illuminate\Support\Facades\Storage;

class BankTransferService
{
    public static function uploadFile($file, $id)
    {
        $response = ['status' => false, 'message' => "", 'data' => ""];
        $fileSize = $file->getClientSize();
        if ($fileSize > 20000000) {
            $response['message'] = "Filesize exceeds 20MB";
            return $response;
        }

        $content = file_get_contents($file->getRealPath());
        $type = $file->getClientOriginalExtension();
        if (substr($type, 0, 2) !== "xl") {
            $response['message'] = "File is not in .XLxx format";
            return $response;
        }

        $fileName = FileService::getFileName($type);
        $originalName = $file->getClientOriginalName();
        $existingFile = File::where('reference_type', 'App\Models\Audited\BankTransfer')->where('reference_id', $id)->first();
        if ($existingFile) {
            $fileContent = FileContent::where('file_id', $existingFile->id)->first();
            if ($fileContent) {
                $fileContent->delete();
            }
            $existingFile->delete();
        }
        $store = FileService::uploadFileInDB($content, $originalName, $type, $id, "App\Models\Audited\BankTransfer");

        return ['status' => true, 'message' => "File Added", 'data' => $store->id];
    }

    public static function transferData($file, $bankTransferId)
    {
        $response = [];
        $response['status'] = true;
        $response['message'] = "";

        if (!$file) {
            $response['status'] = false;
            $response['message'] = "File invalid!";
            return $response;
        }
        $array = Excel::toArray(new ExcelImport, request()->file('file'));
        foreach ($array[0] as $index => $parse) {
            if($index < 7)
                continue;
            if($parse){
                $desciptionArr = explode("/", $parse[5]);
                $transactionId = isset($parse[1]) ? $parse[1] : null;
                $transactionValueDate = isset($parse[2]) ? $parse[2] : null;
                $transactionPostedDate = isset($parse[3]) ? $parse[3] : null;
                $transactionChequeNo = isset($parse[4]) ? $parse[4] : null;
                $transactionDescription = isset($parse[5]) ? $parse[5] : null;
                $transactionType = isset($parse[6]) ? $parse[6] : null;
                $transactionAmount = isset($parse[7]) ? $parse[7] : null;
                $transactionBalance = isset($parse[8]) ? $parse[8] : null;
                $transactionAccount = isset($desciptionArr[2]) ? $desciptionArr[2] : null;
                if($transactionAccount)
                {
                    $bankTransferUserObj = BankTransferUser::where('bank_acct_number', $transactionAccount)->where('bank_transfer_id', $bankTransferId)->first();
                    if ($bankTransferUserObj) {
                        $bankTransferUserObj->comment = $transactionDescription;
                        $bankTransferUserObj->txn_posted_date = $transactionPostedDate;
                        $bankTransferUserObj->bank_transaction_id = $transactionId;
                        $bankTransferUserObj->transaction_amount = $transactionAmount;
                        $bankTransferUserObj->mode_of_transfer = $desciptionArr[1] ? $desciptionArr[1] : null;

                        if (!$bankTransferUserObj->save()) {
                            $response['status'] = false;
                            $response['message'] = $response['message'].$bankTransferUserObj->getErrors();
                        }
                    } else {
                        $response['status'] = false;
                        $response['message'] = $response['message']."Error Parsing in File at line #".$index;
                    }
                }
            }
        }
        return $response;
    }
}
