<?php
namespace App\Services\Bank;

use App\Models\Users\User;

class BankService
{
    public static function getReferenceForArray()
    {
        return ['User', 'Company', 'Vendor'];
    }
    public static function getReferenceType($value)
    {
        if ($value == 'User') {
            return 'App\Models\Users\User';
        } elseif ($value == 'Company') {
            return 'App\Models\Company';
        } elseif ($value == 'Vendor') {
            return 'App\Models\Vendor';
        } else {
            return '';
        }
    }
    public static function getNameFromReferenceType($value)
    {
        if ($value == 'App\Models\Users\User') {
            return 'User';
        } elseif ($value == 'App\Models\Company') {
            return 'Company';
        } elseif ($value == 'App\Models\Vendor') {
            return 'Vendor';
        } else {
            return '';
        }
    }
    public static function getReferenceKeysArray($value)
    {
        $userObj = User::where('is_active', 1)->get();
        if ($value == 'User') {
            return $userObj;
        } elseif ($value == 'Company') {
            return $userObj;
        } elseif ($value == 'Vendor') {
            return $userObj;
        } else {
            return [];
        }
    }
}
