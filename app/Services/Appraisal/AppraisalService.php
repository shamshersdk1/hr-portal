<?php
namespace App\Services\Appraisal;

use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\{AppraisalBonusType , AppraisalBonus};
use App\Models\DateTime\FinancialYear;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use App\Models\FixedBonus\FixedBonus;
use App\Models\Salary\PrepCurrentGross;
use App\Models\Salary\PrepGrossPaid;
use App\Models\Salary\PrepGrossToBePaid;
use App\Models\Salary\PrepSalary;
use App\Models\Salary\PrepTds;
use App\Models\VariablePay\VariablePay;
use App\Services\DateTime\CalendarService;
use App\Services\Leave\UserLeaveService;
use App\Models\Salary\PrepItSaving;
use App\Models\Appraisal\PrepAppraisal;
use App\Notifications\UserInfoUpdateNotification;
use OwenIt\Auditing\Models\Audit;
use App\Models\Users\User;
use Carbon\Carbon;



class AppraisalService
{
    public static function getCurrentMonthAppraisal($userId, $monthId)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = "";

        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            $response['status'] = false;
            $response['errors'] = "Month not found!";
            return $response;
        }

        $firstDay = $monthObj->getFirstDay();
        $lastDay = $monthObj->getLastDay();

        $appraisalList = [];

        $appraisalObj = Appraisal::where('user_id', $userId)->where('effective_date', '>=', $firstDay)->where('effective_date', '<=', $lastDay)->orderBy('effective_date', 'ASC')->get();
        if ($appraisalObj == null || !isset($appraisalObj[0])) {
            $appraisalList = Appraisal::where('user_id', $userId)->where('effective_date', '<', $firstDay)->orderBy('effective_date', 'DESC')->limit(1)->get();
        } elseif ($appraisalObj[0]->effective_date != $firstDay) {
            $appraisalPrevObj = Appraisal::where('user_id', $userId)->where('effective_date', '<', $appraisalObj[0]->effective_date)->orderBy('effective_date', 'DESC')->limit(1)->get();
            $appraisalList = $appraisalPrevObj->merge($appraisalObj);
        } else {
            $appraisalList = $appraisalObj;
        }

        return $appraisalList;
    }

    public static function generateCurrentData($appraisalList, $monthId, $userId)
    {
        $data = [];

        $monthlyGross = [];
        $monthlyGrossSalary = 0;

        $monthObj = Month::find($monthId);

        $firstDay = $monthObj->getFirstDay();
        $lastDay = $monthObj->getLastDay();

        $totalBonus = 0;

        $noOfWorking = 0;

        foreach ($appraisalList as $key => $appraisal) {
            //getting start date of appraisal
            if ($appraisal->effective_date < $firstDay) {
                $data['start_date'][$appraisal->id] = $firstDay;
            } else {
                $data['start_date'][$appraisal->id] = $appraisal->effective_date;
            }
            //getting end date of appraisal
            if ($appraisalList[$key] == null || !isset($appraisalList[$key])) {
                $data['end_date'][$appraisal->id] = $lastDay;
            } else {
                $dt = date('Y-m-d', strtotime($appraisalList[$key]->effective_date . ' -1 day'));
                $data['end_date'][$appraisal->id] = $dt;
            }
            //no of working days in appraisal
            $data['no_of_working'][$appraisal->id] = UserLeaveService::calculateWorkingDays($data['start_date'][$appraisal->id], $data['end_date'][$appraisal->id], $userId);
            $noOfWorking += $data['no_of_working'][$appraisal->id];

            foreach ($appraisal->appraisalComponent as $component) {
                $value = ($component->value / $monthObj->getWorkingDays()) * $data['no_of_working'][$appraisal->id];
                // $data[$monthObj->id][$appraisal->id][$component->appraisalComponentType->code] = $value;
                $monthlyGross[$component->appraisalComponentType->id] = $monthlyGross[$component->appraisalComponentType->id] ?? 0 + $value;
                if ($component->appraisalComponentType->code == 'basic' || $component->appraisalComponentType->code == 'lta' || $component->appraisalComponentType->code == 'hra' || $component->appraisalComponentType->code == 'car-allowance' || $component->appraisalComponentType->code == 'food-allowance' || $component->appraisalComponentType->code == 'special-allowance' || $component->appraisalComponentType->code == 'stipend') {
                    $monthlyGrossSalary += $value;
                }
            }
        }

        $variableBonusSetting = MonthSetting::where('month_id', $monthId)->where('key', 'variable-pay')->first();
        if ($variableBonusSetting && $variableBonusSetting->value == 'locked') {

            $variableAppraisalBonuses = VariablePay::where('user_id', $userId)->where('month_id', $monthId)->get();

            if ($variableAppraisalBonuses && isset($variableAppraisalBonuses)) {
                foreach ($variableAppraisalBonuses as $appraisalBonus) {

                    foreach ($appraisalBonus->lastMonthPayComponents as $bonus) {
                        $type = AppraisalBonusType::where('code', $bonus->key)->first();
                        if ($type) {
                            if (isset($monthlyGross['appraisal_bonus'][$type->id])) {
                                $monthlyGross['appraisal_bonus'][$type->id] += $bonus->value;
                            } else {
                                $monthlyGross['appraisal_bonus'][$type->id] = $bonus->value;
                            }

                            $totalBonus += $bonus->value;
                        }
                    }
                }
            }
        }

        $fixedBonusSetting = MonthSetting::where('month_id', $monthId)->where('key', 'fixed-bonus')->first();
        if ($fixedBonusSetting && $fixedBonusSetting->value == 'locked') {
            $fixedAppraisalBonuses = FixedBonus::where('user_id', $userId)->where('month_id', $monthId)->get();
            if ($fixedAppraisalBonuses && isset($fixedAppraisalBonuses)) {
                foreach ($fixedAppraisalBonuses as $appraisalBonus) {
                    foreach ($appraisalBonus->monthlyDeductionComponents as $bonus) {
                        $type = AppraisalBonusType::where('code', $bonus->key)->first();
                        if ($type) {
                            if (isset($monthlyGross['appraisal_bonus'][$type->id])) {
                                $monthlyGross['appraisal_bonus'][$type->id] += $bonus->value;
                            } else {
                                $monthlyGross['appraisal_bonus'][$type->id] = $bonus->value;
                            }

                            $totalBonus += $bonus->value;
                        }
                    }
                }
            }
        }

        $monthlyGross['no_of_working'] = $noOfWorking;
        $monthlyGross['monthly_gross_salary'] = $monthlyGrossSalary;
        $monthlyGross['monthly_bonus'] = $totalBonus;
        // $data['monthly_gross_salary'] = $monthlyGrossSalary;
        return $monthlyGross;
    }

    public static function calculateFutureAppraisal($appraisalId, $monthId)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = "";

        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            $response['status'] = false;
            $response['errors'] = "Month not found!";
            return $response;
        }

        $monthList = [];
        $monthList = FinancialYear::getUpcomingMonth($monthObj->financial_year_id);

        $data = [];

        $appraisal = Appraisal::find($appraisalId);
        $annualGross = 0;

        foreach ($monthList as $month) {
            $monthlyGrossSalary = 0;
            $totalBonus = 0;

            $firstDate = date('Y-m-01', strtotime($month->year . '-' . $month->month . '-01'));
            $last = date('Y-m-d', strtotime($month->year . '-' . $month->month . '-01'));
            $lastDate = date("Y-m-t", strtotime($last));

            $data['start_date'][$month->month] = $firstDate;
            $data['end_date'][$month->month] = $lastDate;
            $data['no_of_working'][$month->month] = CalendarService::calculateWorkingDays($data['start_date'][$month->month], $data['end_date'][$month->month]);

            foreach ($appraisal->appraisalComponent as $component) {
                $value = $component->value;
                $data[$month->month][$component->appraisalComponentType->id] = $value;

                if ($component->appraisalComponentType->code == 'basic' || $component->appraisalComponentType->code == 'lta' || $component->appraisalComponentType->code == 'hra' || $component->appraisalComponentType->code == 'car-allowance' || $component->appraisalComponentType->code == 'food-allowance' || $component->appraisalComponentType->code == 'special-allowance' || $component->appraisalComponentType->code == 'stipend') {
                    $monthlyGrossSalary += $value;
                }
            }
            foreach ($appraisal->appraisalBonus as $bonus) {
                if (!isset($monthlyGross['appraisal_bonus'][$bonus->appraisalBonusType->id])) {
                    $data[$month->month]['appraisal_bonus'][$bonus->appraisalBonusType->id] = 0;
                }
                if ($bonus->value_date != null) {
                    if ($bonus->value_date >= $firstDate && $bonus->value_date <= $lastDate) {
                        $value = $bonus->value;
                        $data[$month->month]['appraisal_bonus'][$bonus->appraisalBonusType->id] += $value;
                        $totalBonus += $value;
                    }
                }
                // else {
                //     $value = $bonus->value;
                //     $data[$month->month]['appraisal_bonus'][$bonus->appraisalBonusType->id] += $value;
                //     $totalBonus += $value;
                // }
            }

            $data[$month->month]['monthly_gross_salary'] = $monthlyGrossSalary;
            $data[$month->month]['monthly_bonus'] = $totalBonus;

            $annualGross += $data[$month->month]['monthly_gross_salary'];
        }
        $data['annual_gross_salary'] = $annualGross;

        return $data;
    }

    public static function getComparedPrepComponents($currPrepSalId, $prevPrepSalId, $url)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = "";

        $prepSalObj = PrepSalary::find($currPrepSalId);
        if (!$prepSalObj) {
            $response['status'] = false;
            $response['errors'] = 'Current Prep Salary Id Not Found!';
            return $response;
        }
        $prevPrepSalObj = PrepSalary::find($prevPrepSalId);
        if (!$prevPrepSalObj) {
            $response['status'] = false;
            $response['errors'] = 'Prev Prep Salary Id Not Found!';
            return $response;
        }

        $prevData = null;
        $currData = null;
        if ($url == 'prep-salary/paid-comparison/{prepSalId}') {
            $prevData = PrepGrossPaid::with('items')->where('prep_salary_id', $prevPrepSalId)->groupBy('user_id')->get();
            $currData = PrepGrossPaid::with('items')->where('prep_salary_id', $currPrepSalId)->groupBy('user_id')->get();
        } elseif ($url == 'prep-salary/current-gross-comparison/{prepSalId}') {
            $prevData = PrepCurrentGross::with('items')->where('prep_salary_id', $prevPrepSalId)->groupBy('user_id')->get();
            $currData = PrepCurrentGross::with('items')->where('prep_salary_id', $currPrepSalId)->groupBy('user_id')->get();
        } elseif ($url == 'prep-salary/future-pay-comparison/{prepSalId}') {
            $prevData = PrepGrossToBePaid::with('items')->where('prep_salary_id', $prevPrepSalId)->groupBy('user_id')->get();
            $currData = PrepGrossToBePaid::with('items')->where('prep_salary_id', $currPrepSalId)->groupBy('user_id')->get();
        } elseif ($url == 'prep-salary/tds-comparison/{prepSalId}') {
            $prevData = PrepTds::with('items')->where('prep_salary_id', $prevPrepSalId)->groupBy('user_id')->get();
            $currData = PrepTds::with('items')->where('prep_salary_id', $currPrepSalId)->groupBy('user_id')->get();
        } elseif ($url == 'prep-salary/it-saving-comparison/{prepSalId}') {
            $prevData = PrepItSaving::with('items')->where('prep_salary_id', $prevPrepSalId)->groupBy('user_id')->get();
            $currData = PrepItSaving::with('items')->where('prep_salary_id', $currPrepSalId)->groupBy('user_id')->get();
        } elseif ($url == 'prep-salary/appraisal-comparison/{prepSalId}') {
            $prevData = PrepAppraisal::with('items')->where('prep_salary_id', $prevPrepSalId)->groupBy('user_id')->get();
            $currData = PrepAppraisal::with('items')->where('prep_salary_id', $currPrepSalId)->groupBy('user_id')->get();
        }

        $keys = [];
        $data = [];
        foreach ($currData as $curr) {
            if ($curr->items) {
                foreach ($curr->items as $item) {
                    if (!isset($keys[$item->key])) {
                        $keys[$item->key] = $item->key;
                    }
                    $data[$curr->user_id][$item->key]['curr'] = $item->value;
                }
            }
        }
        foreach ($prevData as $prev) {
            if ($prev->items) {
                foreach ($prev->items as $item) {
                    if (!isset($keys[$item->key])) {
                        $keys[$item->key] = $item->key;
                    }
                    $data[$prev->user_id][$item->key]['prev'] = $item->value;
                }
            }
        }

        $data['keys'] = $keys;
        $response['data'] = $data;
        return $response;
    }

    public static function sendmailOnAppraisalUpdate($id,$code)
    {   

        $audit = Audit::find($id);
        $user = User::find($audit->user_id);

        $title = "The title of your message";
        $old_values = [$code => $audit->old_values['value'],$code.' effective_date' => $audit->old_values['value_date']?? null];
        $new_values = [$code => $audit->new_values['value'],$code.' effective_date' => $audit->new_values['value_date']?? null];

        if(count($new_values)>0){
            $user->notify((new UserInfoUpdateNotification($title,$old_values,$new_values,$user))->delay(Carbon::now()->addSeconds(10)));
        }
    }

    public static function getAudits($start, $end)
    {
        
        // $toDate = date('Y-m-d');
        // $appraisalAudits = Audit::orderBy('created_at')->whereBetween('created_at', [$fromDate.' 00:00:00', $toDate.' 06:00:00'])->whereIn('auditable_type',['App\Models\Appraisal\Appraisal','App\Models\Appraisal\AppraisalComponent','App\Models\Appraisal\AppraisalBonus'])->get();
        $appraisalAudits = Audit::with('auditable')->orderBy('created_at')->whereBetween('updated_at',[$start,$end])->whereIn('auditable_type',['App\Models\Appraisal\Appraisal','App\Models\Appraisal\AppraisalComponent','App\Models\Appraisal\AppraisalBonus'])->get();
      //  $appraisalAudits = Audit::with('auditable')->orderBy('created_at')->whereDate('updated_at','<=',$end)->whereIn('auditable_type',['App\Models\Appraisal\Appraisal','App\Models\Appraisal\AppraisalComponent','App\Models\Appraisal\AppraisalBonus'])->get();
        $data = [];
        foreach($appraisalAudits as $audit)
        {
            $userId = NULL;
            $temp['appraisal_id']= $audit->auditable_id;
            if($audit->auditable_type != 'App\Models\Appraisal\Appraisal') {
                $userId = $audit->auditable->appraisal->user_id;
                $temp['appraisal_id']= $audit->auditable->appraisal->id;
            } else {
                $userId = $audit->auditable->user_id;
            }
            
            $temp['new_values']= $audit->new_values;
            $temp['old_values']= $audit->old_values;
            $data[$userId][] = $temp;
        }    
   
        return $data;
    }

    public static function getFixedBonusNotificationData($monthId){
        $appraisalBonusTypes = AppraisalBonusType::all();
        $fixedBonuses = FixedBonus::whereMonth_id($monthId)->get();
        $data = [];
        foreach($fixedBonuses as $fixedBonus)
        {
            foreach($fixedBonus->monthlyDeductionComponents as $component)
            {
                $appraisalBonusValue = AppraisalBonus::find($component->appraisal_bonus_id);
                if($component->value != $appraisalBonusValue->value)
                {
                    $data[$fixedBonus->user_id]['user_name'] = $fixedBonus->user->name;
                    $data[$fixedBonus->user_id]['employee_id'] = $fixedBonus->user->employee_id;
                    foreach($appraisalBonusTypes as $type)
                    {
                        $data[$fixedBonus->user_id][$type->code."_current"] = '';
                        $data[$fixedBonus->user_id][$type->code."_actual"] = '';
                    }
                    $data[$fixedBonus->user_id][$component->key."_current"] = $component->value;
                    $data[$fixedBonus->user_id][$component->key."_actual"] = $appraisalBonusValue->value;  
                }
            }
        }
        return $data;
    }

    public static function getVariablePayNotificationData($monthId){
        $appraisalBonusTypes = AppraisalBonusType::all();
        $variableBonuses = VariablePay::whereMonth_id($monthId)->get();
        $data = [];
        foreach($variableBonuses as $variableBonus)
        {
            foreach($variableBonus->lastMonthPayComponents as $component)
            {
                $appraisalBonusValue = AppraisalBonus::find($component->appraisal_bonus_id);
                if($component->value != $appraisalBonusValue->value/12)
                {
                    $data[$variableBonus->user_id]['user_name'] = $variableBonus->user->name;
                    $data[$variableBonus->user_id]['employee_id'] = $variableBonus->user->employee_id;
                    foreach($appraisalBonusTypes as $type)
                    {
                        $data[$variableBonus->user_id][$type->code."_current"] = '';
                        $data[$variableBonus->user_id][$type->code."_actual"] = '';
                    }
                    $data[$variableBonus->user_id][$component->key."_current"] = $component->value;
                    $data[$variableBonus->user_id][$component->key."_actual"] = $appraisalBonusValue->value/12;  
                }
            }
        }
        return $data;
    }
    
}
