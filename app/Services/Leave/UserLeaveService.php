<?php
namespace App\Services\Leave;

use App\Models\DateTime\Calendar;
use App\Models\Users\User;
use Auth;
use DB;
use Exception;
use Log;
use App\Models\Leave\LeaveType;
use App\Models\DateTime\CalendarYear;
use App\Models\Leave\UserLeaveTransaction;

class UserLeaveService
{
    public static function calculateWorkingDays($startDate, $endDate, $id)
    {
        $user = User::find($id);
        $count = 0;
        if (!empty($user->workingDayGroup) && !empty($user->workingDayGroup->workingDays)) {
            $group = $user->workingDayGroup->workingDays;
            $nonWorkingDays = [];
            if (!empty($group)) {
                if (!$group->monday) {
                    $nonWorkingDays[] = 1;
                }
                if (!$group->tuesday) {
                    $nonWorkingDays[] = 2;
                }
                if (!$group->wednesday) {
                    $nonWorkingDays[] = 3;
                }
                if (!$group->thursday) {
                    $nonWorkingDays[] = 4;
                }
                if (!$group->friday) {
                    $nonWorkingDays[] = 5;
                }
                if (!$group->saturday) {
                    $nonWorkingDays[] = 6;
                }
                if (!$group->sunday) {
                    $nonWorkingDays[] = 7;
                }
            } else {
                $nonWorkingDays = [6, 7];
            }
        } else {
            $nonWorkingDays = [6, 7];
        }

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));

        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('type', 'Holiday')->pluck('date')->toArray();
        for ($date = $startDate; strtotime($date) <= strtotime($endDate); $date = date('Y-m-d', strtotime($date . '+1 day'))) {
            if (in_array(date('N', strtotime($date)), $nonWorkingDays) || in_array(date('Y-m-d', strtotime($date)), $holidays)) {
                continue;
            } else {
                $count++;
            }
        }
        return $count;
    }

    public static function getUserAvailableLeaveBalance($calendarYear, $userId)
    {
        $leaveTypes = LeaveType::all();
        $count = 0;
        $leaves = [];
        foreach ($leaveTypes as $type) {
            $id = $type->id;
            $count = self::countAvailableLeaves($calendarYear, $userId, $id);
            $leaves[$type->code] = number_format($count, 1);
        }
        return $leaves;
    }

    public static function countAvailableLeaves($calendarYear, $userId, $leaveTypeId)
    {
        $calendarYear = $calendarYear ?: date('Y');
        $calendarYearObj = CalendarYear::where('year', $calendarYear)->first();
        $calendarYearId = $calendarYearObj->id;
        if ($calendarYearId && $userId && $leaveTypeId) {
            $transactionSum = UserLeaveTransaction::where('calendar_year_id', $calendarYearId)->where('user_id', $userId)->where('leave_type_id', $leaveTypeId)->sum('days');
            return $transactionSum;
        }
        return null;
    }

    public static function addUserLeaves($data)
    {
        if (empty($data)) {
            return false;
        }
        $user = User::find($data['user_id']);
        if (!$user) {
            return false;
        }
        if (empty($data['calendar_year_id'])) {
            $calendarObj = CalendarYear::where('year', date('Y'))->first();
            if (!$calendarObj) {
                return false;
            }
            $data['calendar_year_id'] = $calendarObj->id;
        }
        $carryForward = 0;
        $checkIfExists = UserLeaveTransaction::where('calendar_year_id', $data['calendar_year_id'])->where('user_id', $data['user_id'])->where('reference_type', $data['reference_type'])->where('reference_id', $data['reference_id'])->where('leave_type_id', $data['leave_type_id'])->first();
        if ($data['reference_type'] == 'App\Models\Leave\LeaveCalendarYear' && $checkIfExists) {
            return false;
        }
        return UserLeaveTransaction::saveData($data);
    }
public static function getAllUserRemainingLeaves($yearId, $userId = null, $startDate = null, $endDate = null)
    {
        $response = [];
        $response['status'] = false;
        $response['message'] = '';
        $response['data'] = '';

        $yearObj = CalendarYear::where('id', $yearId)->first();
        if (!$yearObj) {
            $response['status'] = false;
            $response['message'] = 'Calendar Year not found';
            return $response;
        }

        if ($userId == null) {
            if ($startDate != null && $endDate != null) {
                $remainingLeaves = DB::table('user_leave_transactions')
                    ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                    ->where('calendar_year_id', $yearObj->id)
                    ->whereDate('created_at', '>=', $startDate)
                    ->whereDate('created_at', '<=', $endDate)
                    ->groupBy('user_id', 'leave_type_id')
                    ->get();
            } else {
                $remainingLeaves = DB::table('user_leave_transactions')
                    ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                    ->where('calendar_year_id', $yearObj->id)
                    ->groupBy('user_id', 'leave_type_id')
                    ->get();
            }
            foreach ($remainingLeaves as $remainingLeave) {
                $user = User::find($remainingLeave->user_id);
                if (!$user) {
                    $response['status'] = false;
                    $response['message'] = 'User not found';
                    return $response;
                }
                $leaveObj = LeaveType::find($remainingLeave->leave_type_id);
                if (!$leaveObj) {
                    $response['status'] = false;
                    $response['message'] = 'Leave type not found';
                    return $response;
                }
                $leave_code = $leaveObj->code;

                $response[$remainingLeave->user_id]["username"] = $user->name;
                $response[$remainingLeave->user_id]["userId"] = $user->id;
                $response[$remainingLeave->user_id]["employee_id"] = $user->employee_id;
                $response[$remainingLeave->user_id][$leave_code] = number_format($remainingLeave->days, 1);
            }
            $response['status'] = true;

        } else {
            $user = User::find($userId);

            if (!$user) {
                $response['status'] = false;
                $response['message'] = 'User not found';
                return $response;
            }
            if ($startDate != null && $endDate != null) {
                $remainingLeaves = DB::table('user_leave_transactions')
                    ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                    ->where('calendar_year_id', $yearObj->id)
                    ->whereDate('created_at', '>=', $startDate)
                    ->whereDate('created_at', '<=', $endDate)
                    ->where('user_id', $userId)
                    ->groupBy('user_id', 'leave_type_id')
                    ->get();
            } else {
                $remainingLeaves = DB::table('user_leave_transactions')
                    ->select('user_id', 'leave_type_id', DB::raw('SUM(days) as days'))
                    ->where('calendar_year_id', $yearObj->id)
                    ->where('user_id', $userId)
                    ->groupBy('user_id', 'leave_type_id')
                    ->get();
            }

            if (count($remainingLeaves) > 0) {
                foreach ($remainingLeaves as $remainingLeave) {

                    $leaveObj = LeaveType::find($remainingLeave->leave_type_id);

                    if (!$leaveObj) {
                        $response['status'] = false;
                        $response['message'] = 'Leave type not found';
                        return $response;
                    }
                    $leave_code = $leaveObj->code;

                    $response[$remainingLeave->user_id]["username"] = $user->name;
                    $response[$remainingLeave->user_id]["userId"] = $user->id;
                    $response[$remainingLeave->user_id]["employee_id"] = $user->employee_id;
                    $response[$remainingLeave->user_id][$leave_code] = number_format($remainingLeave->days, 1);
                }
            } else {
                $leaves = LeaveType::all();

                foreach ($leaves as $leave) {
                    $response[$userId][$leave->code] = 0;
                }
            }
            $response['status'] = true;
        }
        return $response;
    }

}
