<?php
namespace App\Services\Leave;


use App\Models\DateTime\ { Calendar,CalendarYear} ;
use App\Models\Leave\ { Leave, LeaveCalendarYear};
use App\Models\Users\User;


class NewLeaveService
{
    public static function calculateWorkingDays($startDate, $endDate, $id)
    {
        $user = User::find($id);
        $count = 0;
        if (!empty($user->workingDayGroup) && !empty($user->workingDayGroup->workingDays)) {
            $group = $user->workingDayGroup->workingDays;
            $nonWorkingDays = [];
            if (!empty($group)) {
                if (!$group->monday) {
                    $nonWorkingDays[] = 1;
                }
                if (!$group->tuesday) {
                    $nonWorkingDays[] = 2;
                }
                if (!$group->wednesday) {
                    $nonWorkingDays[] = 3;
                }
                if (!$group->thursday) {
                    $nonWorkingDays[] = 4;
                }
                if (!$group->friday) {
                    $nonWorkingDays[] = 5;
                }
                if (!$group->saturday) {
                    $nonWorkingDays[] = 6;
                }
                if (!$group->sunday) {
                    $nonWorkingDays[] = 7;
                }
            } else {
                $nonWorkingDays = [6, 7];
            }
        } else {
            $nonWorkingDays = [6, 7];
        }
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $holidays = Calendar::whereDate('date', '>=', $startDate)->whereDate('date', '<=', $endDate)->where('type', 'Holiday')->pluck('date')->toArray();
        for ($date = $startDate; strtotime($date) <= strtotime($endDate); $date = date('Y-m-d', strtotime($date . '+1 day'))) {
            if (in_array(date('N', strtotime($date)), $nonWorkingDays) || in_array(date('Y-m-d', strtotime($date)), $holidays)) {
                continue;
            } else {
                $count++;
            }
        }
        return $count;
    }

    /*
    function : getUserWorkingDays
    Returns working days of user considering joining date and release date.
    response : int(working days)
     */
    public static function getUserWorkingDays($startDate,$endDate,$userId)
    {
        $user = User::find($userId);
        if(!$user)
            return false;
        if(($user->joining_date > $startDate) && ($user->joining_date < $endDate))
            $startDate = $user->joining_date;
        if(($user->release_date > $startDate) && ($user->release_date < $endDate))
            $endDate = $user->release_date;
        $days_worked = self::calculateWorkingDays($startDate, $endDate, $userId);
        return $days_worked;
    }

    public static function getTotalLeaves($start_date, $end_date, $user_id, $type)
    {
        $days = Leave::where('user_id', $user_id)->where(function ($query) use ($start_date, $end_date) {
            $query->where('start_date', '<=', $end_date)->orWhere('end_date', '<=', $start_date);
        })->where('status', 'approved');
        if ($type != 'all') {
            $days = $days->where('type', $type);
        }
        $days = $days->sum('days');
        return $days;
    }

    public static function getUserProRataLeaves($userId, $leaveTypeId, $calendarYearId)
    {
        $userObj = User::find($userId);
        if ($userObj) {
            $year = date('Y');
            $calendarYearObj = CalendarYear::find($calendarYearId);
            if (!$calendarYearObj) {
                return false;
            }
            $leaveCalendarYearObj = LeaveCalendarYear::where('calendar_year_id', $calendarYearObj->id)->where('leave_type_id', $leaveTypeId)->first();
            if (!$leaveCalendarYearObj) {
                return false;
            }
            $joiningMonth = date('m', (strtotime($userObj->joining_date)));
            $proRataLeave = ceil(($leaveCalendarYearObj->max_allowed_leave * (13 - $joiningMonth)) / 12);
            return $proRataLeave;
        }
    }

}
