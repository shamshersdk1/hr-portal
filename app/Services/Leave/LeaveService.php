<?php
namespace App\Services\Leave;

use App\Models\Leave\Leave;
use App\Models\Leave\LeaveType;
use DateTime;

class LeaveService
{
    public static function getTotalLeaves($start_date, $end_date, $user_id, $type)
    {
        $count = 0;

        $leaveTypeObj = LeaveType::where('code', $type)->first();

        if (!$leaveTypeObj) {
            return 0;
        }

        if ($type != 'all') {
            $leaves = Leave::where('user_id', $user_id)->where('status', 'approved')->where('leave_type_id', $leaveTypeObj->id)->get();
        } else {
            $leaves = Leave::where('user_id', $user_id)->where('status', 'approved')->get();
        }

        foreach ($leaves as $leave) {
            if ($leave->start_date >= $start_date && $leave->start_date <= $end_date && $leave->end_date >= $start_date && $leave->end_date <= $end_date) {
                $count += $leave->days;
            } elseif ($leave->start_date >= $start_date && $leave->start_date <= $end_date) {
                $datetime1 = new DateTime($end_date);
                $datetime2 = new DateTime($leave->start_date);
                $interval = $datetime1->diff($datetime2);

                $count += $interval->d + 1; //end_date - $leave->start_date;
            } elseif ($leave->end_date >= $start_date && $leave->end_date <= $end_date) {
                $datetime1 = new DateTime($leave->end_date);
                $datetime2 = new DateTime($start_date);
                $interval = $datetime1->diff($datetime2);

                $count += $interval->d + 1; //leave->end_date - $start_date;
            }
        }
        return $count;
    }
}
