<?php namespace App\Services\File;

use App\Models\File\File;
use Redirect;
use Validator;

class FileService
{

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function getFileName($type)
    {

        do {
            $x = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
            $filename = $x . "-" . time() . "." . $type;
        } while (file_exists($filename));

        return $filename;
    }

    public static function getFolder()
    {
        $path = date('Y/m/d/');
        return $path;
    }

    public static function uploadFile($file, $id,$reference_type)
    {
        $response = ['status' => false, 'message' => ""];
        $fileSize = $file->getClientSize();
        if ($fileSize > 20000000) {
            $response['message'] = "Filesize exceeds 20MB";
            return $response;
        }
        $content = file_get_contents($file->getRealPath());
        $type = $file->extension();
        $fileName = FileService::getFileName($type);
        $originalName = $file->getClientOriginalName();
        $store = FileService::uploadFileInDB($content, $originalName, $type, $id, $reference_type);
        return ['status' => true, 'message' => "File Added"];
    }

    public static function uploadFileInDB($content, $original_name, $type, $reference_id, $reference_type)
    {
        // CODE FOR FILE MODEL ENTRY
        if (!$reference_id || !$reference_type  ) {
            return Redirect::back()->withInput()->withErrors(['References are missing']);
        }

        return File::createRecord($content, $original_name, $type, $reference_id, $reference_type);
    }

}
