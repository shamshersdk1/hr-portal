<?php
namespace App\Services\SalaryService;

use App\Jobs\PrepareSalary\Components\LoanJob;
use App\Models\DateTime\Month;
use App\Models\Salary\PrepLoanEmi;
use App\Models\Salary\PrepSalary;
use App\Models\Salary\PrepSalaryComponent;
use App\Models\Salary\PrepSalaryComponentType;
use App\Models\Salary\PrepSalaryExecution;
use App\Models\Salary\PrepUser;
use App\Traits\ComponentDispatch;
use App\Models\Loan\LoanEmi;

class LoanSalaryComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;
    private $userId, $month, $year, $componentId, $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\LoanJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepLoanEmi';
        $this->setPrepTableName($modelName);
    }

    public function getValue()
    {
        return array("Zack" => "Zara", "Anthony" => "Any",
            "Ram" => "Rani", "Salim" => "Sara",
            "Raghav" => "Ravina");
    }

    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }

    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $monthObj = Month::find($componentObj->salary->month_id);
        if ($monthObj->loanSetting ? $monthObj->loanSetting->value == "open" : true) {
            $response['errors'] = "Loan deduction Month not locked";
            $response['status'] = true;
            return $response;
        }
        return $response;
    }

    public function isRequiredPush($user_id, $month_id)
    {
        return LoanEmi::where('user_id',$user_id)->where('month_id',$month_id)->exists();
    }

    public function getHtml()
    {

        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepLoanObj = PrepLoanEmi::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->cursor();

        $response = [];
        if (count($prepLoanObj) > 0) {
            foreach ($prepLoanObj as $loanObj) {
                $response[$loanObj->id]['amount'] = isset($loanObj->loan->amount) ? $loanObj->loan->amount : 0;
                $response[$loanObj->id]['remaining_amount'] = isset($loanObj->loan->remaining) ? $loanObj->loan->remaining : 0;
                $response[$loanObj->id]['emi'] = isset($loanObj->emi_amount) ? $loanObj->emi_amount : 0;

            }
        }
        return $response;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId);
        if (!$prepSalaryComponent) {
            return false;
        }

        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code', 'user')->first();
        if (!$userComponentTypeId) {
            return false;
        }

        $loanComponentType = PrepSalaryComponentType::where('code', 'loan')->first();
        if (!$loanComponentType) {
            return false;
        }

        $loanComponent = $prepSalaryObj->components->where('prep_salary_component_type_id', $loanComponentType->id)->first();
        if (!$loanComponent) {
            return false;
        }

        foreach ($prepSalaryObj->prepUsers as $prepUser) {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->where('component_id', $loanComponent->id)->where('user_id', $prepUser->user_id)->first();
            if ($prepSalaryExecution) {
                if ($prepSalaryExecution->status != "completed") {
                    dispatch(new LoanJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
                }

            } else {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id, 'component_id' => $loanComponent->id, 'user_id' => $prepUser->user_id, 'status' => 'init', 'counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new LoanJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }

    }
}
