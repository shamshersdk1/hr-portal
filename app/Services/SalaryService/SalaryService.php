<?php
namespace App\Services\SalaryService;

use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Bank\Account;
use App\Models\Bank\AccountType;
use App\Models\Bonus\PrepBonus;
use App\Models\DateTime\Month;
use App\Models\Salary\{PrepSalary, PrepSalaryComponent, PrepSalaryComponentType, PrepSalaryExecution, PrepTds, PrepAppraisalBonus};
use App\Models\Transaction;
use App\Models\Users\User;
use App\Services\Payroll\TDSService;
use DB;
use App\Models\VariablePay\VariablePay;
use App\Models\FixedBonus\FixedBonus;
use App\Services\File\FileService;
use App\Models\File\File;
use Excel;
use App\Models\Bank\PaymentTransferAccount;
use App\Exports\ExcelImport;
use App\Models\Deduction\FoodDeduction;
use App\Models\Deduction\VpfDeduction;
use App\Models\Salary\UserSalary;
use App\Services\DateTime\CalendarService;

class SalaryService
{
    public static function checkDependency($prepSalaryComponentId)
    {
        $response['status'] = false;
        $response['errors'] = [];
        $response['dependentOn'] = [];
        $prepSalaryComponent = PrepSalaryComponent::with('type.dependsOn')->find($prepSalaryComponentId);
        if (!$prepSalaryComponent) {
            $response['status'] = false;
            $response['errors'] = 'Prep Salary Component Not Found';
        }
        $userComponentTypeId = PrepSalaryComponentType::where('code', 'user')->first();
        if (!$userComponentTypeId) {
            return false;
        }
        if ($prepSalaryComponent) {
            if ($prepSalaryComponent->type && $prepSalaryComponent->type->dependsOn) {
                foreach ($prepSalaryComponent->type->dependsOn as $dependent) {
                    if ($dependent->dependent_id != $userComponentTypeId->id) {
                        $response['status'] = true;
                        array_push($response['dependentOn'], $dependent->dependent_id);
                    }
                    //  array_push($response['dependentOn'], $dependent->dependent_id);
                    // foreach ($dependent->dependentOn->prepSalaryComponent as $component) {
                    //     if ($prepSalaryComponent->prep_salary_id == $component->prep_salary_id) {
                    //        if(!$dependent->dependentOn->id == $userComponentTypeId->id){
                    //         $response['status'] = true;
                    //         array_push($response['dependentOn'], $dependent->dependentOn->id);
                    //         }
                    //     }
                    // }
                }
            }
        }
        return $response;
    }

    public static function checkExecutionStatus($prepSalaryComponentId, $userId, $prepSalaryId)
    {
        $prepExecutionObj = PrepSalaryExecution::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->where('component_id', $prepSalaryComponentId)->first();
        if (!$prepExecutionObj) {
            return false;
        }

        if ($prepExecutionObj->status != "completed") {
            return false;
        }
        return true;
    }

    public static function checkPreCondition($prepSalaryId)
    {
        $response['status'] = false;
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        $prepUserComponentTypeId = PrepSalaryComponentType::where('code', 'user')->first()->id;
        $prepSalaryComponent = PrepSalaryComponent::where('prep_salary_id', $prepSalaryId)->where('prep_salary_component_type_id', $prepUserComponentTypeId)->first();
        if (!$prepSalaryComponent) {
            return $response;
        }

        // if ($prepSalaryComponent->status == 'open') {
        //     $response['status'] = true;
        //     $response['errors'] = "Please lock the prep user.";
        //     return $response;
        // }

        if ($prepSalaryObj->month->workingDaySetting ? $prepSalaryObj->month->workingDaySetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the User Working days";
            return $response;
        }

        if ($prepSalaryObj->month->loanSetting ? $prepSalaryObj->month->loanSetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the loan emi deduction.";
            return $response;
        }

        if ($prepSalaryObj->month->loanInterestSetting ? $prepSalaryObj->month->loanInterestSetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the loan interest income.";
            return $response;
        }

        if ($prepSalaryObj->month->fixedBonusSetting ? $prepSalaryObj->month->fixedBonusSetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the fixed bonus.";
            return $response;
        }

        if ($prepSalaryObj->month->variablePaySetting ? $prepSalaryObj->month->variablePaySetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the variable bonus.";
            return $response;
        }

        if ($prepSalaryObj->month->itSavingMonthSetting ? $prepSalaryObj->month->itSavingMonthSetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the it saving deduction.";
            return $response;
        }

        if ($prepSalaryObj->month->vpfSetting ? $prepSalaryObj->month->vpfSetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the vpf deduction month.";
            return $response;
        }

        if ($prepSalaryObj->month->insuranceSetting ? $prepSalaryObj->month->insuranceSetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the insurance month.";
            return $response;
        }

        if ($prepSalaryObj->month->adhocPaymentSetting ? $prepSalaryObj->month->adhocPaymentSetting->value == "open" : true) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the adhoc payment month.";
            return $response;
        }
        if ($prepSalaryComponent->is_generated == 0) {
            $response['status'] = true;
            $response['errors'] = "Please finalize the user month";
            return $response;
        }
        return $response;
    }

    public static function getSalarySheetData($id)
    {
        $sql = DB::select(DB::RAW("SELECT
            `user_id`,
            `name` as 'Name',
            `employee_id` as 'Employee_ID',
            `basic` + `hra` + `car_allowance` + `food_allowance` + `special_allowance` + `stipend` + `lta` as 'Monthly_Gross_Salary',
            `basic` as 'Basic',
            `hra` as 'HRA',
            `car_allowance` as 'Car_Allowance',
            `food_allowance` as 'Food_Allowance',
            `lta` as 'Leave_Travel_Allowance',
            `special_allowance` as 'Special_Allowance',
            `stipend` as 'Stipend',
            `pf_employee` as 'PF_Employee',
            `pf_employeer` as 'PF_Employeer',
            `pf_other` as 'PF_Other',
            `esi` as 'ESI',
            `pt` as 'Professional_Tax',
            `vpd` as 'VPF_Deduction',
            `insurance` as 'Medical_Insurance',
            `food_deduction_amount` as 'Food_Deduction',
            `adhoc_amount` as 'Adhoc_Amount',
            `loan` as 'Loan_Deduction',
            `tds` as 'TDS',
            `bonus` as 'Appraisal_Bonus',
            `pf_employee` + `pt` + `esi` + `tds`  + CAST(`vpd` as decimal) + CAST(`insurance` as decimal) + CAST(`food_deduction_amount` as decimal) + CAST(`loan` as decimal) as 'Total_Deduction',
            `basic` + `hra` + `car_allowance` + `food_allowance` + `lta` + `special_allowance` + `stipend` + `pf_employee` + `pt`  + `tds`  + CAST(`vpd` as decimal) + CAST(`insurance` as decimal) + CAST(`food_deduction_amount` as decimal) + `esi` + CAST(`loan` as decimal) + `bonus` + CAST(`adhoc_amount` as decimal) as 'Net_Payable'
            FROM (
            SELECT prep_current_gross_id,
            pu.user_id,
            u.id,
            u.name,
            u.employee_id,
            IFNULL(VPFS.vpfamount,0) as vpd,
            IFNULL(INCS.inc_amount,0) as insurance,
            IFNULL(PLET.lamount,0) as loan,
            IFNULL(ptc.value, 0) as tds,
            IFNULL(AB.bamount,0) as bonus,
            IFNULL(FD.fdamount,0) as food_deduction_amount,
            IFNULL(ADCH.adh_amount,0) as adhoc_amount,
            MAX(CASE WHEN pcgi.key = 'basic' THEN pcgi.value ELSE 0 END) `basic`,
            MAX(CASE WHEN pcgi.key = 'hra' THEN pcgi.value END) `hra`,
            MAX(CASE WHEN pcgi.key = 'car-allowance' THEN pcgi.value END) `car_allowance`,
            MAX(CASE WHEN pcgi.key = 'lta' THEN pcgi.value END) `lta`,
            MAX(CASE WHEN pcgi.key = 'food-allowance' THEN pcgi.value END) `food_allowance`,
            MAX(CASE WHEN pcgi.key = 'special-allowance' THEN pcgi.value END) `special_allowance`,
            MAX(CASE WHEN pcgi.key = 'stipend' THEN pcgi.value END) `stipend`,
            MAX(CASE WHEN pcgi.key = 'pf-employee' THEN pcgi.value END) `pf_employee`,
            MAX(CASE WHEN pcgi.key = 'pf-employeer' THEN pcgi.value END) `pf_employeer`,
            MAX(CASE WHEN pcgi.key = 'pf-other' THEN pcgi.value END) `pf_other`,
            MAX(CASE WHEN pcgi.key = 'esi' THEN pcgi.value END) `esi`,
            MAX(CASE WHEN pcgi.key = 'professional-tax' THEN pcgi.value END) `pt`
            FROM prep_current_gross_items as pcgi, prep_current_gross as pcg, users as u, prep_tds_components as ptc, prep_users as pu
            LEFT JOIN prep_tds pt ON pu.user_id = pt.user_id
            LEFT JOIN
            (
                SELECT user_id, SUM(value) as inc_amount FROM prep_insurances
                WHERE prep_salary_id = $id
                GROUP BY user_id
            ) as INCS
            ON pu.user_id = INCS.user_id
            LEFT JOIN
            (
                SELECT user_id, SUM(value) as adh_amount FROM prep_adhoc_payments pap,prep_adhoc_payment_components papc
                WHERE prep_salary_id = $id AND papc.prep_adhoc_payment_id = pap.id
                GROUP BY user_id
            ) as ADCH
            ON pu.user_id = ADCH.user_id
            LEFT JOIN
            (
                SELECT user_id, SUM(value) as vpfamount FROM prep_vpfs
                WHERE prep_salary_id = $id
                GROUP BY user_id
            ) as VPFS
            ON pu.user_id = VPFS.user_id
            LEFT JOIN
            (
                SELECT user_id, SUM(value) as bamount FROM prep_appraisal_bonuses
                WHERE prep_salary_id = $id
                GROUP BY user_id
            ) as AB
            ON pu.user_id = AB.user_id
            LEFT JOIN
            (
                SELECT user_id, SUM(value) as fdamount
                FROM prep_food_deductions
                WHERE prep_salary_id = $id
                GROUP BY user_id
            ) as FD
            ON pu.user_id = FD.user_id
            LEFT JOIN
            (
                SELECT user_id, SUM(emi_amount) as lamount FROM prep_loan_emi
                WHERE prep_salary_id = $id
                GROUP BY user_id
            ) as PLET
            ON pu.user_id = PLET.user_id
            where
            pt.prep_salary_id = pu.prep_salary_id AND pcg.id = pcgi.prep_current_gross_id AND pu.prep_salary_id = pcg.prep_salary_id AND pcg.prep_salary_id = $id and pu.user_id = pcg.user_id and u.id = pu.user_id AND pt.user_id = u.id and ptc.prep_tds_id = pt.id AND ptc.key = 'tds-for-the-month' AND pcg.prep_salary_id = pu.prep_salary_id
            GROUP BY prep_current_gross_id
            ) as prep_salary"
        ));

        $bonusData = [];
        $prepBonuses = PrepBonus::where('prep_salary_id', $id)->get();
        foreach ($prepBonuses as $prepB) {
            if (!isset($bonusData[$prepB->user_id]['Total_Other_Bonus'])) {
                $bonusData[$prepB->user_id]['Total_Other_Bonus'] = 0;
            }
            if (!isset($bonusData[$prepB->user_id]['Onsite_Bonus'])) {
                $bonusData[$prepB->user_id]['Onsite_Bonus'] = 0;
            }
            if (!isset($bonusData[$prepB->user_id]['Referral_Bonus'])) {
                $bonusData[$prepB->user_id]['Referral_Bonus'] = 0;
            }
            if (!isset($bonusData[$prepB->user_id]['Additional_Work_Day_Bonus'])) {
                $bonusData[$prepB->user_id]['Additional_Work_Day_Bonus'] = 0;
            }
            if (!isset($bonusData[$prepB->user_id]['Performance_Bonus'])) {
                $bonusData[$prepB->user_id]['Performance_Bonus'] = 0;
            }
            if (!isset($bonusData[$prepB->user_id]['TechTalk_Bonus'])) {
                $bonusData[$prepB->user_id]['TechTalk_Bonus'] = 0;
            }
            if (!isset($bonusData[$prepB->user_id]['User_Timesheet_Extra'])) {
                $bonusData[$prepB->user_id]['User_Timesheet_Extra'] = 0;
            }
            if ($prepB->bonus) {
                if ($prepB->bonus->reference_type == 'App\Models\Admin\OnSiteBonus') {
                    $bonusData[$prepB->user_id]['Total_Other_Bonus'] += $prepB->bonus->amount;
                    $bonusData[$prepB->user_id]['Onsite_Bonus'] += $prepB->bonus->amount;
                } else if ($prepB->bonus->reference_type == 'App\Models\Admin\ReferralBonus') {
                    $bonusData[$prepB->user_id]['Total_Other_Bonus'] += $prepB->bonus->amount;
                    $bonusData[$prepB->user_id]['Referral_Bonus'] += $prepB->bonus->amount;
                } else if ($prepB->bonus->reference_type == 'App\Models\Admin\AdditionalWorkDaysBonus') {
                    $bonusData[$prepB->user_id]['Total_Other_Bonus'] += $prepB->bonus->amount;
                    $bonusData[$prepB->user_id]['Additional_Work_Day_Bonus'] += $prepB->bonus->amount;
                } else if ($prepB->bonus->reference_type == 'App\Models\Admin\TechTalkBonusesUser') {
                    $bonusData[$prepB->user_id]['Total_Other_Bonus'] += $prepB->bonus->amount;
                    $bonusData[$prepB->user_id]['TechTalk_Bonus'] += $prepB->bonus->amount;
                } else if ($prepB->bonus->reference_type == 'App\Models\Admin\PerformanceBonus') {
                    $bonusData[$prepB->user_id]['Total_Other_Bonus'] += $prepB->bonus->amount;
                    $bonusData[$prepB->user_id]['Performance_Bonus'] += $prepB->bonus->amount;
                } else if ($prepB->bonus->reference_type == 'App\Models\User\UserTimesheetExtra') {
                    $bonusData[$prepB->user_id]['Total_Other_Bonus'] += $prepB->bonus->amount;
                    $bonusData[$prepB->user_id]['User_Timesheet_Extra'] += $prepB->bonus->amount;
                }
            }
        }

        $savingObj = AccountType::where('code', 'saving-account')->first();
        $data = [];
        foreach ($sql as $query) {
            $data[$query->user_id]['Employee_ID'] = $query->Employee_ID;
            $data[$query->user_id]['Name'] = $query->Name;

            if ($savingObj) {
                $accountObj = Account::where('bank_account_type_id', $savingObj->id)->where('reference_type', 'App\Models\Users\User')->where('reference_id', $query->user_id)->first();
                if ($accountObj) {
                    $data[$query->user_id]['Bank'] = $accountObj->account_number;
                } else {
                    $data[$query->user_id]['Bank'] = '-';
                }
            } else {
                $data[$query->user_id]['Bank'] = '-';
            }

            $data[$query->user_id]['Monthly_Gross_Salary'] = $query->Monthly_Gross_Salary;
            $data[$query->user_id]['Basic'] = $query->Basic;
            $data[$query->user_id]['HRA'] = $query->HRA;
            $data[$query->user_id]['Car_Allowance'] = $query->Car_Allowance;
            $data[$query->user_id]['Food_Allowance'] = $query->Food_Allowance;
            $data[$query->user_id]['Leave_Travel_Allowance'] = $query->Leave_Travel_Allowance;
            $data[$query->user_id]['Special_Allowance'] = $query->Special_Allowance;
            $data[$query->user_id]['Stipend'] = $query->Stipend;
            $data[$query->user_id]['PF_Employee'] = $query->PF_Employee;
            $data[$query->user_id]['PF_Employeer'] = $query->PF_Employeer;
            $data[$query->user_id]['PF_Other'] = $query->PF_Other;
            $data[$query->user_id]['ESI'] = $query->ESI;
            $data[$query->user_id]['Professional_Tax'] = $query->Professional_Tax;
            $data[$query->user_id]['VPF_Deduction'] = $query->VPF_Deduction;
            $data[$query->user_id]['Medical_Insurance'] = $query->Medical_Insurance;
            $data[$query->user_id]['Food_Deduction'] = $query->Food_Deduction;
            $data[$query->user_id]['Loan_Deduction'] = $query->Loan_Deduction;
            $data[$query->user_id]['Adhoc_Amount'] = $query->Adhoc_Amount;
            $data[$query->user_id]['TDS'] = $query->TDS;
            $data[$query->user_id]['Appraisal_Bonus'] = $query->Appraisal_Bonus;
            $data[$query->user_id]['Total_Deduction'] = $query->Total_Deduction;
            $data[$query->user_id]['Onsite_Bonus'] = isset($bonusData[$query->user_id]['Onsite_Bonus']) ? $bonusData[$query->user_id]['Onsite_Bonus'] : 0;
            $data[$query->user_id]['Referral_Bonus'] = isset($bonusData[$query->user_id]['Referral_Bonus']) ? $bonusData[$query->user_id]['Referral_Bonus'] : 0;
            $data[$query->user_id]['TechTalk_Bonus'] = isset($bonusData[$query->user_id]['TechTalk_Bonus']) ? $bonusData[$query->user_id]['TechTalk_Bonus'] : 0;
            $data[$query->user_id]['Additional_Work_Day_Bonus'] = isset($bonusData[$query->user_id]['Additional_Work_Day_Bonus']) ? $bonusData[$query->user_id]['Additional_Work_Day_Bonus'] : 0;
            $data[$query->user_id]['Performance_Bonus'] = isset($bonusData[$query->user_id]['Performance_Bonus']) ? $bonusData[$query->user_id]['Performance_Bonus'] : 0;
            $data[$query->user_id]['User_Timesheet_Extra'] = isset($bonusData[$query->user_id]['User_Timesheet_Extra']) ? $bonusData[$query->user_id]['User_Timesheet_Extra'] : 0;
            $data[$query->user_id]['Total_Other_Bonus'] = isset($bonusData[$query->user_id]['Total_Other_Bonus']) ? $bonusData[$query->user_id]['Total_Other_Bonus'] : 0;

            $data[$query->user_id]['Net_Payable'] = $query->Net_Payable + (isset($bonusData[$query->user_id]['Total_Other_Bonus']) ? $bonusData[$query->user_id]['Total_Other_Bonus'] : 0);
        }
        return $data;
    }

    public static function _userPlayslipData($monthId, $user_id)
    {

        $days_worked = 21;

        // $userWorkingObj = PrepWorkingDay::where('user_id', $user_id)->where('prep_salary_id', $id)->first();
        $month = Month::find($monthId);
        $userWorkingObj = UserSalary::where('user_id', $user_id)->where('month_id', $monthId)->first();
        if ($userWorkingObj) {
            // $days_worked = $userWorkingObj->user_worked_days;
            $workingDays = $userWorkingObj->working_day;
            $days_worked = $userWorkingObj->working_day + $userWorkingObj->lop;
        }

        $user_transactions = Transaction::where([
            ['month_id', '=', $monthId],
            ['user_id', '=', $user_id],
            ['amount', '!=', 0],
            ['reference_type', '!=', 'App\Models\Audited\BankTransfer\BankTransferUser'],
            ['reference_type', '!=', 'App\Models\Audited\BankTransfer\BankTransferHoldUser'],
        ])->get();
        $net_transfer = 0;
        $total_earning = 0;
        $total_deduction = 0;
        $deduction = [];
        $earning = [];
        $onsite_bonus = 0;
        $bonus = 0;
        $additional_bonus = 0;
        $extra_bonus = 0;
        $performance_bonus = 0;
        $tech_talk_bonus = 0;
        foreach ($user_transactions as $t) {

            if ($t->is_company_expense) {
                continue;
            }

            if ($t->amount > 0) {
                $earning[] = $t;
                $total_earning += $t->amount;
            } else {
                $deduction[] = $t;
                $total_deduction += $t->amount;
            }
            if ($t->reference->reference_type == 'App\Models\Admin\OnSiteBonus') {
                $onsite_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\AdditionalWorkDaysBonus') {
                $additional_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\User\UserTimesheetExtra') {
                $extra_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\PerformanceBonus') {
                $performance_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\TechTalkBonusesUser') {
                $tech_talk_bonus += $t->amount;
            } elseif ($t->reference == 'App\Models\Admin\Bonus') {
                $bonus += $t->amount;
            }

            $net_transfer += $t->amount;
        }
        $user = User::find($user_id);
        return compact('deduction', 'earning', 'user', 'total_earning', 'total_deduction', 'net_transfer', 'days_worked', 'onsite_bonus', 'additional_bonus', 'extra_bonus', 'performance_bonus', 'tech_talk_bonus', 'bonus', 'month', 'workingDays');
    }

    public static function _userTDSData($monthId, $user_id)
    {
        $monthObj = Month::find($monthId);
        if ($monthObj == null) {
            return null;
        }

        $prepSalariesArr = TDSService::getUserPrepSalaries($user_id, $monthObj->financial_year_id);

        $userWorkingObj = UserSalary::where('user_id', $user_id)->where('month_id', $monthId)->first();
        if ($userWorkingObj) {
            $workingDays = $userWorkingObj->working_day;
            $days_worked = $userWorkingObj->working_day + $userWorkingObj->lop;
        }

        $tdsSummary = [];
        if (!empty($prepSalariesArr) && isset($prepSalariesArr[$monthId])) {
            $prepSalId = $prepSalariesArr[$monthId];
            $tdsObj = PrepTds::where('prep_salary_id', $prepSalId)->where('user_id', $user_id)->first();
            if ($tdsObj->components != null && count($tdsObj->components) > 0) {
                foreach ($tdsObj->components as $component) {
                    $tdsSummary[$component->key] = $component->value;
                }
            }
        }

        $user = User::find($user_id);

        return compact('tdsSummary', 'user', 'monthObj');
    }

    public static function getComparedSalarySheet($prepSalaryId)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = '';

        $prepSalObj = PrepSalary::find($prepSalaryId);
        if (!$prepSalObj) {
            $response['status'] = false;
            $response['errors'] = 'Prep Salary Id Not Found!';
            return $response;
        }

        $currMonthObj = Month::find($prepSalObj->month_id);
        if (!$currMonthObj) {
            $response['status'] = false;
            $response['errors'] = 'Current Month Not Found!';
            return $response;
        }

        $prevMonthObj = Month::where('id','<',$prepSalObj->month_id)->orderBy('id', 'DESC')->first();
        if (!$prevMonthObj) {
            $response['status'] = false;
            $response['errors'] = 'Previous Month Not Found!';
            return $response;
        }

        $currData = self::getSalarySheetData($prepSalaryId);
        $tranData = Transaction::where('prep_salary_id', '!=', 0)->groupBy('user_id', 'prep_salary_id', 'reference_type', 'reference_id')->where('is_company_expense', 0)->get();
        if (!$tranData) {
            $response['status'] = false;
            $response['errors'] = 'Transaction Data Not Found!';
            return $response;
        }

        $apprCompType = AppraisalComponentType::all();

        $combinedData = [];
        foreach ($currData as $userId => $data) {
            $tranObj = Transaction::where('user_id', $userId)->where('month_id', $prevMonthObj->id)->where('prep_salary_id', '!=', 0)->first();
            $insuranceAmount = Transaction::where('user_id', $userId)->where('month_id', $prevMonthObj->id)->where('prep_salary_id', '!=', 0)->where('reference_type','App\Models\Admin\Insurance\InsuranceDeduction')->sum('amount');
            if ($tranObj) {
                $prevPrepSalId = $tranObj->prep_salary_id;

                $userTrans = $tranData->where('user_id', $userId)->where('prep_salary_id', $prevPrepSalId)->where('reference_type', 'App\Models\Appraisal\AppraisalComponentType');

                $combinedData[$userId]['Employee_ID'] = $data['Employee_ID'];
                $combinedData[$userId]['Name'] = isset($data['Name']) ? $data['Name'] : 0;
                $combinedData[$userId]['Bank'] = isset($data['Bank']) ? $data['Bank'] : 0;

                $netData = $tranData->where('user_id', $userId)->where('month_id', $prevMonthObj->id)->where('prep_salary_id', '!=', 0)->sum('amount');
                $combinedData[$userId]['previous']['net_sal_compare'] = $netData;
                $combinedData[$userId]['previous']['net_sal_compare_ex_bonus'] = Transaction::getNetPayableExBonus($userId, $prevMonthObj->id);
                $combinedData[$userId]['current']['net_sal_compare'] = isset($data['Net_Payable']) ? $data['Net_Payable'] : 0;
                $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] = $insuranceAmount;
                if ($userTrans && $prevPrepSalId) {
                    foreach ($apprCompType as $type) {
                        $combinedData[$userId]['previous'][$type->code] = $userTrans->where('reference_id', $type->id)->sum('amount');

                        if ($type->code == 'basic') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Basic']) ? $data['Basic'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Basic']) ? $data['Basic'] : 0;
                        } else if ($type->code == 'hra') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['HRA']) ? $data['HRA'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['HRA']) ? $data['HRA'] : 0;
                        } else if ($type->code == 'car-allowance') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Car_Allowance']) ? $data['Car_Allowance'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Car_Allowance']) ? $data['Car_Allowance'] : 0;
                        } else if ($type->code == 'food-allowance') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Food_Allowance']) ? $data['Food_Allowance'] : 0;
                        } else if ($type->code == 'pf-employee') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['PF_Employee']) ? $data['PF_Employee'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['PF_Employee']) ? $data['PF_Employee'] : 0;
                        } else if ($type->code == 'pf-employeer') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['PF_Employeer']) ? $data['PF_Employeer'] : 0;
                        } else if ($type->code == 'pf-other') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['PF_Other']) ? $data['PF_Other'] : 0;
                        } else if ($type->code == 'esi') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['ESI']) ? $data['ESI'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['ESI']) ? $data['ESI'] : 0;
                        } else if ($type->code == 'professional-tax') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Professional_Tax']) ? $data['Professional_Tax'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Professional_Tax']) ? $data['Professional_Tax'] : 0;
                        } else if ($type->code == 'lta') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Leave_Travel_Allowance']) ? $data['Leave_Travel_Allowance'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Leave_Travel_Allowance']) ? $data['Leave_Travel_Allowance'] : 0;
                        } else if ($type->code == 'stipend') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Stipend']) ? $data['Stipend'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Stipend']) ? $data['Stipend'] : 0;
                        } else if ($type->code == 'special-allowance') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Special_Allowance']) ? $data['Special_Allowance'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Special_Allowance']) ? $data['Special_Allowance'] : 0;
                        } else if ($type->code == 'tds') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['TDS']) ? $data['TDS'] : 0;
                            $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['TDS']) ? $data['TDS'] : 0;
                        }
                    }

                }

            } else {
                $combinedData[$userId]['Employee_ID'] = $data['Employee_ID'];
                $combinedData[$userId]['Name'] = isset($data['Name']) ? $data['Name'] : 0;
                $combinedData[$userId]['Bank'] = isset($data['Bank']) ? $data['Bank'] : 0;

                $netData = Transaction::where('is_company_expense', 0)->where('user_id', $userId)->where('month_id', $prevMonthObj->id)->where('prep_salary_id', '!=', 0)->sum('amount');
                $combinedData[$userId]['previous']['net_sal_compare_ex_bonus'] = Transaction::getNetPayableExBonus($userId, $prevMonthObj->id);
                $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] = 0;
                $combinedData[$userId]['previous']['net_sal_compare'] = $netData;
                $combinedData[$userId]['current']['net_sal_compare'] = isset($data['Net_Payable']) ? $data['Net_Payable'] : 0;

                foreach ($apprCompType as $type) {
                    $combinedData[$userId]['previous'][$type->code] = 0;

                    if ($type->code == 'basic') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['Basic']) ? $data['Basic'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Basic']) ? $data['Basic'] : 0;
                    } else if ($type->code == 'hra') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['HRA']) ? $data['HRA'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['HRA']) ? $data['HRA'] : 0;
                    } else if ($type->code == 'car-allowance') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['Car_Allowance']) ? $data['Car_Allowance'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Car_Allowance']) ? $data['Car_Allowance'] : 0;
                    } else if ($type->code == 'food-allowance') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['Food_Allowance']) ? $data['Food_Allowance'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Food_Allowance']) ? $data['Food_Allowance'] : 0;
                    } else if ($type->code == 'pf-employee') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['PF_Employee']) ? $data['PF_Employee'] : 0;
                    } else if ($type->code == 'pf-employeer') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['PF_Employeer']) ? $data['PF_Employeer'] : 0;
                    } else if ($type->code == 'pf-other') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['PF_Other']) ? $data['PF_Other'] : 0;
                    } else if ($type->code == 'esi') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['ESI']) ? $data['ESI'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['ESI']) ? $data['ESI'] : 0;
                    } else if ($type->code == 'professional-tax') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['Professional_Tax']) ? $data['Professional_Tax'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Professional_Tax']) ? $data['Professional_Tax'] : 0;
                    } else if ($type->code == 'lta') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['Leave_Travel_Allowance']) ? $data['Leave_Travel_Allowance'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Leave_Travel_Allowance']) ? $data['Leave_Travel_Allowance'] : 0;
                    } else if ($type->code == 'stipend') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['Stipend']) ? $data['Stipend'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Stipend']) ? $data['Stipend'] : 0;
                    } else if ($type->code == 'special-allowance') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['Special_Allowance']) ? $data['Special_Allowance'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['Special_Allowance']) ? $data['Special_Allowance'] : 0;
                    } else if ($type->code == 'tds') {
                        $combinedData[$userId]['current'][$type->code] = isset($data['TDS']) ? $data['TDS'] : 0;
                        $combinedData[$userId]['current']['net_sal_compare_ex_bonus'] += isset($data['TDS']) ? $data['TDS'] : 0;
                    }
                }
            }
        }

        $response['data']['combined'] = $combinedData;
        $response['data']['prev_month'] = $prevMonthObj;
        return $response;
    }

    public static function getComparedBankTransfer($monthId)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = '';

        $currMonthObj = Month::find($monthId);
        if (!$currMonthObj) {
            $response['status'] = false;
            $response['errors'] = 'Current Month Not Found!';
            return $response;
        }

        // $prepSalObj = PrepSalary::where('month_id', $monthId)->where('status', 'closed')->orderBy('created_at', 'DESC')->first();
        $prepSalObj = PrepSalary::find(7);
        if (!$prepSalObj) {
            $response['status'] = false;
            $response['errors'] = 'Prep Salary Id Not Found!';
            return $response;
        }

        $currData = self::getSalarySheetData($prepSalObj->id);

        $tranData = Transaction::where('prep_salary_id', '!=', 0)->groupBy('user_id', 'prep_salary_id', 'reference_type', 'reference_id')->where('is_company_expense', 0)->get();
        if (!$tranData) {
            $response['status'] = false;
            $response['errors'] = 'Transaction Data Not Found!';
            return $response;
        }

        $apprCompType = AppraisalComponentType::all();

        $combinedData = [];
        foreach ($currData as $userId => $data) {
            // $tranObj = Transaction::where('user_id', $userId)->where('month_id', $currMonthObj->id)->where('prep_salary_id', '!=', 0)->first();
            $tranObj = Transaction::where('user_id', $userId)->where('month_id', $prepSalObj->month_id)->where('prep_salary_id', '!=', 0)->first();
            if ($tranObj) {
                $prevPrepSalId = $tranObj->prep_salary_id;

                $userTrans = $tranData->where('user_id', $userId)->where('prep_salary_id', $prevPrepSalId)->where('reference_type', 'App\Models\Appraisal\AppraisalComponentType');

                $combinedData[$userId]['Employee_ID'] = $data['Employee_ID'];
                $combinedData[$userId]['Name'] = isset($data['Name']) ? $data['Name'] : 0;
                $combinedData[$userId]['Bank'] = isset($data['Bank']) ? $data['Bank'] : 0;

                if ($userTrans && $prevPrepSalId) {
                    foreach ($apprCompType as $type) {
                        $combinedData[$userId]['previous'][$type->code] = $userTrans->where('reference_id', $type->id)->sum('amount');

                        if ($type->code == 'basic') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Basic']) ? $data['Basic'] : 0;
                        } else if ($type->code == 'hra') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['HRA']) ? $data['HRA'] : 0;
                        } else if ($type->code == 'car-allowance') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Car_Allowance']) ? $data['Car_Allowance'] : 0;
                        } else if ($type->code == 'food-allowance') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Food_Allowance']) ? $data['Food_Allowance'] : 0;
                        } else if ($type->code == 'pf-employee') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['PF_Employee']) ? $data['PF_Employee'] : 0;
                        } else if ($type->code == 'pf-employeer') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['PF_Employeer']) ? $data['PF_Employeer'] : 0;
                        } else if ($type->code == 'pf-other') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['PF_Other']) ? $data['PF_Other'] : 0;
                        } else if ($type->code == 'esi') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['ESI']) ? $data['ESI'] : 0;
                        } else if ($type->code == 'professional-tax') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Professional_Tax']) ? $data['Professional_Tax'] : 0;
                        } else if ($type->code == 'lta') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Leave_Travel_Allowance']) ? $data['Leave_Travel_Allowance'] : 0;
                        } else if ($type->code == 'stipend') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Stipend']) ? $data['Stipend'] : 0;
                        } else if ($type->code == 'special-allowance') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['Special_Allowance']) ? $data['Special_Allowance'] : 0;
                        } else if ($type->code == 'tds') {
                            $combinedData[$userId]['current'][$type->code] = isset($data['TDS']) ? $data['TDS'] : 0;
                        }
                    }
                }
            }
        }

        $response['data']['combined'] = $combinedData;

        return $response;
    }

    public static function getComponent($componentName,$salaryId)
    {
        $prepSalary = PrepSalary::find($salaryId);
        if(!$prepSalary)
            \Log::error('Prep Salary not found');
        $appraisalComponentType = PrepSalaryComponentType::where('code',$componentName)->first();
        if(!$appraisalComponentType)
            \Log::error('Prep Salary Component Type not found');
        $appraisalComponent = $prepSalary->components->where('prep_salary_component_type_id',$appraisalComponentType->id)->first();
        if(!$appraisalComponent)
            \Log::error('Component Type not found');
        return $appraisalComponent;
    }

    public static function isReque($dependents,$salaryId,$userId)
    {
        foreach($dependents as $dependent)
        {
            $prepSalaryComponent = PrepSalaryComponent::where('prep_salary_id',$salaryId)->where('prep_salary_component_type_id',$dependent)->first();
            if($prepSalaryComponent) {
                $executionStatus = self::checkExecutionStatus($prepSalaryComponent->id,$userId,$salaryId);
                if(!$executionStatus)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static function variablePayEntryInPrep($salaryId,$userId)
    {
        $data['status'] = false;
        $data['errors'] = '';
        $prepSalary = PrepSalary::find($salaryId);
        $variablePays = VariablePay::where('user_id',$userId)->where('month_id',$prepSalary->month_id)->get();
        foreach($variablePays as $variablePay)
        {
            foreach($variablePay->lastMonthPayComponents as $component)
            {
                $prepAppraisalBonus = PrepAppraisalBonus::create(['prep_salary_id' => $salaryId,'user_id' => $userId,'appraisal_bonus_id' => $component->appraisal_bonus_id,'value' => $component->value,'status' => 'open']);
                if(!$prepAppraisalBonus->isValid())
                {
                    \Log::error($prepAppraisalBonus->getErrors());
                    $data['errors'] = $prepAppraisalBonus->getErrors();
                    $data['status'] = true;
                }
            }
        }
        return $data;
    }

    public static function fixedBonusEntryInPrep($salaryId,$userId)
    {
        $data['status'] = false;
        $data['errors'] = '';
        $prepSalary = PrepSalary::find($salaryId);
        $fixedBonuses = FixedBonus::where('user_id',$userId)->where('month_id',$prepSalary->month_id)->get();
        foreach($fixedBonuses as $fixedBonus)
        {
            foreach($fixedBonus->monthlyDeductionComponents as $component)
            {
                $prepAppraisalBonus = PrepAppraisalBonus::create(['prep_salary_id' => $salaryId,'user_id' => $userId,'appraisal_bonus_id' => $component->appraisal_bonus_id,'value' => $component->value,'status' => 'open']);
                if(!$prepAppraisalBonus->isValid())
                {
                    \Log::error($prepAppraisalBonus->getErrors());
                    $data['errors'] = $prepAppraisalBonus->getErrors();
                    $data['status'] = true;
                }
            }
        }
        return $data;
    }

    public static function uploadFileDuePayment($file, $id)
    {
        $response = ['status' => false, 'message' => "", 'data' => ""];
        $fileSize = $file->getClientSize();
        if ($fileSize > 20000000) {
            $response['message'] = "Filesize exceeds 20MB";
            return $response;
        }

        $content = file_get_contents($file->getRealPath());
        $type = $file->getClientOriginalExtension();
        if (substr($type, 0, 2) !== "xl") {
            $response['message'] = "File is not in .XLxx format";
            return $response;
        }

        $fileName = FileService::getFileName($type);
        $originalName = $file->getClientOriginalName();
        $existingFile = File::where('reference_type', 'App\Models\Bank\PaymentTransfer')->where('reference_id', $id)->first();
        if ($existingFile) {
            $fileContent = FileContent::where('file_id', $existingFile->id)->first();
            if ($fileContent) {
                $fileContent->delete();
            }
            $existingFile->delete();
        }
        $store = FileService::uploadFileInDB($content, $originalName, $type, $id, "App\Models\Audited\PaymentTransfer");

        return ['status' => true, 'message' => "File Added", 'data' => $store->id];
    }

    public static function paymentTransferData($file, $paymentTransferId)
    {
        $response = [];
        $response['status'] = true;
        $response['message'] = "";

        if (!$file) {
            $response['status'] = false;
            $response['message'] = "File invalid!";
            return $response;
        }
        $array = Excel::toArray(new ExcelImport, request()->file('file'));
        foreach ($array[0] as $index => $parse) {
            if($index < 7)
                continue;
            if($parse){
                $desciptionArr = explode("/", $parse[5]);
                $transactionId = isset($parse[1]) ? $parse[1] : null;
                $transactionValueDate = isset($parse[2]) ? $parse[2] : null;
                $transactionPostedDate = isset($parse[3]) ? $parse[3] : null;
                $transactionChequeNo = isset($parse[4]) ? $parse[4] : null;
                $transactionDescription = isset($parse[5]) ? $parse[5] : null;
                $transactionType = isset($parse[6]) ? $parse[6] : null;
                $transactionAmount = isset($parse[7]) ? $parse[7] : null;
                $transactionBalance = isset($parse[8]) ? $parse[8] : null;
                $transactionAccount = isset($desciptionArr[2]) ? $desciptionArr[2] : null;

                if($transactionAccount)
                {
                    $bankTransferUserObj = PaymentTransferAccount::where('account_number', $transactionAccount)->where('payment_transfer_id', $paymentTransferId)->first();
                    if ($bankTransferUserObj) {
                        $bankTransferUserObj->comment = $transactionDescription;
                        $bankTransferUserObj->txn_posted_date = $transactionPostedDate;
                        $bankTransferUserObj->transaction_id = $transactionId;
                        $bankTransferUserObj->transaction_amount = $transactionAmount;
                        $bankTransferUserObj->mode_of_transfer = $desciptionArr[1] ? $desciptionArr[1] : null;


                        if (!$bankTransferUserObj->save()) {
                            $response['status'] = false;
                            $response['message'] = $response['message'].$bankTransferUserObj->getErrors();
                        }
                    } else {
                        $response['status'] = false;
                        $response['message'] = $response['message']."Error Parsing in File at line #".$index;
                    }
                }
            }
        }
        return $response;
    }

    public static function checkBank($userId)
    {
        $response['status'] = false;
        $response['error'] = "Account not found";
        $accountTypeObj = AccountType::where('code','saving-account')->firstOrFail();
        $accountObj = Account::where(['bank_account_type_id' => $accountTypeObj->id,'reference_type' => 'App\Models\Users\User','reference_id' => $userId])->first();
        if($accountObj)
            return $response;
        $response['status'] = true;
        return $response;
    }


    public static function getUserWorkingNotificationData($monthId)
    {
        $userSalaries = UserSalary::whereMonth_id($monthId)->get();
        $monthObj = Month::find($monthId);
        $data = [];
        foreach($userSalaries as $userSalary)
        {            
            if(($userSalary->working_day != CalendarService::getWorkingDays($monthObj->month, $monthObj->year)) ||  ($userSalary->lop != 0))
            {
                $data[$userSalary->user_id]['user_name'] = $userSalary->user->name;
                $data[$userSalary->user_id]['employee_id'] = $userSalary->user->employee_id;
                $data[$userSalary->user_id]['working_day_current']=  $userSalary->working_day ?? '';
                $data[$userSalary->user_id]['working_day_actual'] = CalendarService::getWorkingDays($monthObj->month, $monthObj->year);
                $data[$userSalary->user_id]['lop_current'] =  $userSalary->lop ?? '';
                $data[$userSalary->user_id]['lop_actual'] = 0;
            }
        }
        return $data;
    }

    public static function getVpfDeductionNotificationData($monthId)
    {
        $vpfs = VpfDeduction::whereMonth_id($monthId)->get();
        $data = [];
        foreach($vpfs as $vpf)
        {           
            if($vpf->amount != -$vpf->vpf->amount)
            {
                $data[$vpf->user_id]['user_name'] = $vpf->user->name;
                $data[$vpf->user_id]['employee_id'] = $vpf->user->employee_id;
                $data[$vpf->user_id]['vpf_current']=  $vpf->amount ?? '';
                $data[$vpf->user_id]['vpf_actual'] = $vpf->vpf->amount ?? '';
            }
        }
        return $data;
    }

    public static function getFoodDeductionNotificationData($monthId)
    {
        $foodDeductions = FoodDeduction::whereMonth_id($monthId)->get();
        $data = [];
        foreach($foodDeductions as $foodDeduction)
        {            
            if($foodDeduction->amount != -1500)
            {
                $data[$foodDeduction->user_id]['user_name'] = $foodDeduction->user->name;
                $data[$foodDeduction->user_id]['employee_id'] = $foodDeduction->user->employee_id;
                $data[$foodDeduction->user_id]['food_amount_current']=  $foodDeduction->amount ?? '';
                $data[$foodDeduction->user_id]['food_amount_actual'] = -1500;
            }
        }
        return $data;
    }

}
