<?php

namespace App\Services\SalaryService;

use App\Jobs\PrepareSalary\Components\AdhocPaymentJob;
use App\Models\Salary\{PrepSalaryComponent,PrepAdhocPayment,PrepAdhocPaymentComponent,PrepUser,PrepSalaryComponentType,PrepSalaryExecution,PrepSalary};
use App\Models\DateTime\Month;
use App\Models\AdhocPayments\AdhocPayment;
use App\Traits\ComponentDispatch;

class AdhocPaymentSalaryComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;
    private $userId, $month, $year, $componentId, $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\AdhocPaymentJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepAdhocPayment';
        $this->setPrepTableName($modelName);
    }
    public function getValue()
    {
        return [];
    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $monthObj = Month::find($componentObj->salary->month_id);
        if ($monthObj->adhocPaymentSetting ? $monthObj->adhocPaymentSetting->value == "open" : true) {
            $response['errors'] = "Adhoc Payment Month not locked";
            $response['status'] = true;
            return $response;
        }
        return $response;
    }
    public function isRequiredPush($user_id, $month_id)
    {
        return AdhocPayment::where('user_id',$user_id)->where('month_id',$month_id)->exists();
    }
    public function getHtml()
    {

        $componentObj = PrepSalaryComponent::find($this->componentId);
        $adhocPaymentValue=PrepAdhocPayment::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->first();
        $response = [];
        if($adhocPaymentValue)
        {
            $response = $adhocPaymentValue->components->toArray();
        }
        return $response;

    }

    public function getAdhocPayments($prepSalaryId, $userId)
    {
        $prepAdhocObj = PrepAdhocPayment::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        $amount = 0;
        if($prepAdhocObj)
        {
            foreach($prepAdhocObj->components as $component)
            {
                if($component->value > 0)
                {
                    $amount += $component->value;
                }
            }
        }
        return $amount;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $adhocPaymentComponentType = PrepSalaryComponentType::where('code','adhoc-payment')->first();
        if(!$adhocPaymentComponentType)
            return false;
        $adhocPaymentComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$adhocPaymentComponentType->id)->first();
        if(!$adhocPaymentComponent)
            return false;

        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$adhocPaymentComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new AdhocPaymentJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $adhocPaymentComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new AdhocPaymentJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }

    }

}
