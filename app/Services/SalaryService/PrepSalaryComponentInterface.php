<?php
namespace App\Services\SalaryService;

interface PrepSalaryComponentInterface {

    public function getValue();
    public function setMonthYear($month, $year);
    public function setUserId($userId);
    public function generate();
    public function setComponent($componentId);
    public function getHtml();
    public function queue();

}
