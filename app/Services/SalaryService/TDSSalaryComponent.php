<?php
namespace App\Services\SalaryService;

use App\Jobs\PrepareSalary\Components\TDSSalaryJob;
use App\Models\DateTime\FinancialYear;
use App\Models\Appraisal\Appraisal;
use App\Models\Salary\PrepSalary;
use App\Models\Salary\PrepSalaryComponent;
use App\Models\Salary\PrepTds;
use App\Models\Salary\PrepTdsComponent;
use App\Models\Salary\PrepUser;
use App\Models\DateTime\Month;
use App\Services\SalaryService\AppraisalBonusSalaryComponent;
use App\Services\SalaryService\BonusSalaryComponent;
use App\Services\SalaryService\CurrentGrossComponent;
use App\Services\SalaryService\GrossPaidComponent;
use App\Services\SalaryService\GrossToBePaidComponent;
use App\Services\SalaryService\ItSavingSalaryComponent;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Salary\PrepSalaryComponentType;
use App\Models\Salary\PrepSalaryExecution;
use App\Traits\ComponentDispatch;

class TDSSalaryComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;
    private $userId;
    private $month;
    private $year;
    private $componentId;

    private $grossAnnualSalary;
    private $rentYearly;
    private $taxPayable;
    private $pfOther;
    private $pfEmployeer;
    private $professionalTax;
    private $hraExemption;
    private $itInvestment;
    private $itDeduction;
    private $deduction;
    private $taxableIncome;
    private $tds;
    private $balanceTaxPayable;
    private $annualBonus;
    private $adhocPayment;
    private $rebate;
    private $taxPayableAfterRebate;
    private $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\TDSSalaryJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepTds';
        $this->setPrepTableName($modelName);
    }

    public function getValue()
    {
        return [];
    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function getUserId()
    {
        return $this->userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        return $response;
    }
    public function getHtml()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $PrepTdsObj = Preptds::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->first();

        $response = [];

        if ($PrepTdsObj) {
            $PrepTdsItems = $PrepTdsObj->components;
            if (count($PrepTdsItems) > 0) {
                foreach ($PrepTdsItems as $item) {
                    $temp = [];
                    $temp['key'] = $item->key;
                    $temp['value'] = $item->value;
                    $response[] = $temp;
                }
            }
        }

        return $response;

    }
    private function getGrossPFOther()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $toPaidObj = new GrossToBePaidComponent;
        $toBePaidValue = $toPaidObj->getValueByKey('pf-other', $componentObj->prep_salary_id, $this->userId);

        $paidObj = new GrossPaidComponent;
        $paidValue = $paidObj->getValueByKey('pf-other', $componentObj->prep_salary_id, $this->userId);

        $currObj = new CurrentGrossComponent;
        $currValue = $currObj->getValueByKey('pf-other', $componentObj->prep_salary_id, $this->userId);

        $getPf = ($toBePaidValue + $paidValue + $currValue);

        $this->pfOther = $getPf;

        return $getPf;
    }
    private function getGrossPFEmployeer()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $toPaidObj = new GrossToBePaidComponent;
        $toBePaidValue = $toPaidObj->getValueByKey('pf-employeer', $componentObj->prep_salary_id, $this->userId);

        $paidObj = new GrossPaidComponent;
        $paidValue = $paidObj->getValueByKey('pf-employeer', $componentObj->prep_salary_id, $this->userId);

        $currObj = new CurrentGrossComponent;
        $currValue = $currObj->getValueByKey('pf-employeer', $componentObj->prep_salary_id, $this->userId);

        $getPfEmployeer = ($toBePaidValue + $paidValue + $currValue);

        $this->pfEmployeer = $getPfEmployeer;

        return $getPfEmployeer;
    }
    private function getGrossAnnualSalary()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);
        if (!$componentObj) {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }

        $keys = ['lta', 'hra', 'car-allowance', 'food-allowance', 'basic', 'special-allowance', 'stipend'];

        $toPaidObj = new GrossToBePaidComponent;
        $toBePaidValue = $toPaidObj->getAnnualValueByKeys($keys, $componentObj->prep_salary_id, $this->userId);

        $paidObj = new GrossPaidComponent;
        $paidValue = $paidObj->getAnnualValueByKeys($keys, $componentObj->prep_salary_id, $this->userId);

        $currObj = new CurrentGrossComponent;
        $currValue = $currObj->getAnnualValueByKeys($keys, $componentObj->prep_salary_id, $this->userId);

        $getAnnualGrossSal = $toBePaidValue + $paidValue + $currValue;

        $this->grossAnnualSalary = $getAnnualGrossSal;

        return $getAnnualGrossSal;
    }
    private function getGrossProfessionalTax()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $toPaidObj = new GrossToBePaidComponent;
        $toBePaidValue = $toPaidObj->getValueByKey('professional-tax', $componentObj->prep_salary_id, $this->userId);

        $paidObj = new GrossPaidComponent;
        $paidValue = $paidObj->getValueByKey('professional-tax', $componentObj->prep_salary_id, $this->userId);

        $currObj = new CurrentGrossComponent;
        $currValue = $currObj->getValueByKey('professional-tax', $componentObj->prep_salary_id, $this->userId);

        $getProfessionalTax = ($toBePaidValue + $paidValue + $currValue);

        $this->professionalTax = $getProfessionalTax;

        return $getProfessionalTax;
    }
    private function getRentYearly()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);
        if (!$componentObj) {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }
        $itSavingObj = new ItSavingSalaryComponent;
        $rent = $itSavingObj->getValueByKey('rent_yearly', $componentObj->prep_salary_id, $this->userId);

        $this->rentYearly = $rent;

        return $rent;
    }
    private function getHRAExemption()
    {
        $exemption = 0;

        $grossAnnualSalary = $this->getGrossAnnualSalary();
        $rentYearly = $this->getRentYearly();

        $componentObj = PrepSalaryComponent::find($this->componentId);
        $currentComponent = new CurrentGrossComponent;
        $currentBasic = $currentComponent->getValueByKey('basic', $componentObj->prep_salary_id, $this->userId);
        $currentHra = $currentComponent->getValueByKey('hra', $componentObj->prep_salary_id, $this->userId);


        $paidComponent = new GrossPaidComponent;
        $paidBasic = $paidComponent->getValueByKey('basic', $componentObj->prep_salary_id, $this->userId);
        $paidHra = $paidComponent->getValueByKey('hra', $componentObj->prep_salary_id, $this->userId);

        $toBePaidComponent = new GrossToBePaidComponent;
        $toBePaidBasic = $toBePaidComponent->getValueByKey('basic', $componentObj->prep_salary_id, $this->userId);
        $toBePaidHra = $toBePaidComponent->getValueByKey('hra', $componentObj->prep_salary_id, $this->userId);


        $basic = $currentBasic + $paidBasic + $toBePaidBasic;
        $hra = $currentHra + $paidHra + $toBePaidHra;
        $basicRent = $basic * 0.1;

        $exemption1 = $hra;
        $exemption2 = (40 / 100) * $basic;
        $exemption3 = $rentYearly - $basicRent;

        $exemption = min($exemption1, $exemption2, $exemption3);

        if ($exemption < 0) {
            $exemption = 0;
        }

        $this->hraExemption = (-1) * ($exemption);

        return (-1) * ($exemption);
    }
    private function getStandardDeduction()
    {
        return -50000;
    }
    private function getTotalDeductions()
    {
        $deduction = $this->getHRAExemption() + $this->getStandardDeduction() + $this->getGrossProfessionalTax() + $this->getITSavingDeduction() + $this->getITSavingInvestment();
        $this->deduction = $deduction;
        return $this->deduction;
    }
    private function getITSavingInvestment()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $itSavingObj = new ItSavingSalaryComponent;
        $itInvestment = $itSavingObj->getItSavingInvestment($componentObj->prep_salary_id, $this->userId);

        $this->itInvestment = (-1) * (min($itInvestment, 150000));

        return (-1) * (min($itInvestment, 150000));
    }
    private function getITSavingDeduction()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $itSavingObj = new ItSavingSalaryComponent;
        $itDeduction = $itSavingObj->getItSavingDeduction($componentObj->prep_salary_id, $this->userId);

        $this->itDeduction = (-1) * ($itDeduction);

        return (-1) * ($itDeduction);
    }
    private function getAdhocPayment()
    {
        $amount = 0;
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $adhocPaymentObj = new AdhocPaymentSalaryComponent;
        $adhocPayment = $adhocPaymentObj->getAdhocPayments($componentObj->prep_salary_id, $this->userId);

        $paidObj = new GrossPaidComponent;
        $paidAmount = $paidObj->getAdhocPaymentPaid($componentObj->prep_salary_id, $this->userId);
        $amount = $adhocPayment +  $paidAmount;
        $this->adhocPayment = $amount;
        return $amount;
    }
    private function getCTCBonus()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $paidObj = new GrossPaidComponent;
        $paidBonus = $paidObj->getCTCPaidBonus($componentObj->prep_salary_id, $this->userId);

        $toPaidObj = new GrossToBePaidComponent;
        $toBePaidBonus = $toPaidObj->getTotalAppraisalBonus($componentObj->prep_salary_id, $this->userId);

        $currObj = new AppraisalBonusSalaryComponent;
        $currBonus = $currObj->getAnnualBonus($componentObj->prep_salary_id, $this->userId);
        $totalBonus = $paidBonus + $currBonus + $toBePaidBonus;

        $this->annualBonus = $totalBonus;
        return $totalBonus;
    }
    private function getOtherBonus()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $paidObj = new GrossPaidComponent;
        $paidBonus = $paidObj->getNonCTCPaidBonus($componentObj->prep_salary_id, $this->userId);

        $totalCuurentBonus = BonusSalaryComponent::getTotalBonus($componentObj->prep_salary_id, $this->userId);

        $totalBonus = $paidBonus + $totalCuurentBonus;

        return $totalBonus;
    }
    private function getIncomePaid()
    {
        return 0;
    }
    private function getLoanInterest()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $paidValue = 0;
        $paidObj = new GrossPaidComponent;
        $paidValue = $paidObj->getValueByKey('loan-interest-income', $componentObj->prep_salary_id, $this->userId);

        return $paidValue;
    }

    private function getSalaryPaid()
    {
        $amount = 0;
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $paidObj = new GrossPaidComponent;
        $amount = $paidObj->getValueByKey('salary_paid', $componentObj->prep_salary_id, $this->userId);
        return $amount;
    }

    private function getTotalSalary()
    {

        $totalSalary = $this->getGrossAnnualSalary() + $this->getCTCBonus() + $this->getOtherBonus() + $this->getIncomePaid() + $this->getLoanInterest() + $this->getAdhocPayment() ;
        return $totalSalary;
    }

    private function getNetTaxableIncome()
    {
        $netTaxableSal = $this->getTotalSalary() + $this->getTotalDeductions() + $this->getSalaryPaid();
        return $netTaxableSal;
    }

    // private function getTaxableIncome()
    // {
    //     $taxableAmount = $this->getNetTaxableIncome() + $this->getITSavingDeduction() + $this->getITSavingInvestment() + $this->getSalaryPaid();
    //     $this->taxableIncome = $taxableAmount;
    //     return $taxableAmount;
    // }
    private function getAnnualIncomeTax()
    {
        //$tds = self::incomeTaxPayableSlab($data['taxable_income']);

        // Income Tax Payable on Total Income
        // Rs 0 - Rs 250000
        // Rs 250001 - Rs 500000 @ 5%
        // Rs 500001- Rs 1000000  @ 20%
        // Above Rs 1000000 @ 30%
        $taxableAmount = $this->getNetTaxableIncome();

        $incomeTax = 0;
        if ($taxableAmount < 250000) {
            return 0;
        }

        if ($taxableAmount > 1000000) {
            $incomeTax = 12500 + 100000 + (($taxableAmount - 1000000) * 0.3);
        } elseif ($taxableAmount > 500000) {
            $incomeTax = 12500 + (($taxableAmount - 500000) * 0.2);
        } elseif ($taxableAmount > 250000) {
            $incomeTax = ($taxableAmount - 250000) * 0.05;
        }
        $this->tds = (-1) * $incomeTax;

        return (-1) * $incomeTax;
    }

    private function getEduCess()
    {
        if ($this->getNetTaxableIncome() < 250000) {
            return 0;
        }

        return $this->getAnnualIncomeTaxAfterRebate() * 1 * 0.04;
    }

    private function getAnnualIncomeTaxPayable()
    {
        if ($this->getNetTaxableIncome() < 250000) {
            return 0;
        }

        if ($this->getNetTaxableIncome() < 0) {
            $this->taxPayable = 0;
            return 0;
        }

        $taxPayable = $this->getEduCess() + $this->getAnnualIncomeTaxAfterRebate();
        $this->taxPayable = $taxPayable;
        return $taxPayable;
    }
    private function getRebate()
    {
        if ($this->getNetTaxableIncome() < 500000) {
            return 12500;
        }

        return 0;
    }

    private function getAnnualIncomeTaxAfterRebate()
    {
        $taxPayableAfterRebate = 0;
        $taxPayableAfterRebate = $this->getAnnualIncomeTax() + $this->getRebate();
        return min($taxPayableAfterRebate, 0);
    }
    private function tdsPaidTillDate()
    {
        $componentObj = PrepSalaryComponent::find($this->getComponent());

        $value = 0;
        $paidObj = new GrossPaidComponent;
        $value = $paidObj->getValueByKey('tds', $componentObj->prep_salary_id, $this->getUserId());

        $amount = 0;
        $itSavingObj = new ItSavingSalaryComponent;
        $amount = $itSavingObj->getValueByKey('tds',$componentObj->prep_salary_id, $this->getUserId());

        return ($value+$amount);
    }
    private function getBalanceTaxPayable()
    {
        $value = $this->tdsPaidTillDate();
        $balanceTaxPayable = 0;
        if ($this->getAnnualIncomeTaxAfterRebate() < 0) {
            $balanceTaxPayable = $this->getAnnualIncomeTaxPayable() + abs($value);
        }

        $this->balanceTaxPayable = $balanceTaxPayable;

        return $balanceTaxPayable;
        //$data['balance_tax_payable'] = $data['tax_payable'] - $data['gross_annual']['tds_paid_till_date'];
    }
    private function getTDSForTheMonth()
    {
        $monthObj = Month::where('month', $this->month)->where('year', $this->year)->first();
        $noOfRemaingMonth = FinancialYear::getRemainingMonths($monthObj->id);
        //$data['income_tax_for_the_month'] = round($data['balance_tax_payable'] / ($noOfRemaingMonth + 1));
        $tds = $this->getBalanceTaxPayable() / ($noOfRemaingMonth + 1);
        return min($tds,0);
    }

    public function calculateValue($tdsId, $key)
    {
        $tdsObj = PrepTds::find($tdsId);
        $userId = $tdsObj->user_id;

        $this->setUserId($userId);

        $value = 0;
        if ($key == 'gross-annual-salary') {
            $value = $this->getGrossAnnualSalary();
        } elseif ($key == 'gross-professional-tax') {
            $value = $this->getGrossProfessionalTax();
        } elseif ($key == 'gross-pf-employer') {
            $value = $this->getGrossPFEmployeer();
        } elseif ($key == 'gross-pf-other') {
            $value = $this->getGrossPFOther();
        } elseif ($key == 'hra-exemption') {
            $value = $this->getHRAExemption();
        } elseif ($key == 'standard-deduction') {
            $value = $this->getStandardDeduction();
        } elseif ($key == 'total-deduction') {
            $value = $this->getTotalDeductions();
        } elseif ($key == 'it-saving-investment') {
            $value = $this->getITSavingInvestment();
        } elseif ($key == 'it-saving-deduction') {
            $value = $this->getITSavingDeduction();
        } elseif ($key == 'adhoc-payment') {
            $value = $this->getAdhocPayment();
        } elseif ($key == 'loan-interest-income') {
            $value = $this->getLoanInterest();
        } elseif ($key == 'net-taxable-income') {
            $value = $this->getNetTaxableIncome();
        } elseif ($key == 'annual-income-tax') {
            $value = $this->getAnnualIncomeTax();
        } elseif ($key == 'edu-cess') {
            $value = $this->getEduCess();
        } elseif ($key == 'total-annual-income-tax-payable') {
            $value = $this->getAnnualIncomeTaxPayable();
        } elseif ($key == 'rebate') {
            $value = $this->getRebate();
        } elseif ($key == 'annual-income-tax-after-rebate') {
            $value = $this->getAnnualIncomeTaxAfterRebate();
        } elseif ($key == 'balance-tax-payable') {
            $value = round($this->getBalanceTaxPayable());
        } elseif ($key == 'tds-for-the-month') {
            $value = round($this->getTDSForTheMonth());
        } elseif ($key == 'other-bonus') {
            $value = $this->getOtherBonus();
        } elseif ($key == 'ctc-bonus') {
            $value = $this->getCTCBonus();
        } elseif ($key == 'previous-employment-salary') {
            $value = $this->getIncomePaid();
        } elseif ($key == 'tds-paid-till-date') {
            $value = round($this->tdsPaidTillDate());
        } elseif ($key == 'tds-paid-till-date') {
            $value = round($this->tdsPaidTillDate());
        } else {
            $value = 0;
        }
        return $value;
    }

    public static function getTDSDataNew($salaryDataId)
    {
        $salaryData = SalaryUserData::find($salaryDataId);
        if (!$salaryData) {
            return false;
        }

        $monthObj = Month::find($salaryData->month_id);
        $appraisalObj = Appraisal::find($salaryData->appraisal_id);

        if (!$monthObj || !$appraisalObj) {
            return 0;
        }

        $financialyearId = $salaryData->month->financialYear->id;

        $pfOther = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'pf_other');
        $pfEmployeer = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'pf_employeer');

        $data['pf_other'] = $pfOther;
        $data['pf_employeer'] = $pfEmployeer;

        //change
        $data['gross_annual'] = self::grossAnnualSalary($salaryData->user_id, $salaryData->month_id);

        //doubt and change
        $grossAnnualSalary = $data['gross_annual']['gross_annual_salary']; //$salaryData->annual_ctc;
        $data['hra_exemption'] = self::getHRAExemption($salaryData->user_id, $grossAnnualSalary, $financialyearId);
        $data['standard_deduction'] = self::getStandardDeduction($salaryDataId);
        $data['lta'] = self::grossAnnualAmountByKey($salaryData->user_id, $salaryData->month_id, 'leave_travel_allowance');

        $data['professional_tax'] = (200 * 12);
        $data['deductions'] = $data['hra_exemption']['exemption'] + $data['standard_deduction'] + $data['professional_tax'] + $pfOther + $pfEmployeer;
        $data['it_saving'] = ItSaving::getITSavingInfo($salaryData->user_id, $monthObj->id);

        $data['total_it_saving'] = $data['it_saving']['investment'] + $data['it_saving']['deduction'];
        $totalSalary = $data['gross_annual']['gross_annual_salary'] + $data['gross_annual']['gross_annual_fixed_bonus'] + $data['gross_annual']['monthly_variable_bonus'] + $data['gross_annual']['confirmation_bonus'] + $data['gross_annual']['other_bonuses'];
        // $data['total_salary'] = $totalSalary;
        $data['net_taxable_salary'] = $totalSalary - $data['deductions'];
        $data['taxable_income'] = $data['net_taxable_salary'] - $data['total_it_saving'];
        $tds = self::incomeTaxPayableSlab($data['taxable_income']);

        $data['income_tax'] = $tds;
        $eduCess = round($tds * 0.04);
        $data['edu_cess'] = $eduCess;
        $noOfRemaingMonth = FinancialYear::getRemainingMonths($salaryData->month_id);
        $data['tax_payable'] = round($eduCess + $tds);
        $data['balance_tax_payable'] = $data['tax_payable'] - $data['gross_annual']['tds_paid_till_date'];
        $data['tds'] = round($data['balance_tax_payable'] / ($noOfRemaingMonth + 1));

        return $data;
    }

    public function generateUserData($salaryId)
    {
        $response = [];
        $response['status'] = true;
        $response['errors'] = '';
        $keys = ['gross-annual-salary', 'previous-employment-salary', 'ctc-bonus', 'other-bonus', 'hra-exemption', 'standard-deduction', 'it-saving-investment', 'it-saving-deduction', 'gross-professional-tax', 'gross-pf-other', 'gross-pf-employer', 'total-deduction', 'adhoc-payment', 'loan-interest-income', 'net-taxable-income', 'annual-income-tax', 'rebate', 'annual-income-tax-after-rebate', 'edu-cess', 'total-annual-income-tax-payable', 'tds-paid-till-date', 'balance-tax-payable', 'tds-for-the-month'];

        $prepSalaryObj = PrepSalary::find($salaryId);
        PrepTds::where('prep_salary_id', $salaryId)->where('user_id', $this->userId)->get()->each(function ($prepTdsObj) {
            $prepTdsObj->delete();
        });
        $grossSalObj = new PrepTds;
        $grossSalObj->prep_salary_id = $salaryId;
        $grossSalObj->user_id = $this->userId;

        if (!$grossSalObj->save()) {
            $response['status'] = false;
            $response['errors'] = $response['errors'] . "Something went wrong for " . $this->userId . '<br/>';
            return false;
        }

        foreach ($keys as $key) {
            $getValue = $this->calculateValue($grossSalObj->id, $key);

            $checkItem = PrepTdsComponent::where('prep_tds_id', $grossSalObj->id)->where('key', $key)->first();

            if ($checkItem) {
                $response['status'] = false;
                $response['errors'] = $response['errors'] . "Prep Tds Item Record - " . $grossSalObj->id . " already exist <br/>";
                continue;
            }

            $paidObj = new PrepTdsComponent;
            $paidObj->prep_tds_id = $grossSalObj->id;
            $paidObj->key = $key;
            $paidObj->value = $getValue;

            if (!$paidObj->save()) {
                $response['status'] = false;
                $response['errors'] = $response['errors'] . $key . " key not saved for " . $grossSalObj->id . " and user id is - " . $this->userId . " - errors - " . $paidObj->getErrors() . '<br/>';
            }
        }
        return $response;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $grossComponentType = PrepSalaryComponentType::where('code','tds')->first();
        if(!$grossComponentType)
            return false;
        $grossComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$grossComponentType->id)->first();
        if(!$grossComponent)
            return false;

        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$grossComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new TDSSalaryJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id,$grossComponent->id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $grossComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new TDSSalaryJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id,$grossComponent->id));
            }
        }
    }
}
