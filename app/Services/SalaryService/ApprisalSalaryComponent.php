<?php
namespace App\Services\SalaryService;

use App\Jobs\PrepareSalary\Components\AppraisalSalaryJob;
use App\Models\Appraisal\{Appraisal,AppraisalBonus,PrepAppraisal,PrepAppraisalComponent};
use App\Models\DateTime\{FinancialYear,Month};
use App\Models\Salary\{PrepSalary,PrepSalaryComponent,PrepUser};
use App\Models\Users\User;
use App\Services\CalendarService;
use App\Services\SalaryService\{CurrentGrossComponent,GrossPaidComponent,GrossToBePaidComponent};
use App\Traits\ComponentDispatch;

class ApprisalSalaryComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;
    private $userId, $month, $year, $componentId, $jobName,$prepTableName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\AppraisalSalaryJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Appraisal\PrepAppraisal';
        $this->setPrepTableName($modelName);
    }
    public function getValue()
    {
        // return array("Zack"=>"Zara", "Anthony"=>"Any",
        //           "Ram"=>"Rani", "Salim"=>"Sara",
        //           "Raghav"=>"Ravina");
    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        return $response;
    }

    public function getHtml()
    {
        // $prepAppraisal=[];
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepAppraisal = PrepAppraisal::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->get();
        $response = [];
        if (count($prepAppraisal) > 0) {
            foreach ($prepAppraisal as $index => $appraisal) {
                $appraisalId = $appraisal->id;
                $response[$index]['start_date'] = date("d M", strtotime($appraisal->start_date));
                $response[$index]['end_date'] = date("d M", strtotime($appraisal->end_date));
                $response[$index]['component'] = PrepAppraisalComponent::getPrepSalaryComponentValues($appraisalId);
            }
        } else {
            $response['component'] = PrepAppraisalComponent::getPrepSalaryComponentValues(null);
        }

        // if(count($prepAppraisal) > 0){
        //     foreach($prepAppraisal as $appraisal){
        //         $data[$appraisal->appraisal_id]['start_date']=$appraisal->start_date;
        //         $data[$appraisal->appraisal_id]['end_date']=$appraisal->end_date;
        //         $prepAppraisalComponent=$appraisal->prepAppraisalComponent;

        //         if(count($prepAppraisalComponent) > 0){
        //             foreach($prepAppraisalComponent as $component){

        //                 $data[$appraisal->appraisal_id]['component'][$component->appraisalComponentTypes->name]=$component->value;
        //             }
        //         }
        //     }
        // }
        return $response;

        // return view('pages.admin.prep-salary.partials.appraisal',compact('prepAppraisal','userObj','prepUsers'));

    }

    private function getEffectiveDatesForCurrentMonth($userId, $end_date, $start_date)
    {
        $appraisal = Appraisal::where('user_id', $userId)->where('effective_date', '>=', $start_date)->where('effective_date', '<', $end_date)->orderBy('effective_date', 'desc')->first();
        return $appraisal;
    }

    private function getEffectiveDatesFormAnyPrevious($userId, $start_date)
    {
        $appraisal = Appraisal::where('user_id', $userId)->where('effective_date', '<', $start_date)->orderBy('effective_date', 'desc')->first();
        return $appraisal;
    }
    private function insertIntoDataBase($prep_salary_id, $user_id, $appraisal_id, $start_date, $end_date)
    {
        $response['status'] = true;
        $response['message'] = "Saved Successfully";
        $prepAppraisal = new PrepAppraisal();
        $prepAppraisal->user_id = $user_id;
        $prepAppraisal->prep_salary_id = $prep_salary_id;
        $prepAppraisal->appraisal_id = $appraisal_id;
        $prepAppraisal->start_date = $start_date;
        $prepAppraisal->end_date = $end_date;
        if ($prepAppraisal->isValid()) {
            if (!$prepAppraisal->save()) {
                $response['status'] = false;
                $response['errors'] = $prepAppraisal->getErrors();
            }
            PrepAppraisalComponent::saveData($prepAppraisal->id);
        } else {
            $response['status'] = false;
            $response['errors'] = $prepAppraisal->getErrors();
        }
        return $response;
    }

    public function generateUserData($prepSalaryId)
    {
        $response['status'] = true;
        $response['errors'] = '';
        PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $this->userId)->get()->each(function ($prepAppraisalObj) {
            $prepAppraisalObj->delete();
        });
        $prepSalary = PrepSalary::find($prepSalaryId);
        $monthObj = Month::find($prepSalary->month_id);
        $start_date = $monthObj->getFirstDay();
        $end_date = $monthObj->getLastDay();
        $appraisal = $this->getEffectiveDatesForCurrentMonth($this->userId, $end_date, $start_date);
        if (!$appraisal) {
            $appraisal = $this->getEffectiveDatesFormAnyPrevious($this->userId, $start_date);
        }
        if (!$appraisal) {
            return $response;
        }
        $effective_date = $appraisal->effective_date;
        $temp_end_date = $end_date;
        while (strtotime($end_date) >= strtotime($start_date)) {
            $temp_appraisal = $appraisal;
            if ($effective_date == $end_date) {
                $response = $this->insertIntoDataBase($prepSalary->id, $this->userId, $appraisal->id, $end_date, $temp_end_date);
                $appraisal = $this->getEffectiveDatesForCurrentMonth($this->userId, $end_date, $start_date);
                if ($appraisal) {
                    $effective_date = $appraisal->effective_date;
                } else {
                    $appraisal = $this->getEffectiveDatesFormAnyPrevious($this->userId, $start_date);
                    if ($appraisal) {
                        $response = $this->insertIntoDataBase($prepSalary->id, $this->userId, $appraisal->id, $start_date, date("Y-m-d", strtotime("-1 day", strtotime($end_date))));
                        break;
                    } else {
                        break;
                    }
                }
                $temp_end_date = date("Y-m-d", strtotime("-1 day", strtotime($end_date)));
            } else {
                if ($effective_date <= $start_date) {
                    $response = $this->insertIntoDataBase($prepSalary->id, $this->userId, $appraisal->id, $start_date, $temp_end_date);
                    break;
                }
            }
            $end_date = date("Y-m-d", strtotime("-1 day", strtotime($end_date)));
        }
        return $response;
    }

    public static function calculateGrossAppraisalToBePaid($prepSalaryId, $userId)
    {
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        $monthNumber = $prepSalaryObj->month->month_number;
        $remainingMonths = 12 - $monthNumber;

        $prepAppraisal = PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->orderBy('end_date', 'desc')->first();

        $totalAppraisal = 0;
        $grossAppraisal = 0;

        if ($prepAppraisal) {

            $prepAppraisalComponent = $prepAppraisal->prepAppraisalComponent;

            foreach ($prepAppraisalComponent as $component) {
                $code = $component->appraisalComponentTypes->code;
                if ($code == 'basic' || $code == "hra" || $code == "car-allowance" || $code == "food-allowance" ||
                    $code == "lta" || $code == "special-allowance") {
                    $totalAppraisal = $totalAppraisal + $component->value;
                }
            }

            $grossAppraisal = round($totalAppraisal / 12 * $remainingMonths);
        }

        return $grossAppraisal;
        // $recentAppraisalId = 0;
        // $recentAppraisalObj = null;
        // $date = date('Y-m-d');
        // $prepAppraisals = PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->where('start_date', '<=', $date)->orderBy('start_date', 'desc')->get();

        // foreach ($prepAppraisals as $prepAppraisal) {
        //     if ($prepAppraisal->end_date <= $date) {
        //         continue;
        //     }
        //     $recentAppraisalId = $prepAppraisal->appraisal_id;
        //     $recentAppraisalObj = $prepAppraisal;
        //     break;
        // }

        // if ($recentAppraisalObj == null) {
        //     //some error
        // }

        // $getAppraisalComps = $recentAppraisalObj->prepAppraisalComponent;

        // $totalGross = 0;

        // foreach ($getAppraisalComps as $comp) {
        //     $code = $comp->appraisalComponentTypes->code;
        //     if ($code == 'basic' || $code == "hra" || $code == "car-allowance" || $code == "food-allowance" ||
        //         $code == "lta" || $code == "special-allowance") {
        //         $totalGross = $totalGross + $comp->value;
        //     }
        // }

        // return round(($totalGross * $noOfRemainingMonths) / 12);
    }

    public static function calculateAppraisalToBePaidComponent($key, $prepSalaryId, $userId, $month)
    {
        return 100;
        //get remaining months
        // $monthObj = Month::where('month', $month)->first();
        // $monthNumber = $monthObj->month_number;
        // $noOfRemainingMonths = 12 - $monthNumber;

        // //get the recent appraisal
        // $recentAppraisalId = 0;
        // $recentAppraisalObj = null;
        // $date = date('Y-m-d');
        // $prepAppraisals = PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->where('start_date', '<=', $date)->orderBy('start_date', 'desc')->get();

        // foreach ($prepAppraisals as $prepAppraisal) {
        //     if ($prepAppraisal->end_date <= $date) {
        //         continue;
        //     }
        //     $recentAppraisalId = $prepAppraisal->appraisal_id;
        //     $recentAppraisalObj = $prepAppraisal;
        //     break;
        // }

        // if ($recentAppraisalObj == null) {
        //     //some error
        // }

        // $getAppraisalComps = $recentAppraisalObj->prepAppraisalComponent;

        // foreach ($getAppraisalComps as $comp) {
        //     if ($comp->appraisalComponentTypes->code == $key) {
        //         return round(($comp->value * $noOfRemainingMonths) / 12);
        //     }
        // }

        // return 0;
    }

    public static function getComponentValueByKey($appraisal, $key, $noOfMonths = 1)
    {
        $totalAppraisal = 0;

        $prepAppraisalComponent = $appraisal->prepAppraisalComponent;

        foreach ($prepAppraisalComponent as $component) {
            $code = $component->appraisalComponentTypes->code;

            if (is_array($key) && count($key) > 1) {
                foreach ($key as $compKey) {
                    if ($compKey == $code) {
                        $totalAppraisal = $totalAppraisal + $component->value;
                    }
                }

            } else {
                if ($key == $code) {
                    $totalAppraisal = $totalAppraisal + $component->value;
                }
            }

        }

        return $totalAppraisal;
    }

    public static function getBonusValueByKey($appraisal, $key)
    {
        $totalBonus = 0;
        $bonuses = AppraisalBonus::where('appraisal_id', $appraisal->appraisal_id)->get();
        //here
        // $monthObj = Month::where('status', 'in_progress')->orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
        $monthObj = Month::find($appraisal->prepSalary->month_id);
        if (!$monthObj) {
            return 0;
        }
        $startDate = $monthObj->getFirstDay();
        $endDate = $monthObj->getLastDay();

        $financialYearObj = FinancialYear::find($monthObj->financial_year_id);

        foreach ($bonuses as $bonus) {

            $code = $bonus->appraisalBonusType->code;
            if (!empty($bonus->value_date) && $bonus->value_date >= $endDate && $bonus->value_date <= $financialYearObj->end_date) {

                if ($key == $code) {
                    $totalBonus += $bonus->value;
                }
            }
        }
        return $totalBonus;
    }

    public static function calculateAppraisalComponent($appraisalComponent, $prepSalaryId, $userId)
    {
        $appraisals = PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->get();

        $grossComponentValue = 0;
        $totalComponentValue = 0;

        $prepSalary = PrepSalary::find($prepSalaryId);

        $month = $prepSalary->month->month;
        $year = $prepSalary->month->year;

        $totalWorkingDays = CalendarService::getWorkingDays($month, $year);

        if (count($appraisals) > 0) {
            foreach ($appraisals as $appraisal) {
                $startDate = $appraisal->start_date;
                $endDate = $appraisal->end_date;

                $workingDays = CalendarService::getWorkingDaysByDate($month, $year, $startDate, $endDate);

                $prepAppraisalComponent = $appraisal->prepAppraisalComponent;

                foreach ($prepAppraisalComponent as $component) {
                    $code = $component->appraisalComponentTypes->code;
                    if ($code == $appraisalComponent) {
                        $totalComponentValue = $totalComponentValue + $component->value;
                    }
                }
                $grossComponentValue = $grossComponentValue + ($totalComponentValue / ($totalWorkingDays * 12) * $workingDays);
            }
        }
        return $totalComponentValue;

    }

    public function getValuesByKey($key, $prepSalaryId, $userId)
    {
        $toPaidObj = new GrossToBePaidComponent;
        $toBePaidValue = $toPaidObj->getValueByKey($key, $prepSalaryId, $userId);

        $paidObj = new GrossPaidComponent;
        $paidValue = $paidObj->getValueByKey($key, $prepSalaryId, $userId);

        $currObj = new CurrentGrossComponent;
        $currValue = $currObj->getValueByKey($key, $prepSalaryId, $userId);

        return $toBePaidValue + $paidValue + $currValue;
    }

    public static function getDeductedPF($appraisalId, $totalWorkingDays, $userWorkedDays, $amount)
    {
        /*
        check if `basic` + `special allowance` < 15k,
        then deduct the PF OTHER, PF EMPLOYEE, PF EMPLOYEER
         */
        if ($amount == 0) {
            return 0;
        }
        $appraisal = PrepAppraisal::find($appraisalId);

        $basic = self::getComponentValueByKey($appraisal, 'basic', 1);
        $specialAllowance = self::getComponentValueByKey($appraisal, 'special-allowance', 1);

        $totalBasic = (($basic + $specialAllowance) / $totalWorkingDays) * $userWorkedDays;

        if ($totalBasic < 15000) {
            return round($totalBasic * -0.12);
        }
        return $amount;
    }

    public static function getDeductedPFOther($appraisalId, $totalWorkingDays, $userWorkedDays, $amount)
    {
        /*
        check if `basic` + `special allowance` < 15k,
        then deduct the PF OTHER, PF EMPLOYEE, PF EMPLOYEER
         */
        if ($amount == 0) {
            return 0;
        }
        $appraisal = PrepAppraisal::find($appraisalId);

        $basic = self::getComponentValueByKey($appraisal, 'basic', 1);
        $specialAllowance = self::getComponentValueByKey($appraisal, 'special-allowance', 1);

        $totalBasic = (($basic + $specialAllowance) / $totalWorkingDays) * $userWorkedDays;

        if ($totalBasic < 15000) {
            return round($totalBasic * -0.01);
        }
        return $amount;
    }

    public static function getDeductedPT($appraisalId, $totalWorkingDays, $userWorkedDays, $amount)
    {
        /*
        check if MONTHLY GROSS SAL < 15k,
        then deduct the PROFESSIONAL TAX
         */

        $foodDedVal = 0;
        $monthSettingObj = 0;

        if ($amount == 0) {
            return 0;
        }
        $appraisal = PrepAppraisal::find($appraisalId);

        $keys = ['basic', 'hra', 'car-allowance', 'special-allowance', 'lta', 'stipend', 'food-allowance'];
        $monthlyGrossSalary = self::getComponentValueByKey($appraisal, $keys, 1);

        if ($monthlyGrossSalary < 15000) {
            return round($amount / $totalWorkingDays) * $userWorkedDays;
        }

        return $amount;
    }

    public function queue()
    {
        $this->dispatchComponentJob();
    }

}
