<?php
namespace App\Services\SalaryService;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Salary\PrepUser;
use App\Models\Salary\PrepSalaryComponent;
use App\Models\Salary\PrepVpf;
use App\Models\DateTime\Month;
use App\Jobs\PrepareSalary\Components\VpfJob;
use App\Models\Salary\PrepSalaryComponentType;
use App\Models\Salary\PrepSalaryExecution;
use App\Models\Salary\PrepSalary;
use App\Traits\ComponentDispatch;
use App\Models\Deduction\VpfDeduction;

class VpfSalaryComponent implements PrepSalaryComponentInterface {
    use ComponentDispatch;
    private $userId, $month, $year, $componentId, $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\VpfJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepVpf';
        $this->setPrepTableName($modelName);
    }

    public function getValue() {
        return array("Zack"=>"Zara", "Anthony"=>"Any",
                  "Ram"=>"Rani", "Salim"=>"Sara",
                  "Raghav"=>"Ravina");
    }
    public function setMonthYear($month, $year) {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId) {
        $this->componentId = $componentId;
    }
    public function setUserId($userId) {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $monthObj = Month::find($componentObj->salary->month_id);
        if ($monthObj->vpfSetting ? $monthObj->vpfSetting->value == "open" : true) {
            $response['errors'] = "Vpf Month not locked";
            $response['status'] = true;
            return $response;
        }
        return $response;
    }

    public function isRequiredPush($user_id, $month_id)
    {
        return VpfDeduction::where('user_id',$user_id)->where('month_id',$month_id)->exists();
    }

    public function  getHtml(){

        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepVpfObj=PrepVpf::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->cursor();

        $vpfAmount=0;
        $response=[];

        if(count($prepVpfObj)>0){
            foreach($prepVpfObj as $vpfObj){
              $response['amount']= $vpfAmount + $vpfObj->value;

            }
        }

        return $response;
    }

    public static function getVpf($prepSalaryId,$userId){
        $prepVpfObj=PrepVpf::where('prep_salary_id',$prepSalaryId)->where('user_id',$userId)->get();

        $vpfAmount=0;

        if(count($prepVpfObj)>0){
            foreach($prepVpfObj as $vpfObj){
                $vpfAmount=$vpfAmount+$vpfObj->value;
            }
        }

        return $vpfAmount;
    }
    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $vpfComponentType = PrepSalaryComponentType::where('code','vpf')->first();
        if(!$vpfComponentType)
            return false;
        $vpfComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$vpfComponentType->id)->first();
        if(!$vpfComponent)
            return false;

        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$vpfComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new VpfJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $vpfComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new VpfJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }

    }

}
