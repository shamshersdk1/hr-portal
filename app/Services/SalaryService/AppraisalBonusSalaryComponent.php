<?php
namespace App\Services\SalaryService;

use App\Models\Salary\{PrepAppraisalBonus,PrepSalary,PrepSalaryComponent,PrepUser,PrepSalaryComponentType,PrepSalaryExecution};
use App\Models\DateTime\Month;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Jobs\PrepareSalary\Components\AppraisalBonusJob;
use App\Traits\ComponentDispatch;
use App\Models\FixedBonus\FixedBonus;
use App\Models\VariablePay\VariablePay;
class AppraisalBonusSalaryComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;

    private $userId, $month, $year, $componentId, $jobName;
    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\AppraisalBonusJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepAppraisalBonus';
        $this->setPrepTableName($modelName);
    }

    public function getValue()
    {

    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $monthObj = Month::find($componentObj->salary->month_id);
        if ($monthObj->fixedBonusSetting ? $monthObj->fixedBonusSetting->value == "open" : true) {
            $response['errors'] = "Fixed Bonus Month not locked";
            $response['status'] = true;
            return $response;
        }
        if ($monthObj->variablePaySetting ? $monthObj->variablePaySetting->value == "open" : true) {
            $response['errors'] = "Variable Pay Month not locked";
            $response['status'] = true;
            return $response;
        }
        return $response;
    }
    public function isRequiredPush($user_id, $month_id)
    {
        $fixedCheck = FixedBonus::where('user_id',$user_id)->where('month_id',$month_id)->exists();
        $variablePayCheck = VariablePay::where('user_id',$user_id)->where('month_id',$month_id)->exists();
        if($fixedCheck || $variablePayCheck)
            return true;
        return false;
    }
    public function getHtml()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepAppraisalBonusObj = PrepAppraisalBonus::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->get();

        $response = [];
        if (count($prepAppraisalBonusObj) > 0) {
            foreach ($prepAppraisalBonusObj as $appraisalBonus) {
                $response[$appraisalBonus->id]['bonus_type'] = $appraisalBonus->appraisalBonus->appraisalBonusType->description;
                $response[$appraisalBonus->id]['amount'] = $appraisalBonus->value;
            }
        }

        return $response;

    }


    public function getCurrentTotalAppraisalBonus($prepSalaryId)
    {

        $totalAppraisalBonus = 0;

        $prepSalary = PrepSalary::find($prepSalaryId);
        $financialYear = $prepSalary->month->year;

        $appraisalBonuses = PrepAppraisalBonus::where('prep_salary_id', $prepSalaryId)->where('user_id', $this->userId)->get();

        foreach ($appraisalBonuses as $bonus) {
            $appraisalBonus = $bonus->appraisalBonus;
            $year = date('Y', strtotime($appraisalBonus->value_date));
            if ($financialYear == $year) {

                // if ($appraisalBonus->appraisalBonusType->code == 'annual-bonus' || $appraisalBonus->appraisalBonusType->code == 'confirmation') {
                $totalAppraisalBonus = $totalAppraisalBonus + $bonus->value;
                // }
            }
        }

        return $totalAppraisalBonus;

    }

    public function getAnnualBonus($prepSalaryId, $userId)
    {
        $currBonus = PrepAppraisalBonus::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->sum('value');
        return $currBonus;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $appraisalBonusComponentType = PrepSalaryComponentType::where('code','appraisal-bonus')->first();
        if(!$appraisalBonusComponentType)
            return false;
        $appraisalBonusComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$appraisalBonusComponentType->id)->first();
        if(!$appraisalBonusComponent)
            return false;

        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$appraisalBonusComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new AppraisalBonusJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $appraisalBonusComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new AppraisalBonusJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }

    }
}
