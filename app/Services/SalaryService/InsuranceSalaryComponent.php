<?php
namespace App\Services\SalaryService;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Salary\PrepUser;
use App\Models\Salary\PrepSalaryComponent;
use App\Models\Salary\PrepInsurance;
use App\Models\DateTime\Month;
use App\Jobs\PrepareSalary\Components\InsuranceJob;
use App\Models\Salary\PrepSalaryComponentType;
use App\Models\Salary\PrepSalaryExecution;
use App\Models\Salary\PrepSalary;
use App\Traits\ComponentDispatch;

class InsuranceSalaryComponent implements PrepSalaryComponentInterface {
    use ComponentDispatch;
    private $userId, $month, $year, $componentId, $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\InsuranceJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepInsurance';
        $this->setPrepTableName($modelName);
    }

    public function getValue() {
        return array("Zack"=>"Zara", "Anthony"=>"Any",
                  "Ram"=>"Rani", "Salim"=>"Sara",
                  "Raghav"=>"Ravina");
    }
    public function setMonthYear($month, $year) {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId) {
        $this->componentId = $componentId;
    }
    public function setUserId($userId) {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $monthObj = Month::find($componentObj->salary->month_id);
        if ($monthObj->insuranceSetting ? $monthObj->insuranceSetting->value == "open" : true) {
            $response['errors'] = "Insurance Month not locked";
            $response['status'] = true;
            return $response;
        }
        return $response;
    }

    public function  getHtml(){

        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepInsuranceObj=PrepInsurance::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->cursor();

        $insuranceAmount=0;

        $response=[];
        if(count($prepInsuranceObj)>0){
            foreach($prepInsuranceObj as $insuranceObj){
              $response['amount']=$insuranceAmount + $insuranceObj->value;

            }
        }
        return $response;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $insuranceComponentType = PrepSalaryComponentType::where('code','insurance')->first();
        if(!$insuranceComponentType)
            return false;
        $insuranceComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$insuranceComponentType->id)->first();
        if(!$insuranceComponent)
            return false;

        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$insuranceComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new InsuranceJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $insuranceComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new InsuranceJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }

    }

}
