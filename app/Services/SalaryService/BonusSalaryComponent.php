<?php
namespace App\Services\SalaryService;

use App\Models\Bonus\PrepBonus;
use App\Models\Salary\{PrepSalaryComponent,PrepGrossPaid,PrepUser,PrepSalary,PrepSalaryComponentType,PrepSalaryExecution};
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Jobs\PrepareSalary\Components\BonusJob;
use App\Traits\ComponentDispatch;
use App\Models\Bonus\BonusConfirmed;
class BonusSalaryComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;

    private $userId, $month, $year, $componentId, $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\BonusJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepBonus';
        $this->setPrepTableName($modelName);
    }
    public function getValue()
    {
        return array("Zack" => "Zara", "Anthony" => "Any",
            "Ram" => "Rani", "Salim" => "Sara",
            "Raghav" => "Ravina");
    }
    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        return $response;
    }
    public function isRequiredPush($user_id, $month_id)
    {
        return BonusConfirmed::where('user_id',$user_id)->exists();
    }

    public function  getHtml(){

        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepBonusObj=PrepBonus::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->get();

        $extra=0;$additional=0;$onsite=0;$techtalk=0;$referral=0;$performance=0;

        $response=[];
        if(count($prepBonusObj)>0){
            foreach($prepBonusObj as $bonus){
                $bonusObj=$bonus->bonus;
                if($bonusObj){
                    if($bonusObj->reference_type=='App\Models\Admin\OnSiteBonus'){
                        $onsite+=$bonusObj->amount;
                    }
                    elseif($bonusObj->reference_type=='App\Models\Admin\AdditionalWorkDaysBonus'){
                        $additional+=$bonusObj->amount;
                    }elseif($bonusObj->reference_type=='App\Models\User\UserTimesheetExtra'){
                        $extra+=$bonusObj->amount;
                    }elseif($bonusObj->reference_type=='App\Models\Admin\TechTalkBonus'){
                        $techtalk+=$bonusObj->amount;
                    }
                    elseif($bonusObj->reference_type=='App\Models\Admin\ReferralBonus'){
                        $referral+=$bonusObj->amount;
                    }
                    elseif($bonusObj->reference_type=='App\Models\Admin\PerformanceBonus'){
                        $performance+=$bonusObj->amount;
                    }
                    else{

                    }

                }
            }
        }
        $response['bonus']['onsite']=$onsite;
        $response['bonus']['additional']=$additional;
        $response['bonus']['extra']=$extra;
        $response['bonus']['techtalk']=$techtalk;
        $response['bonus']['referral']=$referral;
        $response['bonus']['performance']=$performance;

        return $response;

    }

    public static function getTotalBonus($prepSalaryId,$userId) {
        $bonuses=PrepBonus::where('prep_salary_id',$prepSalaryId)->where('user_id',$userId)->get();
        $paidObj = PrepGrossPaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();

        $overallMonthlyBonus=0;

        if(count($bonuses) > 0){
            foreach($bonuses as $bonus){
                $overallMonthlyBonus=$overallMonthlyBonus + $bonus->bonus->amount;
            }

        }
        return $overallMonthlyBonus;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $bonusComponentType = PrepSalaryComponentType::where('code','bonus')->first();
        if(!$bonusComponentType)
            return false;
        $bonusComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$bonusComponentType->id)->first();
        if(!$bonusComponent)
            return false;
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$bonusComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new BonusJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $bonusComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new BonusJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }

    }


}
