<?php
namespace App\Services\SalaryService;

use App\Models\Appraisal\{AppraisalComponentType,PrepAppraisal};
use App\Models\Salary\{PrepCurrentGross,PrepCurrentGrossItem,PrepSalaryComponent,PrepUser,PrepWorkingDay};
use App\Services\SalaryService\{ApprisalSalaryComponent,PrepSalaryComponentInterface,VpfSalaryComponent,WorkingDayComponent};
use App\Jobs\PrepareSalary\Components\CurrentGrossJob;
use App\Models\Salary\{PrepSalaryComponentType,PrepSalaryExecution,PrepSalary};
use App\Traits\ComponentDispatch;

class CurrentGrossComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;

    private $userId, $month, $year, $componentId, $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\CurrentGrossJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepCurrentGross';
        $this->setPrepTableName($modelName);
    }

    public function getValue()
    {
        return [];
    }

    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }

    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        return $response;
    }

    public function generateUserData($prepSalaryId) {
        $response['status'] = true;
        $response['errors'] = '';
        $appraisalComponentTypes = AppraisalComponentType::where('is_computed', 0)->pluck('code');
        $keys = $appraisalComponentTypes->toArray();
        PrepCurrentGross::where('prep_salary_id', $prepSalaryId)->where('user_id',$this->userId)->get()->each(function ($prepGrossObj) {
            $prepGrossObj->delete();
        });
        $prepCurrentGross = new PrepCurrentGross();
        $prepCurrentGross->prep_salary_id = $prepSalaryId;
        $prepCurrentGross->user_id = $this->userId;

        if ($prepCurrentGross->isValid()) {
            if (!$prepCurrentGross->save()) {
                $response['status'] = false;
                $response['errors'] = $response['errors'].$prepCurrentGross->getErrors();
            }
            foreach ($keys as $key) {
                $getValue = $this->calculateValue($prepCurrentGross->id, $key);

                $prepGrossItem = new PrepCurrentGrossItem();
                $prepGrossItem->prep_current_gross_id = $prepCurrentGross->id;
                $prepGrossItem->key = $key;
                $prepGrossItem->value = $getValue;

                if ($prepGrossItem->isValid()) {
                    if (!$prepGrossItem->save()) {
                        $response['status'] = false;
                        $response['errors'] = $response['errors'].$prepGrossItem->getErrors();
                        continue;
                    }
                }

            }

        } else {
            $response['status'] = false;
            $response['errors'] = $response['errors'].$prepCurrentGross->getErrors();
        }
        return $response;
    }

    public function getAnnualGrossSalary($prepSalaryId, $userId, $key)
    {

        $totalGrossSalary = 0;
        $totalAppraisalBonus = 0;
        $grossSalary = 0;

        $prepAppraisals = PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->get();

        $workingDay = new WorkingDayComponent();

        $totalWorkingDays = $workingDay->getTotalWorkingDays($prepSalaryId, $userId);

        if (count($prepAppraisals) > 0) {
            foreach ($prepAppraisals as $appraisal) {

                $startDate = $appraisal->start_date;
                $endDate = $appraisal->end_date;

                $workingDays = $workingDay->getWorkingDaysByDate($startDate, $endDate, $userId);

                $totalAppraisal = ApprisalSalaryComponent::getComponentValueByKey($appraisal, $key, 1);

                $totalGrossSalary = $totalGrossSalary + $totalAppraisal;

                $grossSalary = $grossSalary + ($totalGrossSalary / ($totalWorkingDays) * $workingDays);
            }
        }

        // $appraisalBonusObj=new AppraisalBonusSalaryComponent();
        // $appraisalBonusObj->setUserId($userId);
        // $totalAppraisalBonus=$appraisalBonusObj->getCurrentTotalAppraisalBonus($prepSalaryId);

        $totalMonthlyBonus = BonusSalaryComponent::getTotalBonus($prepSalaryId, $userId);

        return round($grossSalary + $totalAppraisalBonus + $totalMonthlyBonus);
    }

    public function getAppraisalComponentValue($prepSalaryId, $userId, $key)
    {
        $totalGrossSalary = 0;
        $grossSalary = 0;

        $prepAppraisals = PrepAppraisal::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->get();

        $workingDayObj = PrepWorkingDay::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        $totalWorkingDays = $workingDayObj->company_working_days;
        $userWorkedDays = $workingDayObj->user_worked_days;

        $keyObj = AppraisalComponentType::where('code', $key)->first();

        if (count($prepAppraisals) > 0) {
            foreach ($prepAppraisals as $appraisal) {

                $startDate = $appraisal->start_date;
                $endDate = $appraisal->end_date;

                $totalAppraisal = ApprisalSalaryComponent::getComponentValueByKey($appraisal, $key, 1);

                if ($keyObj->is_conditional_prorata == 1 && $keyObj->prorata_function != null) {
                    $funcName = "" . $keyObj->prorata_function;
                    $totalGrossSalary = ApprisalSalaryComponent::$funcName($appraisal->id, $totalWorkingDays, $userWorkedDays, $totalAppraisal);
                } else if ($keyObj->is_conditional_prorata == 1 && $keyObj->prorata_function == null) {
                    $totalGrossSalary = ($totalAppraisal / $totalWorkingDays) * $userWorkedDays;

                } else {
                    $totalGrossSalary = $totalAppraisal;
                }

                $grossSalary += round($totalGrossSalary);
            }
        }

        return $grossSalary;
    }

    public function calculateValue($currentGrossId, $key)
    {
        $currentGrossObj = PrepCurrentGross::find($currentGrossId);
        $userId = $currentGrossObj->user_id;
        $prepSalaryId = $currentGrossObj->prep_salary_id;

        if ($key == 'annual-gross-salary') {
            $key = ['basic', 'hra', 'food-allowance', 'car-allowance', 'lta', 'special-allowance'];
            $grossSalary = self::getAnnualGrossSalary($prepSalaryId, $userId, $key);

        } elseif ($key == 'appraisal-bonus') {
            $appraisalBonusObj = new AppraisalBonusSalaryComponent();
            $appraisalBonusObj->setUserId($userId);
            $grossSalary = $appraisalBonusObj->getCurrentTotalAppraisalBonus($prepSalaryId);

        } elseif ($key == 'vpf') {
            $grossSalary = VpfSalaryComponent::getVpf($prepSalaryId, $userId);
        } else {
            $grossSalary = self::getAppraisalComponentValue($prepSalaryId, $userId, $key);

        }
        return $grossSalary;
    }

    public function getAnnualValueByKeys($keys, $prepSalaryId, $userId)
    {
        $totalAnnual = 0;

        foreach ($keys as $key) {
            $totalAnnual += $this->getValueByKey($key, $prepSalaryId, $userId);
        }

        return $totalAnnual;
    }

    public function getValueByKey($key, $prepSalaryId, $userId)
    {
        $keyValue = 0;
        $currentObj = PrepCurrentGross::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        if ($currentObj) {
            $currentValueObj = $currentObj->itemByKey($key);
            $keyValue = $currentValueObj->value;
        }
        return $keyValue;
    }

    public function getHtml()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $prepCurrentGrossObj = PrepCurrentGross::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->first();

        $response = [];

        if ($prepCurrentGrossObj) {
            $prepCurrentGross = $prepCurrentGrossObj->items;
            if (count($prepCurrentGross) > 0) {
                foreach ($prepCurrentGross as $item) {
                    $temp =[];
                    $temp['key'] = $item->key;
                    $temp['value'] = $item->value;
                    $response[] = $temp;
                }
            }
        }

        return $response;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $grossComponentType = PrepSalaryComponentType::where('code','current-gross')->first();
        if(!$grossComponentType)
            return false;
        $grossComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$grossComponentType->id)->first();
        if(!$grossComponent)
            return false;

        foreach($prepSalaryObj->prepUsers as $prepUser)
        {

            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$grossComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new CurrentGrossJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $grossComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new CurrentGrossJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }
    }
}
