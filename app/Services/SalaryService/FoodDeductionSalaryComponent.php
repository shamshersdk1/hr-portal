<?php
namespace App\Services\SalaryService;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Salary\PrepUser;
use App\Models\Salary\PrepSalaryComponent;
use App\Models\DateTime\Month;
use App\Models\Salary\PrepFoodDeduction;
use App\Jobs\PrepareSalary\Components\FoodDeductionJob;
use App\Models\Salary\PrepSalaryComponentType;
use App\Models\Salary\PrepSalaryExecution;
use App\Models\Salary\PrepSalary;
use App\Traits\ComponentDispatch;

class FoodDeductionSalaryComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;

    private $userId, $month, $year, $componentId, $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\FoodDeductionJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepFoodDeduction';
        $this->setPrepTableName($modelName);
    }
    public function getValue()
    {
        return array("Zack" => "Zara", "Anthony" => "Any",
            "Ram" => "Rani", "Salim" => "Sara",
            "Raghav" => "Ravina");
    }
    public function setMonthYear($month, $year) {
        $this->month = $month;
        $this->year = $year;
    }
    public function setComponent($componentId) {
        $this->componentId = $componentId;
    }
    public function setUserId($userId) {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $monthObj = Month::find($componentObj->salary->month_id);
        if($monthObj->monthlyFoodDeductionSetting?$monthObj->monthlyFoodDeductionSetting->value == "open":true)
        {
            $response['errors'] = "Food deduction Month not locked";
            $response['status'] = true;
            return $response;
        }
    }

    public function  getHtml(){
        $componentObj = PrepSalaryComponent::find($this->componentId);
        $foodDeduction=PrepFoodDeduction::where('prep_salary_id',$componentObj->salary->id)->where('user_id',$this->userId)->first();
        $response=[];
        $response['amount'] = 0;
        if($foodDeduction)
        {
            $response['amount']= $foodDeduction->value;
        }
        return $response;
    }

    public static function getFoodDeduction($prepSalaryId,$userId){
        $foodDeductionObj=PrepFoodDeduction::where('prep_salary_id',$prepSalaryId)->where('user_id',$userId)->first();
        return $foodDeductionObj->value;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;

        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $foodComponentType = PrepSalaryComponentType::where('code','food-deduction')->first();
        if(!$foodComponentType)
            return false;
        $foodComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$foodComponentType->id)->first();
        if(!$foodComponent)
            return false;
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$foodComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new FoodDeductionJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $foodComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new FoodDeductionJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }

    }

}
