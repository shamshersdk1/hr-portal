<?php
namespace App\Services\SalaryService;

use App\Jobs\PrepareSalary\Components\GrossPaidJob;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Loan\LoanInterest;
use App\Models\Salary\PrepGrossPaid;
use App\Models\Salary\PrepGrossPaidItem;
use App\Models\Salary\PrepSalary;
use App\Models\Salary\PrepSalaryComponent;
use App\Models\Salary\PrepUser;
use App\Models\Transaction;
use App\Services\SalaryService\PrepSalaryComponentInterface;
use App\Models\Salary\PrepSalaryComponentType;
use App\Models\Salary\PrepSalaryExecution;
use App\Traits\ComponentDispatch;

class GrossPaidComponent implements PrepSalaryComponentInterface
{
    use ComponentDispatch;
    private $userId, $month, $year, $componentId, $jobName;

    public function __construct()
    {
        $resolveName = 'App\Jobs\PrepareSalary\Components\GrossPaidJob';
        $this->setJobName($resolveName);
        $modelName = 'App\Models\Salary\PrepGrossPaid';
        $this->setPrepTableName($modelName);
    }

    public function getValue()
    {
        return array("Zack" => "Zara", "Anthony" => "Any",
            "Ram" => "Rani", "Salim" => "Sara",
            "Raghav" => "Ravina");
    }

    public function setMonthYear($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }

    public function setComponent($componentId)
    {
        $this->componentId = $componentId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setJobName($jobName)
    {
        $this->jobName = $jobName;
    }
    public function getJobName()
    {
        return $this->jobName;
    }
    public function getComponent()
    {
        return $this->componentId;
    }
    public function setPrepTableName($prepTableName)
    {
        $this->prepTableName = $prepTableName;
    }
    public function getPrepTableName()
    {
        return $this->prepTableName;
    }
    public function checkLock()
    {
        $response['status'] = false;
        return $response;
    }

    public function generateUserData($prepSalaryId)
    {
        $response['status'] = true;
        $response['errors'] = '';
        $appraisalbonusTypes = AppraisalBonusType::get()->pluck('code');
        $keys = array_merge(['basic', 'hra', 'car-allowance', 'food-allowance', 'special-allowance', 'lta', 'professional-tax', 'tds', 'pf-other', 'pf-employeer', 'vpf', 'other-annual-bonus', 'other-bonus', 'loan-interest-income','adhoc-payment','salary_paid'], $appraisalbonusTypes->toArray());

        $checkGross = PrepGrossPaid::where('user_id', $this->userId)->where('prep_salary_id', $prepSalaryId)->first();

        if ($checkGross && $checkGross->prepSalary->status != 'completed') {
            PrepGrossPaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $this->userId)->delete();
            PrepGrossPaidItem::where('prep_gross_paid_id', $checkGross->id)->delete();

        }
        // add transaction
        $grossSalObj = new PrepGrossPaid;
        $grossSalObj->prep_salary_id = $prepSalaryId;
        $grossSalObj->user_id = $this->userId;

        if (!$grossSalObj->save()) {
            foreach ($grossSalObj->getError() as $error) {
                $response['errors'] .= $error . " # " . $this->userId . '<br/>';
            }
        }

        foreach ($keys as $key) {
            $getValue = $this->calculateValue($grossSalObj->id, $key);
            $checkItem = PrepGrossPaidItem::where('prep_gross_paid_id', $grossSalObj->id)->where('key', $key)->first();
            if ($checkItem) {
                $response['status'] = false;
                $response['errors'] = $response['errors'] . "Prep Gross Paid Item Record - " . $grossSalObj->id . " already exist <br/>";
                continue;
            }
            $paidObj = new PrepGrossPaidItem;
            $paidObj->prep_gross_paid_id = $grossSalObj->id;
            $paidObj->key = $key;
            $paidObj->value = $getValue;
            if (!$paidObj->save()) {
                $response['status'] = false;
                $response['errors'] = "Annual Gross Salary key not saved for " . $grossSalObj->id . '<br/>';
            }
        }
        return $response;
    }
    public function getAppraisalComponentValue($key, $prepSalaryId, $userId)
    {

        return 0;
    }
    public function calculateValue($paidId, $key)
    {
        if ($key == 'loan-interest-income') {
            $grossPaidObj = PrepGrossPaid::find($paidId);
            $prepSalaryObj = PrepSalary::find($grossPaidObj->prep_salary_id);

            $amount = LoanInterest::getGrossLoanInterest($prepSalaryObj->month_id, $grossPaidObj->user_id);
        }
        elseif($key == 'salary_paid'){
            $grossPaidObj = PrepGrossPaid::find($paidId);
            $itSavingObj = new ItSavingSalaryComponent();
            $amount = $itSavingObj->getValueByKey($key,$grossPaidObj->prep_salary_id,$grossPaidObj->user_id);
        }
        else {
            $amount = Transaction::getAppraisalComponentValue($paidId, $key);
        }
        return $amount;
    }

    public function getHtml()
    {
        $componentObj = PrepSalaryComponent::find($this->componentId);

        $PrepGrossPaidObj = PrepGrossPaid::where('prep_salary_id', $componentObj->salary->id)->where('user_id', $this->userId)->first();

        $response = [];
        if ($PrepGrossPaidObj) {
            $prepGrossPaidItems = $PrepGrossPaidObj->items;
            if ($prepGrossPaidItems) {
                foreach ($prepGrossPaidItems as $item) {
                    $temp = [];
                    $temp['key'] = $item->key;
                    $temp['value'] = $item->value;
                    $response[] = $temp;
                }
            }
        }

        return $response;
    }
    public function getAnnualValueByKeys($keys, $prepSalaryId, $userId)
    {
        $totalAnnual = 0;

        foreach ($keys as $key) {
            $totalAnnual += $this->getValueByKey($key, $prepSalaryId, $userId);
        }

        return $totalAnnual;
    }
    public function getValueByKey($key, $prepSalaryId, $userId)
    {
        $keyValue = 0;
        $paidObj = PrepGrossPaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        if ($paidObj) {
            $paidValueObj = $paidObj->itemByKey($key);
            if($paidValueObj)
                $keyValue = $paidValueObj->value;
        }
        return $keyValue;
    }
    public function getCTCPaidBonus($prepSalaryId, $userId)
    {
        $keyValue = 0;

        $appraisalbonusTypes = AppraisalBonusType::get()->pluck('code');
        $keys = array_merge(['other-annual-bonus'], $appraisalbonusTypes->toArray());

        $paidObj = PrepGrossPaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        if ($paidObj) {
            foreach ($keys as $key) {
                $paidValueObj = $paidObj->itemByKey($key);
                $keyValue += $paidValueObj->value;
            }

        }

        return $keyValue;
    }
    public function getNonCTCPaidBonus($prepSalaryId, $userId)
    {
        $keyValue = 0;

        $appraisalbonusTypes = AppraisalBonusType::get()->pluck('code');
        $key = 'other-bonus';

        $paidObj = PrepGrossPaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        if ($paidObj) {
            $paidValueObj = $paidObj->itemByKey($key);
            $keyValue += $paidValueObj->value;
        }

        return $keyValue;
    }

    public function getAdhocPaymentPaid($prepSalaryId, $userId)
    {
        $keyValue = 0;
        $key = 'adhoc-payment';
        $paidObj = PrepGrossPaid::where('prep_salary_id', $prepSalaryId)->where('user_id', $userId)->first();
        if ($paidObj) {
            $paidValueObj = $paidObj->itemByKey($key);
            $keyValue += $paidValueObj->value;
        }
        return $keyValue;
    }

    public function queue()
    {
        $prepSalaryComponent = PrepSalaryComponent::find($this->componentId) ;
        if(!$prepSalaryComponent)
            return false;
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        $userComponentTypeId = PrepSalaryComponentType::where('code','user')->first();
        if(!$userComponentTypeId)
            return false;
        $grossComponentType = PrepSalaryComponentType::where('code','gross-paid-till-now')->first();
        if(!$grossComponentType)
            return false;
        $grossComponent = $prepSalaryObj->components->where('prep_salary_component_type_id',$grossComponentType->id)->first();
        if(!$grossComponent)
            return false;

        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$grossComponent->id)->where('user_id',$prepUser->user_id)->first();
            if($prepSalaryExecution)
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new GrossPaidJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
            else
            {
                $prepSalaryExecutionObj = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryComponent->prep_salary_id,'component_id' => $grossComponent->id,'user_id' => $prepUser->user_id,'status' => 'init','counter' => 0]);
                if (!$prepSalaryExecutionObj->isValid()) {
                    continue;
                }
                dispatch(new GrossPaidJob($prepUser->user_id, $prepSalaryComponent->prep_salary_id));
            }
        }

    }
}
