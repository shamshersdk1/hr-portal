<?php
namespace App\Services\User;
use App\Models\DateTime\CalendarYear;
use App\Models\Users\User;
use App\Models\Leave\ { LeaveCalendarYear, UserLeaveTransaction, LeaveType, UserLeaveBalance};
use App\Services\Leave\NewLeaveService;
use App\Services\Leave\UserLeaveService;
use App\Jobs\Leave\UserLeaveUpdateJob;
use App\Notifications\UserInfoUpdateNotification;
use OwenIt\Auditing\Models\Audit;
use Carbon\Carbon;




class UserService
{
    public static function updateUserLeaveBalance($user_id, $year)
    {
        $year = $year ?: date('Y');
        $calendarYearObj = CalendarYear::where('year', $year)->first();
        $user = User::find($user_id);
        if (!$user || !$calendarYearObj) {
            return false;
        }
        dispatch(new UserLeaveUpdateJob($user->id,$calendarYearObj->id));
    }
    public static function getCarryForward($user_id, $currYear)
    {
        $currYear = $currYear ?: date('Y');
        $currYearObj = CalendarYear::where('year', $currYear)->first();
        if (!$currYearObj) {
            return;
        }
        $prevYear = $currYear - 1;
        $prevYearObj = CalendarYear::where('year', $prevYear)->first();
        if (!$prevYearObj) {
            return;
        }
        $userObj = User::find($user_id);
        if (!$userObj) {
            return;
        }
        $PLObj = LeaveType::where('code', 'paid')->first();
        if (!$PLObj) {
            return;
        }
        $prevPLObj = UserLeaveService::getUserAvailableLeaveBalance($prevYear, $userObj->id);
        $currPLObj = UserLeaveService::getUserAvailableLeaveBalance($currYear, $userObj->id);
        $prevPLCount = $prevPLObj["paid"];
        $currPLCount = $currPLObj["paid"];
        if ($currPLCount + $prevPLCount > 26) {
            $carryForward = 26 - $currPLCount;
        } else {
            $carryForward = $prevPLCount;
        }
        if ($carryForward > 0) {
            $data['leave_type_id'] = $PLObj->id;
            $data['days'] = $carryForward;
            $data['reference_type'] = "App\Models\Leave\LeaveCalendarYear";
            $data['reference_id'] = $prevYearObj->id;
            $data['user_id'] = $user_id;
            $data['calendar_year_id'] = $currYearObj->id;
            UserLeaveBalance::addUserLeaves($data);
        }
        return;
    }
   
    public static function sendmailOnBankAccountUpdate($id, $accountName)
    {   
        $audit = Audit::find($id);
        $user = User::find($audit->user_id);

        $title = "The title of your message";
        $old_values = [$accountName => $audit->old_values['account_number']?? null];
        $new_values = [$accountName => $audit->new_values['account_number']?? null];
        
        if(count($new_values)>0){
            $user->notify(new UserInfoUpdateNotification($title,$old_values,$new_values,$user));
        }
    }


    public static function getAudits($start, $end)
    {
        $userAudits = Audit::orderBy('created_at')->whereBetween('updated_at',[$start,$end])->whereIn('auditable_type',['App\Models\Users\User','App\Models\Users\UserDetail'])->where('event','updated')->get();

        $data = [];
        foreach($userAudits as $audit)
        {
            $userId = NULL;
            if($audit && $audit->auditable_type != 'App\Models\Users\User' ){
                $userId = $audit->auditable->user->id;
            } else {
                $userId = $audit->auditable->id;
            }
            $temp['new_values']= $audit->new_values;
            $temp['old_values']= $audit->old_values;
            $data[$userId][] = $temp;
        }   
        return $data;
    }
    public static function getBankAccountAudits($start,$end)
    {
       
        $bankAudits = Audit::orderBy('created_at')->whereBetween('updated_at',[$start,$end])->whereIn('auditable_type',['App\Models\Bank\Account','App\Models\Bank\AccountMetaValue'])->where('event','updated')->get();
        $data = [];
        foreach($bankAudits as $audit)
        {
            $userId = NULL;
            if($audit->auditable_type == 'App\Models\Bank\Account' ){
                $userId = $audit->auditable->reference->id;
            }elseif($audit->auditable_type == 'App\Models\Bank\AccountMetaValue' ){
                $userId = $audit->auditable->account->reference->id;
            }
            $temp['new_values']= $audit->new_values;
            $temp['old_values']= $audit->old_values;
            $data[$userId][] = $temp;
        }   
        return $data;
    }
}
