<?php namespace App\Services\Loan;


use Redirect;
use Validator;
use App\Models\Users\User;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalBonus;
use App\Models\Loan\LoanEmi;
use App\Models\Loan\LoanRequest;
use App\Notifications\LoanReminderNotification;


class LoanService
{
    public static function loanEligibleCheck($userId,$appraisalBonusId,$amount)
    {
        $response['status'] = false;
        $userObj = User::find($userId);
        if(!$userObj) {
            $response['error'] = "Invalid User #".$userId;
                $response['status'] = true;
            return $response;
        }
        $effectiveAppraisal = Appraisal::where('user_id',$userId)->whereDate('effective_date','<=',date('Y-m-d'))->first();
        if(!$effectiveAppraisal)
        {
            $response['error'] = "No Appraisal Found!";
            $response['status'] = true;
            return $response;
        }
        if(!$appraisalBonusId){
            $response['appraisal_bonus_id'] = null;
            return $response;
        }
        $appraisalBonuTypeObj = AppraisalBonusType::find($appraisalBonusId);
        if(!$appraisalBonuTypeObj) {
            $response['error'] = "Invalid Bonus Type #".$appraisalBonusId;
            $response['status'] = true;
            return $response;
        }
        $appraisalBonusObj = AppraisalBonus::where('appraisal_id',$effectiveAppraisal->id)->where('appraisal_bonus_type_id',$appraisalBonusId)->first();
        if(!$appraisalBonusObj) {
            $response['error'] = "Not elegible to apasdply for ".$appraisalBonuTypeObj->description;
            $response['status'] = true;
            return $response;

        }
        if($amount > $appraisalBonusObj->value) {
            $response['error'] = "Loan amount cannot exceed more than ".$appraisalBonuTypeObj->description ." amount";
            $response['status'] = true;
            return $response;

        }
        $response['appraisal_bonus_id'] = $appraisalBonusObj->id;
        return $response;
    }

    public static function sendLoanRequestReminder($id,$message)
    {
        $loanRequest = LoanRequest::find($id);
        $title = 'The following loan request(s) are pending for review.';
        $color = '#EFD01B';
        
        $loanRequest->notify(new LoanReminderNotification($message,$title,$color));
    }

    public static function getLoanDeductionNotificationData($monthId){
        $loanEmis = LoanEmi::whereMonth_id($monthId)->whereIs_manual_payment(0)->get();
        $data = [];
        foreach($loanEmis as $loanEmi)
        {
            $emiAmount = -min($loanEmi->loan->emi,max(0,($loanEmi->loan->amount - abs($loanEmi->loan->emis()->where('status','paid')->sum('amount')))));
            if($loanEmi->amount != $emiAmount)
            {
                $data[$loanEmi->user_id]['user_name'] = $loanEmi->user->name;
                $data[$loanEmi->user_id]['employee_id'] = $loanEmi->user->employee_id;
                $data[$loanEmi->user_id]['emi_current']=  $loanEmi->amount ?? '';
                $data[$loanEmi->user_id]['emi_actual'] = $emiAmount ?? '';
            }
            
        }
        return $data;
    }

}
