<?php
namespace App\Services\Tds;

use App\Models\AdhocPayments\AdhocPayment;
use App\Models\AdhocPayments\AdhocPaymentComponent;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\DateTime\FinancialYear;
use App\Models\DateTime\Month;
use App\Models\Salary\PrepTds;
use App\Models\Transaction;
use App\Services\Appraisal\AppraisalService;
use App\Models\Salary\PrepSalary;
use App\Models\Salary\UserSalary;

class TDSService
{
    public static function getUserTDSSummary($userId, $financialYearID)
    {
        $response = [];
        $response['data'] = null;
        $response['status'] = false;
        $response['errors'] = null;
        //here
        // $months = Month::where('financial_year_id', $financialYearID)->where('status', 'processed')->get();
        $months = Month::getAllProcessedMonths();

        $components = AppraisalComponentType::get();
        $bonuses = AppraisalBonusType::get();
        $adhocPaymentComponents = AdhocPaymentComponent::all();

        $data = [];
        $monthRow = [];

        $workingId = AppraisalComponentType::getIdByKey('working-days');

        //flags
        $data['fl_appr_bonus_head'] = 0;
        $data['fl_appr_bonus_other'] = 0;
        foreach ($bonuses as $bonus) {
            $data['fl_appr_bonus' . $bonus->id] = 0;
        }
        $data['fl_adhoc_pay_head'] = 0;
        foreach ($adhocPaymentComponents as $adhoc) {
            $data['fl_adhoc_pay' . $adhoc->id] = 0;
        }
        $data['fl_bonus_head'] = 0;
        $data['fl_bonus_onsite'] = 0;
        $data['fl_bonus_additional'] = 0;
        $data['fl_bonus_extra_hour'] = 0;
        $data['fl_bonus_tech_talk'] = 0;
        $data['fl_bonus_performance'] = 0;
        $data['fl_bonus_referral'] = 0;
        $data['fl_bonus_other'] = 0;

        $data['fl_deduct_head'] = 0;
        $data['fl_deduct_vpf'] = 0;
        $data['fl_deduct_food_deduction'] = 0;
        $data['fl_deduct_insurance'] = 0;
        $data['fl_deduct_loan'] = 0;

        foreach ($months as $id => $month) {
            foreach ($components as $component) {
                $obj = Transaction::where('reference_type', 'App\Models\Appraisal\AppraisalComponentType')->where('reference_id', $component->id)->where('user_id', $userId)->where('month_id', $month->id)->first();
                if ($obj) {
                    $data[$month->year . '_' . $month->month]['appraisal_components'][$component->id] = $obj->amount;
                }
            }
            $data[$month->year . '_' . $month->month]['month'] = $month->formatMonth();
            $workingDayObj = UserSalary::where('month_id', $month->id)->where('user_id', $userId)->first();
            if ($workingDayObj) {
                $data[$month->year . '_' . $month->month]['worked_days'] = $workingDayObj->working_day + $workingDayObj->lop;
            } else {
                $data[$month->year . '_' . $month->month]['worked_days'] = 0;
            }

            $data[$month->year . '_' . $month->month]['monthly_gross_salary'] = Transaction::getMonthyGrossSalary($userId, $month->id);
            $data[$month->year . '_' . $month->month]['monthly_bonus'] = Transaction::getAnnualBonusPaid($userId, $month->id);

            $vpf = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\VpfDeduction');
            if ($vpf != 0) {
                $data['fl_deduct_head'] = 1;
                $data['fl_deduct_vpf'] = 1;
                $data[$month->year . '_' . $month->month]['vpf'] = $vpf;
                $vpf = 0;
            }

            $food = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Audited\FoodDeduction\FoodDeduction');
            if ($food != 0) {
                $data['fl_deduct_head'] = 1;
                $data['fl_deduct_food_deduction'] = 1;
                $data[$month->year . '_' . $month->month]['food_deduction'] = $food;
                $food = 0;
            }

            $insurance = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\Admin\Insurance\InsuranceDeduction');
            if ($insurance != 0) {
                $data['fl_deduct_head'] = 1;
                $data['fl_deduct_insurance'] = 1;
                $data[$month->year . '_' . $month->month]['insurance'] = $insurance;
                $insurance = 0;
            }

            $loan = Transaction::getMontlyPaidByType($userId, $month->id, 'App\Models\LoanEmi');
            if ($loan != 0) {
                $data['fl_deduct_head'] = 1;
                $data['fl_deduct_loan'] = 1;
                $data[$month->year . '_' . $month->month]['loan'] = $loan;
                $loan = 0;
            }

            $apprBonus = Transaction::where('reference_type', 'App\Models\Appraisal\AppraisalBonus')->where('user_id', $userId)->where('month_id', $month->id)->get();
            $data[$month->year . '_' . $month->month]['appraisal_bonuses']['other'] = 0;
            foreach ($apprBonus as $bonus) {
                if ($bonus->amount != 0) {
                    if ($bonus->reference_id == null || $bonus->reference_id == 0) {
                        $data['fl_appr_bonus_head'] = 1;
                        $data['fl_appr_bonus_other'] = 1;
                        $data[$month->year . '_' . $month->month]['appraisal_bonuses']['other'] += $bonus->amount;
                    } else {
                        $obj = $bonus->reference;
                        if ($obj) {
                            $data['fl_appr_bonus_head'] = 1;
                            $data['fl_appr_bonus' . $obj->appraisal_bonus_type_id] = 1;
                            $data[$month->year . '_' . $month->month]['appraisal_bonuses'][$obj->appraisal_bonus_type_id] = $bonus->amount;
                        }
                    }
                }
            }

            $objs = Transaction::where('reference_type', 'App\Models\Audited\AdhocPayments\AdhocPayment')->where('user_id', $userId)->where('month_id', $month->id)->get();
            foreach ($objs as $obj) {
                $adhocPayment = AdhocPayment::find($obj->reference_id);
                if ($obj->amount != 0 && $adhocPayment->component->id != null) {
                    $data['fl_adhoc_pay_head'] = 1;
                    $data['fl_adhoc_pay' . $adhocPayment->component->id] = 1;
                    $data[$month->year . '_' . $month->month]['adhoc_payments'][$adhocPayment->component->id] = $obj->amount;
                }
            }
            $otherBonuses = Transaction::getMontlyOtherBonus($userId, $month->id, 'App\Models\Admin\Bonus');

            $data[$month->year . '_' . $month->month]['bonuses']['onsite'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['additional'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['extra_hour'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['tech_talk'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['performance'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['referral'] = 0;
            $data[$month->year . '_' . $month->month]['bonuses']['other'] = 0;

            foreach ($otherBonuses as $bonus) {
                if ($bonus->amount != 0) {
                    $data['fl_bonus_head'] = 1;
                    if ($bonus->reference_id == null || $bonus->reference_id == 0) {
                        $data['fl_bonus_other'] = 1;
                        $data[$month->year . '_' . $month->month]['bonuses']['other'] += $bonus->amount;
                    } elseif ($bonus->bonusType) {
                        if ($bonus->bonusType->type == 'onsite') {
                            $data['fl_bonus_onsite'] = 1;
                            $data[$month->year . '_' . $month->month]['bonuses']['onsite'] += $bonus->amount;
                        } elseif ($bonus->bonusType->type == 'referral') {
                            $data['fl_bonus_referral'] = 1;
                            $data[$month->year . '_' . $month->month]['bonuses']['referral'] += $bonus->amount;
                        } elseif ($bonus->bonusType->type == 'techtalk') {
                            $data['fl_bonus_tech_talk'] = 1;
                            $data[$month->year . '_' . $month->month]['bonuses']['tech_talk'] += $bonus->amount;
                        } elseif ($bonus->bonusType->type == 'additional') {
                            $data['fl_bonus_additional'] = 1;
                            $data[$month->year . '_' . $month->month]['bonuses']['additional'] += $bonus->amount;
                        } elseif ($bonus->bonusType->type == 'extra') {
                            $data['fl_bonus_extra_hour'] = 1;
                            $data[$month->year . '_' . $month->month]['bonuses']['extra_hour'] += $bonus->amount;
                        } elseif ($bonus->bonusType->type == 'performance') {
                            $data['fl_bonus_performance'] = 1;
                            $data[$month->year . '_' . $month->month]['bonuses']['performance'] += $bonus->amount;
                        }
                    }
                }
            }
        }
        //here
        $processedMonthId = Month::getLastProcessedMonth();
        if ($processedMonthId) {
            $monthObj = Month::find($processedMonthId);
        } else {
            $currentMonth = date('m');
            $currentYear = date('Y');
            $monthObj = Month::where('month', $currentMonth)->where('financial_year_id', $financialYearID)->first();
        }
        // $monthObj = Month::where('status', 'processed')->where('financial_year_id', $financialYearID)->orderBy('year', 'desc')->orderBy('month', 'desc')->first();
        if (!$monthObj) {
            $response['errors'] = 'Last processed month object not found!';
            $response['data'] = $data;
            return $response;
        }
        $appraisalList = AppraisalService::getCurrentMonthAppraisal($userId, $monthObj->id);

        if ($appraisalList == null || !isset($appraisalList[0])) {
            $response['errors'] = 'No Appraisal found!';
            $response['data'] = $data;
            return $response;
        }

        $futureAppraisal = $appraisalList->last();
        $futureData = AppraisalService::calculateFutureAppraisal($futureAppraisal->id, $monthObj->id, $userId);

        $upcomingMonths = FinancialYear::getUpcomingMonth($financialYearID, $monthObj->month);

        foreach ($upcomingMonths as $upcomingMonth) {
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['worked_days'] = $futureData['no_of_working'][$upcomingMonth->month];
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['monthly_gross_salary'] = $futureData[$upcomingMonth->month]['monthly_gross_salary'];
            $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['monthly_bonus'] = $futureData[$upcomingMonth->month]['monthly_bonus'];

            foreach ($components as $component) {
                $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['appraisal_components'][$component->id] = isset($futureData[$upcomingMonth->month][$component->id]) ? $futureData[$upcomingMonth->month][$component->id] : 0;
            }
            foreach ($bonuses as $bonus) {
                if (isset($futureData[$upcomingMonth->month]['appraisal_bonus'][$bonus->id]) && $futureData[$upcomingMonth->month]['appraisal_bonus'][$bonus->id] != 0) {
                    $data['fl_appr_bonus_head'] = 1;
                    $data['fl_appr_bonus' . $bonus->id] = 1;
                    $data[$upcomingMonth->year . '_' . $upcomingMonth->month]['appraisal_bonuses'][$bonus->id] = $futureData[$upcomingMonth->month]['appraisal_bonus'][$bonus->id];
                }
            }
        }

        $response['status'] = true;
        $response['data'] = $data;
        return $response;
    }

    public static function getUserAnnualSummary($tdsData, $financialYearId, $userId)
    {
        $summary = [];
        $prepSalariesArr = self::getUserPrepSalaries($userId, $financialYearId);
        //here
        // $months = Month::where('financial_year_id', $financialYearId)->where('status', 'processed')->get();
        $months = Month::getAllProcessedMonths();
        if (!$months || empty($months)) {
            return [];
        }
        foreach ($months as $id => $month) {
            if (!empty($prepSalariesArr) && isset($prepSalariesArr[$month->id])) {
                $prepSalId = $prepSalariesArr[$month->id];
                $tdsObj = PrepTds::where('prep_salary_id', $prepSalId)->where('user_id', $userId)->first();
                if ($tdsObj) {
                    if ($tdsObj->components != null && count($tdsObj->components) > 0) {
                        foreach ($tdsObj->components as $component) {
                            $summary[$month->year . '_' . $month->month][$component->key] = $component->value;
                        }
                    }
                }
            }
        }

        return $summary;
    }
    public static function getUserPrepSalaries($userId, $financialId)
    {
        $arr = [];
        //here
        // $months = Month::where('financial_year_id', $financialId)->where('status', 'processed')->get();
        $months = Month::getAllProcessedMonths();
        if (!$months || empty($months)) {
            return [];
        }
        $monthPrepCheck = PrepSalary::whereIn('status', ['closed', 'processed' , 'finalizing'])->orderBy('created_at', 'DESC')->first();
        if($monthPrepCheck)
        {
            $arr[$monthPrepCheck->month_id] = $monthPrepCheck->id;
        }
        foreach ($months as $month) {
            $transObj = Transaction::where('user_id', $userId)->where('month_id', $month->id)->where('prep_salary_id', '!=', 0)->first();
            if ($transObj) {
                $arr[$month->id] = $transObj->prep_salary_id;
            }
        }

        return $arr;
    }
}
