<?php

namespace App\Listeners\User;

use App\Events\User\UserUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Services\User\UserService;

class UserUpdatedHandle
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserUpdated  $event
     * @return void
     */
    public function handle(UserUpdated $event)
    {
        $year = date('Y');
        UserService::updateUserLeaveBalance($event->userId, $year);
    }
}
