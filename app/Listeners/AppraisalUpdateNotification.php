<?php

namespace App\Listeners;

use App\Events\AppraisalUpdate;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Appraisal\{ Appraisal, AppraisalBonusType };
use Illuminate\Support\Carbon;
use App\Notifications\AppraisalAuditNotification;
use App\Models\SystemSetting\SystemSetting;
use Notification;

class AppraisalUpdateNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AppraisalUpdate  $event
     * @return void
     */
    public function handle(AppraisalUpdate $event)
    {
        $isNotifyUser = SystemSetting::where('key','AppraisalAuditUserEmailNotify')->first();
        $isNotifyAdmin = SystemSetting::where('key','AppraisalAuditAdminEmailNotify')->first();

        $bonusTypes = AppraisalBonusType::all();
        $date = datetime_in_view(Carbon::now());
        $appraisal = Appraisal::find($event->appraisalId);
        $subject = 'Appraisal | Update | '.$appraisal->user->name.' | '. $appraisal->user->employee_id;    
        if($isNotifyUser && $isNotifyUser->value == 1) {
            \Log::info('Appraisal Update template sent to user : '.$appraisal->user->email);
            $appraisal->user->notify(new AppraisalAuditNotification($subject, $appraisal->user, $date, $appraisal, $bonusTypes));
        }
        if($isNotifyAdmin && $isNotifyAdmin->value == 1) {
            $isNotifyAdminEmails = SystemSetting::where('key','AppraisalAuditAdminEmailNotityEmailAddress')->first();
            if($isNotifyAdmin && !empty($isNotifyAdminEmails->value)) {
                \Log::info('Appraisal Update template sent to admin : '.$isNotifyAdminEmails->value);
                Notification::route('mail', $isNotifyAdminEmails->value)
                    ->notify(new AppraisalAuditNotification($subject, $appraisal->user, $date, $appraisal, $bonusTypes));
            }
        }
    }
}
