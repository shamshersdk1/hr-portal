<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Queue\Events\JobFailed;
use App\Models\AccessLog;
use DB;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::before(function (JobProcessing $event) {
            DB::beginTransaction();
        });

        Queue::after(function (JobProcessed $event) {
            DB::commit();
        });

        Queue::failing(function ( JobFailed $event ) {
            DB::rollBack();
            AccessLog::accessLog(null, $event->job->resolveName(), 'NA', 'handle', 'catch-block', $event->exception->getMessage());
        });

        Blade::component('components.appraisal.appraisal-info','appraisalinfo');
    }
}
