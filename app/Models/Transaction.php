<?php

namespace App\Models;

use App\Models\Appraisal\AppraisalBonus;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Bank\Account;
use App\Models\Bank\AccountType;
use App\Models\BaseModel;
use App\Models\DateTime\Month;
use App\Models\Salary\PrepGrossPaid;
use App\Models\Salary\PrepSalary;
use App\Models\Salary\UserSalary;
use DB;
use Watson\Validating\ValidatingTrait;

class Transaction extends BaseModel
{
    use ValidatingTrait;
    protected $fillable = ['month_id', 'financial_year_id', 'user_id', 'reference_id', 'reference_type', 'amount', 'type', 'is_company_expense', 'prep_salary_id'];
    protected $rules = [
        'month_id' => 'required | exists:months,id',
        'financial_year_id' => 'required | exists:financial_years,id',
        'reference_type' => 'required | string',
        'reference_id' => 'required | numeric',
        'type' => 'required | string',
        'amount' => 'required | numeric',
        'user_id' => 'required | exists:users,id',
        //'prep_salary_id' => 'required | exists:prep_salary,id'
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id');
    }
    public function bonusType()
    {
        return $this->belongsTo('App\Models\Bonus\Bonus', 'reference_id');
    }
    public function reference()
    {
        return $this->morphTo();
    }
    public static function getMonthyGrossSalary($userId, $monthId)
    {
        $transactions = self::where('user_id', $userId)->where('month_id', $monthId)->get();
        $salary = 0;

        foreach ($transactions as $transaction) {
            if ($transaction->reference_type == 'App\Models\Appraisal\AppraisalComponentType') {
                if ($transaction->reference->code == 'basic' || $transaction->reference->code == 'hra' || $transaction->reference->code == 'car-allowance' || $transaction->reference->code == 'food-allowance' || $transaction->reference->code == 'special-allowance' || $transaction->reference->code == 'lta' || $transaction->reference->code == 'stipend') {
                    $salary += $transaction->amount;
                }
            }
        }
        return $salary;
    }

    public static function getAnnualBonusPaid($userId, $monthId)
    {
        $refType = 'App\Models\Appraisal\AppraisalBonusType';
        $transactions = self::where('user_id', $userId)->where('month_id', $monthId)->where('reference_type', $refType)->get();
        $bonus = 0;
        foreach ($transactions as $transaction) {
            $bonus += $transaction->amount;
        }
        return $bonus;
    }

    public static function getTotalEarning($userId, $monthId)
    {
        /*
        Total Earnings For the month = Monthly Gross Salary + Annual Bonus + Confirmation Bonus  + Monthly Variable Bonus
         */

        return self::getMonthyGrossSalary($userId, $monthId);
    }
    public static function getTotalDeduction($userId, $monthId)
    {
        /*
    Total Deductions for the month =  Employee’s P.F + Voluntary P.F (VPF) + Employee’s E.S.I + Professional Tax +
    Food    Deduction + Medical Insurance + T.D.S + Loan Deduction
     */
    }
    public static function getPaidTillDate($userId, $financialYearId, $referenceType, $referenceId = null)
    {
        if ($referenceId == null) {
            return self::where('user_id', $userId)->where('financial_year_id', $financialYearId)->where('reference_type', $referenceType)->sum('amount');
        } else {
            return self::where('user_id', $userId)->where('financial_year_id', $financialYearId)->where('reference_type', $referenceType)->where('reference_id', $referenceId)->sum('amount');
        }
    }
    public static function getMontlyPaidByType($userId, $monthId, $referenceType, $referenceId = null)
    {
        if ($referenceId == null) {
            return self::where('user_id', $userId)->where('month_id', $monthId)->where('reference_type', $referenceType)->sum('amount');
        } else {
            $transObj = self::where('user_id', $userId)->where('month_id', $monthId)->where('reference_type', $referenceType)->where('reference_id', $referenceId)->first();
            return $transObj->amount;
        }

    }
    public static function getPrevMonthNet($userId)
    {
        $date = date('Y-m-d');
        $userSalaryObj = UserSalary::where('user_id', $userId)->where('status','completed')->orderBy('month_id','DESC')->skip(1)->take(1)->first();
        
        if(!$userSalaryObj)
            return 0;

        $netPayableLastMonth = self::where('is_company_expense', 0)->where('user_id', $userId)->where('month_id', $userSalaryObj->month_id)->where('prep_salary_id','<>',0)->sum('amount'); 
        return $netPayableLastMonth;
    }
    public static function getMontlyOtherBonus($userId, $monthId, $referenceType, $referenceId = null)
    {
        $bonuses = self::where('user_id', $userId)->where('month_id', $monthId)->where('reference_type', $referenceType)->get();
        return $bonuses;
    }

    public static function getAppraisalComponentValue($paidId, $key)
    {
        $grossPaidObj = PrepGrossPaid::find($paidId);
        $prepSalaryObj = PrepSalary::find($grossPaidObj->prep_salary_id);
        $monthObj = Month::find($prepSalaryObj->month_id);

        $userId = $grossPaidObj->user_id;

        $refType = 'App\Models\Appraisal\AppraisalComponentType';

        $amount = 0;
        if ($key == 'vpf') {
            $amount = Transaction::where('user_id', $userId)->where('financial_year_id', $monthObj->financial_year_id)->where('reference_type', 'App\Models\Admin\VpfDeduction')->sum('amount');
        } elseif ($key == 'other-annual-bonus') {
            $amount = Transaction::where('user_id', $userId)->where('financial_year_id', $monthObj->financial_year_id)->where('reference_id', 0)->where('reference_type', 'App\Models\Appraisal\AppraisalBonus')->sum('amount');
            // foreach ($transObj as $trans) {
            //     //if ($trans->reference_id == null || $trans->reference_id == 0) {
            //     $amount += $trans->amount;
            //     //}
            // }
        } elseif ($key == 'other-bonus') {
            $amount = Transaction::where('user_id', $userId)->where('financial_year_id', $monthObj->financial_year_id)->where('reference_type', 'App\Models\Admin\Bonus')->sum('amount');
        } elseif ($key == 'adhoc-payment') {
            $amount = Transaction::where('user_id', $userId)->where('financial_year_id', $monthObj->financial_year_id)->where('amount', '>',0)->where('reference_type', 'App\Models\Audited\AdhocPayments\AdhocPayment')->sum('amount');
        } else {
            $apprCompTypeObj = AppraisalComponentType::where('code', $key)->first();
            if ($apprCompTypeObj) {
                $referenceId = $apprCompTypeObj->id;
                $transAmount = Transaction::where('user_id', $userId)->where('financial_year_id', $monthObj->financial_year_id)->where('reference_type', $refType)->where('reference_id', $referenceId)->sum('amount');
                $amount = $transAmount;
            } else {
                $apprBonusTypeObj = AppraisalBonusType::where('code', $key)->first();
                if ($apprBonusTypeObj) {
                    $transObj = Transaction::where('user_id', $userId)->where('financial_year_id', $monthObj->financial_year_id)->where('reference_type', 'App\Models\Appraisal\AppraisalBonus')->get();
                    foreach ($transObj as $trans) {
                        $apprObj = AppraisalBonus::find($trans->reference_id);
                        if ($apprObj && $apprObj->appraisal_bonus_type_id == $apprBonusTypeObj->id) {
                            $amount += $trans->amount;
                        }
                    }
                }
            }

        }
        return $amount;
    }
    public static function getNetPayableExBonus($userId, $monthId) {
        $amount = Transaction::where('user_id',$userId)->where('month_id',$monthId)->where('is_company_expense',0)->where('prep_salary_id','<>',0)->whereIn('reference_type',['App\Models\Appraisal\AppraisalComponentType','App\Models\Admin\Insurance\InsuranceDeduction','App\Models\Audited\FoodDeduction\FoodDeduction'])
        ->sum('amount');
        return $amount ?? 0;
    }
    public static function saveTransaction($data)
    {

        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;

        $obj = new self;
        // $obj->date = !empty($data['date']) ? $data['date'] : date("Y-m-d");
        $obj->month_id = !empty($data['month_id']) ? $data['month_id'] : null;
        $obj->financial_year_id = !empty($data['financial_year_id']) ? $data['financial_year_id'] : null;
        $obj->user_id = !empty($data['user_id']) ? $data['user_id'] : null;
        $obj->reference_id = !empty($data['reference_id']) ? $data['reference_id'] : 0;
        $obj->reference_type = !empty($data['reference_type']) ? $data['reference_type'] : null;
        $obj->amount = !empty($data['amount']) ? $data['amount'] : null;
        $obj->type = !empty($data['type']) ? $data['type'] : null;
        $obj->is_company_expense = !empty($data['is_company_expense']) ? $data['is_company_expense'] : 0;

        if (!$obj->save()) {
            $response['message'] = $obj->getErrors();
        } else {
            $response['status'] = true;
            $response['message'] = "Transaction added successfully";
            $response['data'] = $obj->toArray();
            $response['id'] = $obj->id;
        }
        return $response;
    }

    public static function getSalarySheet($monthId)
    {
        $sql = "SELECT  employee_id, name,`user_id`, working_days, `working_days`+`lop` as days_worked, `basic`+`hra`+`car_allowance`+`food_allowance`+`special_allowance`+ `lta`+`stipend` as monthly_gross_salary, `basic`,`hra`,`car_allowance`,`food_allowance`,`food_deduction`,`pf_employee`, `pf_employeer`,`pf_other`,`esi`, `esi_employer`,`professional_tax`,`lta`,`stipend`,`special_allowance`,`tds`,`vpf`, `loan_deduction`,`adhoc_amount`, `medical_insurance`, `ctc_bonus`,`non_ctc_bonus`, 0 as onsite_bonus,0 as additional_work_day_bonus, 0 as extra_hour_bonus,0 as tech_talk_bonus,0 as `performance_bonus`,0 as referral_bonus,`food_deduction`+ `pf_employee`+ `esi`+ `professional_tax`+`tds`+ `medical_insurance` + `vpf`+ `loan_deduction` as total_deduction, `basic`+`hra`+`car_allowance`+`food_allowance`+`food_deduction`+`pf_employee`+`esi`+`professional_tax`+`lta`+`stipend`+`special_allowance`+`tds`+`vpf`+`loan_deduction`+ `adhoc_amount`+ `medical_insurance`+ `ctc_bonus` as `net_payable`
            from (SELECT u.employee_id, u.name, t.user_id as user_id, wd.working_days as working_days, wd.lop as lop,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 1  THEN t.amount ELSE 0 END) `basic`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 2  THEN t.amount ELSE 0 END) `hra`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 3  THEN t.amount ELSE 0 END) `car_allowance`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 4  THEN t.amount ELSE 0 END) `food_allowance`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Audited\\\FoodDeduction\\\FoodDeduction' THEN t.amount ELSE 0 END) `food_deduction`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 5  THEN t.amount ELSE 0 END) `pf_employee`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 6  THEN t.amount ELSE 0 END) `pf_employeer`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 7  THEN t.amount ELSE 0 END) `pf_other`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 8  THEN t.amount ELSE 0 END) `esi`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 9  THEN t.amount ELSE 0 END) `esi_employer`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 10  THEN t.amount ELSE 0 END) `professional_tax`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 11  THEN t.amount ELSE 0 END) `lta`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 12  THEN t.amount ELSE 0 END) `stipend`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 13  THEN t.amount ELSE 0 END) `special_allowance`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalComponentType' AND t.reference_id = 14  THEN t.amount ELSE 0 END) `tds`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Appraisal\\\AppraisalBonus'  THEN t.amount ELSE 0 END) `ctc_bonus`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Admin\\\Bonus'  THEN t.amount ELSE 0 END) `non_ctc_bonus`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Admin\\\VpfDeduction'  THEN t.amount ELSE 0 END) `vpf`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Admin\\\Insurance\\\InsuranceDeduction'  THEN t.amount ELSE 0 END) `medical_insurance`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\LoanEmi'  THEN t.amount ELSE 0 END) `loan_deduction`,
                SUM(CASE WHEN t.reference_type = 'App\\\Models\\\Audited\\\AdhocPayments\\\AdhocPayment'  THEN t.amount ELSE 0 END) `adhoc_amount`
             FROM `transactions` t, `users` u, `user_details` ud, `working_days` wd
             WHERE t.month_id =$monthId AND u.id= t.user_id AND ud.user_id = u.id AND wd.month_id = t.month_id AND wd.user_id = t.user_id AND u.id = wd.user_id and t.prep_salary_id != 0
             GROUP BY t.user_id
            ORDER BY `u`.`employee_id` ASC) AS TEST";

        $data = DB::select(DB::RAW($sql));
        $resultArray = json_decode(json_encode($data), true);
        $i = 1;

        $savingObj = AccountType::where('code', 'saving-account')->first();

        foreach ($resultArray as &$user) {
            $user['sl_no'] = $i++;
            if ($savingObj) {
                $account = Account::where('bank_account_type_id', $savingObj->id)->where('reference_type', 'App\Models\Users\User')->where('reference_id', $user['user_id'])->first();
                if ($account) {
                    $user['bank_ac_no'] = $account->account_number;
                }
            }
            $bonuses = Transaction::where('reference_type',
                'App\Models\Admin\Bonus')->where('user_id', $user['user_id'])->where('month_id', $monthId)->get();
            if (count($bonuses) > 0) {
                foreach ($bonuses as $bonus) {

                    if ($bonus->reference->reference_type == 'App\Models\Admin\OnSiteBonus') {
                        $user['onsite_bonus'] += $bonus->amount;
                    } elseif ($bonus->reference->reference_type == 'App\Models\Admin\AdditionalWorkDaysBonus') {
                        $user['additional_work_day_bonus'] += $bonus->amount;
                    } elseif ($bonus->reference->reference_type == 'App\Models\User\UserTimesheetExtra') {
                        $user['extra_hour_bonus'] += $bonus->amount;
                    } elseif ($bonus->reference->reference_type == 'App\Models\Admin\PerformanceBonus') {
                        $user['performance_bonus'] += $bonus->amount;
                    } elseif ($bonus->reference->reference_type == 'App\Models\Admin\TechTalkBonusesUser') {
                        $user['tech_talk_bonus'] += $bonus->amount;
                    } elseif ($bonus->reference->reference_type == 'App\Models\Admin\ReferralBonus') {
                        $user['referral_bonus'] += $bonus->amount;
                    }
                    $user['net_payable'] += $bonus->amount;
                }
            }
        }
        return $resultArray;

    }
}
