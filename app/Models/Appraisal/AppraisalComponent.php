<?php

namespace App\Models\Appraisal;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Arr;
use Validator;

class AppraisalComponent extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $table='appraisal_components';
    public $timestamps=false;
    protected $fillable = ['appraisal_id','component_id','value'];
    protected $rules= [ 
        'appraisal_id'=>'required|exists:appraisals,id',
        'component_id'=>'required|exists:appraisal_component_types,id',
        //'value' =>'required | numeric',
    ];
    
    protected $auditExclude = [ 'id','appraisal_id','component_id'];
    public function transformAudit(array $data): array
    {
        if(Arr::has($data,'event')){
            if($data['event'] == "created"){
                $appraisalComponent = AppraisalComponent::find($data['auditable_id']);
                $appraisal = Appraisal::find($appraisalComponent->appraisal_id);
                $lastAppraisal = Appraisal::orderBy('effective_date','desc')->whereDate('effective_date','<',$appraisal->effective_date)->where('id','<>',$appraisal->id)->where('user_id', $appraisal->user_id)->first();
                if($lastAppraisal)
                {
                    $obj = AppraisalComponent::where('appraisal_id',$lastAppraisal->id)->where('component_id',$appraisalComponent->component_id)->first() ?? null;
                    if($obj && $obj->value)
                    {
                        $data['old_values'][str_replace('-', '_',$appraisalComponent->appraisalComponentType->code)] = $obj->value;
                    }
                }
            }
        }
        if (Arr::has($data, 'new_values.value')) {
            $code = str_replace('-', '_', AppraisalComponentType::find($this->getAttribute('component_id'))->code);
            if(!empty($data['old_values']['value']) && $data['event'] != 'created' ){
                $data['old_values'][$code] = $data['old_values']['value'];
            }
            if(!empty($data['new_values']['value'])){
                $data['new_values'][$code] = $data['new_values']['value'];
            }
        }
        $appraisalComponent = AppraisalComponent::find($data['auditable_id']);
        if (Arr::has($data, 'old_values.'.str_replace('-', '_',$appraisalComponent->appraisalComponentType->code)) && !(Arr::has($data, 'new_values.'.str_replace('-', '_',$appraisalComponent->appraisalComponentType->code))))
        {
            $data['new_values'][str_replace('-', '_',$appraisalComponent->appraisalComponentType->code)] = $appraisalComponent->value;
        }
        unset($data['old_values']['value']);
        unset($data['new_values']['value']);
        return $data;
    }
    public function appraisalComponentType(){
        return $this->belongsTo('App\Models\Appraisal\AppraisalComponentType','component_id','id');
    }
    public function appraisalType(){
        return $this->belongsTo('App\Models\Appraisal\AppraisalType','appraisal_id','id');
    }
    public function appraisal()
    {
        return $this->hasOne('App\Models\Appraisal\Appraisal', 'id', 'appraisal_id');
    }
}