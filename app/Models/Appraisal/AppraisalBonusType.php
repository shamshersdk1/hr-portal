<?php

namespace App\Models\Appraisal;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class AppraisalBonusType extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'appraisal_bonus_type';
    public $timestamps = false;
    protected $fillable = ['code', 'description'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($appraisalBonusObj) {
            if (count($appraisalBonusObj->appraisalBonuses) > 0) {
                return false;
            }
        });
    }

    protected $rules = [
        'code' => 'required|unique:appraisal_bonus_type',
        'description' => 'required | string',
    ];

    public function appraisalBonuses()
    {
        return $this->hasMany('App\Models\Appraisal\AppraisalBonus', 'appraisal_bonus_type_id');
    }
}
