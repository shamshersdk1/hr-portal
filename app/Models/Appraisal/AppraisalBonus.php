<?php

namespace App\Models\Appraisal;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;
use App\Models\Appraisal\AppraisalBonusType;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;


class AppraisalBonus extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $table = 'appraisal_bonuses';
    public $timestamps = false;
    protected $fillable = ['appraisal_id', 'appraisal_bonus_type_id', 'value', 'value_date'];

    protected $rules = [
        'appraisal_id' => 'required|exists:appraisals,id',
        'appraisal_bonus_type_id' => 'required|exists:appraisal_bonus_type,id',
        'value' => 'required | numeric',
    ];
    protected $auditExclude = [ 'id','appraisal_id','appraisal_bonus_type_id'];

    public function transformAudit(array $data): array
    {
            if(Arr::has($data,'event')){
                if($data['event'] == "created"){
                    $appraisalBonus = AppraisalBonus::find($data['auditable_id']);
                    $appraisal = Appraisal::find($appraisalBonus->appraisal_id);
                    $lastAppraisal = Appraisal::orderBy('effective_date','desc')->whereDate('effective_date','<',$appraisal->effective_date)->where('id','<>',$appraisal->id)->where('user_id', $appraisal->user_id)->first();
                    if($lastAppraisal)
                    {
                        $obj = AppraisalBonus::where('appraisal_id',$lastAppraisal->id)->where('appraisal_bonus_type_id',$appraisalBonus->appraisal_bonus_type_id)->first() ?? null;
                        if($obj && $obj->value)
                        {
                            $data['old_values'][str_replace('-', '_',$appraisalBonus->appraisalBonusType->code).'_amount'] = $obj->value;
                        }
                        if($obj && $obj->value_date)
                        {
                            $data['old_values'][str_replace('-', '_',$appraisalBonus->appraisalBonusType->code).'_date'] = $obj->value_date;
                        }
                    }
                }
            }
            if (Arr::has($data, 'new_values.value')) {
                $code = str_replace('-', '_', AppraisalBonusType::find($this->getAttribute('appraisal_bonus_type_id'))->code);
                if(!empty($data['old_values']['value'])){
                    $data['old_values'][$code.'_amount'] = $data['old_values']['value'];
                }
                if(!empty($data['new_values']['value'])){
                    $data['new_values'][$code.'_amount'] = $data['new_values']['value'];
                }
            }
            if (Arr::has($data, 'new_values.value_date')) {
                $code = str_replace('-', '_', AppraisalBonusType::find($this->getAttribute('appraisal_bonus_type_id'))->code);
                if(!empty($data['old_values']['value_date'])) {
                    $oldValue = $data['old_values']['value_date'];
                    $data['old_values'][$code.'_date'] = date("D, jS M Y", strtotime($oldValue));
                }
                if(!empty($data['new_values']['value_date'])) {
                    $newValue = $data['new_values']['value_date']??'';
                    $data['new_values'][$code.'_date'] = date("D, jS M Y", strtotime($newValue));
                }
            }

            $appraisalBonus = AppraisalBonus::find($data['auditable_id']);
            if($appraisalBonus)
            {
                $code = str_replace('-', '_',$appraisalBonus->appraisalBonusType->code);
                if (Arr::has($data, 'old_values.'.$code.'_amount') && !(Arr::has($data, 'new_values.'.$code.'_amount')))
                {
                    $data['new_values'][$code.'_amount'] = $appraisalBonus->value;
                }
                if (Arr::has($data, 'old_values.'.$code.'_date') && !(Arr::has($data, 'new_values.'.$code.'_date')))
                {
                    $data['new_values'][$code.'_date'] = date("D, jS M Y", strtotime($appraisalBonus->value_date));
                }
            }
        unset($data['old_values']['value']);
        unset($data['new_values']['value']);
        unset($data['old_values']['value_date']);
        unset($data['new_values']['value_date']);
        return $data;
    }

    public function appraisalBonusType()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonusType', 'appraisal_bonus_type_id', 'id');
    }
    public function appraisalType()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalType', 'appraisal_id', 'id');
    }
    public function appraisal()
    {
        return $this->hasOne('App\Models\Appraisal\Appraisal', 'id', 'appraisal_id');
    }
}
