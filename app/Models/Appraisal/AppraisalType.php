<?php

namespace App\Models\Appraisal;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class AppraisalType extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'appraisal_types';
    public $timestamps = false;
    protected $fillable = ['name', 'code'];

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($appraisalTypeObj) {
            if (count($appraisalTypeObj->appraisals) > 0) {
                return false;
            }
        });
    }

    protected $rules = [
        'code' => 'required | unique:appraisal_types',
        'name' => 'required | string',
    ];

    public function appraisals()
    {
        return $this->hasMany('App\Models\Appraisal\Appraisal', 'type_id', 'id');
    }
}
