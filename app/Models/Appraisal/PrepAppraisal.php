<?php

namespace App\Models\Appraisal;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepAppraisal extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_appraisals';
    public $timestamps = false;

    protected $rules = [
        'prep_salary_id' => 'required',
        'user_id' => 'required|exists:users,id',
        'appraisal_id' => 'required|exists:appraisals,id',
        'start_date' => 'required|date',
        'end_date' => 'required|date|after:start_date',

    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($prepAppraisalObj) {
            $prepAppraisalObj->prepAppraisalComponent()->delete();
        });
    }

    public function prepAppraisalComponent()
    {
        return $this->hasMany('App\Models\Appraisal\PrepAppraisalComponent', 'prep_appraisal_id');
    }
    public function prepAppraisalBonus()
    {
        return $this->hasMany('App\Models\Salary\PrepAppraisalBonus', 'prep_salary_id');
    }
    public function appraisal()
    {
        return $this->belongsTo('App\Models\Appraisal\Appraisal', 'appraisal_id', 'id');
    }
    public function items()
    {
        return $this->prepAppraisalComponent();
    }
    public function prepSalary()
    {
        return $this->belongsTo('App\Models\Salary\PrepSalary', 'prep_salary_id', 'id');
    }
}
