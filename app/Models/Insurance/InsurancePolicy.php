<?php namespace App\Models\Insurance;

use App\Models\Admin\Insurance\Insurance;
use App\Models\BaseModel;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class InsurancePolicy extends BaseModel
{
    use ValidatingTrait;
    protected $table = 'insurance_policies';
    use SoftDeletes;
    protected $fillable = ['vendor_id', 'name', 'policy_number', 'start_date', 'end_date', 'coverage_amount', 'amount_paid', 'premium_amount'];
    protected $appends = ['has_sub_group'];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function insurances()
    {
        return $this->hasMany('App\Models\Insurance\Insurance', 'insurance_policy_id', 'id')->orderBy('id', 'ASC');
    }
    public function subGroups()
    {
        return $this->hasMany('App\Models\Insurance\InsurancePolicy', 'parent_id', 'id');
    }
    public function getHasSubGroupAttribute()
    {
        if (count($this->subGroups) > 0) {
            return true;
        }
        return false;
    }
}
