<?php namespace App\Models\Insurance;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Insurance extends BaseModel
{

    use ValidatingTrait;
    use SoftDeletes;
    public function insurable()
    {
        return $this->morphTo();
    }
    public function addedBy()
    {
        return $this->belongsTo('App\Models\Users\User', 'created_by', 'id');
    }
    public function policy()
    {
        return $this->belongsTo('App\Models\Insurance\InsurancePolicy', 'insurance_policy_id', 'id');
    }
}
