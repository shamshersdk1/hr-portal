<?php

namespace App\Models\Onsite;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class OnsiteAllowance extends Model
{
    use ValidatingTrait;

    public $timestamps = true;
    protected $fillable = ['user_id','start_date','end_date','project_id','status','type','amount','notes','approver_id'];

    protected $rules = [
        'user_id' => 'required|exists:users,id',
        'project_id' => 'required|exists:projects,id',
        'start_date' => 'date',
        'end_date' => 'date',
        'status' => 'required',
        'type' => 'required',
        'amount' => 'required|numeric',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }

    public function approver()
    {
        return $this->belongsTo('App\Models\Users\User', 'approver_id', 'id');
	}
	public function project()
    {
        return $this->belongsTo('App\Models\Project\Project', 'project_id', 'id');
    }
}
