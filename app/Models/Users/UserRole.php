<?php
namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class UserRole extends Model
{

    use ValidatingTrait;
    protected $table = 'user_roles';
    public $timestamps = false;
    protected $fillable = ['user_id', 'role_id'];

    private $rules = array(
        'user_id' => 'required | exists:users,id',
        'role_id' => 'required',
    );
}
