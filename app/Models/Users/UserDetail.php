<?php
namespace App\Models\Users;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class UserDetail extends BaseModel implements Auditable
{
    use ValidatingTrait;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $fillable = ['user_id','aadhar_no','uan_no'];

    private $rules = array(
        'user_id' => 'required',
    );

    /**
     * The database table used by the model.
     *
     * @var string
     */
    // protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }
}
