<?php
namespace App\Models\Users;

// use Illuminate\Auth\Authenticatable;
use App\Models\Bank\Account;
use App\Models\Bank\AccountMetaKey;
use App\Models\Bank\AccountMetaValue;
use App\Models\Bank\AccountType;
use App\Models\Transaction;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Traits\HasRoles;
use Watson\Validating\ValidatingTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;



class User extends Authenticatable implements AuthenticatableContract, CanResetPasswordContract, Auditable
{
    // extends Authenticatable;
    use CanResetPassword;
    use ValidatingTrait;
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    use HasRoles;
    use Notifiable;

    protected $appends = ['user_id_name'];
    protected $fillable = ['name', 'email', 'first_name', 'middle_name', 'last_name', 'joining_date', 'confirmation_date', 'release_date', 'employee_id', 'dob', 'parent_id',
        'pan', 'uan_no', 'months', 'years', 'gender', 'designation', 'role'];

    public function getUserIdNameAttribute()
    {
        if ($this) {
            return $this->employee_id . ' - ' . $this->name;
        }

        return null;
    }

    private $rules = array(
        'name' => 'required|string',
        'first_name' => 'required|string',
        'dob' => 'required|date',
        'email' => 'required|email|unique',
        'employee_id' => 'required|unique',
        'joining_date' => 'required|date',
        'parent_id' => 'required',
        'gender' => 'required',
        'designation' => 'required|exists:designations,id'
    );

    protected $auditExclude = [
        'password',
        'remember_token',
        'is_billable',
        'role',
    ];

    public function generateTags(): array
    {
        return [
            $this->name,
            $this->email,
        ];
    }

    public function transformAudit(array $data): array
    {
        if (Arr::has($data, 'new_values.parent_id')) {
            $old_values = NULL;
            $new_values = NULL;
            if(!empty($this->getOriginal('parent_id'))){
                $old_values = User::find($this->getOriginal('parent_id'))->name;
            }
            if(!empty($this->getAttribute('parent_id'))){
                $new_values = User::find($this->getAttribute('parent_id'))->name;
            }
            $data['old_values']['reporting_manager'] = $old_values;
            $data['new_values']['reporting_manager'] = $new_values;
        }
        if (Arr::has($data, 'new_values.designation')) {
            $old_values = NULL;
            $new_values = NULL;
            if(!empty($this->getOriginal('designation'))){
                $old_values = Designation::find($this->getOriginal('designation'))->designation;
            }
            if(!empty($this->getAttribute('designation'))){
                $new_values = Designation::find($this->getAttribute('designation'))->designation;
            }
            $data['old_values']['user_designation'] = $old_values;
            $data['new_values']['user_designation'] = $new_values;
        }
        unset($data['old_values']['parent_id']);
        unset($data['new_values']['parent_id']);
        unset($data['old_values']['designation']);
        unset($data['new_values']['designation']);
        return $data;
    }

    public function userDesignation()
    {
        return $this->hasOne('App\Models\Users\Designation', 'id', 'designation');
    }
    public function userDetails()
    {
        return $this->hasOne('App\Models\Users\UserDetail', 'user_id', 'id');
    }
    public function accounts($id = null)
    {
        if ($id == null) {
            return null;
        }

        $account = Account::where('reference_id', $this->id)->where('reference_type', 'App\Models\Users\User')->where('bank_account_type_id', $id)->first();
        if ($account) {
            return $account->account_number;
        } else {
            return null;
        }
    }
    public function savingAccount()
    {
        $accountType = AccountType::where('code', 'saving-account')->first();
        if ($accountType) {
            $account = $this->hasOne('App\Models\Bank\Account', 'reference_id', 'id')->where('reference_type', 'App\Models\Users\User')->where('bank_account_type_id', $accountType->id);
            return $account;
        }
        return null;
    }
    public function esiAccount()
    {
        $accountType = AccountType::where('code', 'esi-account')->first();
        if ($accountType) {
            $account = $this->hasOne('App\Models\Bank\Account', 'reference_id', 'id')->where('reference_type', 'App\Models\Users\User')->where('bank_account_type_id', $accountType->id);
            return $account;
        }
        return null;
    }
    public function uanAccount()
    {
        //pf
        $accountType = AccountType::where('code', 'uan-account')->first();
        if ($accountType) {
            $account = $this->hasOne('App\Models\Bank\Account', 'reference_id', 'id')->where('reference_type', 'App\Models\Users\User')->where('bank_account_type_id', $accountType->id);
            return $account;
        }
        return null;
    }
    public function panAccount()
    {
        //tds
        $accountType = AccountType::where('code', 'pan-account')->first();
        if ($accountType) {
            $account = $this->hasOne('App\Models\Bank\Account', 'reference_id', 'id')->where('reference_type', 'App\Models\Users\User')->where('bank_account_type_id', $accountType->id);
            return $account;
        }
        return null;
    }
    public function accountMetaValue($key)
    {
        $accountType = AccountType::where('code', 'saving-account')->first();
        if ($accountType) {
            $account = Account::where('reference_id', $this->id)->where('reference_type', 'App\Models\Users\User')->where('bank_account_type_id', $accountType->id)->first();
            if ($account) {
                $keyObj = AccountMetaKey::where('bank_account_type_id', $accountType->id)->where('key', $key)->first();
                if ($keyObj) {
                    $value = AccountMetaValue::where('bank_account_id', $account->id)->where('bank_account_meta_key_id', $keyObj->id)->first();
                    return $value;
                }
            }
        }
        return null;
    }

    public function checkRole($code)
    {
        if ($this->roles()->where('code', $code)->count()) {
            return true;
        }
        return false;
    }
    public function isAdmin()
    {
        if ($this->roles()->where('code', 'admin')->count()) {
            return true;
        }
        return false;
    }
    public function loans()
    {
        return $this->hasMany('App\Models\Loan\Loan', 'user_id', 'id');
    }
    public function prevAmountPayable()
    {
        $tranSum = Transaction::getPrevMonthNet($this->id);
        return $tranSum;
    }
    public function getExperience()
    {
        $time1 = empty($this->release_date) || ($this->release_date < $this->confirmation_date) ? date("Y-m-d") : $this->release_date;
        $time2 = $this->confirmation_date;
        $precision = 3;
        // If not numeric then convert timestamps
        if (!is_int($time1)) {
            $time1 = strtotime($time1);
        }
        if (!is_int($time2)) {
            $time2 = strtotime($time2);
        }
        // If time1 > time2 then swap the 2 values
        if ($time1 > $time2) {
            list($time1, $time2) = array($time2, $time1);
        }
        // Set up intervals and diffs arrays
        $intervals = array('year', 'month', 'day', 'hour', 'minute', 'second');

        $diffs = array();
        foreach ($intervals as $interval) {
            // Create temp time from time1 and interval
            $ttime = strtotime('+1 ' . $interval, $time1);
            // Set initial values
            $add = 1;
            $looped = 0;
            // Loop until temp time is smaller than time2
            while ($time2 >= $ttime) {
                // Create new temp time from time1 and interval
                $add++;
                $ttime = strtotime("+" . $add . " " . $interval, $time1);
                $looped++;
            }
            $time1 = strtotime("+" . $looped . " " . $interval, $time1);
            if (!empty($this->years) && $interval == 'year') {
                $looped = $looped + $this->years;
            }
            if (!empty($this->months) && $interval == 'month') {
                $looped = $looped + $this->months;
                if ($looped > 11) {
                    $diffs['year'] = $diffs['year'] + 1;
                    $looped = $looped - 12;
                }
            }
            $diffs[$interval] = $looped;
        }
        $count = 0;
        $times = array();
        foreach ($diffs as $interval => $value) {
            // Break if we have needed precission
            if ($count >= $precision) {
                break;
            }
            // Add value and interval if value is bigger than 0
            if ($value > 0) {
                if ($value != 1) {
                    $interval .= "s";
                }
                // Add value and interval to times array
                $times[] = $value . " " . $interval;
                $count++;
            }
        }
        // Return string with times
        return implode(", ", $times);
    }

    public function account($typeId)
    {
        return $this->hasOne('App\Models\Bank\Account', 'reference_id', 'id')->where('bank_account_type_id', $typeId)->first();
    } 

}
