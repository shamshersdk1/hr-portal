<?php

namespace App\Models\Users;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;

class Role extends BaseModel
{

    use ValidatingTrait;

    protected $table = 'roles';
    public $timestamps = true;
    private $rules = array(
        'name' => 'required',
    );

    public function users()
    {
        return $this->belongsToMany('App\Models\Users\User', 'user_roles', 'role_id', 'user_id');
    }
}
