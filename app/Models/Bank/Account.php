<?php

namespace App\Models\Bank;

use App\Models\Bank\AccountMetaValue;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;
use App\Models\Bank\AccountMetaKey;
use Illuminate\Support\Arr;


class Account extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use ValidatingTrait;

    protected $fillable = ['bank_account_type_id', 'reference_id', 'reference_type', 'account_number'];

    protected $rules = [
        'bank_account_type_id' => 'required | exists:account_types,id',
        'account_number' => 'required',
    ];
    protected $auditExclude = [
        'id',
        'bank_account_type_id',
        'reference_type',
        'reference_id',
        'created_at',
        'updated_at'
    ];

    public function transformAudit(array $data): array
    {
        if (Arr::has($data, 'auditable_id')) {
            $accountType = ucfirst(str_replace('-', ' ',Self::find($data['auditable_id'])->accountType->code));
            if(isset($accountType)){
                $old_values = NULL;
                $new_values = NULL;
                if(!empty($this->getOriginal('account_number'))){
                    $old_values = $this->getOriginal('account_number');
                }
                if(!empty($this->getAttribute('account_number'))){
                    $new_values = $this->getAttribute('account_number');
                }
                $data['old_values'][$accountType ] = $old_values;
                $data['new_values'][$accountType ] = $new_values;
            }
        }
        unset($data['old_values']['account_number']);
        unset($data['new_values']['account_number']);
        return $data;
    }

    public function accountType()
    {
        return $this->belongsTo('App\Models\Bank\AccountType', 'bank_account_type_id', 'id');
    }

    public function metaValue($id)
    {
        $meta = AccountMetaValue::where('bank_account_id', $this->id)->where('bank_account_meta_key_id', $id)->first();
        if ($meta) {
            return $meta->value;
        } else {
            return null;
        }
    }

    public function ifscMetaValue()
    {
        $metaKey = AccountMetaKey::where('key','ifsc')->first();
        if($metaKey)
        {
            $meta = AccountMetaValue::where('bank_account_id', $this->id)->where('bank_account_meta_key_id', $metaKey->id)->first();
            return $meta;
        } else {
            return 0;
        }
    }

    // public function reference()
    // {
    //     if ($this->reference_type == 'App\Models\Users\User') {
    //         return $this->user();
    //     } elseif ($this->reference_type == 'App\Models\Company') {
    //         return $this->company();
    //     } elseif ($this->reference_type == 'App\Models\Vendor') {
    //         return $this->vendor();
    //     } else {
    //         '';
    //     }
    // }
    public function reference()
    {
        return $this->morphTo('reference');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'reference_id', 'id');
    }
    public function company()
    {
        //change
        return $this->belongsTo('App\Models\Users\User', 'reference_id', 'id');
    }
    public function vendor()
    {
        //change
        return $this->belongsTo('App\Models\Users\User', 'reference_id', 'id');
    }

    public function metaValues()
    {
        return $this->hasMany('App\Models\Bank\AccountMetaValue', 'bank_account_id', 'id');
    }
    public static function boot()
    {
        parent::boot();

        static::deleting(function ($bankAccount) {
            $bankAccount->metaValues()->delete();
        });
    }

}
