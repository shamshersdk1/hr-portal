<?php

namespace App\Models\Bank;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class AccountMetaKey extends Model
{
    use ValidatingTrait;

    protected $fillable = ['bank_account_type_id', 'key', 'is_required', 'field_type'];

    protected $rules = [
        'bank_account_type_id' => 'required | exists:account_types,id',
        'key' => 'required | string',
        'is_required' => 'required | boolean',
        'field_type' => 'required | string',
    ];

    public function accountType()
    {
        return $this->belongsTo('App\Models\Bank\AccountType', 'bank_account_type_id', 'id');
    }
}
