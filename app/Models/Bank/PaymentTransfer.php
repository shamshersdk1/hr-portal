<?php

namespace App\Models\Bank;

use App\Jobs\BankTransfer\PaymentTransferJob;
use App\Models\Bank\PaymentTransfer;
use App\Models\DateTime\Month;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PaymentTransfer extends Model
{
    use ValidatingTrait;

    protected $fillable = ['month_id', 'account_type_id', 'status'];

    protected $rules = [
        'month_id' => 'required | exists:months,id',
        'account_type_id' => 'required | exists:account_types,id',
    ];

    public function accountType()
    {
        return $this->belongsTo('App\Models\Bank\AccountType', 'account_type_id', 'id');
    }

    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }

    public function transferAccounts()
    {
        return $this->hasMany('App\Models\Bank\PaymentTransferAccount', 'payment_transfer_id', 'id');
    }

    public static function transferAmount($data, $accountTypeId)
    {
        $response = [];
        $response['status'] = true;
        $response['message'] = '';
        $response['errors'] = null;

        $currMonth = date('m');
        $currYear = date('Y');
        $monthObj = Month::where('month', $currMonth)->where('year', $currYear)->first();
        if (!$monthObj) {
            $response['status'] = false;
            $response['errors'] = 'Month not found.';
            return $response;
        }

        try {
            $payment = PaymentTransfer::create(['month_id' => $monthObj->id, 'account_type_id' => $accountTypeId, 'status' => 'initiated']);
            if ($payment->isValid()) {
                $response['message'] = $payment->id;

                foreach ($data as $account => $amount) {
                    if ($account) {
                        $job = (new PaymentTransferJob($payment->id, $account, $amount, $accountTypeId));
                        dispatch($job);
                    }
                }
                return $response;
            } else {
                $response['status'] = false;
                $response['errors'] = $payment->getErrors();
                return $response;
            }
        } catch (Exception $e) {
            DB::rollback();
            $response['status'] = false;
            $response['errors'] = $e->getMessage();
            return $response;
        }
    }
}
