<?php

namespace App\Models\Bank;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class BankTransaction extends Model
{
    use ValidatingTrait;

    protected $fillable = ['financial_year_id', 'month_id', 'account_id', 'account_type_id', 'amount', 'reference_type', 'reference_id'];

    protected $rules = [
        'account_id' => 'required | exists:accounts,id',
        'account_type_id' => 'required | exists:account_types,id',
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Bank\Account', 'account_id', 'id');
    }

    public function accountType()
    {
        return $this->belongsTo('App\Models\Bank\AccountType', 'account_type_id', 'id');
    }
}
