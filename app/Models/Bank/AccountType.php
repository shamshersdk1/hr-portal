<?php

namespace App\Models\Bank;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class AccountType extends Model
{
    use ValidatingTrait;

    protected $fillable = ['name', 'code','is_editable'];

    protected $rules = [
        'name' => 'required | string | unique',
        'code' => 'required | string | unique',
    ];

    public function metaKeys()
    {
        return $this->hasMany('App\Models\Bank\AccountMetaKey', 'bank_account_type_id', 'id')->where('is_required', 1);
    }

    public function userAccount($id)
    {
        return $this->hasOne('App\Models\Bank\Account', 'bank_account_type_id', 'id')->where('reference_id', $id)->first();
    }

    public function bankTransactions($accountTypeId)
    {
        $amount = $this->hasMany('App\Models\Bank\BankTransaction', 'account_type_id', 'id')->where('account_type_id', $accountTypeId)->sum('amount');
        return $amount;
    }
}
