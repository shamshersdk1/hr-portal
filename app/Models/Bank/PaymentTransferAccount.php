<?php

namespace App\Models\Bank;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PaymentTransferAccount extends Model
{
    use ValidatingTrait;

    protected $fillable = ['ifsc_code','account_number','comment','txn_posted_date','payment_transfer_id', 'account_id', 'amount', 'transaction_amount', 'mode_of_transfer', 'transaction_id'];

    protected $rules = [
        'payment_transfer_id' => 'required | exists:payment_transfers,id',
        'account_id' => 'required | exists:accounts,id',
        'amount' => 'required',
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\Bank\Account', 'account_id', 'id');
    }
}
