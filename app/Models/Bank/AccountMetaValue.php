<?php

namespace App\Models\Bank;

use App\Models\Bank\AccountMetaValue;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;
use Illuminate\Support\Arr;

class AccountMetaValue extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['bank_account_id', 'bank_account_meta_key_id', 'value'];

    protected $rules = [
        'bank_account_id' => 'required | exists:accounts,id',
        'bank_account_meta_key_id' => 'required | exists:account_meta_keys,id',
        'value' => 'required',
    ];
    protected $auditExclude = [
        'id',
        'bank_account_id',
        'bank_account_meta_key_id',
        'created_at',
        'updated_at'
    ];

    public function transformAudit(array $data): array
    {
      
        if (Arr::has($data, 'auditable_id')) {
            $accountMetaKey = ucfirst(str_replace('-', ' ',Self::find($data['auditable_id'])->accountMetaKey->key));
            if(isset($accountMetaKey)){
                $old_values = NULL;
                $new_values = NULL;
                if(!empty($this->getOriginal('value'))){
                    $old_values = $this->getOriginal('value');
                }
                if(!empty($this->getAttribute('value'))){
                    $new_values = $this->getAttribute('value');
                }
                $data['old_values'][$accountMetaKey ] = $old_values;
                $data['new_values'][$accountMetaKey ] = $new_values;
            }
        }
        unset($data['old_values']['value']);
        unset($data['new_values']['value']);
        return $data;
    }

    public function account()
    {
        return $this->belongsTo('App\Models\Bank\Account', 'bank_account_id', 'id');
    }

    public function accountMetaKey()
    {
        return $this->belongsTo('App\Models\Bank\AccountMetaKey', 'bank_account_meta_key_id', 'id');
    }

    public static function saveData($data, $accountId)
    {
        foreach ($data['meta'] as $key => $entry) {
            $accountTypeObj = AccountType::where('name', $data['bank_account_type_id'])->first();
            if (!$accountTypeObj) {
                return false;
            }
            $accountTypeId = $accountTypeObj->id;

            $keyObj = AccountMetaKey::where('key', $key)->where('bank_account_type_id', $accountTypeId)->first();

            if (!$keyObj) {
                return false;
            }

            $metaObj = AccountMetaValue::create(['bank_account_id' => $accountId, 'bank_account_meta_key_id' => $keyObj->id, 'value' => $entry]);
            if ($metaObj->isInvalid()) {
                return false;
            }
        }
        return true;
    }
    public static function updateData($data, $accountId)
    {
        $accountObj = Account::find($accountId);
        if (!$accountObj) {
            return false;
        }

        foreach ($data['meta'] as $key => $entry) {
            $keyObj = AccountMetaKey::where('key', $key)->where('bank_account_type_id', $accountObj->bank_account_type_id)->first();
            if (!$keyObj) {
                return false;
            }

            $metaObj = AccountMetaValue::where('bank_account_id', $accountId)->where('bank_account_meta_key_id', $keyObj->id)->first();
            $metaObj->value = $entry;

            if (!$metaObj->save()) {
                return false;
            }
        }
        return true;
    }
}
