<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// use Watson\Validating\ValidatingTrait;

// use \App\Traits\CreatedAtTrait;
// use \App\Traits\DeletedAtTrait;
// use \App\Traits\UpdatedAtTrait;

/*
We are going to use a trait for validation
https://github.com/dwightwatson/validating
 */

class BaseModel extends Model
{

    // use ValidatingTrait;
    //use SoftDeletes;

    protected $json_fields = [];
    protected $hash_fields = [];

    protected $defaults = [];

    public function __construct(array $attributes = [])
    {
        $this->setRawAttributes($this->defaults, true);
        //$this->setValidator($this->getValidator());
        parent::__construct($attributes);
    }

    public function getHashFields()
    {
        return $this->hash_fields;
    }

    public function getJsonFields()
    {
        return $this->json_fields;
    }

    public function scopeBuildQuery($query)
    {
        return $query;
    }

    protected function makeValidator($rules = [])
    {

        // Get the model attributes.
        $attributes = $this->getModel()->getAttributes();

        if ($this->getInjectUniqueIdentifier()) {
            $rules = $this->injectUniqueIdentifierToRules($rules);
        }

        $messages = [];

        if (!empty($this->validationMessages)) {
            $messages = $this->validationMessages;
        }

        $this->getValidator()->resolver(function ($translator, $data, $rules, $messages) {
            return new \App\Services\CustomValidator($translator, $data, $rules, $messages);
        });

        $validator = $this->getValidator()->make($attributes, $rules, $messages);

        if ($this->getValidationAttributeNames()) {
            $validator->setAttributeNames($this->getValidationAttributeNames());
        }

        return $validator;
    }

}
