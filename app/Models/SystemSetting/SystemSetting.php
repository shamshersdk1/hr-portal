<?php

namespace App\Models\SystemSetting;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class SystemSetting extends Model
{
    use ValidatingTrait;

    protected $table = 'system_settings';
    public $timestamps = true;
    protected $fillable = ['key', 'value'];

    protected $rules = [
        'key' => 'required | string | unique:system_settings',
        'value' => 'required | string',
    ];
}
