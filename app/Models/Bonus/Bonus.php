<?php

namespace App\Models\Bonus;

use App\Models\DateTime\Month;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    //protected $appends = ["draft_amount"];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }
    public function reference()
    {
        return $this->morphTo('reference');
    }
    public function bonusConfirm()
    {
        return $this->hasOne('App\Models\Bonus\BonusConfirmed', 'bonus_id', 'id');
    }
    public function salaryProcess()
    {
        return $this->belongsTo('App\Models\Bonus\PrepBonus', 'id', 'bonus_id');
    }
    public function approver()
    {
        return $this->belongsTo('App\Models\Users\User', 'approved_by', 'id');
    }
}
