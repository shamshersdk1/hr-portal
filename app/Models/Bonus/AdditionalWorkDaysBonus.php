<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Model;

class AdditionalWorkDaysBonus extends Model
{
    protected $table = 'additional_work_days_bonuses';
}
