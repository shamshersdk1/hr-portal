<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnSiteAllowance extends Model
{
    use SoftDeletes;
    protected $table = 'onsite_allowances';
	public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $hidden = array('amount', 'notes','approver_id');
    public function scopeBuildQuery($query)
    {
        return $query;
    }
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }
    public function approver()
    {
        return $this->belongsTo('App\Models\Users\User', 'approver_id', 'id');
	}
	public function project()
    {
        return $this->belongsTo('App\Models\Project\Project', 'project_id', 'id');
    }
}
