<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Model;

class ReferralBonus extends Model
{
    protected $table = 'referral_bonuses';
}
