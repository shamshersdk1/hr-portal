<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class BonusConfirmed extends Model
{
    use ValidatingTrait;
    
    protected $table = 'bonuses_confirmed';

    protected $fillable = ['bonus_id', 'user_id'];
    protected $rules = [
        'bonus_id' => 'required | exists:bonuses,id',
        'user_id' => 'required | exists:users,id',
    ];

    public function bonus()
    {
        return $this->belongsTo('App\Models\Bonus\Bonus', 'bonus_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }

    public static function getCurrentMonthBonus($userId, $monthId)
    {
        $data = [];
        $data['referral'] = 0;
        $data['onsite'] = 0;
        $data['additional'] = 0;
        $data['tech_talk'] = 0;
        $data['extra_hour'] = 0;
        $data['performance'] = 0;

        $confirmObj = BonusConfirmed::where('user_id', $userId)->get();
        if ($confirmObj && isset($confirmObj)) {
            foreach ($confirmObj as $confirm) {
                if ($confirm->bonus->type == 'referral') {
                    $data['referral'] += $confirm->bonus->amount;
                } else if ($confirm->bonus->type == 'onsite') {
                    $data['onsite'] += $confirm->bonus->amount;
                } else if ($confirm->bonus->type == 'additional') {
                    $data['additional'] += $confirm->bonus->amount;
                } else if ($confirm->bonus->type == 'techtalk') {
                    $data['tech_talk'] += $confirm->bonus->amount;
                } else if ($confirm->bonus->type == 'extra') {
                    $data['extra_hour'] += $confirm->bonus->amount;
                } else if ($confirm->bonus->type == 'performance') {
                    $data['performance'] += $confirm->bonus->amount;
                }
            }
        }

        return $data;
    }
}
