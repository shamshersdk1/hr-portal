<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Model;

class TechTalkBonusesUser extends Model
{
    protected $table = 'tech_talk_bonuses_user';
}
