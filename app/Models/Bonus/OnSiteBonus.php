<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Model;

class OnSiteBonus extends Model
{
    protected $table = 'on_site_bonuses';
}
