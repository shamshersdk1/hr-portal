<?php

namespace App\Models\Bonus;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepBonus extends Model
{
    use ValidatingTrait;
    protected $fillable = ['prep_salary_id','bonus_id','user_id'];

    protected $rules = [
        'prep_salary_id' => 'required | exists:prep_salary,id',
        'bonus_id' => 'required | exists:bonuses,id',
        'user_id' => 'required | exists:users,id',
    ];

    public function bonus()
    {
        return $this->belongsTo('App\Models\Bonus\Bonus', 'bonus_id', 'id');
    }

}
