<?php

namespace App\Models\Loan;

use App\Events\LoanDisbursed;
use App\Jobs\Loan\LoanRequestJob;
use App\Models\AccessLog;
use App\Models\Loan;
use App\Models\LoanTransaction;
use App\Services\FileService;
use Auth;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Events\Loan\LoanRequested;
use App\Traits\ValidationTrait;
use App\Models\Users\User;
use Illuminate\Notifications\Notifiable;
use Config;

class LoanRequest extends Model
{
    use SoftDeletes;
    use ValidationTrait;
    use Notifiable;


    protected $fillable = ['user_id','approved_amount','amount','emi','emi_start_date','application_date','appraisal_bonus_id','description','status'];

    protected $rules= array(
        'user_id' => 'required | exists:users,id',
        'amount' => 'required | numeric',
    );

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }


    public function activities()
    {
        return $this->hasMany('App\Models\Admin\LoanRequestActivity');
    }

    public function submittedBy()
    {
        $activity = $this->activities()->where('status', "approved")->where('role', "Reporting Manager")->orderBy('id', "desc")->first();
        return (!empty($activity) && !empty($activity->user->name)) ? $activity->user->name : null;
    }

    // public function reviewedBy()
    // {
    //     $activity = $this->activities()->where('status', "approved")->where('role', "Human Resources")->orderBy('created_at', "asc")->first();
    //     return (!empty($activity) && !empty($activity->user->name)) ? $activity->user->name : null;
    // }

    public function reconciledBy()
    {
        $activity = $this->activities()->where('status', "approved")->where('role', "Management")->orderBy('id', "desc")->first();
        return (!empty($activity) && !empty($activity->user->name)) ? $activity->user->name : null;
    }

    public function approvedBy()
    {
        $activity = $this->activities()->where('status', "approved")->where('role', "Human Resources")->orderBy('id', "desc")->first();
        return (!empty($activity) && !empty($activity->user->name)) ? $activity->user->name : null;
    }

    public function loan()
    {
        return $this->hasOne('App\Models\Loan', 'loan_request_id', 'id');
    }

    public function appraisalBonus()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonus');

    }

    public function file()
    {
        return $this->morphOne('App\Models\File\File', 'reference');
    }

    public static function routeNotificationForSlack($notification)
    {
        return Config::get('loan.loan_request_webhook');
    }

}
