<?php

namespace App\Models\Loan;

use App\Models\DateTime\Month;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;
use App\Models\Salary\UserSalary;

class LoanEmi extends Model
{
    use SoftDeletes;
    use ValidatingTrait;
    private $rules = array(
        'amount' => 'required | numeric ',
    );

    protected $fillable = ['loan_id','user_id','month_id','amount','actual_amount','appraisal_bonus_id','status','paid_on','is_manual_payment','balance','transaction_id','comment'];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loan\Loan', 'loan_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User');
    }

    public static function regenerateEmi($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        $monthObj = Month::find($month_id);
        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->vpfSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        LoanEmi::where('month_id', $month_id)->where('is_manual_payment','0')->delete();
        $users = UserSalary::where('month_id',$month_id)->pluck('user_id')->toArray();
        $loans = Loan::where('status','in_progress')->whereDate('emi_start_date','<=',$monthObj->getLastDay())->whereIn('user_id',$users)->get();
        $errors = [];

        foreach ($loans as $loan) {
            if($loan->appraisal_bonus_id != null && Month::getMonth($loan->appraisalBonus->value_date ?? null) != $month_id)
            {
                continue;
            }
            if($loan->amount < (abs($loan->emis()->where('status','paid')->sum('amount'))))
            {
                $response['message'] = "Amount Exceed for user".$loan->user->name;
                $response['status'] = true;
            }
            $deductionObj = new LoanEmi();
            $deductionObj->month_id = $month_id;
            $deductionObj->loan_id = $loan->id;
            $deductionObj->user_id = $loan->user_id;
            $deductionObj->amount = -min($loan->emi,max(0,($loan->amount - abs($loan->emis()->where('status','paid')->sum('amount')))));
            $deductionObj->actual_amount = $loan->amount;
            $deductionObj->status = "pending";
            $deductionObj->due_date = $monthObj->getLastDay();
            $deductionObj->balance = $loan->amount - (abs($loan->emis()->where('status','paid')->sum('amount')) + abs($deductionObj->amount)) ;
            // $deductionObj->method = "NA";
            if (!$deductionObj->save()) {
                $response['status'] = true;
                $errors[] = $deductionObj->getErrors();
            }
        }
        return $response;
    }

    public static function updateData($data, $monthId)
    {
        $response['status'] = false;
        $response['message'] = "";
        $response['data'] = null;
        foreach ($data as $index => $loan) {
            $deductionObj = LoanEmi::find($index);
            // print_r(abs($deductionObj->loan->emis()->sum('amount')));
            // die;
            if(abs($data[$index]) > ($deductionObj->loan->amount - abs($deductionObj->loan->emis()->where('status','paid')->sum('amount') - $data[$index]))) {
                $response['message'] = $response['message']."\nDeduction Amount exceed for user:".$deductionObj->user->name;
                $response['status'] = true;
            } else {
                $deductionObj->amount = $data[$index];
                if (!$deductionObj->save()) {
                    $response['status'] = true;
                    $response['message'] = $response['message'].$deductionObj->getErrors();
                }
            }


        }
        return $response;
    }

    public static function getCurrMonthDeduction($monthId, $userId)
    {
        $loan = self::where('month_id', $monthId)->where('user_id', $userId)->sum('amount');
        return $loan;
    }
}
