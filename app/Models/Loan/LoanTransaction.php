<?php

namespace App\Models\Loan;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;


class LoanTransaction extends Model
{
    use ValidatingTrait;
	protected $table = 'loan_transactions';
	public $timestamps = true;
	public $fillable = ['loan_id','amount','type','status','comment','paid_on'];

    private $rules = array(
            'loan_id'   => 'required',
            'amount'    => 'required',
            'status'    => 'required'
        );

    public function loan()
    {
        return $this->belongsTo(Loan::class, 'loan_id', 'id');
    }

    public function loan_request()
    {
        return $this->hasOne(LoanRequest::class, 'id', 'loan_request_id');
    }

}
