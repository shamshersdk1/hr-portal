<?php

namespace App\Models\Loan;

use App\Jobs\PrepareSalary\Regenerate\LoanInterestJob;
use App\Models\DateTime\Month;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Salary\UserSalary;

class LoanInterest extends Model
{
    public function loan()
    {
        return $this->belongsTo(Loan::class, 'loan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User');
    }

    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }

    public static function getGrossLoanInterest($monthId, $userId)
    {
        $monthObj = Month::find($monthId);

        $totalInterest = 0;
        $totalInterest = LoanInterest::where('financial_year_id', $monthObj->financial_year_id)->where('user_id', $userId)->sum('interest');

        return $totalInterest;
    }

    public static function updateData($data, $monthId)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        foreach ($data as $index => $loan) {
            $interestObj = LoanInterest::find($index);
            $interestObj->interest = $data[$index];
            if (!$interestObj->save()) {
                $errors[] = $interestObj->getErrors();
                return $response;
            }
        }
        $response['status'] = true;
        $response['message'] = "Loan interest income regenerated";
        $response['data'] = null;
        return $response;
    }

    public static function regenerateInterest($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        $monthObj = Month::find($month_id);
        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->loanInterestSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        $users = UserSalary::where('month_id',$month_id)->get();
        foreach ($users as $user) {
            if ($user->user->loans) {
                $job = (new LoanInterestJob($user->user_id, $month_id));
                dispatch($job);
            }
        }

        $response['status'] = true;
        $response['message'] = "Loan interest income regenerated";
        $response['data'] = null;
        return $response;
    }
}
