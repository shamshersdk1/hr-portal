<?php

namespace App\Models\Loan;

use Illuminate\Database\Eloquent\Model;


class Loan extends Model
{

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
    public function loan_transactions()
    {
        return $this->hasMany('App\Models\Loan\LoanTransaction', 'loan_id', 'id');
    }

    public function loan_request()
    {
        return $this->hasOne(LoanRequest::class, 'id', 'loan_request_id');
    }
    public function emis()
    {
        return $this->hasMany(LoanEmi::class, 'loan_id', 'id');
    }

    public function appraisalBonus()
    {
        return $this->belongsTo('App\Models\Appraisal\AppraisalBonus');
    }

    public static function outstandingAmount($loanId)
    {
        $loan = Loan::find($loanId);
        $outstandingAmount = $loan->amount - abs($loan->emis()->where('status','paid')->sum('amount'));
        return $outstandingAmount;
    }
}
