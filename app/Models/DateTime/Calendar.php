<?php

namespace App\Models\DateTime;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class Calendar extends Model
{

    use ValidatingTrait;
    protected $table = 'non_working_calendar';
    public $timestamps = true;

    private $rules = array(
        'date' => 'required',
        'type' => 'required',
    );
}
