<?php

namespace App\Models\DateTime;

use App\Models\Salary\PrepSalary;
use App\Services\DateTime\CalendarService;
use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    public $timestamps = false;
    protected $fillable = ['working_days'];

    public function formatMonth()
    {
        $dateObj = \DateTime::createFromFormat('Y-m', $this->year . '-' . $this->month);
        $monthName = $dateObj->format('F, Y');
        return $monthName;
    }
    public function monthlyVariableBonusSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'monthly-variable-bonus');
    }
    public function monthlyFoodDeductionSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'monthly-food-deduction');
    }
    public function vpfSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'vpf-deduction');
    }
    public function insuranceSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'insurance-deduction');
    }
    public function getFirstDay()
    {
        return date('Y-m-01', strtotime($this->year . '-' . $this->month . '-01'));
    }
    public function getLastDay()
    {
        $startDate = date('Y-m-d', strtotime($this->year . '-' . $this->month . '-01'));
        return date("Y-m-t", strtotime($startDate));
    }
    public function getWorkingDays()
    {
        return CalendarService::getWorkingDays($this->month, $this->year);
    }
    public function loanInterestSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'loan-interest-income');
    }
    public function loanSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'loan-deduction');
    }
    public function financialYear()
    {
        return $this->belongsTo('App\Models\DateTime\FinancialYear', 'financial_year_id', 'id');
    }
    public function workingDaySetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'working-day');
    }
    public function fixedBonusSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'fixed-bonus');
    }
    public function variablePaySetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'variable-pay');
    }
    public function itSavingMonthSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'it-saving-month');
    }
    public function adhocPaymentSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'adhoc-payment');
    }
    public function nonCtcBonusSetting()
    {
        return $this->hasOne('App\Models\DateTime\MonthSetting', 'month_id', 'id')->where('key', 'non-ctc-bonus');
    }
    public static function getMonthList($year = null)
    {
        if ($year != null) {
            $months = Month::where('year', $year)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        } else {
            $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        }

        return $months;
    }

    public static function getMonth($date)
    {
        // return the id of the month
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));

        $monthObj = Month::where('year', $year)->where('month', $month)->first();
        if ($monthObj) {
            return $monthObj->id;
        } else {
            return 0;
        }
    }

    public static function getLastMonth($monthId)
    {
        if ($monthId) {
            $lastMonth = Month::where('id', '<>', $monthId)->orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
            if ($lastMonth) {
                return $lastMonth;
            }
        }
        return null;
    }
    public static function getMonthByDate($date)
    {
        // return the id of the month
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));

        $monthObj = Month::where('year', $year)->where('month', $month)->first();
        if ($monthObj) {
            return $monthObj;
        } else {
            return 0;
        }
    }
    public static function getInProgressMonth()
    {
        //prep salary in progress month
        $prepSalaryObj = PrepSalary::whereIn('status', ['open', 'processed', 'in_progress', 'finalizing'])->orderBy('created_at', 'DESC')->first();
        if ($prepSalaryObj) {
            return $prepSalaryObj->month_id;
        }

        //curr month if prev month salary is processed
        $curMonth = date('m');
        $year = date('Y');
        $curMonthObj = self::where('month', $curMonth)->where('year', $year)->first();
        if ($curMonthObj) {
            $prevMonthObj = self::where('month', $curMonthObj->month - 1)->where('financial_year_id', $curMonthObj->financial_year_id)->first();
            if ($prevMonthObj) {
                $prepSal = PrepSalary::where('month_id', $prevMonthObj->id)->whereIn('status', ['closed', 'finalized'])->first();
                if ($prepSal) {
                    return $curMonthObj->id;
                }
            }
        }

        //last month processed salary's next month
        $prepSalaryObj = PrepSalary::where('status', 'closed')->orderBy('created_at', 'DESC')->first();
        if ($prepSalaryObj) {
            $monthObj = self::find($prepSalaryObj->month_id);
            if ($monthObj) {
                $inProgressMonth = self::where('month', $monthObj->month + 1)->where('financial_year_id', $monthObj->financial_year_id)->first();
                if ($inProgressMonth) {
                    return $inProgressMonth->id;
                }
            }
        }

        return null;
    }

    public static function getLastProcessedMonth()
    {
        $curMonth = date('m');
        $year = date('Y');
        $curMonthObj = self::where('month', $curMonth)->where('year', $year)->first();
        if ($curMonthObj) {
            $currPrepSal = PrepSalary::where('month_id', $curMonthObj->id)->where('status', 'closed')->first();
            if ($currPrepSal) {
                return $curMonthObj->id;
            }
            $prevMonthObj = self::where('month', $curMonthObj->month - 1)->where('financial_year_id', $curMonthObj->financial_year_id)->first();
            if ($prevMonthObj) {
                $prepSal = PrepSalary::where('month_id', $prevMonthObj->id)->where('status', 'closed')->first();
                if ($prepSal) {
                    return $prevMonthObj->id;
                }
            }
        }

        $prepSalaryObj = PrepSalary::where('status', 'closed')->orderBy('created_at', 'DESC')->first();
        if ($prepSalaryObj) {
            return $prepSalaryObj->month_id;
        }

        return null;
    }

    public static function getAllProcessedMonths($financialYearId = null)
    {
        if ($financialYearId == null) {
            $curYear = date('Y');
            $financialYearObj = FinancialYear::where('status', 'running')->orderBy('year', 'DESC')->first();
            if ($financialYearObj) {
                $financialYearId = $financialYearObj->id;
            }
        } else {
            $financialYearObj = FinancialYear::find($financialYearId);
        }
        if (!$financialYearObj) {
            return [];
        }

        $months = Month::where('financial_year_id', $financialYearObj->id)->orderBy('year', 'ASC')->orderBy('month', 'ASC')->get();
        if (!$months || empty($months)) {
            return [];
        }

        $data = [];
        $lastProcessedMonthId = self::getLastProcessedMonth();
        foreach ($months as $month) {
            $data[$month->id] = $month;
        }
        if ($lastProcessedMonthId) {
            $lastObj = self::find($lastProcessedMonthId);
            if ($lastObj) {
                if ($lastObj->month >= 4 && $lastObj->month <= 12) {
                    foreach ($months as $month) {
                        if ($month->year == $lastObj->year && $month->month <= $lastObj->month) {
                            $data[$month->id] = $month;
                        }
                    }
                    return $data;
                }
                if ($lastObj->month >= 1 && $lastObj->month <= 3) {
                    foreach ($months as $month) {
                        if (($month->year < $lastObj->year) || ($month->year == $lastObj->year && $month->month <= $lastObj->month)) {
                            $data[$month->id] = $month;
                        }
                    }
                    return $data;
                }
            }
        }
        return $data;
    }
}
