<?php

namespace App\Models\DateTime;

use Illuminate\Database\Eloquent\Model;

class MonthSetting extends Model
{
    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }

    public static function monthStatusToggle($monthId, $key)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        $monthSettingObj = MonthSetting::where('month_id', $monthId)->where('key', $key)->first();
        if ($monthSettingObj) {
            if ($monthSettingObj->value == 'locked') {
                $monthSettingObj->value = "open";
                if (!$monthSettingObj->save()) {
                    $errors[] = $deductionObj->getErrors();
                    $response['message'] = $errors;
                    return $response;
                }
                $response['message'] = 'Month UnLocked of ' . $monthSettingObj->month->formatMonth();
            } else {
                $monthSettingObj->value = "locked";
                if (!$monthSettingObj->save()) {
                    $errors[] = $deductionObj->getErrors();
                    $response['message'] = $errors;
                    return $response;
                }
                $response['message'] = 'Month Locked of ' . $monthSettingObj->month->formatMonth();
            }
        } else {
            $monthSettingObj = new MonthSetting();
            $monthSettingObj->month_id = $monthId;
            $monthSettingObj->key = $key;
            $monthSettingObj->value = "locked";
            if (!$monthSettingObj->save()) {
                $errors[] = $deductionObj->getErrors();
                $response['message'] = $errors;
                return $response;
            }
            $response['message'] = 'Month Locked of ' . $monthSettingObj->month->formatMonth();
        }
        $response['status'] = true;
        $response['data'] = null;
        return $response;
    }
}
