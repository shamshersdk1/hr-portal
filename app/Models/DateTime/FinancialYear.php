<?php

namespace App\Models\DateTime;

use App\Models\BaseModel;
use App\Models\DateTime\Month;
use DateInterval;
use DatePeriod;
use DateTime;

class FinancialYear extends BaseModel
{
    protected $table = 'financial_years';
    private $rules = [];
    public function months()
    {
        return $this->hasMany('App\Models\DateTime\Month', 'financial_year_id', 'id');
    }
    public static function getRemainingMonths($monthId)
    {
        $monthObj = Month::find($monthId);
        if ($monthObj) {
            if ($monthObj->month >= 4 && $monthObj->month <= 12) {
                return (12 - ($monthObj->month - 3));
            } else {
                return (12 - ($monthObj->month + 9));
            }
        }
    }

    public static function getAllFinancialMonth($financialYearId)
    {
        $obj = self::find($financialYearId);
        if (!$obj) {
            return [];
        }
        $startYear = $obj->year;
        $endYear = $obj->year + 1;

        $startDate = date($startYear . '-04-01');
        $endDate = date($endYear . '-03-01');
        $months = [];
        $start = (new DateTime($startDate))->modify('first day of this month');
        $end = (new DateTime($endDate))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $months[] = ['month' => $dt->format("n"), 'year' => $dt->format("Y")];
        }
        return $months;
    }
    public static function getUpcomingMonth($financialYearId, $month = null)
    {
        $financialYearObj = FinancialYear::find($financialYearId);

        if ($month == null) {
            $lastProcessedMonthId = Month::getLastProcessedMonth();
            if ($lastProcessedMonthId) {
                $monthObj = Month::find($lastProcessedMonthId);
                if ($monthObj) {
                    $currMonth = $monthObj->month;
                } else {
                    $currMonth = date('m');
                }
            } else {
                $currMonth = date('m');
            }
        } else {
            $currMonth = $month;
        }
        $upcomingMonths = [];

        if ($financialYearObj->start_date <= date('Y-m-d') && $financialYearObj->end_date >= date('Y-m-d')) {

            if ($currMonth >= 4 && $currMonth <= 12) {
                for ($i = $currMonth + 1; $i <= 12; $i++) {
                    if (!isset($upcomingMonths[$i])) {
                        $upcomingMonths[$i] = (object) array();
                    }
                    $upcomingMonths[$i]->year = $financialYearObj->year;
                    $upcomingMonths[$i]->month = $i;
                }

                for ($i = 1; $i <= 3; $i++) {
                    if (!isset($upcomingMonths[$i])) {
                        $upcomingMonths[$i] = (object) array();
                    }
                    $upcomingMonths[$i]->year = $financialYearObj->year + 1;
                    $upcomingMonths[$i]->month = $i;
                }
            } else {
                for ($i = $currMonth + 1; $i <= 3; $i++) {
                    if (!isset($upcomingMonths[$i])) {
                        $upcomingMonths[$i] = (object) array();
                    }
                    $upcomingMonths[$i]->year = $financialYearObj->year + 1;
                    $upcomingMonths[$i]->month = $i;
                }
            }
        }

        return $upcomingMonths;
    }
}
