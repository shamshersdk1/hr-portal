<?php

namespace App\Models\DateTime;

use Illuminate\Database\Eloquent\Model;

class NonWorkingCalendar extends Model
{
    protected $table = 'non_working_calendar'; 

    public static function is_holiday($date){
        $result = self::where([
            ['date','=',date('Y-m-d', strtotime($date))],
            ['type','=','Holiday']
        ]
        )->first();
        if($result){
            return $result;
        }
        return false;
    }

    public static function isWeekend($date) {
        return (date('N', strtotime($date)) >= 6);
    }

}
