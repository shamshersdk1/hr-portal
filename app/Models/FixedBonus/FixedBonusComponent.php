<?php

namespace App\Models\FixedBonus;

use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;

class FixedBonusComponent extends Model
{
    use ValidatingTrait;

    protected $table = 'fixed_bonus_components';
    public $timestamps = false;

    protected $fillable = ['fixed_bonus_id','key','value','appraisal_bonus_id'];

    protected $rules = [
        'fixed_bonus_id' => 'required | exists:fixed_bonuses,id',
        'key' => 'required',
        'value' => 'required',
        'appraisal_bonus_id' => 'required | exists:appraisal_bonuses,id',
    ];
}
