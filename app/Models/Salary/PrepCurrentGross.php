<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepCurrentGross extends Model
{
    use ValidatingTrait;

    public $timestamps = true;

    protected $table = 'prep_current_gross';

    protected $rules = [
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\Salary\PrepCurrentGrossItem');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($prepCurrentGross) {
            $prepCurrentGross->items()->delete();
        });
    }

    public function itemByKey($key)
    {
        return $this->hasOne('App\Models\Salary\PrepCurrentGrossItem', 'prep_current_gross_id', 'id')->where('key', $key)->first();
    }

    public function prepSalary()
    {
        return $this->belongsTo('App\Models\Salary\PrepSalary', 'prep_salary_id', 'id');
    }

}
