<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class Salary extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'salary';
    public $timestamps = true;
    protected $fillable = ['financial_year_id','month_id', 'working_day', 'status'];

    protected $rules = [
        'financial_year_id' => 'required | exists:financial_years,id',
        'month_id' => 'required | exists:months,id',
        'working_day' => 'required | numeric',
        'status' => 'required',
    ];

    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }

    public function financialYear()
    {
        return $this->belongsTo('App\Models\DateTime\FinancialYear', 'financial_year_id', 'id');
    }
}
