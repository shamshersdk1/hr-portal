<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;
use App\Models\Salary\ {PrepSalary,PrepSalaryComponent};
use App\Models\Users\User;

class PrepSalaryExecution extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_salary_executions';
    public $timestamps = false;
    protected $fillable = ['prep_salary_id','component_id','user_id','status','counter'];

    protected $rules = [
		 'prep_salary_id' => 'required | exists:prep_salary,id',
         'user_id' => 'required|exists:users,id',
         'component_id' => 'required|exists:prep_salary_components,id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }
    public static function componentStatus($prepSalaryId)
    {
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        if(!$prepSalaryObj)
        {
            return false;
        }
        $prepExecutions = self::where('prep_salary_id',$prepSalaryId)->get();
        if(!count($prepExecutions)>0)
        {
            return false;
        }
        foreach($prepSalaryObj->components as $component)
        {
            $count = self::where('prep_salary_id',$prepSalaryId)->where('component_id',$component->id)->where('status','!=','completed')->where('status','!=','failed')->count();
            if($count == 0)
            {
                $component->is_generated = 1;
                $component->save();
            }else{
                $component->is_generated = 0;
                $component->save();
            }
        }
        $count = $prepSalaryObj->components()->where('is_generated','0')->count();
        if($count == 0 && $prepSalaryObj->status == 'in_progress')
        {
            $prepSalaryObj->status = 'processed';
            $prepSalaryObj->save();
        }
        $count = self::where('prep_salary_id',$prepSalaryId)->where('status','failed')->count();
        if($count > 0)
        {
            $prepSalaryObj->status = 'processed';
            $prepSalaryObj->save();
        }
        return true;
    }

    public static function setExecutionInProgress($salaryId,$userId,$componentId)
    {
        $prepSalary = PrepSalary::find($salaryId);
        if(!$prepSalary) {
            \Log::error('Prep Salary status is completed');
            return false;
        }
        $user = User::find($userId);
        if(!$user) {
            \Log::error('User not found');
            return false;
        }
        $component = PrepSalaryComponent::find($componentId);
        if(!$component) {
            \Log::error('Component not found');
            return false;
        }
        $prepSalaryExecutionObj = self::firstOrCreate(['prep_salary_id' => $prepSalary->id, 'user_id' => $user->id, 'component_id' => $component->id]);
        $prepSalaryExecutionObj->status = "in_progress";
        if($prepSalaryExecutionObj->save())
            return true;
        return false;
    }

    public static function setExecutionToCompleted($salaryId,$userId,$componentId)
    {
        $prepSalary = PrepSalary::find($salaryId);
        if(!$prepSalary) {
            \Log::error('Prep Salary status is completed');
            return false;
        }
        $user = User::find($userId);
        if(!$user) {
            \Log::error('User not found');
            return false;
        }
        $component = PrepSalaryComponent::find($componentId);
        if(!$component) {
            \Log::error('Component not found');
            return false;
        }
        $prepSalaryExecutionObj = self::where('prep_salary_id',$prepSalary->id)->where('user_id',$user->id)->where('component_id',$component->id)->first();
        $prepSalaryExecutionObj->status = "completed";
        if($prepSalaryExecutionObj->save())
            return true;
        return false;
    }

    public static function setExecutionToFailed($salaryId,$userId,$componentId)
    {
        $prepSalary = PrepSalary::find($salaryId);
        if(!$prepSalary) {
            \Log::error('Prep Salary status is completed');
            return false;
        }
        $user = User::find($userId);
        if(!$user) {
            \Log::error('User not found');
            return false;
        }
        $component = PrepSalaryComponent::find($componentId);
        if(!$component) {
            \Log::error('Component not found');
            return false;
        }
        $prepSalaryExecutionObj = self::where('prep_salary_id',$prepSalary->id)->where('user_id',$user->id)->where('component_id',$component->id)->first();
        $prepSalaryExecutionObj->status = "failed";
        if($prepSalaryExecutionObj->save())
            return true;
        return false;
    }

}
