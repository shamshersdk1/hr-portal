<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepSalaryComponent extends Model
{
    use ValidatingTrait;
    
    protected $table = 'prep_salary_components';
    public $timestamps = false;

    protected $rules = array(
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'prep_salary_component_type_id' => 'required|exists:prep_salary_component_types,id',
    );

    public function type()
    {
        return $this->belongsTo('App\Models\Salary\PrepSalaryComponentType', 'prep_salary_component_type_id', 'id');
    }

    public function salary()
    {
        return $this->belongsTo('App\Models\Salary\PrepSalary', 'prep_salary_id', 'id');
    }

    public function dependsOn()
    {
        return $this->hasMany('App\Models\Salary\PrepSalaryComponentTypeDependency', 'component_id', 'prep_salary_component_type_id');
    }

    public function dependentComponent()
    {
        return $this->hasMany('App\Models\Salary\PrepSalaryComponentTypeDependency', 'dependent_id', 'prep_salary_component_type_id');
    }
    public function totalExecutions()
    {
        return $this->hasMany('App\Models\Salary\PrepSalaryExecution', 'component_id', 'id')->count();
    }

    public function countExecutions($key)
    {
        return $this->hasMany('App\Models\Salary\PrepSalaryExecution', 'component_id', 'id')->where('status', $key)->count();
    }
}
