<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class PrepSalary extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'prep_salary';

    protected $rules = [
        'month_id' => 'required | exists:months,id',
        'status' => 'required',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($prepSalaryObj) {
            $prepSalaryObj->prepUsers()->delete();
            $prepSalaryObj->components()->delete();
        });
    }

    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }
    public function components()
    {
        return $this->hasMany('App\Models\Salary\PrepSalaryComponent');
    }
    public function prepUsers()
    {
        return $this->hasMany('App\Models\Salary\PrepUser');
    }

    public function checkAll()
    {
        $components = PrepSalaryComponent::where('prep_salary_id', $this->id)->get();

        foreach ($components as $component) {
            if ($component->is_generated == 0) {
                return false;
            }
        }
        return true;
    }
}
