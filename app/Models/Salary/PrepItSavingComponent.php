<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepItSavingComponent extends Model
{
    use ValidatingTrait;
    
    protected $table = 'prep_it_savings_components';
    public $timestamps = false;

    protected $rules = [
        'prep_it_saving_id' => 'required|exists:prep_it_savings,id',
    ];
}
