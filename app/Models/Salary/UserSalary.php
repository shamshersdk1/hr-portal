<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class UserSalary extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'user_salary';
    public $timestamps = true;
    protected $fillable = ['salary_id','user_id', 'financial_year_id', 'month_id','working_day','lop','status','comment'];

    protected $rules = [
        'salary_id' => 'required | exists:salary,id',
        'user_id' => 'required | exists:users,id',
        'financial_year_id' => 'required | exists:financial_years,id',
        'month_id' => 'required | exists:months,id',
        'working_day' => 'required | numeric',
        'status' => 'required',
    ];

    public function salary()
    {
        return $this->belongsTo('App\Models\Salary\Salary', 'salary_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }

    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }

    public function financialYear()
    {
        return $this->belongsTo('App\Models\DateTime\FinancialYear', 'financial_year_id', 'id');
    }

}
