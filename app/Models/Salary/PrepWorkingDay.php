<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepWorkingDay extends Model
{
    use ValidatingTrait;
    
    protected $table = 'prep_working_days';
    public $timestamps = false;

    protected $rules = [
        'user_id' => 'required|exists:users,id',
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'company_working_days' => 'required',
        'user_working_days' => 'required',
        'user_worked_days' => 'required',
        'pl_count' => 'required',
        'sl_count' => 'required',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }
}
