<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepAdhocPaymentComponent extends Model
{
    use ValidatingTrait;

    public $timestamps = false;
    protected $fillable = ['prep_adhoc_payment_id','key','value','adhoc_payment_id'];
    public $table = "prep_adhoc_payment_components";

    protected $rules = [
        'prep_adhoc_payment_id' => 'required|exists:prep_adhoc_payments,id',
        'key' => 'required',
        'value' => 'required | numeric',
        'adhoc_payment_id' => 'required|exists:adhoc_payments,id',
    ];

}
