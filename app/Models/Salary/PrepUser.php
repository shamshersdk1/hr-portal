<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepUser extends Model
{
    use ValidatingTrait;
 
    protected $table = 'prep_users';
    public $timestamps = false;

    protected $fillable = ['prep_salary_id','user_id'];

    protected $rules = [
        'prep_salary_id' => 'required | exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
    ];

    public function loans()
    {
        return $this->hasMany('App\Models\Loan\Loan', 'user_id', 'user_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }
    public function itSaving()
    {
        return $this->hasOne('App\Models\Admin\ItSaving', 'user_id', 'user_id');
    }
}
