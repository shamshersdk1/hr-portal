<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepGrossPaidItem extends Model
{
    use ValidatingTrait;

    public $timestamps = true;

    protected $rules = [
        'prep_gross_paid_id' => 'required|exists:prep_gross_paids,id',
        'key' => 'required',
        'value' => 'required',
    ];

}
