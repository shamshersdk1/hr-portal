<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepInsurance extends Model
{
    use ValidatingTrait;
    protected $table = 'prep_insurances';
    public $timestamps = false;
    protected $fillable = ['prep_salary_id', 'user_id', 'insurance_deduction_id', 'value'];

    protected $rules = [
        'prep_salary_id' => 'required|exists:prep_salary,id',
        'user_id' => 'required|exists:users,id',
        'insurance_deduction_id' => 'required | exists:insurance_deductions,id',
        'value' => 'required | numeric',
    ];
}
