<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepFoodDeduction extends Model
{
    use ValidatingTrait;

    protected $table = 'prep_food_deductions';
    public $timestamps = false;
    protected $fillable = ['prep_salary_id', 'user_id', 'food_deduction_id', 'value'];

    protected $rules = [
        'prep_salary_id' => 'required | exists:prep_salary,id',
        'user_id' => 'required | exists:users,id',
        'food_deduction_id' => 'required | exists:food_deductions,id',
        'value' => 'required | numeric',
    ];
}
