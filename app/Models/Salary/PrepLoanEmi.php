<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepLoanEmi extends Model
{
    use ValidatingTrait;
    protected $table = 'prep_loan_emi';
    public $timestamps = false;
    protected $fillable = ['prep_salary_id','user_id','loan_emi_id','emi_amount'];

    protected $rules = [
        'prep_salary_id' => 'required | exists:prep_salary,id',
        'loan_emi_id' => 'required | exists:loan_emis,id',
        'user_id'=>'required|exists:users,id',
        'emi_amount' => 'required | numeric',
    ];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loan\Loan', 'loan_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }

    public function salary()
    {
        return $this->belongsTo('App\Models\Salary\PrepSalary', 'prep_salary_id', 'id');
    }
}
