<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepBonus extends Model
{
    use ValidatingTrait;

    protected $rules = [
        'prep_salary_id' => 'required | exists:prep_salary,id',
        'bonus_id' => 'required | exists:bonuses,id',
        'user_id' => 'required | exists:users,id',
    ];

    public function bonus()
    {
        return $this->belongsTo('App\Models\Bonus\Bonus', 'bonus_id', 'id');
    }

}
