<?php

namespace App\Models\Salary;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class PrepGrossToBePaidItem extends Model
{
    use ValidatingTrait;

    public $timestamps = true;

    protected $rules = [
        'prep_gross_to_be_paid_id' => 'required|exists:prep_gross_to_be_paids,id',
        'key' => 'required',
        'value' => 'required',
    ];

}
