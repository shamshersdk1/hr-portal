<?php

namespace App\Models\BankTransfer;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class BankTransfer extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    public $timestamps = true;
    protected $fillable = ['month_id', 'status'];
    public $table = 'bank_transfers';

    protected $rules = [
        'status' => 'required',
    ];

    public function transferUsers()
    {
        return $this->hasMany('App\Models\BankTransfer\BankTransferUser', 'bank_transfer_id', 'id');
    }

    public function holdUsers()
    {
        return $this->hasMany('App\Models\BankTransfer\BankTransferHoldUser', 'bank_transfer_id', 'id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }
}
