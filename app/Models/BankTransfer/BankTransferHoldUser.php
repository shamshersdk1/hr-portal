<?php

namespace App\Models\BankTransfer;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class BankTransferHoldUser extends Model
{
    use ValidatingTrait;

    public $timestamps = true;
    protected $fillable = ['bank_transfer_id','user_id','amount'];
    public $table = 'bank_transfer_hold_users';

    protected $rules = [
        'bank_transfer_id' => 'required | exists:bank_transfers,id',
        'user_id' => 'required | exists:users,id',
        'amount' => 'required | numeric',
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }
    public function bankTransfer()
    {
        return $this->belongsTo('App\Models\BankTransfer\BankTransfer', 'bank_transfer_id', 'id');
    }
}
