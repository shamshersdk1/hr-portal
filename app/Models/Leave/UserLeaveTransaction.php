<?php

namespace App\Models\Leave;

use Illuminate\Database\Eloquent\Model;
use App\Models\DateTime\CalendarYear;
use Watson\Validating\ValidatingTrait;

class UserLeaveTransaction extends Model
{
    use ValidatingTrait;

    protected $fillable = ['calendar_year_id','user_id','reference_type','reference_id','leave_type_id','calculation_date', 'days'];

    private $rules = array(
        'calendar_year_id' => 'required|exists:calendar_years,id',
        'user_id' => 'required | exists:users,id',
        'reference_type' => 'required',
        'reference_id' => 'required',
        'leave_type_id' => 'required|exists:leave_types,id',
        'days' => 'required',
        'calculation_date' => 'required'
    );

    public static function saveData($data)
    {
        $status = true;
        $year = date('Y');
        $calendarObj = CalendarYear::where('year', $year)->first();
        $leaveTransaction = new UserLeaveTransaction();
        $leaveTransaction->user_id = $data['user_id'];
        $leaveTransaction->calendar_year_id = !empty($data['calendar_year_id']) ? $data['calendar_year_id'] : $calendarObj->id;
        $leaveTransaction->leave_type_id = $data['leave_type_id'];
        $leaveTransaction->calculation_date = !empty($data['date']) ? $data['date'] : date('Y-m-d');
        if ($data['reference_type'] == 'App\Models\Leave\Leave') {
            $leaveTransaction->days = (-1) * $data['days'];
        } else {
            if (!empty($data['type']) && $data['type'] == 'debit') {
                $leaveTransaction->days = (-1) * $data['days'];
            } else {
                $leaveTransaction->days = $data['days'];
            }
        }
        // if ($data['created_at']) {
        //     $leaveTransaction->created_at = $data['created_at'];
        // }
        $leaveTransaction->reference_type = $data['reference_type'];
        $leaveTransaction->reference_id = $data['reference_id'];
        if (!$leaveTransaction->save()) {
            $status = false;
            \Log::info('leaveTransaction save error' . json_encode($leaveTransaction->getErrors()));
        }

        return $status;

    }
}
