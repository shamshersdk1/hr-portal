<?php

namespace App\Models\Leave;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class LeaveCalendarYear extends Model
{
    use ValidatingTrait;

    protected $fillable = ['calendar_year_id','leave_type_id','max_allowed_leave'];

    protected $rules = [
        'calendar_year_id' => 'required | exists:calendar_years,id',
        'leave_type_id' => 'required | exists:leave_types,id',
        'max_allowed_leave' => 'required | numeric | min:1', 
    ];

    public function leaveType()
    {
        return $this->belongsTo('App\Models\Leave\LeaveType', 'leave_type_id', 'id');
    }
    public function calendarYear()
    {
        return $this->belongsTo('App\Models\DateTime\CalendarYear', 'calendar_year_id', 'id');
    }
}
