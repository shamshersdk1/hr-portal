<?php

namespace App\Models\Leave;

use App\Models\BaseModel;
use Watson\Validating\ValidatingTrait;

class Leave extends BaseModel
{

    use ValidatingTrait;

    protected $table = 'leaves';
    public $timestamps = true;
    protected $appends = array('sickLeaves', 'paidLeaves', 'maxSickLeaves', 'maxPaidLeaves');

    private $rules = array(
        'user_id' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
    );

    public static function isOnLeaveToday($userId, $date)
    {
        $leaveData = Leave::where('user_id', $userId)
            ->where('start_date', '<=', $date)
            ->where('end_date', '>=', $date)
            ->where('status', 'approved')->first();
        $response = $leaveData ? $leaveData : null;
        return $response;
    }

}
