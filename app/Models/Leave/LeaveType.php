<?php namespace App\Models\Leave;

use App\Models\BaseModel;

class LeaveType extends BaseModel
{

    protected $table = 'leave_types';
    public $timestamps = true;

}
