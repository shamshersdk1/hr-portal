<?php

namespace App\Models\Deduction;

use App\Models\DateTime\Month;
use App\Models\Vpf\VPF;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Salary\UserSalary;

class VpfDeduction extends Model
{
    use SoftDeletes;
    //protected $table = 'vpf_deductions';
    public function user()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
    public function vpf()
    {
        return $this->belongsTo('App\Models\Vpf\VPF');
    }
    public static function getCurrMonthVpf($monthId, $userId)
    {
        $vpfObj = VpfDeduction::where('month_id', $monthId)->where('user_id', $userId)->first();
        if ($vpfObj) {
            return $vpfObj->amount;
        } else {
            return 0;
        }
    }
    public static function regenerateVpf($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;

        $monthObj = Month::find($month_id);

        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->vpfSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        VpfDeduction::where('month_id', $month_id)->delete();
        $users = UserSalary::where('month_id',$month_id)->pluck('user_id')->toArray();
        $vpfs = VPF::where('status','enable')->whereIn('user_id',$users)->get();
        $errors = [];
        foreach ($vpfs as $vpf) {
            $deductionObj = new VpfDeduction();
            $deductionObj->month_id = $month_id;
            $deductionObj->user_id = $vpf->user_id;
            $deductionObj->amount = -$vpf->amount;
            $deductionObj->vpf_id = $vpf->id;
            if (!$deductionObj->save()) {
                $errors[] = $deductionObj->getErrors();
            }
        }
        $response['status'] = true;
        $response['message'] = "VPF deduction regenerated";
        $response['data'] = null;
        return $response;
    }

    public static function store($request, $monthId)
    {
        foreach ($request->new_amount as $index => $vpf) {
            $vpr = VpfDeduction::find($index);
            $vpr->amount = $request->new_amount[$index];
            if ($vpr->save()) {} else {
                return false;
            }
        }
        return true;
    }
}
