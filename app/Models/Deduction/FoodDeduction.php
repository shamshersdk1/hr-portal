<?php

namespace App\Models\Deduction;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class FoodDeduction extends Model
{
    use ValidatingTrait;

    protected $table = 'food_deductions';
    public $timestamps = false;

    protected $fillable = ['user_id', 'month_id', 'amount', 'actual_amount'];

    protected $rules = [
        'user_id' => 'required | exists:users,id',
        'month_id' => 'required | exists:months,id',
        'amount' => 'required | numeric | max:0',
        'actual_amount' => 'required | numeric | max:0',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }
    public function month()
    {
        return $this->belongsTo('App\Models\DateTime\Month', 'month_id', 'id');
    }
    public static function getCurrMonthDeduction($monthId, $userId)
    {
        $food = FoodDeduction::where('month_id', $monthId)->where('user_id', $userId)->sum('amount');
        return $food;
    }
}
