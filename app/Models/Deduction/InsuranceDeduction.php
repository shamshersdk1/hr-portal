<?php namespace App\Models\Deduction;

use App\Models\BaseModel;
use App\Models\DateTime\Month;
use App\Models\Insurance\InsurancePolicy;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Salary\UserSalary;

class InsuranceDeduction extends BaseModel
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User');
    }
    public function insurance()
    {
        return $this->belongsTo('App\Models\Insurance\InsurancePolicy', 'insurance_policy_id');
    }

    public static function regenerateInsurance($month_id)
    {
        $response['status'] = false;
        $response['message'] = null;
        $response['data'] = null;
        $monthObj = Month::find($month_id);
        if (!$monthObj) {
            $response['message'] = "Invalid month id " . $month_id;
            return $response;
        }
        if ($monthObj->insuranceSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        InsuranceDeduction::where('month_id', $month_id)->delete();
        $errors = [];
        $users = UserSalary::where('month_id',$month_id)->pluck('user_id')->toArray();
        $insurancePolicies = InsurancePolicy::where('parent_id', null)->where('status', 'approved')->get();
        foreach ($insurancePolicies as $insurancePolicy) {
            foreach ($insurancePolicy->subGroups as $subCategory) {
                foreach ($subCategory->insurances as $insurance) {
                    if(in_array($insurance->insurable_id,$users))
                    {
                        $deductionObj = new InsuranceDeduction();
                        $deductionObj->month_id = $month_id;
                        $deductionObj->user_id = $insurance->insurable_id;
                        $deductionObj->insurance_policy_id = $insurance->insurance_policy_id;
                        $deductionObj->amount = -$subCategory->premium_amount;
                        if (!$deductionObj->save()) {
                            $errors[] = $deductionObj->getErrors();
                        }
                    }
                }
            }
        }

        $response['status'] = true;
        $response['message'] = "Insurance deduction regenerated";
        $response['data'] = null;
        return $response;
    }

    public static function getCurrMonthDeduction($monthId, $userId)
    {
        $insurance = self::where('month_id', $monthId)->where('user_id', $userId)->sum('amount');
        return $insurance;
    }
}
