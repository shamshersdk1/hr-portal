<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class File extends Model
{

    use ValidatingTrait;
    protected $table = 'files';
    public $timestamps = true;
    use SoftDeletes;

    public function reference()
    {
        return $this->morphTo();
    }

    private $rules = array(
        'path' => 'required',
    );

    protected $fillable = [
        'id',
        'name',
        'type',
        'path',
        'reference_type',
        'reference_id',
    ];

    public function fileContent()
    {
        return $this->hasOne(FileContent::class);
    }

    public static function createRecord($content, $original_name, $type, $reference_id, $reference_type)
    {

        $file = File::create([
            'name' => $original_name,
            'type' => $type,
            'path' => "empty_path",
            'reference_type' => $reference_type,
            'reference_id' => $reference_id
            ]);

        $fileContent = FileContent::create([
                                'file_id' => $file->id,
                                'content' => $content
                            ]);
        return $file;
    }

    public function url()
    {
        return url("/db-file/".$this->id);
    }

}
