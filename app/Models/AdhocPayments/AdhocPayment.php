<?php

namespace App\Models\AdhocPayments;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class AdhocPayment extends Model
{
    use ValidatingTrait;
    protected $table = 'adhoc_payments';
    public $timestamps = false;

    protected $fillable = ['adhoc_payment_component_id','month_id','user_id','comment','amount'];

    protected $rules = [
        'adhoc_payment_component_id' => 'required | exists:adhoc_payment_components,id',
        'month_id' => 'required | exists:months,id',
        'user_id' => 'required | exists:users,id',
        'comment' => 'required | string',
        'amount' => 'required | numeric',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }

    public function component()
    {
        return $this->belongsTo('App\Models\AdhocPayments\AdhocPaymentComponent', 'adhoc_payment_component_id', 'id');
    }

    public static function getCurrMonthValue($monthId, $userId)
    {
        $adhocPaymentAmount = self::where('month_id', $monthId)->where('user_id', $userId)->sum('amount');
        return $adhocPaymentAmount;
    }
}
