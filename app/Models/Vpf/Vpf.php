<?php

namespace App\Models\Vpf;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class VPF extends Model implements Auditable
{
    use SoftDeletes;
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;
    protected $table = 'vpfs';
    protected $fillable = ['user_id','amount','created_by','updated_by'];

    private $rules = array(
        'user_id' => 'required',
        'amount' => 'required',
        'created_by' => 'required',
    );

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\Users\User', 'created_by');
    }

}
