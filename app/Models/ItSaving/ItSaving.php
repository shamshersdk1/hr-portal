<?php

namespace App\Models\ItSaving;

use App\Models\Appraisal\AppraisalComponentType;
use App\Models\BaseModel;
use App\Models\DateTime\FinancialYear;
use App\Models\DateTime\Month;
use App\Models\Deduction\VpfDeduction;
use App\Models\Transaction;
use App\Services\Appraisal\AppraisalService;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItSaving extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'it_savings';

    public function user()
    {
        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');
    }

    public function others()
    {
        return $this->hasMany('App\Models\ItSaving\ItSavingOther', 'it_savings_id', 'id');
    }

    public static function getItInvestment($userId, $financialYearId)
    {
        $investment = 0;

        $itSavingObj = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();
        if (!$itSavingObj) {
            return 0;
        }

        $investment = $itSavingObj->pf +
        $itSavingObj->pension_scheme_1 +
        $itSavingObj->pension_scheme_1b +
        $itSavingObj->ppf +
        $itSavingObj->central_pension_fund +
        $itSavingObj->lic +
        $itSavingObj->housing_loan_repayment +
        $itSavingObj->term_deposit +
        $itSavingObj->national_saving_scheme +
        $itSavingObj->tax_saving +
        $itSavingObj->children_expense +
        $itSavingObj->other_investment;

        if ($itSavingObj->other_multiple_investments != null) {
            $investmentArray = json_decode($itSavingObj->other_multiple_investments, true);

            if (is_array($investmentArray) && count($investmentArray) > 0) {
                if (isset($investmentArray['amount']) && is_array($investmentArray['amount'])) {
                    foreach ($investmentArray['amount'] as $amount) {
                        if (is_numeric($amount)) {
                            $investment += $amount;
                        }
                    }
                }
            }
        }

        return $investment;
    }

    public static function getItDeduction($userId, $financialYearId)
    {
        $deduction = 0;

        $itSavingObj = ItSaving::where('user_id', $userId)->where('financial_year_id', $financialYearId)->first();
        if (!$itSavingObj) {
            return 0;
        }

        $deduction = $itSavingObj->medical_insurance_premium +
        $itSavingObj->medical_treatment_expense +
        $itSavingObj->educational_loan +
        $itSavingObj->donation +
        $itSavingObj->rent_without_receipt +
        $itSavingObj->physical_disablity +
        $itSavingObj->other_deduction;

        if ($itSavingObj->other_multiple_deductions != null) {
            $deductionArray = json_decode($itSavingObj->other_multiple_deductions, true);

            if (is_array($deductionArray) && count($deductionArray) > 0) {
                if (isset($deductionArray['amount']) && is_array($deductionArray['amount'])) {
                    foreach ($deductionArray['amount'] as $amount) {
                        if (is_numeric($amount)) {
                            $deduction += $amount;
                        }
                    }
                }
            }
        }

        return $deduction;
    }

    public static function GetTableColumns()
    {

        $sql = "Select DISTINCT COLUMN_NAME  from information_schema.columns where table_name='it_savings' AND COlUMN_NAME NOT IN ('id','user_id','financial_year_id','salary_paid','agree','previous_form_16_12b','status','created_at','updated_at','deleted_at')";
        $itSavingComponents = DB::select($sql);

        return $itSavingComponents;
    }

    public function getMonthTotalPf($monthId)
    {
        $monthObj = Month::find($monthId);
        $pfEmployee = AppraisalComponentType::getIdByKey('pf-employee');
        $pfEmployeeAmount = 0;
        $vpfAmount = 0;
        $amount = 0;
        $pfEmployeeAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Appraisal\AppraisalComponentType', $pfEmployee);
        $vpfAmount = Transaction::getPaidTillDate($this->user_id, $this->financial_year_id, 'App\Models\Admin\VpfDeduction');
        $appraisalList = AppraisalService::getCurrentMonthAppraisal($this->user_id, $monthObj->id);
        if ($appraisalList == null || !isset($appraisalList[0])) {
            $response['errors'] = 'No Appraisal found!';
          //  $response['data'] = $data;
            return $response;
        }
        $currData = AppraisalService::generateCurrentData($appraisalList, $monthObj->id, $this->user_id);
        $currentPfAmount = abs(isset($currData[$pfEmployee])?$currData[$pfEmployee]:0);
        $currentVpfAmount = abs(VpfDeduction::getCurrMonthVpf($monthObj->id, $this->user_id));
        $currentFuturePf = $currentPfAmount * FinancialYear::getRemainingMonths($monthObj->id);
        $amount = $currentVpfAmount + $currentPfAmount + $currentFuturePf + abs($vpfAmount) + abs($pfEmployeeAmount) + abs($this->pf);
        return $amount;
    }
}
