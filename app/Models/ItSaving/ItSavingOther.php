<?php

namespace App\Models\ItSaving;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Watson\Validating\ValidatingTrait;

class ItSavingOther extends Model implements Auditable
{
    use ValidatingTrait;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'it_savings_others';
    public $timestamps = false;

    protected $rules = [
        'it_savings_id' => 'required | exists:it_savings,id',
        'title' => 'required| string',
        'value' => 'required| numeric',
        'type' => 'required| string',
    ];

    protected $fillable = ['it_savings_id', 'title', 'value', 'type'];

}
