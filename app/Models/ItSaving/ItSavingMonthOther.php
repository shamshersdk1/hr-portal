<?php

namespace App\Models\ItSaving;

use Illuminate\Database\Eloquent\Model;
use Watson\Validating\ValidatingTrait;

class ItSavingMonthOther extends Model
{
    use ValidatingTrait;
    protected $table = 'it_saving_months_others';
    protected $fillable = ['it_saving_month_id','title','type','value'];

    public $timestamps = false;

    protected $rules = [
        'it_saving_month_id' => 'required|exists:it_saving_months,id',
        'title' => 'required | string',
        'type' => 'required | string',
        'value' => 'required | numeric',
    ];

    public static function createMonthlyRecord($saving, $monthlySavingId)
    {
        foreach($saving->others as $other)
        {
            $savingOtherObj = self::create(['it_saving_month_id' => $monthlySavingId,'title' => $other->title,'type' => $other->type,'value' => $other->value]);
            if(!$savingOtherObj->isValid())
            {
                return false;
            }
        }
        return true;
    }
}
