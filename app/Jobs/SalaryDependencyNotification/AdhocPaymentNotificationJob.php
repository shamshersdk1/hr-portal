<?php

namespace App\Jobs\SalaryDependencyNotification;

use App\Models\AdhocPayments\AdhocPayment;
use App\Models\DateTime\Month;
use App\Models\SystemSetting\SystemSetting;
use App\Notifications\AdhocPaymentNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Notification;

class AdhocPaymentNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $monthId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($monthId)
    {
        $this->monthId = $monthId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $monthObj = Month::find($this->monthId);
        $adhocPayments = AdhocPayment::where('month_id',$this->monthId)->get();
        $data = [];
        foreach($adhocPayments as $key => $adhocPayment)
        {            
            $data[$key]['user_name'] = $adhocPayment->user->name;
            $data[$key]['employee_id'] = $adhocPayment->user->employee_id;
            $data[$key]['type'] = $adhocPayment->component->description;
            $data[$key]['comment'] = $adhocPayment->comment;
            $data[$key]['amount'] = $adhocPayment->amount;
          }

        $isNotifyAdmin = SystemSetting::where('key','SalaryDependencyAdminEmailNotify')->first();
        if($isNotifyAdmin && !empty($data))
        {
            $subject = "Salary Dependency | Adhoc Payment | ".$monthObj->formatMonth();
            $isNotifyAdminEmails = SystemSetting::where('key','SalaryDependencyAdminEmailNotifyAddress')->first();
            Notification::route('mail', $isNotifyAdminEmails->value)
            ->notify(new AdhocPaymentNotification($subject, $data , date('Y-m-d'))) ;
        }
    }
}
