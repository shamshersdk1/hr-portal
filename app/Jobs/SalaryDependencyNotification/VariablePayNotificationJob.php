<?php

namespace App\Jobs\SalaryDependencyNotification;

use App\Models\DateTime\Month;
use App\Models\SystemSetting\SystemSetting;
use App\Notifications\SalaryDependencyNotification;
use App\Services\Appraisal\AppraisalService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Notification;

class VariablePayNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $monthId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($monthId)
    {
        $this->monthId = $monthId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $monthObj = Month::find($this->monthId);
        $data = AppraisalService::getVariablePayNotificationData($this->monthId);
        $isNotifyAdmin = SystemSetting::where('key','SalaryDependencyAdminEmailNotify')->first();
        if($isNotifyAdmin && !empty($data))
        {
            $subject = "Salary Dependecy | Variable Bonuses | ".$monthObj->formatMonth();
            $isNotifyAdminEmails = SystemSetting::where('key','SalaryDependencyAdminEmailNotifyAddress')->first();
            Notification::route('mail', $isNotifyAdminEmails->value)
            ->notify(new SalaryDependencyNotification($subject, $data , date('Y-m-d'))) ;
        }
    }
}
