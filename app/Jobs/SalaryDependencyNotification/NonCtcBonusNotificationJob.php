<?php

namespace App\Jobs\SalaryDependencyNotification;

use App\Models\Bonus\BonusConfirmed;
use App\Models\DateTime\Month;
use App\Models\SystemSetting\SystemSetting;
use App\Notifications\NonCtcBonusNotification;
use App\Services\SalaryService\SalaryService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Notification;

class NonCtcBonusNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $monthId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($monthId)
    {
        $this->monthId = $monthId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $monthObj = Month::find($this->monthId);
        $bonusConfirms = BonusConfirmed::all();
        $data = [];
        foreach($bonusConfirms as $bonusConfirm)
        {            
            $data[$bonusConfirm->user_id]['user_name'] = $bonusConfirm->user->name;
            $data[$bonusConfirm->user_id]['employee_id'] = $bonusConfirm->user->employee_id;
            $data[$bonusConfirm->user_id]['all_bonuses'][$bonusConfirm->bonus->reference_type]['amount'] =  ($data[$bonusConfirm->user_id]['all_bonuses'][$bonusConfirm->bonus->reference_type]['amount'] ?? 0) + $bonusConfirm->bonus->amount ?? '';
            $data[$bonusConfirm->user_id]['all_bonuses'][$bonusConfirm->bonus->reference_type]['count'] =  ($data[$bonusConfirm->user_id]['all_bonuses'][$bonusConfirm->bonus->reference_type]['count'] ?? 0) + 1 ?? '';
            $data[$bonusConfirm->user_id]['amount'] =  ($data[$bonusConfirm->user_id]['amount'] ?? 0) + $bonusConfirm->bonus->amount;
        }

        $isNotifyAdmin = SystemSetting::where('key','SalaryDependencyAdminEmailNotify')->first();
        if($isNotifyAdmin && !empty($data))
        {
            $subject = "Salary Dependency | Non Ctc Bonus | ".$monthObj->formatMonth();
            $isNotifyAdminEmails = SystemSetting::where('key','SalaryDependencyAdminEmailNotifyAddress')->first();
            Notification::route('mail', $isNotifyAdminEmails->value)
            ->notify(new NonCtcBonusNotification($subject, $data , date('Y-m-d'))) ;
        }
    }
}
