<?php

namespace App\Jobs\SalaryDependencyNotification;

use App\Services\Appraisal\AppraisalService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\SystemSetting\SystemSetting;
use App\Models\DateTime\Month;
use Notification;
use App\Notifications\SalaryDependencyNotification;

class FixedBonusNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $monthId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($monthId)
    {
        $this->monthId = $monthId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $monthObj = Month::find($this->monthId);
        $data = AppraisalService::getFixedBonusNotificationData($this->monthId);        
        $isNotifyAdmin = SystemSetting::where('key','SalaryDependencyAdminEmailNotify')->first();
        if($isNotifyAdmin && !empty($data))
        {
            $subject = "Salary Dependecy | Fixed Bonuses | ".$monthObj->formatMonth();
            $isNotifyAdminEmails = SystemSetting::where('key','SalaryDependencyAdminEmailNotifyAddress')->first();
            Notification::route('mail', $isNotifyAdminEmails->value)
            ->notify(new SalaryDependencyNotification($subject, $data , date('Y-m-d'))) ;
        }

    }
}
