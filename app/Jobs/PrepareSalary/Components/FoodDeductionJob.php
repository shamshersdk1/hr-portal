<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Salary\{PrepSalary,PrepSalaryExecution,PrepFoodDeduction};
use App\Models\Deduction\FoodDeduction;
use App\Services\SalaryService\SalaryService;
use Exception;

class FoodDeductionJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'food-deduction';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw new Exception(get_class($this).' Job Requed');
        }
        $data['status'] = true;
        $prepSalary = PrepSalary::find($this->salaryId);
        $foodDeductionObj = FoodDeduction::where('month_id',$prepSalary->month_id)->where('user_id',$this->userId)->first();
        PrepFoodDeduction::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->delete();
        if($foodDeductionObj)
        {
            $prepFoodDeduction = PrepFoodDeduction::create(['prep_salary_id' => $this->salaryId,'user_id' => $this->userId,'food_deduction_id' => $foodDeductionObj->id, 'value' => $foodDeductionObj->amount]);
            if(!$prepFoodDeduction->isValid())
            {
                $data['status'] = false;
                \Log::error(get_class($this)." ".$prepFoodDeduction->getErrors());
            }
        }
        ($data['status']) ? PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id) : PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
    }
}
