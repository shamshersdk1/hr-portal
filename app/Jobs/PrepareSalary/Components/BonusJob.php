<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\Bonus\BonusConfirmed;
use App\Models\Bonus\PrepBonus;
use App\Services\SalaryService\SalaryService;
use App\Models\Salary\PrepSalaryExecution;


class BonusJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentName;
    /**P
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'bonus';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw Exception(get_class($this).' Job Requed');
        }
        $data['status'] = true;
        PrepBonus::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->get()->each(function ($prepBonus) {
            $prepBonus->delete();
        });
        $bonuses = BonusConfirmed::where('user_id',$this->userId)->get();
        foreach($bonuses as $bonus)
        {
            $prepBonus = PrepBonus::create(['prep_salary_id' => $this->salaryId,'user_id' => $this->userId,'bonus_id' => $bonus->bonus_id]);
            if($prepBonus->isInvalid())
            {
                \Log::error(get_class($this)." ".$prepBonus->getErrors());
                $data['status'] = false;
            }
        }
        ($data['status']) ? PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id) : PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
    }
}
