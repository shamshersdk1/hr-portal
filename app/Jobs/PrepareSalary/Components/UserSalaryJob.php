<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Audited\Salary\PrepUser;
use App\Models\AccessLog;
use Exception;



class UserSalaryJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($salaryId)
    {
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $response['status'] = true;
            $users = User::where('is_active','1')->get();
            if(!count($users)>0)
            {
                $response['errors'] = "No User found";
                $response['status'] = false;
                throw new Exception($response['errors']);
            }
            PrepUser::where('prep_salary_id',$this->salaryId)->delete();
            foreach($users as $user)
            {
                $prepUserObj = new PrepUser();
                $prepUserObj->user_id = $user->id;
                $prepUserObj->prep_salary_id = $this->salaryId;
                if($prepUserObj->isValid()){
                    if(!$prepUserObj->save()){
                        $response['errors'] = $response['errors']."Something went wrong";
                        $response['status'] = false;
                    }
                }
                else{
                    $response['errors'] = $response['errors'].$prepUserObj->getErrors();
                    $response['status'] = false;
                }
            }
            if(!$response['status'])
                throw new Exception($response['errors']);
        }
        catch(Exception $e)
        {
            AccessLog::accessLog(null, 'App\Jobs\PrepareSalary\Components\UserSalaryJob', 'Generate', 'handle', 'catch-block', $e->getMessage().'. salary_id '.$this->salaryId);
        }
    }
}
