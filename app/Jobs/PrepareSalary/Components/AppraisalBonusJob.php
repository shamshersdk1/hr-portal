<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\Salary\PrepAppraisalBonus;
use App\Services\SalaryService\SalaryService;
use App\Models\Salary\PrepSalaryExecution;

class AppraisalBonusJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'appraisal-bonus';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw new Exception(get_class($this).' Job Requed');
        }
        PrepAppraisalBonus::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->get()->each(function ($prepBonus) {
            $prepBonus->delete();
        });
        $variablePayResponse = SalaryService::variablePayEntryInPrep($this->salaryId,$this->userId);
        $fixedBonusResponse = SalaryService::fixedBonusEntryInPrep($this->salaryId,$this->userId);
        if($variablePayResponse['status'] || $fixedBonusResponse['status'])
            PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
        else
            PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id);
    }
}
