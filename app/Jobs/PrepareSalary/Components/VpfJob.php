<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\Salary\{PrepSalary,PrepSalaryExecution,PrepVpf};
use App\Models\Deduction\VpfDeduction;
use App\Services\SalaryService\SalaryService;

class VpfJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'vpf';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw new Exception(get_class($this).' Job Requed');
        }
        $data['status'] = true;
        $prepSalary = PrepSalary::find($this->salaryId);
        $vpfDeductionObj = VpfDeduction::where('user_id',$this->userId)->where('month_id',$prepSalary->month_id)->first();
        PrepVpf::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->delete();
        if($vpfDeductionObj)
        {
            $prepVpf = PrepVpf::create(['prep_salary_id' => $this->salaryId ,'user_id' => $this->userId, 'vpf_deduction_id' =>  $vpfDeductionObj->id,'value' => $vpfDeductionObj->amount]);
            if($prepVpf->isInvalid())
            {
                $data['status'] = false;
                \Log::error(get_class($this)." ".$prepVpf->getErrors());
            }
        }
        ($data['status']) ? PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id) : PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
    }
}
