<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\Salary\{PrepSalary,PrepSalaryExecution,PrepLoanEmi};
use App\Services\SalaryService\SalaryService;
use App\Models\Loan\LoanEmi;

class LoanJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'loan';
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw new Exception(get_class($this).' Job Requed');
        }
        $data['status'] = true;
        PrepLoanEmi::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->get()->each(function ($prepLoanEmi) {
            $prepLoanEmi->delete();
        });
        $prepSalary = PrepSalary::find($this->salaryId);
        $loanObjs  = LoanEmi::where('user_id',$this->userId)->where('month_id',$prepSalary->month_id)->where('is_manual_payment','0')->get();
        foreach($loanObjs as $loanObj)
        {
            $prepLoanDeduction = PrepLoanEmi::create(['prep_salary_id' => $this->salaryId,'user_id' => $this->userId, 'loan_emi_id' =>  $loanObj->id,'emi_amount' => $loanObj->amount]);
            if($prepLoanDeduction->isInvalid()) {
                \Log::error(get_class($this)." ".$prepLoanDeduction->getErrors());
                $data['status'] = false;
            }
        }
        ($data['status']) ? PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id) : PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
    }
}
