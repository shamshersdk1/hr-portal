<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Models\Salary\{PrepSalary,PrepAdhocPayment,PrepAdhocPaymentComponent,PrepSalaryExecution};
use App\Services\SalaryService\SalaryService;
use App\Models\AdhocPayments\AdhocPayment;


class AdhocPaymentJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'adhoc-payment';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw new Exception(get_class($this).' Job Requed');
        }

        $data['status'] = true;
        $prepSalary = PrepSalary::find($this->salaryId);
        $adhocPaymentObjs = AdhocPayment::where('user_id',$this->userId)->where('month_id',$prepSalary->month_id)->get();
        if(count($adhocPaymentObjs)>0)
        {
            PrepAdhocPayment::where('prep_salary_id', $this->salaryId)->where('user_id',$this->userId)->get()->each(function ($adhocObj) {
                $adhocObj->delete();
            });
            $prepAdhocPayment = PrepAdhocPayment::create(['prep_salary_id' => $this->salaryId ,'user_id' => $this->userId]);
            if($prepAdhocPayment->isInvalid())
            {
                $data['status'] = false;
                \Log::error(get_class($this)." ".$prepAdhocPayment->getErrors());
            }
            foreach($adhocPaymentObjs as $adhocPaymentObj)
            {
                $prepAdhocPaymentComponent = PrepAdhocPaymentComponent::create(['prep_adhoc_payment_id' => $prepAdhocPayment->id, 'key' => $adhocPaymentObj->component->key,'value' => $adhocPaymentObj->amount,'adhoc_payment_id' => $adhocPaymentObj->id]);
                if($prepAdhocPaymentComponent->isInvalid())
                {
                    \Log::error(get_class($this)." ".$prepAdhocPaymentComponent->getErrors());
                    $data['status'] = false;
                }
            }
        }
        ($data['status']) ? PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id) : PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
    }
}
