<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Salary\{PrepSalary,PrepSalaryExecution};
use App\Services\SalaryService\{SalaryService,TDSSalaryComponent};
use Exception;


class TDSSalaryJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentId,$componentName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $salaryId, $componentId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'tds';
        $this->componentId = $componentId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw new Exception(get_class($this).' Job Requed');
        }
        $prepSalary = PrepSalary::find($this->salaryId);
        $tdsObj = new TDSSalaryComponent;
        $tdsObj->setComponent($this->componentId);
        $tdsObj->setUserId($this->userId);
        $tdsObj->setMonthYear($prepSalary->month->month, $prepSalary->month->year);
        $data = $tdsObj->generateUserData($this->salaryId);
        ($data['status']) ? PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id) : PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
    }
}
