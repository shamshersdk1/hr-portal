<?php

namespace App\Jobs\PrepareSalary\Components;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Exception;
use App\Services\SalaryService\{SalaryService,GrossPaidComponent};
use App\Models\Salary\PrepSalaryExecution;


class GrossPaidJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'gross-paid-till-now';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw new Exception(get_class($this).' Job Requed');
        }
        $salaryObj = new GrossPaidComponent;
        $salaryObj->setUserId($this->userId);
        $data = $salaryObj->generateUserData($this->salaryId);
        ($data['status']) ? PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id) : PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
    }
}
