<?php

namespace App\Jobs\PrepareSalary\Components;

use App\Models\Deduction\InsuranceDeduction;
use App\Models\Salary\{PrepInsurance,PrepSalary,PrepSalaryExecution};
use App\Services\SalaryService\SalaryService;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class InsuranceJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$salaryId,$componentName;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
        $this->componentName = 'insurance';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $component = SalaryService::getComponent($this->componentName,$this->salaryId);
        PrepSalaryExecution::setExecutionInProgress($this->salaryId, $this->userId, $component->id);
        $response = SalaryService::checkDependency($component->id);
        if($response['status']) {
            $requeu = SalaryService::isReque($response['dependentOn'],$this->salaryId,$this->userId);
            if($requeu)
                throw new Exception(get_class($this).' Job Requed');
        }
        $data['status'] = true;
        $prepSalary = PrepSalary::find($this->salaryId);
        $insuranceDeductionObjs = InsuranceDeduction::where('user_id', $this->userId)->where('month_id', $prepSalary->month_id)->get();
        PrepInsurance::where('prep_salary_id', $this->salaryId)->where('user_id', $this->userId)->delete();
        foreach ($insuranceDeductionObjs as $insuranceDeductionObj) {
            $prepInsuranceDeduction = PrepInsurance::create(['prep_salary_id' => $this->salaryId, 'user_id' => $this->userId, 'insurance_deduction_id' => $insuranceDeductionObj->id, 'value' => $insuranceDeductionObj->amount]);
            if ($prepInsuranceDeduction->isInvalid()) {
                \Log::error(get_class($this)." ".$prepInsuranceDeduction->getErrors());
                $data['status'] = false;
            }
        }
        ($data['status']) ? PrepSalaryExecution::setExecutionToCompleted($this->salaryId, $this->userId, $component->id) : PrepSalaryExecution::setExecutionToFailed($this->salaryId, $this->userId, $component->id);
    }
}
