<?php

namespace App\Jobs\PrepareSalary\Transaction;

use App\Models\AccessLog;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Salary\PrepCurrentGross;
use App\Models\Salary\PrepCurrentGrossItem;
use App\Models\Salary\PrepSalary;
use App\Models\Transaction;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use DB;

class AppraisalComponentJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalaryObj = PrepSalary::find($this->salaryId);
        $grossObj = PrepCurrentGross::where("prep_salary_id", $this->salaryId)->where("user_id", $this->userId)->first();
        $appraisalTypes = AppraisalComponentType::where("is_computed", "0")->get();
        foreach ($appraisalTypes as $appraisalType) {
            $currentGrossObj = $grossObj->itemByKey($appraisalType->code);
            if ($currentGrossObj->value != 0) {
                $currentGrossObj->value > 0 ? $type = "credit" : $type = "debit";
                $transactionObj = Transaction::create(['prep_salary_id' => $this->salaryId,'month_id' => $prepSalaryObj->month_id, 'financial_year_id' => $prepSalaryObj->month->financial_year_id, 'user_id' => $this->userId, 'reference_id' => $appraisalType->id, 'reference_type' => 'App\Models\Appraisal\AppraisalComponentType', 'amount' => $currentGrossObj->value, 'type' => $type, 'is_company_expense' => $appraisalType->is_company_expense]);
                if (!$transactionObj->isValid()) {
                    \Log::error(get_class($this)." ".$transactionObj->getErrors());
                }
            }
        }
    }
}
