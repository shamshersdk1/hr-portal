<?php

namespace App\Jobs\PrepareSalary\Transaction;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Salary\PrepSalary;
use App\Models\Transaction;
use App\Models\Salary\PrepFoodDeduction;
use App\Models\AccessLog;
use Exception;
use DB;

class FoodDeductionJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalaryObj = PrepSalary::find($this->salaryId);
        $foodDeductionObj = PrepFoodDeduction::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->first();
        if($foodDeductionObj && $foodDeductionObj->value){
            $foodDeductionObj->value > 0 ? $type = "credit" : $type = "debit";
            $transactionObj = Transaction::create(['prep_salary_id' => $this->salaryId,'month_id' => $prepSalaryObj->month_id,'financial_year_id' => $prepSalaryObj->month->financial_year_id ,'user_id' => $this->userId,'reference_id' => $foodDeductionObj->food_deduction_id,'reference_type' => 'App\Models\Audited\FoodDeduction\FoodDeduction','amount' => $foodDeductionObj->value,'type' => $type,'is_company_expense' => 0]);
            if(!$transactionObj->isValid()) {
                \Log::error(get_class($this)." ".$transactionObj->getErrors());
            }
        }
    }
}
