<?php

namespace App\Jobs\PrepareSalary\Transaction;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Salary\PrepSalary;
use App\Models\Transaction;
use App\Models\Salary\PrepLoanEmi;
use App\Models\AccessLog;
use App\Models\Loan\LoanEmi;
use Exception;
use DB;

class LoanDeductionJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
            $prepSalaryObj = PrepSalary::find($this->salaryId);
            $loans = PrepLoanEmi::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->get();
            foreach($loans as $loan){
                if($loan->emi_amount){
                    $loan->emi_amount > 0 ? $type = "credit" : $type = "debit";
                    $transactionObj = Transaction::create(['prep_salary_id' => $this->salaryId,'month_id' => $prepSalaryObj->month_id,'financial_year_id' => $prepSalaryObj->month->financial_year_id ,'user_id' => $this->userId,'reference_id' => $loan->loan_emi_id,'reference_type' => 'App\Models\LoanEmi','amount' => $loan->emi_amount,'type' => $type,'is_company_expense' => 0]);
                    if($transactionObj->isValid()){
                        $loanEmi = LoanEmi::find($loan->loan_emi_id);
                        if($loanEmi->loan->remaining > 0)
                        {
                            $loanEmi->transaction_id = $transactionObj->id;
                            $loanEmi->paid_on = date("Y-m-d");
                            $loanEmi->status = 'paid';
                            $loanEmi->balance = $loanEmi->loan->remaining - abs($transactionObj->amount);
                            if(!$loanEmi->save()) {
                                \Log::error(get_class($this)." ".$loanEmi->getErrors());
                            }
                        }
                    }
                    else {
                        \Log::error(get_class($this)." ".$transactionObj->getErrors());
                    }
                }
            }
    }
}
