<?php

namespace App\Jobs\PrepareSalary\Transaction;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Salary\PrepSalary;
use App\Models\Transaction;
use App\Models\Salary\PrepInsurance;
use App\Models\AccessLog;
use Exception;
use DB;

class InsuranceDeductionJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalaryObj = PrepSalary::find($this->salaryId);
        $prepInsuranceObjs = PrepInsurance::where('prep_salary_id',$this->salaryId)->where('user_id',$this->userId)->get();
        foreach($prepInsuranceObjs as $prepInsuranceObj) {
            $prepInsuranceObj->value > 0 ? $type = "credit" : $type = "debit";
            $transactionObj = Transaction::create(['prep_salary_id' => $this->salaryId,'month_id' => $prepSalaryObj->month_id,'financial_year_id' => $prepSalaryObj->month->financial_year_id ,'user_id' => $this->userId,'reference_id' => $prepInsuranceObj->insurance_deduction_id,'reference_type' => 'App\Models\Admin\Insurance\InsuranceDeduction','amount' => $prepInsuranceObj->value,'type' => $type,'is_company_expense' => 0]);
            if(!$transactionObj->isValid()) {
                \Log::error(get_class($this)." ".$transactionObj->getErrors());
            }
        }
    }
}
