<?php

namespace App\Jobs\PrepareSalary\Transaction;

use App\Models\Salary\PrepSalary;
use App\Models\Salary\UserSalary;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserSalaryUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $salaryId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$salaryId)
    {
        $this->userId = $userId;
        $this->salaryId = $salaryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $prepSalaryObj = PrepSalary::find($this->salaryId);
        $prepSalaryObj AND UserSalary::whereUser_id($this->userId)->whereMonth_id($prepSalaryObj->month_id)->update(['status' => 'completed','prep_salary_id' => $this->salaryId]);
    }
}
