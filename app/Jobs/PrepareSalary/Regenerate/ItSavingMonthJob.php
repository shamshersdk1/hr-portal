<?php

namespace App\Jobs\PrepareSalary\Regenerate;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\DateTime\Month;
use App\Models\ItSaving\ItSaving;
use App\Models\ItSaving\ItSavingMonth;
use App\Models\ItSaving\ItSavingMonthOther;
use App\Models\AccessLog;
use Exception;

class ItSavingMonthJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $monthId;
    protected $itSavingId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($monthId,$itSavingId)
    {
        $this->monthId = $monthId;
        $this->itSavingId = $itSavingId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $saving = ItSaving::find($this->itSavingId);
        $monthObj = Month::find($this->monthId);
        if($saving && $saving->user && $saving->user->is_active)
        {
            $saving->pf = $saving->getMonthTotalPf($this->monthId);
            if(!isset($saving->pf['errors']))
            {
                $response = ItSavingMonth::createMonthlyRecord($monthObj->id, $saving);
                if($response['status'])
                {
                    \Log::error(get_class($this)." ".$response['data']);
                }
                ItSavingMonthOther::createMonthlyRecord($saving,$response['data']->id);
            }
        }
    }
}
