<?php

namespace App\Jobs\PrepareSalary\Regenerate;

use App\Models\DateTime\FinancialYear;
use App\Models\DateTime\Month;
use App\Models\Loan\Loan;
use App\Models\Loan\LoanEmi;
use App\Models\Loan\LoanInterest;
use App\Models\Users\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Session;

class LoanInterestJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $monthId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $monthId = null)
    {
        $this->userId = $userId;
        $this->monthId = $monthId;
    }
    /**

     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $financialYearObj = FinancialYear::where('status', 'running')->first();
        if ($financialYearObj) {
            $financialYearId = $financialYearObj->id;

            if ($this->monthId == null) {
                $month = date('m');
                $monthObj = Month::where('month', $month)->where('financial_year_id', $financialYearId)->first();
                if ($monthObj) {
                    $this->monthId = $monthObj->id;
                }
            } else {
                $monthObj = Month::find($this->monthId);
            }
            if ($this->userId && $monthObj) {
                $userObj = User::find($this->userId);
                if ($userObj) {
                    $interestRate = config('app.interest_rate');
                    $minAmount = config('app.min_outstanding_amount');

                    $str = $monthObj->year . "-" . $monthObj->month . "-" . date('d');
                    $currDate = date($str);

                    $totalPaidAmount = LoanEmi::where('status', 'paid')->where('month_id', '<=', $this->monthId)->where('transaction_id', '!=', null)->where('user_id', $userObj->id)->sum('amount');
                    $totalLoanAmount = Loan::where('user_id', $userObj->id)->where('status', '!=', 'pending')->whereDate('application_date', "<=", $currDate)->sum('amount');

                    LoanInterest::where('user_id', $this->userId)->where('month_id', $this->monthId)->delete();

                    $outStandingAmt = 0;
                    $outStandingAmt = $totalLoanAmount + $totalPaidAmount;

                    if ($outStandingAmt > $minAmount) {
                        $loanInterestObj = new LoanInterest;
                        $loanInterestObj->user_id = $this->userId;
                        $loanInterestObj->month_id = $this->monthId;
                        $loanInterestObj->financial_year_id = $financialYearId;
                        $loanInterestObj->total_outstanding_amount = $outStandingAmt;
                        $loanInterestObj->interest = round($interestRate * $outStandingAmt);

                        if (!$loanInterestObj->save()) {
                            \Log::error(get_class($this)." ".$loanInterestObj->getErrors());
                        }
                    }
                }
            }
        }
    }
}
