<?php

namespace App\Jobs\BankTransfer;

use App\Models\AccessLog;
use App\Models\BankTransfer\BankTransfer;
use App\Models\BankTransfer\BankTransferUser;
use App\Models\Bank\Account;
use App\Models\Bank\AccountType;
use App\Models\Bank\BankTransaction;
use App\Models\DateTime\Month;
use App\Models\Transaction;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class BankTransferJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $bankTransferId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bankTransferId, $userId)
    {
        $this->userId = $userId;
        $this->bankTransferId = $bankTransferId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currentDate = date("Y-m-d");
        $month = Month::getMonthByDate($currentDate);
        $amount = Transaction::where('user_id', $this->userId)->where('is_company_expense', 0)->sum('amount');

        $accountTypeObj = AccountType::where('code', 'saving-account')->first();
        if (!$accountTypeObj) {
            \Log::error(get_class($this)." ".'Account Type not found!');
        }

        $account = Account::where('bank_account_type_id', $accountTypeObj->id)->where('reference_type', 'App\Models\Users\User')->where('reference_id', $this->userId)->first();
        if (!$account) {
            \Log::error(get_class($this)." ".'Savings Account not found!');
        }

        $bankAccountNumber = $account->account_number;
        if ($bankAccountNumber != null && $bankAccountNumber != 0) {
            $bankTransferUserObj = BankTransferUser::create(['bank_transfer_id' => $this->bankTransferId, 'user_id' => $this->userId, 'bank_acct_number' => $bankAccountNumber, 'amount' => $amount, 'transaction_amount' => $amount, 'mode_of_transfer' => 'NEFT']);
            $amount = ($amount * -1);
            $amount > 0 ? $type = "credit" : $type = "debit";
            if($bankTransferUserObj->isValid())
            {
                $transactionObj = Transaction::create(['month_id' => $month->id, 'financial_year_id' => $month->financial_year_id, 'user_id' => $this->userId, 'reference_id' => $bankTransferUserObj->id, 'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferUser', 'amount' => $amount, 'type' => $type]);
                if ($transactionObj->isInvalid()) {
                    \Log::error(get_class($this)." ".$transactionObj->getErrors());
                }
            } else {
                \Log::error(get_class($this)." ".$bankTransferUserObj->getErrors());
            }
            $check = BankTransaction::where('financial_year_id', $month->financial_year_id)->where('month_id', $month->id)->where('account_id', $account->id)->where('account_type_id', $accountTypeObj->id)->where('reference_type', 'App\Models\Audited\BankTransfer\BankTransferUser')->where('reference_id', $accountTypeObj->id)->first();
            if (!$check) {
                $bankTransactionObj = BankTransaction::create(['financial_year_id' => $month->financial_year_id, 'month_id' => $month->id, 'account_id' => $account->id, 'account_type_id' => $accountTypeObj->id, 'amount' => (-1) * $amount, 'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferUser', 'reference_id' => $accountTypeObj->id]);
            } else {
                $bankTransactionObj = BankTransaction::updateOrCreate(['id' => $check->id], ['financial_year_id' => $month->financial_year_id, 'month_id' => $month->id, 'account_id' => $account->id, 'account_type_id' => $accountTypeObj->id, 'amount' => (-1) * $amount, 'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferUser', 'reference_id' => $accountTypeObj->id]);
            }
            if (!$bankTransactionObj->isValid()) {
                $status = true;
                \Log::error(get_class($this)." ".$bankTransactionObj->getErrors());
            }
        } else {
            \Log::error(get_class($this)." "."User Account not found!");
        }
    }
}
