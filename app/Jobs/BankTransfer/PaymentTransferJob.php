<?php

namespace App\Jobs\BankTransfer;

use App\Models\BankTransfer\BankTransfer;
use App\Models\Bank\BankTransaction;
use App\Models\Bank\PaymentTransferAccount;
use App\Models\DateTime\FinancialYear;
use App\Models\DateTime\Month;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Bank\Account;

class PaymentTransferJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $paymentId;
    protected $account;
    protected $amount;
    protected $accountTypeId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($paymentId, $accountNumber, $amount, $accountTypeId)
    {
        $this->paymentId = $paymentId;
        $this->account = $accountNumber;
        $this->amount = $amount;
        $this->accountTypeId = $accountTypeId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currMonth = date('m');
        $currYear = date('Y');
        $monthObj = Month::where('month', $currMonth)->where('year', $currYear)->first();
        if (!$monthObj) {
            \Log::error(get_class($this) . " " . 'Month not found.');
        }

        $financialYearObj = FinancialYear::where('status', 'running')->orderBy('year', 'DESC')->first();
        if (!$financialYearObj) {
            \Log::error(get_class($this) . " " . 'Financial Year not found.');
        }
        $accountObj = Account::find($this->account);
        if (!$accountObj) {
            \Log::error(get_class($this) . " " . 'Account not found.');
        }
        $paymentAccount = PaymentTransferAccount::create(['account_number' => $accountObj->account_number,'ifsc_code' => $accountObj->ifscMetaValue()->value ?? null,'payment_transfer_id' => $this->paymentId, 'account_id' => $this->account, 'amount' => $this->amount]);
        if ($paymentAccount->isValid()) {
            //reverse payment
            $bankTransaction = BankTransaction::create(['financial_year_id' => $financialYearObj->id, 'month_id' => $monthObj->id, 'account_id' => $this->account, 'account_type_id' => $this->accountTypeId, 'amount' => (-1) * $this->amount, 'reference_type' => 'App\Models\Bank\PaymentTransfer', 'reference_id' => $this->paymentId]);
            if ($bankTransaction->isInvalid()) {
                \Log::error(get_class($this) . " " . $bankTransaction->getErrors());
            }
        } else {
            \Log::error(get_class($this) . " " . $paymentAccount->getErrors());
        }
    }
}
