<?php

namespace App\Jobs\BankTransfer;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\DateTime\Month;
use App\Models\BankTransfer\BankTransfer;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use App\Models\BankTransfer\BankTransferHoldUser;
use Exception;
use App\Models\AccessLog;

class BankTransferHoldJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId;
    protected $bankTransferId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bankTransferId,$userId)
    {
        $this->userId = $userId;
        $this->bankTransferId = $bankTransferId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $currentDate = date("Y-m-d");
        $month = Month::getMonthByDate($currentDate);
        $amount = Transaction::where('user_id',$this->userId)->where('is_company_expense',0)->sum('amount');
        $bankTransferHoldUserObj = BankTransferHoldUser::create(['bank_transfer_id' => $this->bankTransferId,'user_id' => $this->userId, 'amount' => $amount]);
        $amount = ($amount * -1);
        $amount > 0 ? $type = "credit" : $type = "debit";
        if($bankTransferHoldUserObj->isValid()) {
            $transactionObj = Transaction::create(['month_id' => $month->id,'financial_year_id' => $month->financial_year_id ,'user_id' => $this->userId,'reference_id' => $bankTransferHoldUserObj->id,'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferHoldUser','amount' => $amount,'type' => $type]);
            if($transactionObj->isInvalid()) {
                \Log::error(get_class($this)." ".$transactionObj->getErrors());
            }
        }
        else {
            \Log::error(get_class($this)." ".$bankTransferHoldUserObj->getErrors());
        }
    }
}
