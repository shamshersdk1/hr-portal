<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Salary\PrepSalaryComponent;
use App\Models\Salary\PrepSalaryExecution;
use App\Jobs\DummyJob;

class ComponentPushJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $dependencyArray = [];
    protected $userId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($dependencyArray, $user_id)
    {
        $this->dependencyArray = $dependencyArray;
        $this->userId = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $chain = [];
        foreach($this->dependencyArray as $component)
        {
            $prepSalaryComponent = PrepSalaryComponent::find($component);
            $name = $prepSalaryComponent->type->service_class;
            $strModelObj = new $name();
            $jobName = $strModelObj->getJobName();
            if(method_exists($name,'isRequiredPush'))
            {
                $pushCheck = $strModelObj->isRequiredPush($this->userId,$prepSalaryComponent->salary->month_id);
                if($pushCheck)
                {
                    $chain[] = new $jobName($this->userId,$prepSalaryComponent->prep_salary_id);
                } else {
                    PrepSalaryExecution::where('prep_salary_id',$prepSalaryComponent->prep_salary_id)->where('component_id',$prepSalaryComponent->id)->where('user_id',$this->userId)->update(['status' => 'completed']);
                }
            } else {
                if($jobName == 'App\Jobs\PrepareSalary\Components\TDSSalaryJob')
                    $chain[] = new $jobName($this->userId,$prepSalaryComponent->prep_salary_id,$prepSalaryComponent->id);
                else
                    $chain[] = new $jobName($this->userId,$prepSalaryComponent->prep_salary_id);
            }
        }
        DummyJob::withChain($chain)->dispatch();
    }
}
