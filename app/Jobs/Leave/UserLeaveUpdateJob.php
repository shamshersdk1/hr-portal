<?php

namespace App\Jobs\Leave;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\DateTime\CalendarYear;
use App\Models\Users\User;
use App\Models\Leave\ { LeaveCalendarYear, UserLeaveTransaction};
use App\Services\Leave\NewLeaveService;
use App\Jobs\Leave\SetCarryForwardJob;

class UserLeaveUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$calendarYearId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$calendarYearId)
    {
        $this->userId = $userId;
        $this->calendarYearId = $calendarYearId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->userId);
        $calendarYearObj = CalendarYear::find($this->calendarYearId);
        $leaveCalendarYears = LeaveCalendarYear::where('calendar_year_id', $calendarYearObj->id)->get();
        foreach ($leaveCalendarYears as $leaveCalendarYear) {
           if ($leaveCalendarYear->leaveType) {
                $data = [];
                $data['leave_type_id'] = $leaveCalendarYear->leave_type_id;
                $data['reference_type'] = "App\\Models\\Leave\\LeaveCalendarYear";
                $data['reference_id'] = $leaveCalendarYear->id;
                $data['user_id'] = $user->id;
                $data['calendar_year_id'] = $leaveCalendarYear->calendar_year_id;
                $leaveTransactionObj = UserLeaveTransaction::where('leave_type_id', $leaveCalendarYear->leave_type_id)->where('reference_type', $data['reference_type'])->where('reference_id', $data['reference_id'])->where('user_id', $user->id)->where('calendar_year_id', $data['calendar_year_id'])->first();
                if(!$leaveTransactionObj) {
                    if (date('Y', strtotime($user->joining_date)) == $calendarYearObj->year) {
                        $data['days'] = NewLeaveService::getUserProRataLeaves($user->id, $leaveCalendarYear->leave_type_id, $calendarYearObj->id);
                    } else {
                        $data['days'] = $leaveCalendarYear->max_allowed_leave;
                    }
                    $data['days'] = $data['days'];
                    $data['calculation_date'] = date('Y-m-d');
                    UserLeaveTransaction::firstOrCreate($data);
                }
           }
        }
        // if (date('Y', strtotime($user->confirmation_date)) != $calendarYearObj->year) {
        //     dispatch(new SetCarryForwardJob($user->id,$calendarYearObj->year));
        // }
    }
}
