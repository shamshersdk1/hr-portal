<?php

namespace App\Jobs\Leave;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\DateTime\CalendarYear;
use App\Models\Users\User;
use App\Models\Leave\LeaveType;
use App\Services\Leave\UserLeaveService;
use App\Models\Leave\LeaveCalendarYear;

class SetCarryForwardJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userId,$calendarYearId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId,$calendarYearId)
    {
        $this->userId = $userId;
        $this->calendarYearId = $calendarYearId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       \Log::info('year ID '.$this->calendarYearId);
       \Log::info('userId ID '.$this->userId);
        $currYearObj = CalendarYear::find($this->calendarYearId);
        if (!$currYearObj) {
            return false;
        }
        $prevYear = $currYearObj->year - 1;
        $prevYearObj = CalendarYear::where('year', $prevYear)->first();
        if (!$prevYearObj) {
            return false;
        }
        $userObj = User::find($this->userId);
        if (!$userObj) {
            return false;
        }
        $leaveTypes = LeaveType::all();
        $prevLeaveObj = UserLeaveService::getUserAvailableLeaveBalance($prevYearObj->year, $userObj->id);
        $prevPLCount = [];
        $paidLeaveID = LeaveType::where('code','paid')->first()->id;


        foreach($leaveTypes as $type)
        {
            $prevCount[$type->code] = $prevLeaveObj[$type->code];
            if($type->code =="sick" && $prevCount['sick'] > 1 )
            {
                $leaveCalendar = LeaveCalendarYear::where('calendar_year_id',$prevYearObj->id)->where('leave_type_id',$type->id)->first();
                $data['leave_type_id'] = $paidLeaveID;
                $data['days'] = ceil($prevCount['sick']/2);
                $data['reference_type'] = "App\Models\Leave\LeaveCalendarYear";
                $data['reference_id'] = $leaveCalendar->id;
                $data['user_id'] = $userObj->id;
                $data['calendar_year_id'] = $currYearObj->id;
                UserLeaveService::addUserLeaves($data);
            }
            else
            {
                if($prevCount[$type->code] <= -1 || $prevCount[$type->code] >= 1)
                {
                    if(!(($type->code == "optional-holiday" || $type->code == "unpaid") && $prevCount[$type->code] <= -1)) {
                        
                        $leaveCalendar = LeaveCalendarYear::where('calendar_year_id',$prevYearObj->id)->where('leave_type_id',$type->id)->first();
                        if($prevCount[$type->code] >= 1)
                            $data['leave_type_id'] = $paidLeaveID;
                        else
                            $data['leave_type_id'] = $type->id;
                        $data['days'] = $prevCount[$type->code];
                        $data['reference_type'] = "App\Models\Leave\LeaveCalendarYear";
                        $data['reference_id'] = $leaveCalendar->id;
                        $data['user_id'] = $userObj->id;
                        $data['calendar_year_id'] = $currYearObj->id;
                        UserLeaveService::addUserLeaves($data);
                    }
                }
            }
        }

        // if(abs($prevSLCount) == 0.5) {
        //     $sLcarry = 0;
        // } else {
        //     $sLcarry = ceil($prevSLCount/2);
        // }

        // if($prevOLCount > 0) {
        //     $totalPL = $prevPLCount + $sLcarry + $prevOLCount;
        // } else {
        //     $totalPL = $prevPLCount + $sLcarry;
        // }




        //     $data['leave_type_id'] = $PLObj->id;
        //     $data['days'] = $carryForward;
        //     $data['reference_type'] = "App\Models\Leave\LeaveCalendarYear";
        //     $data['reference_id'] = $prevYearObj->id;
        //     $data['user_id'] = $userObj->id;
        //     $data['calendar_year_id'] = $currYearObj->id;
        //     UserLeaveService::addUserLeaves($data);
    }
}
