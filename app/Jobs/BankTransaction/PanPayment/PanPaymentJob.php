<?php

namespace App\Jobs\BankTransaction\PanPayment;

use App\Models\Bank\Account;
use App\Models\Bank\AccountType;
use App\Models\Bank\BankTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Transaction;

class PanPaymentJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $userId, $monthId, $financialYearId, $bankTransferUserId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $monthId, $financialYearId, $bankTransferUserId)
    {
        $this->userId = $userId;
        $this->monthId = $monthId;
        $this->financialYearId = $financialYearId;
        $this->bankTransferUserId = $bankTransferUserId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $appraisalType = AppraisalComponentType::where("code", "tds")->first();
        $amount = Transaction::where('month_id',$this->monthId)->where('user_id',$this->userId)->where('reference_type','App\Models\Appraisal\AppraisalComponentType')->where('reference_id',$appraisalType->id)->sum('amount');
        $accountTypeObj = AccountType::where('code', 'pan-account')->first();
        $account = Account::where('bank_account_type_id', $accountTypeObj->id)->where('reference_type', 'App\Models\Users\User')->where('reference_id', $this->userId)->first();
        if($account) {
            $bankTransactionObj = BankTransaction::updateOrCreate(['financial_year_id' => $this->financialYearId, 'month_id' => $this->monthId, 'account_id' => $account->id, 'account_type_id' => $accountTypeObj->id, 'amount' => $amount, 'reference_type' => 'App\Models\BankTransfer\BankTransferUser', 'reference_id' => $this->bankTransferUserId]);
            if (!$bankTransactionObj->isValid()) {
                \Log::error(get_class($this)." ".$bankTransactionObj->getErrors());
            }
        }

    }
}
