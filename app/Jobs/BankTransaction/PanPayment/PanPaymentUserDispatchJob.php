<?php

namespace App\Jobs\BankTransaction\PanPayment;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\BankTransfer\BankTransfer;
use App\Jobs\BankTransaction\PanPayment\PanPaymentJob;

class PanPaymentUserDispatchJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $bankTransferId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bankTransferId)
    {
        $this->bankTransferId = $bankTransferId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $bankTransfer = BankTransfer::find($this->bankTransferId);
        if($bankTransfer)
        {
            foreach($bankTransfer->transferUsers as $bankTransferUser)
            {
                dispatch(new PanPaymentJob($bankTransferUser->user_id, $bankTransfer->month_id, $bankTransfer->month->financial_year_id, $bankTransferUser->id));
            }
        }
    }
}
