<?php

namespace App\Jobs\BankTransaction\PT;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Salary\PrepSalary;
use App\Models\Appraisal\{PrepAppraisal,AppraisalComponentType,PrepAppraisalComponent};
use App\Models\Bank\AccountType;
use App\Models\Bank\Account;
use App\Models\Bank\BankTransaction;
use App\Models\Transaction;


class PtJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userId, $monthId, $financialYearId, $bankTransferUserId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $monthId, $financialYearId, $bankTransferUserId)
    {
        $this->userId = $userId;
        $this->monthId = $monthId;
        $this->financialYearId = $financialYearId;
        $this->bankTransferUserId = $bankTransferUserId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $appraisalType = AppraisalComponentType::where("code",'professional-tax')->firstOrFail();
        $amount = Transaction::where('month_id',$this->monthId)->where('user_id',$this->userId)->where('reference_type','App\Models\Appraisal\AppraisalComponentType')->where('reference_id',$appraisalType->id)->sum('amount');
        $accountTypeObj = AccountType::firstOrCreate(['code' => 'pt-account'],['name' => 'PT-Account','is_editable' => true]);
        $account = Account::firstOrCreate(['bank_account_type_id' => $accountTypeObj->id,'reference_type' => 'App\Models\Users\User','reference_id' => $this->userId,'account_number' => $this->userId."PT"]);
        $bankTransactionObj = BankTransaction::updateOrCreate(['financial_year_id' => $this->financialYearId, 'month_id' => $this->monthId, 'account_id' => $account->id, 'account_type_id' => $accountTypeObj->id, 'amount' => $amount, 'reference_type' => 'App\Models\BankTransfer\BankTransferUser', 'reference_id' => $this->bankTransferUserId]);
        if (!$bankTransactionObj->isValid()) {
            \Log::error(get_class($this)." ".$bankTransactionObj->getErrors());
        }
    }
}
