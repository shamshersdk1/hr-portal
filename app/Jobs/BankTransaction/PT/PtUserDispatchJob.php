<?php

namespace App\Jobs\BankTransaction\PT;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\BankTransfer\BankTransfer;
use App\Jobs\BankTransaction\PT\PtJob;

class PtUserDispatchJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $bankTransferId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bankTransferId)
    {
        $this->bankTransferId = $bankTransferId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $bankTransfer = BankTransfer::find($this->bankTransferId);
        if($bankTransfer)
        {
            foreach($bankTransfer->transferUsers as $bankTransferUser)
            {
                dispatch(new PtJob($bankTransferUser->user_id, $bankTransfer->month_id, $bankTransfer->month->financial_year_id, $bankTransferUser->id));
            }
        }
    }
}
