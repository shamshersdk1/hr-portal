<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use OwenIt\Auditing\Models\Audit;
use Carbon\Carbon;
use App\Models\Appraisal\{ Appraisal, AppraisalBonusType };
use App\Services\Appraisal\AppraisalService;
use App\Notifications\AuditNotification;
use App\Notifications\AppraisalAuditNotification;
use App\Models\Users\User;
use App\Models\SystemSetting\SystemSetting;
use Notification;
use Config;
use App\Jobs\Audit\AppraisalAuditJob;



class AppraisalInfoUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'audit:user-appraisal-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send appraisal detail audit report to user and HR';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $isNotifyAdmin = SystemSetting::where('key','AppraisalAuditAdminEmailNotify')->first();

        $from = Carbon::now()->subMinutes(6)->toDateTimeString();
        $to = Carbon::now()->subMinutes(1)->toDateTimeString();
        
        $data = AppraisalService::getAudits($from,$to);
        
        if(count($data) > 0) {
            $date = date_in_view(Carbon::now());
            $bonusTypes = AppraisalBonusType::all();
            foreach($data as $id => $audit) {
                $temp = (array)$audit;
                $appraisalId = !empty($temp[0]['appraisal_id'])?$temp[0]['appraisal_id']:NULL;
                $appraisal = Appraisal::find($appraisalId);
                
                if(!$appraisal)
                    return false;

                $user = User::find($id);
                $subject = 'Appraisal | Update | '.$user->name.' | '. $user->employee_id;    

                if($isNotifyAdmin && $isNotifyAdmin->value == 1) {
                    $isNotifyAdminEmails = SystemSetting::where('key','AppraisalAuditAdminEmailNotityEmailAddress')->first();
                    if($isNotifyAdmin && !empty($isNotifyAdminEmails->value)) {
                        \Log::info('User Audit report sent to admin : '.$isNotifyAdminEmails->value);
                        $appraisal = Appraisal::find($appraisalId);
                        Notification::route('mail', $isNotifyAdminEmails->value)
                            ->notify(new AuditNotification($subject, $audit, $user, $date));
                    }
                }
            }
        }     
    }
}