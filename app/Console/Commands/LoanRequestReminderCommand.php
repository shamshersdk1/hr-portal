<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Users\User;
use App\Models\Loan\LoanRequest;
use App\Services\Loan\LoanService;
use Carbon\Carbon;
use Config;


class LoanRequestReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:loan-request-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the loan request reminder on slack for unprocessed request';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $loanRequests = LoanRequest::with('user')->where('updated_at','>=',Carbon::now()->subdays(1))->whereIn('status', ['pending','submitted','review','reconcile'])->get();
        $loanRequestBaseUrl = Config::get('loan.loan_request_base_url').'admin';

        if(!$loanRequests){
            return Redirect::back()->withErrors('Something went wrong!');
        }
        foreach($loanRequests as $loanRequest){
            $status = ['reconcile'=>'reconciled-loan-requests','pending'=>'pending-loan-requests','submitted'=>'submitted-loan-requests','review'=>'reviewed-loan-requests'];
            $url = "";
            foreach($status as $key=>$item){
                if($loanRequest->status === $key){
                    $url = $loanRequestBaseUrl."/".$status[$key]."/".$loanRequest->user->id;
                }
            }
            $message = " -Loan Request <".$url."| #".$loanRequest->user->id."> | ".$loanRequest->user->name." | Status: `".$loanRequest->status."`.\n";
            LoanService::sendLoanRequestReminder($loanRequest->id,$message);
        }
    }
}
