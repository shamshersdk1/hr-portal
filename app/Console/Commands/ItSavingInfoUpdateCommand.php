<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use OwenIt\Auditing\Models\Audit;
use App\Notifications\UserInfoUpdateNotification;
use App\Models\Users\User;
use Carbon\Carbon;

class ItSavingInfoUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:it-saving-update-mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail when any updation in IT Saving details';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $from = Carbon::now()->toDateTimeString();
        $to = Carbon::now()->subMinutes(1)->toDateTimeString();
        $auditedData = Audit::where('auditable_type','App\Models\Admin\ItSaving')->whereDate('updated_at','<=',$to)->get();
        if(count($auditedData)>0){
            foreach($auditedData as $audit){
                $user = User::find($audit->user_id);
        
                $title = "The title of your message";
                $old_values = $audit->old_values;
                $new_values = $audit->new_values;
        
                if(count($new_values)>0){
                    $user->notify((new UserInfoUpdateNotification($title,$old_values,$new_values,$user))->delay(Carbon::now()->addSeconds(10)));
                }
            }
        }
    }
}
