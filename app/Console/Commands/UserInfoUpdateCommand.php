<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\User\UserService;
use App\Models\Users\User;
use OwenIt\Auditing\Models\Audit;
use Carbon\Carbon;
use Config;
use App\Notifications\AuditNotification;
use App\Models\SystemSetting\SystemSetting;
use Notification;

class UserInfoUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'audit:user-detail-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send user detail audit report to user and HR';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $isNotifyUser = SystemSetting::where('key','UserAuditUserEmailNotify')->first();
        $isNotifyAdmin = SystemSetting::where('key','UserAuditAdminEmailNotify')->first();
        
        $from = Carbon::now()->subMinutes(6)->toDateTimeString();
        $to = Carbon::now()->subMinutes(1)->toDateTimeString();

        $data = UserService::getAudits($from, $to);
        if(count($data) > 0) {
            $date = date_in_view(Carbon::now());
            foreach($data as $userId => $audit){
                $user = User::find($userId);
                $subject = 'User Detail | Update | '.$user->name.' | '. $user->employee_id;
                if($isNotifyUser && $isNotifyUser->value == 1) {
                    \Log::info('User Audit report sent to user : '.$user->email);
                    $user->notify(new AuditNotification($subject, $audit, $user, $date));
                }
                if($isNotifyAdmin && $isNotifyAdmin->value == 1) {
                    $isNotifyAdminEmails = SystemSetting::where('key','UserAuditAdminEmailNotifyEmailAddress')->first();
                    if($isNotifyAdmin && !empty($isNotifyAdminEmails->value)) {
                        \Log::info('User Audit report sent to admin : '.$isNotifyAdminEmails->value);    
                        Notification::route('mail', $isNotifyAdminEmails->value)
                            ->notify(new AuditNotification($subject, $audit, $user, $date));
                    }
                }
            }
        }
    }
}
