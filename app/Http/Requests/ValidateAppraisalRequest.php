<?php

namespace App\Http\Requests;

use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use Illuminate\Foundation\Http\FormRequest;

class ValidateAppraisalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // if(!$this->user()->hasRole('admin')){
        //     return false;
        // }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'user_id' => 'required | exists:users,id',
            'effective_date' => 'required | date',
            'type_id' => 'required | exists:appraisal_types,id',

        ];

        $appraisalBonus = $this->request->get('bonus');
        if ($appraisalBonus && count($appraisalBonus) > 0) {
            foreach ($appraisalBonus as $key => $bonus) {
                if (isset($bonus->value) ? $bonus->value : null) {
                    $rules['bonus.' . $key . '.value'] = 'numeric';
                    $rules['bonus.' . $key . '.value_date'] = 'date';
                }
            }
        }

        return $rules;
    }
    public function messages()
    {
        $messages = [];
        $appraisalComponent = $this->request->get('component');
        if ($appraisalComponent && count($appraisalComponent) > 0) {
            foreach ($appraisalComponent as $key => $val) {
                $appraisalComponent = AppraisalComponentType::find($key);
                if ($appraisalComponent) {
                    $messages['component.' . $key . '.integer'] = 'Value must be integer for component ' . $appraisalComponent->code;
                    //   $messages['component.'.'.exists'] = 'Component doesnot exists';
                }
            }
        }
        $appraisalBonus = $this->request->get('bonus');
        if ($appraisalBonus && count($appraisalBonus) > 0) {
            foreach ($appraisalBonus as $key => $bonus) {
                $appraisalBonusType = AppraisalBonusType::find($key);
                if ($appraisalBonus) {
                    $messages['bonus.' . $key . '.value' . '.integer'] = 'Value must be integer for bonus ' . $appraisalBonusType->description;
                    $messages['bonus.' . $key . '.value_date' . '.date'] = 'Value date must be of type date ' . $appraisalBonusType->description;

                }
            }
        }

        return $messages;
    }
}
