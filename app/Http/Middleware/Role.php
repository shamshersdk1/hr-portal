<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Redirect;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = $this->getRequiredRoleForRoute($request->route());
        if (empty($roles)) {
            return $next($request);
        }
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->hasRole('admin') && $user->role == "admin") {
                return $next($request);
            }
            foreach ($roles as $role) {
                if ($role == 'admin') {
                    continue;
                }
                if ($user->hasRole($role)) {
                    return $next($request);
                }
            }
            if ($user->roles()->count() > 0) {
                return Redirect::to('/dashboard');
            } else {
                return view('pages.404');
            }

        }
        return Redirect::to('/');
    }
    private function getRequiredRoleForRoute($route)
    {
        $actions = $route->getAction();
        return isset($actions['roles']) ? $actions['roles'] : null;
    }
}
