<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateSession
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->user = \Auth::check() ? \Auth::user() : null;

        return $next($request);
    }
}
