<?php

namespace App\Http\Controllers\CalendarLeaveCredit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Leave\LeaveCalendarYear;
use App\Models\DateTime\CalendarYear;
use App\Models\Leave\LeaveType;
use Redirect;

class CalendarLeaveCreditController extends Controller
{
    public function index()
    {
        $calendarLeaveCredits = LeaveCalendarYear::with('leaveType')->with('calendarYear')->get();
        $leaveTypes = LeaveType::all();
        return view('calendar.calendar-leave-credit.index',compact('calendarLeaveCredits','leaveTypes'));
    }

    public function store()
    {   
        $data = request()->all();
        $calendarObj = CalendarYear::orderby('year','desc')->first();
        $leaveCalendarYear = LeaveCalendarYear::create(['calendar_year_id' => $calendarObj->id,'leave_type_id' => $data['leave_type'],'max_allowed_leave' => $data['max_leave']]);
        if($leaveCalendarYear->isInvalid()){
            return redirect::back()->withErrors($leaveCalendarYear->getErrors())->withInput();;
        }
        return redirect()->back()->with('message', 'You has been successfully added!');
    }
}
