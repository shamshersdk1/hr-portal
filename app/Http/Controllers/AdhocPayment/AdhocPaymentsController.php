<?php

namespace App\Http\Controllers\AdhocPayment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\SalaryDependencyNotification\AdhocPaymentNotificationJob;
use App\Models\AdhocPayments\AdhocPaymentComponent;
use App\Models\AdhocPayments\AdhocPayment;
use App\Models\Users\User;
use App\Models\DateTime\Month;
use Redirect;
use App\Models\DateTime\MonthSetting;

class AdhocPaymentsController extends Controller
{

    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('adhoc-payments.adhoc-payments.index', compact('months'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $adhocComponent = AdhocPaymentComponent::find($data['adhoc_payment_component_id']);
        if(!$adhocComponent)
        {
            return redirect()->back()->withErrors("Adhoc Payment Component Type not found")->withInput();
        }
        $adhocPaymentObj = AdhocPayment::create(['adhoc_payment_component_id' => $data['adhoc_payment_component_id'],'month_id' => $data['month_id'],'user_id' =>  $data['user_id'],'amount' =>  $adhocComponent->type=='credit' ? abs($data['amount']) : -abs($data['amount']) ,'comment' =>  $data['comment']]);
        if(!$adhocPaymentObj->isValid())
        {
            return redirect()->back()->withErrors($adhocPaymentObj->getErrors() )->withInput();
        }
        return redirect()->back()->with('message', 'Saved Successfully');
    }

    public function show($id)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $adhocPayments = AdhocPayment::where('month_id',$id)->get();
        $adhocPaymentsComponents = AdhocPaymentComponent::all();
        $users = User::where('is_active',1)->get();
        $month = Month::find($id);
        if (!$adhocPaymentsComponents) {
            return view('adhoc-payments.adhoc-payments.index')->with('message', 'no records found.');
        }
        return view('adhoc-payments.adhoc-payments.show', compact('month','adhocPaymentsComponents','users','adhocPayments','monthList'));
    }

    public function edit($id)
    {
        $adhocPaymentObj = AdhocPayment::find($id);
        if (!$adhocPaymentObj) {
            return redirect()->back()->withErrors("Not Found")->withInput();
        }
        $adhocComponents = AdhocPaymentComponent::all();
        if (!count($adhocComponents) > 0) {
            return redirect()->back()->withErrors("Adhoc Component Not Found")->withInput();
        }
        $users = User::where('is_active',1)->get();
        return view('adhoc-payments.adhoc-payments.edit', compact('users','adhocComponents','adhocPaymentObj'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $adhocComponent = AdhocPaymentComponent::find($data['adhoc_payment_component_id']);
        $adhocPaymentObj = AdhocPayment::updateOrCreate(['id' => $id],['adhoc_payment_component_id' => $data['adhoc_payment_component_id'],'user_id' =>  $data['user_id'],'amount' =>  $adhocComponent->type=='credit' ? abs($data['amount']) : -abs($data['amount']) ,'comment' =>  $data['comment']]);
        if(!$adhocPaymentObj->isValid())
        {
            return redirect()->back()->withErrors($adhocPaymentObj->getErrors() )->withInput();
        }
        return redirect('/adhoc-payments/'.$adhocPaymentObj->month_id)->with('message', 'Updated!');
    }

    public function destroy($id)
    {
        $adhocPaymentObj = AdhocPayment::find($id);
        if($adhocPaymentObj && $adhocPaymentObj->delete())
        {
            return Redirect::back()->withMessage("Record of ".$adhocPaymentObj->user->name." has been deleted.");
        }
        return Redirect::back()->withErrors(["Entry not found!"]);
    }

    public function lockMonth($monthId){
        $response = MonthSetting::monthStatusToggle($monthId,'adhoc-payment');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');
        if(MonthSetting::where('month_id',$monthId)->where('key','adhoc-payment')->first()->value == 'locked')
            dispatch(new AdhocPaymentNotificationJob($monthId));
        return Redirect::back()->withMessage($response['message']);
    }
}
