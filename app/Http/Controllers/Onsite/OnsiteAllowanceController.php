<?php

namespace App\Http\Controllers\Onsite;

use App\Http\Controllers\Controller;
use App\Models\Onsite\OnsiteAllowance;
use App\Models\Project\Project;
use App\Models\Users\User;

class OnsiteAllowanceController extends Controller
{
    public function index()
    {
        $onsiteAllowances = OnsiteAllowance::all();
        return view('onsite.index', compact('onsiteAllowances'));
    }

    public function create()
    {
        $projects = Project::all();
        $users = User::whereIs_active(true)->get();
        return view('onsite.add', compact('projects','users'));
    }

    public function store()
    {
        $onstiteAllowanceObj = OnsiteAllowance::create(request()->all());
        if ($onstiteAllowanceObj->isValid())
            return redirect('onsite-allowance')->withMessage('Successfully Saved!');
        return redirect()->back()->withErrors($onstiteAllowanceObj->getErrors())->withInput();
    }

    public function show($id)
    {
        //
    }

    public function edit(OnsiteAllowance $onsiteAllowance)
    {
        $projects = Project::all();
        $users = User::whereIs_active(true)->get();
        return view('onsite.edit', compact('onsiteAllowance','users','projects'));
    }

    public function update(OnsiteAllowance $onsiteAllowance)
    {
        $onsiteAllowance->update(request()->all());
        if ($onsiteAllowance->isValid())
            return redirect('onsite-allowance')->with('message', 'Update Successful!');
        return redirect()->back()->withErrors($onsiteAllowance->getErrors())->withInput();
    }

    public function destroy(OnsiteAllowance $onsiteAllowance)
    {
        $onsiteAllowance->delete();
        return redirect()->back()->with('message', 'Successfully Deleted');
    }
}
