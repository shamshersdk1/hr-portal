<?php

namespace App\Http\Controllers\WorkingDay;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DateTime\Month;
use App\Models\Users\User;
use Redirect;
use App\Models\DateTime\MonthSetting;
use App\Services\Leave\NewLeaveService;
use App\Models\Salary\UserSalary;
use App\Models\Salary\Salary;
use App\Services\SalaryService\SalaryService;
use App\Services\DateTime\CalendarService;
use App\Jobs\SalaryDependencyNotification\UserWorkingNotificationJob;
use App\Models\Appraisal\Appraisal;

class UserWorkingDayController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('user-working-day.index', compact('months'));
    }

    public function show($monthId)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $month = Month::find($monthId);
        if (!$month) {
            return Redirect::back()->withInput()->withErrors(['month not found']);
        }
        $allUsers=User::orderBy("employee_id")->get();
        $userObjs = UserSalary::with('user')->where('month_id',$monthId)->get();

        $status = false;
        $errors = [];
        if(count($userObjs) > 0)
        {
            foreach($userObjs as $userObj)
            {
                $response = SalaryService::checkBank($userObj->user_id);
                if($response['status'])
                {
                    $status = true;
                    $msg = " ".$userObj->user->name." | ".$userObj->user->employee_id." | Salary Account Detail Missing.";
                    $errors[] = $msg;
                }
                $appraisalObj = Appraisal::where('user_id', $userObj->user_id)->whereDate('effective_date','<=',$month->getLastDay())->first();
                if(!$appraisalObj)
                {
                    $status = true;
                    $msg = " ".$userObj->user->name." | ".$userObj->user->employee_id." | Appraisal Missing.";
                    $errors[] = $msg;
                }
            }
        }
        if($status)
            return view('user-working-day.show', compact('allUsers','month','userObjs','monthList'))->withErrors($errors);

        $startDate = $month->getFirstDay();
        $endDate = $month->getLastDay();
        if(!count($userObjs) > 0)
        {
            $userObjs = User::all();
            foreach($userObjs as $key=>$userObj)
                $userObjs[$key]['working_day'] = NewLeaveService::getUserWorkingDays($startDate,$endDate,$userObj->id);
        }
        return view('user-working-day.show', compact('allUsers','month','userObjs','monthList'));
    }

    public function lockMonth($monthId){
        $response = MonthSetting::monthStatusToggle($monthId,'working-day');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');
        if(MonthSetting::where('month_id',$monthId)->where('key','working-day')->first()->value == 'locked')
            dispatch(new UserWorkingNotificationJob($monthId));
        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();
        $monthObj = Month::find($id);
        $salaryObj = Salary::firstOrCreate(['month_id' => $id,'financial_year_id' => $monthObj->financial_year_id,'working_day' => CalendarService::getWorkingDays($monthObj->month, $monthObj->year)],['status' => 'pending']);
        $errors = [];
        $status = false;
        if(isset($data['options']))
        {
            foreach($data['options'] as $selected)
            {
                $userWorkingObj = UserSalary::create(['salary_id' => $salaryObj->id,'user_id' => $selected,'working_day' => $data['working_days'][$selected] , 'lop'=> $data['lops'][$selected] ?? 0, 'month_id' => $data['month_id'], 'financial_year_id' => $salaryObj->financial_year_id, 'status' => 'pending', 'comment' => $data['comment'][$selected] ?? null]);
                if($userWorkingObj->isInvalid())
                {
                    $status = true;
                    $errors = array_merge($errors,$userWorkingObj->getErrors()->toArray());
                }
            }
        }else
        {
            foreach($data['working_days'] as $key => $selected)
            {
                $userWorkingObj = UserSalary::find($key);
                $userWorkingObj->working_day = $selected;
                if($userWorkingObj->save())
                {
                    $status = true;
                    $errors = array_merge($errors,$userWorkingObj->getErrors()->toArray());
                }
            } 
            foreach($data['lops'] as $key => $selected)
            {
                $userWorkingObj = UserSalary::find($key);
                $userWorkingObj->lop = $selected;
                if($userWorkingObj->save())
                {
                    $status = true;
                    $errors = array_merge($errors,$userWorkingObj->getErrors()->toArray());
                }
            } 
        }
        if($status)
            return redirect()->back()->withErrors($errors);
        return redirect('/user-working-day/'.$data['month_id'])->withMessage("Added");
    }

    public function storeUser()
    {
        $salaryObj = Salary::where('month_id',request('month_id'))->firstOrFail();
        $workingDay = NewLeaveService::getUserWorkingDays($salaryObj->month->getFirstDay(),$salaryObj->month->getLastDay(),request('user_id'));
        $userWorkingObj = UserSalary::create(['salary_id' => $salaryObj->id,'user_id' => request('user_id'),'working_day' => $workingDay , 'lop'=> 0, 'month_id' => request('month_id'), 'financial_year_id' => $salaryObj->financial_year_id, 'status' => 'pending']);
        if($userWorkingObj->isInvalid())
        {
            return redirect()->back()->withErrors($userWorkingObj->getErrors());
        }
        return redirect()->back()->withMessage("Added");
    }

    public function destroy($id)
    {
        $userWorkingObj = UserSalary::find($id);
        if ($userWorkingObj && $userWorkingObj->delete())
            return redirect()->back()->withMessage("deleted successfully");
        return Redirect::back()->withErrors(["Something went wrong!"]);
    }
}
