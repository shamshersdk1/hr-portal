<?php

namespace App\Http\Controllers\ItSaving;

use App\Http\Controllers\Controller;
use App\Models\ItSaving\ItSaving;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use App\Models\ItSaving\ItSavingMonth;
use App\Jobs\PrepareSalary\Regenerate\ItSavingMonthJob;
use Illuminate\Http\Request;
use App\Models\Salary\UserSalary;

class ItSavingController extends Controller
{
    public function monthUserList($month=null)
    {
        $months = Month::orderBy('year','DESC')->orderBy('month','DESC')->get();
        if(!count($months)  > 0)
        {
            return redirect()->back()->withErrors('No Month Found!')->withInput();
        }
        $monthObj = $months[0];
        if($month)
        {
            $monthObj = Month::find($month);
        }
        $financial_year_id = $monthObj->financial_year_id;
        $ItSavingObjMonths = ItSavingMonth::where("month_id",$monthObj->id)->orderBy('updated_at','DESC')->distinct('user_id')->get();
        $ItSavingMonthObj = new ItSavingMonth;
        $columns=$ItSavingMonthObj->getTableColumns();
        return view('it-saving.month-view', compact('ItSavingObjMonths','columns','monthObj','months'));
    }

    public function regenerate(Request $request)
    {
        $monthObj = Month::find($request->month_id);
        $users = UserSalary::where('month_id',$monthObj->id)->pluck('user_id')->toArray();
        $savings = ItSaving::where('financial_year_id', $monthObj->financial_year_id)->whereIn('user_id',$users)->get();
        if($monthObj->vpfSetting?$monthObj->vpfSetting->value != "locked":true)
        {
            return redirect()->back()->withErrors('Vpf deduction Month not locked');
        }

        ItSavingMonth::where('financial_year_id', $monthObj->financial_year_id)->where('month_id', $monthObj->id)->delete();
        foreach ($savings as $saving) {
            dispatch(new ItSavingMonthJob($monthObj->id,$saving->id));
        }
        return redirect('/it-saving-month/'.$monthObj->id);
    }

    public function lockMonth($monthId){
        $response = MonthSetting::monthStatusToggle($monthId,'it-saving-month');
        if(!empty($response['status']) && $response['status'] == false)
            return redirect()->back()->withErrors('Something went wrong!');
        return redirect()->back()->withMessage($response['message']);
    }
}
