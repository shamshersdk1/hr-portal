<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\DateTime\Month;
use App\Models\Transaction;
use App\Models\Users\User;
use App\Services\SalaryService\TransactionService;
use DB;
use Illuminate\Http\Request;
use Redirect;

class TransactionController extends Controller
{
    public function create()
    {
        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();
        $monthList = Month::orderBy('year', 'Desc')->orderBy('month', 'Desc')->get();

        $appraisalComponentsTypes = AppraisalComponentType::where('is_company_expense', 0)->get();
        $appraisalBonuses = AppraisalBonusType::all();

        return view('transaction.add', compact('appraisalComponentsTypes', 'appraisalBonuses', 'users', 'monthList'));
    }

    public function store(Request $request)
    {
        $monthObj = Month::find($request->month_id);

        $data = [];
        $data['month_id'] = $request->month_id;
        $data['financial_year_id'] = $monthObj->financial_year_id;
        $data['user_id'] = $request->user_id;
        $data['reference_id'] = $request->reference_id;
        $data['reference_type'] = $request->reference_type;
        $data['type'] = ($request->amount < 0) ? "debit" : "credit";
        $data['amount'] = $request->amount;

        $response = Transaction::saveTransaction($data);

        if ($response['status'] == true) {
            return redirect('/transaction-summary')->with('message', $response['message']);
        }
        return Redirect::back()->withInput()->withErrors($response['message']);
    }

    public function summary($id = null)
    {
        if ($id != null) {
            $monthObj = Month::find($id);
        } else {
            //here
            $inProgressMonthId = Month::getInProgressMonth();
            if (!$inProgressMonthId) {
                return Redirect::back()->withErrors(['In Progress month id not found!!']);
            }
            $monthObj = Month::find($inProgressMonthId);
            // $monthObj = Month::where('status', 'in_progress')->orderBy('year', 'DESC')->orderBy('month', 'DESC')->first();
        }
        if (!$monthObj) {
            return Redirect::back()->withErrors(['Month not found']);
        }

        $users = User::orderBy('employee_id', 'ASC')->get();
        if (!$users || empty($users)) {
            return Redirect::back()->withErrors(['Users not found']);
        }

        $transUsers = DB::select(DB::raw("SELECT DISTINCT user_id
            from transactions WHERE is_company_expense = 0 order by user_id"));

        $userArray = [];
        foreach ($transUsers as $user) {
            $userArray[$user->user_id] = $user->user_id;
        }

        $data = TransactionService::transactionSummary($monthObj->id);

        $monthArray = Month::orderBy('year', 'Desc')->orderBy('month', 'Desc')->get();

        return view('transaction.summary', compact('users', 'data', 'monthArray', 'monthObj', 'userArray'));
    }
}
