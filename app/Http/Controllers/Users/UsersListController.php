<?php
namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Bank\Account;
use App\Models\Bank\AccountMetaKey;
use App\Models\Bank\AccountMetaValue;
use App\Models\Bank\AccountType;
use App\Models\Users\Designation;
use App\Models\Users\User;
use App\Models\Users\UserDetail;
use DB;
use Exception;
use Illuminate\Http\Request;
use Redirect;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Users\Role as PortalRole;
use App\Models\Users\UserRole;
use App\Events\User\UserUpdated;

class UsersListController extends Controller
{
    use HasRoles;

    public function index()
    {
        $users = User::orderBy('employee_id', 'ASC')->get();
        if (!$users || count($users) == 0) {
            return Redirect::back()->withErrors('No User Found!');
        }
        return view('users.userList', ['usersList' => $users]);
    }

    public function edit($id)
    {
        $user = User::find($id);
        if (!$user) {
            return Redirect::back()->withErrors('User not found!');
        }
        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();
        $designations = Designation::where('status', 1)->get();
        if (count($designations) == 0) {
            // return Redirect::back()->withErrors('Designations Not Found!');
        }
        $accountTypes = AccountType::all();
        // if (!$accountTypes || count($accountTypes) == 0) {
        //     return Redirect::back()->withErrors('No Account Type Found!');
        // }

        return view('users.edit', compact('user', 'accountTypes', 'users', 'designations'));
    }

    public function updateOrCreate(Request $request, $userId, $bankAccountId = null)
    {
        $data = $request->all();
        $user = User::find($userId);

        if (!$user) {
            return Redirect::back()->withErrors('User not found!');
        }

        if (!$data['account_type_id']) {
            return Redirect::back()->withErrors('Account type not found!');
        }

        $accountType = AccountType::find($data['account_type_id']);
        if (!$accountType) {
            return Redirect::back()->withErrors('Account type not found!');
        }

        if (!$data['account']) {
            return Redirect::back()->withErrors('Enter Account number!');
        }

        if (isset($data['account_id'])) {
            $bankAccountId = $data['account_id'];
        }

        if ($bankAccountId) {
            $bankAccountObj = Account::find($bankAccountId);
            if (!$bankAccountObj) {
                return Redirect::back()->withErrors('Account not found!');
            }
        }

        DB::beginTransaction();
        try {
            if ($bankAccountId) {
                $bankAccount = Account::updateOrCreate(['id' => $bankAccountObj->id], ['bank_account_type_id' => $data['account_type_id'], 'account_number' => $data['account'], 'reference_type' => 'App\Models\Users\User', 'reference_id' => $userId]);
            } else {
                $bankAccount = Account::create(['bank_account_type_id' => $data['account_type_id'], 'account_number' => $data['account'], 'reference_type' => 'App\Models\Users\User', 'reference_id' => $userId]);
            }

            if ($bankAccount->isValid()) {
                if (!isset($data['keys'])) {
                    DB::commit();
                } else {
                    $metaStatus = self::saveUserAccountKeys($data['keys'], $bankAccount->id, $data['account_type_id']);
                    if ($metaStatus == true) {
                        DB::commit();
                    } else {
                        throw new Exception("Unable to Save. Please Check Key's values!");
                    }
                }
            } else {
                return redirect()->back()->withErrors($bankAccount->getErrors())->withInput();
            }

        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }

        return redirect('user/' . $userId . '/edit')->withMessage('Account Saved Successfully');
    }

    public function deleteAccount($userId, $bankAccountId)
    {
        $backAccount = Account::find($bankAccountId);
        if ($backAccount && $backAccount->delete()) {
            return redirect('user/' . $userId . '/edit')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Account Not Found!')->withInput();
    }

    public static function saveUserAccountKeys($data, $accountId, $typeId)
    {
        foreach ($data as $key => $values) {
            $keyObj = AccountMetaKey::where('key', $key)->where('bank_account_type_id', $typeId)->first();
            if (!$keyObj) {
                return false;
            }

            $findKeyValue = AccountMetaValue::where('bank_account_id', $accountId)->where('bank_account_meta_key_id', $keyObj->id)->first();
            if ($findKeyValue) {
                $metaObj = AccountMetaValue::updateOrCreate(['id' => $findKeyValue->id], ['bank_account_id' => $accountId, 'bank_account_meta_key_id' => $keyObj->id, 'value' => $values]);
            } else {
                $metaObj = AccountMetaValue::create(['bank_account_id' => $accountId, 'bank_account_meta_key_id' => $keyObj->id, 'value' => $values]);
            }

            if ($metaObj->isInvalid()) {
                return false;
            }

        }
        return true;
    }

    public function showRoles($userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return redirect()->back()->withErrors('User Not Found!')->withInput();
        }

        $roles = Role::all();
        $assignedRoles = $user->roles;
        return view('users.assign-roles', compact('user', 'roles', 'assignedRoles'));
    }

    public function showPermissions($userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return redirect()->back()->withErrors('User Not Found!')->withInput();
        }

        $permissions = Permission::all();
        $assignedPermissions = $user->getAllPermissions();
        return view('users.assign-permissions', compact('user', 'permissions', 'assignedPermissions'));
    }

    public function saveRoles(Request $request, $userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return redirect()->back()->withErrors('User not found!');
        }

        $data = $request->all();
        if (isset($data['roles'])) {
            $user->assignRole($data['roles']);
        } else {
            return redirect()->back()->withErrors('Select atleast one role!');
        }
        return redirect('user/' . $userId . '/assign-roles')->with('message', 'Roles Successfully Added.');
    }

    public function savePermissions(Request $request, $userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return redirect()->back()->withErrors('User not found!');
        }

        $data = $request->all();
        if (isset($data['permissions'])) {
            $user->givePermissionTo($data['permissions']);
        } else {
            return redirect()->back()->withErrors('Select atleast one permission!');
        }
        return redirect('user/' . $userId . '/assign-permissions')->with('message', 'Permissions Successfully Added.');
    }

    public function deleteRole($userId, $roleId)
    {
        $user = User::find($userId);
        $role = Role::find($roleId);
        if ($role && $user) {
            if ($user->removeRole($role)) {
                return redirect('user/' . $userId . '/assign-roles')->with('message', 'Successfully Deleted');
            }
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }

    public function deletePermission($userId, $permissionId)
    {
        $user = User::find($userId);
        $permission = Permission::find($permissionId);
        if ($user && $permission) {
            if ($user->revokePermissionTo($permission)) {
                return redirect('user/' . $userId . '/assign-permissions')->with('message', 'Successfully Deleted');
            }
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }

    public function update(Request $request, $userId)
    {
        $data = $request->all();
        $user = User::find($userId);
        if (!$user) {
            return redirect()->back()->withErrors('User not found')->withInput();
        }
        $obj = User::updateOrCreate(['id' => $user->id], $data);
        $objDetail = UserDetail::updateOrCreate(['user_id' => $user->id], ['aadhar_no' => $data['aadhar_no']]);
        if ($obj->isValid() && $objDetail->isValid()) {
            return redirect()->back()->withMessage('Updated');
        }

        return redirect()->back()->withErrors($obj->getErrors() . $objDetail->getErrors())->withInput();
    }
    public function activeToggle($userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return redirect()->back()->withErrors('User not found')->withInput();
        }
        $user->is_active = $user->is_active ? 0 : 1;
        if ($user->save()) {
            return redirect()->back()->withMessage('Done');
        }

        return redirect()->back()->withErrors("Something Went Wrong")->withInput();
    }

    public function create()
    {
        $users = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();
        $designations = Designation::where('status', 1)->get();
        if (count($designations) == 0) {
            return Redirect::back()->withErrors('Designations Not Found!');
        }
        return view('users.add', compact('users', 'designations'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $obj = User::Create(['name' => $data['name'], 'first_name' => $data['first_name'], 'email' => $data['email'],
            'middle_name' => $data['middle_name'], 'last_name' => $data['last_name'], 'joining_date' => $data['joining_date'], 'confirmation_date' => $data['confirmation_date'],
            'employee_id' => $data['employee_id'], 'dob' => $data['dob'], 'parent_id' => $data['parent_id'], 'months' => $data['months'], 'years' => $data['years'], 'designation' => $data['designation'], 'gender' => $data['gender'], 'role' => 'user']);
        if ($obj->isInvalid()) {
            return redirect()->back()->withErrors($obj->getErrors())->withInput();
        }    
        event(new UserUpdated($obj->id));
        $userRole = PortalRole::where('code','user')->first();
        if($userRole) {
            $role = UserRole::Create(['user_id' => $obj->id,'role_id' => $userRole->id]);
            if($role->isInvalid())
                return redirect()->back()->withErrors($role->getErrors())->withInput();
        }
        if (isset($data['aadhar_no'])) {
            $objDetail = UserDetail::Create(['user_id' => $obj->id, 'aadhar_no' => $data['aadhar_no']]);
            if ($objDetail->isInvalid()) {
                return redirect()->back()->withErrors($objDetail->getErrors())->withInput();
            }
        }
        return redirect('user/'.$obj->id.'/edit')->withMessage('#'.$obj->id.' Employee "'.$obj->name . ' | '.  $obj->employee_id . '" added sucessfully.');
    }

}
