<?php
namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index()
    {
        $permissions = Permission::all();
        return view('permission.index', compact('permissions'));
    }

    public function create()
    {
        return view('permission.add');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $permission = Permission::create(['name' => $data['permission_name']]);
        if ($permission) {
            return redirect('/permission')->with('message', 'Successfully Added');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }

    public function showRoles($id)
    {
        $permission = Permission::find($id);
        if (!$permission) {
            return redirect()->back()->withErrors('Permission not found!');
        }
        $roles = Role::all();

        $assignedRoles = $permission->roles;
        return view('permission.assign', compact('permission', 'roles', 'assignedRoles'));
    }

    public function assignRoles(Request $request, $id)
    {
        $permission = Permission::find($id);
        if (!$permission) {
            return redirect()->back()->withErrors('Permission not found!');
        }

        $data = $request->all();
        if (isset($data['roles'])) {
            foreach ($data['roles'] as $role) {
                $roleObj = Role::find($role);
                if ($roleObj) {
                    $permission->assignRole($roleObj);
                } else {
                    return redirect()->back()->withErrors('Something went wrong with role id -> ' . $role);
                }
            }
        } else {
            return redirect()->back()->withErrors('Select atleast one role!');
        }
        return redirect('permission/' . $id . '/roles')->with('message', 'Roles Successfully Added.');
    }

    public function edit($id)
    {
        dd('edit');
    }

    public function delete($id)
    {
        $permission = Permission::find($id);
        if ($permission && $permission->delete()) {
            return redirect('/permission')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }

    public function deleteRole($id, $roleId)
    {
        $permission = Permission::find($id);
        $role = Role::find($roleId);
        if ($permission && $role) {
            if ($permission->removeRole($role)) {
                return redirect('permission/' . $id . '/roles')->with('message', 'Successfully Deleted');
            }
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
