<?php
namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('role.index', compact('roles'));
    }

    public function create()
    {
        return view('role.add');
    }

    public function showPermissions($id)
    {
        $role = Role::find($id);
        if (!$role) {
            return redirect()->back()->withErrors('Role not found!');
        }
        $permissions = Permission::all();

        $assignedPermissions = $role->permissions;
        return view('role.assign', compact('role', 'permissions', 'assignedPermissions'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $role = Role::create(['name' => $data['role_name']]);
        if ($role) {
            return redirect('/role')->with('message', 'Successfully Added');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }

    public function assignPermissions(Request $request, $id)
    {
        $role = Role::find($id);
        if (!$role) {
            return redirect()->back()->withErrors('Role not found!');
        }

        $data = $request->all();
        if (isset($data['permissions'])) {
            foreach ($data['permissions'] as $permission) {
                $permissionObj = Permission::find($permission);
                if ($permissionObj) {
                    $role->givePermissionTo($permissionObj);
                } else {
                    return redirect()->back()->withErrors('Something went wrong with Permission id -> ' . $permission);
                }
            }
        } else {
            return redirect()->back()->withErrors('Select atleast one permission!');
        }
        return redirect('role/' . $id . '/permissions')->with('message', 'Permissions Successfully Added.');
    }

    public function edit($id)
    {
        dd('edit');
    }

    public function delete($id)
    {
        $role = Role::find($id);
        if ($role && $role->delete()) {
            return redirect('/role')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }

    public function deletePermission($id, $permissionId)
    {
        $role = Role::find($id);
        $permission = Permission::find($permissionId);
        if ($role && $permission) {
            if ($role->revokePermissionTo($permission)) {
                return redirect('role/' . $id . '/permissions')->with('message', 'Successfully Deleted');
            }
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
