<?php

namespace App\Http\Controllers\Loan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loan\Loan;
use App\Models\Loan\LoanEmi;
use App\Models\File\File;
use App\Services\File\FileService;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Users\User;
use App\Models\Loan\LoanRequest;
use App\Services\Loan\LoanService;
use App\Models\DateTime\Month;
use Redirect;
class LoanController extends Controller
{
    public function index()
    {
        $loans = Loan::get();
        return view('loan.index', compact('loans'));
    }

    public function show($id)
    {
        $loan = Loan::with('loan_transactions', 'loan_request')->find($id);
        if ($loan) {
            $file = null;
            if ($loan->loan_request) {
                $file = File::where("reference_type", "App\Models\Admin\LoanRequest")->where("reference_id", $loan->loan_request->id)->first();
            }

            $loanCompleteFile = File::where("reference_type", "App\Models\Admin\Loan")->where("reference_id", $loan->id)->first();
            $loanTransactions = LoanEmi::where('loan_id', $id)->orderBy('created_at', 'DESC')->get();
            //$loanInterests = LoanInterest::where('loan_id', $id)->orderBy('created_at', 'ASC')->get();
            $remainingAmount = $loan->amount - abs($loan->emis()->where('status','paid')->sum('amount'));
            return view('loan.show', compact('remainingAmount','loan', 'loanTransactions', 'file','loanCompleteFile'));
        } else {
            return redirect()->back()->withErrors(['Invalid Link followed']);
        }
    }

    public function create()
    {
        $loan_types = AppraisalBonusType::where('is_loanable', 1)->get();
        if (!count($loan_types) > 0) {
            return redirect()->back()->withInput()->withErrors("Loan Types not found");
        }
        $users = User::where('is_active', 1)->get();
        return view('loan.add', compact('users', 'loan_types'));
    }

    public function completeLoan($id, Request $request)
    {
        if(!$request->file)
        {
            return redirect()->back()->withErrors("Please upload file first.");
        }
        $loan = Loan::find($id);
        if($loan)
        {
            $remainingAmount = ($loan->emis()->where('status','paid')->sum('amount')) + ($loan->amount);
            if($remainingAmount > 0)
            {
                return redirect()->back()->withErrors("Loan has remaining amount.");
            }
            $response = FileService::uploadFile($request->file,$id,'App\Models\Admin\Loan');
            if(!$response['status'])
            {
               return redirect()->back()->withErrors($response['message']);
            }
            $loan->status = "completed";
            if(!$loan->update())
            {
                return redirect()->back()->withErrors("Something went wrong!");
            }
        }
        return redirect()->back()->withMessage("Loan Completed.");
    }

    public function store(Request $request)
    {
        $response['status'] = false;
        $input = $request->all();

        if (!$input) {
            return redirect("loans")->withErrors('Empty Input');
        }
        $role = isset($input['from_admin']) ? 'Admin' : 'User';
        $response = LoanService::loanEligibleCheck($input['user_id'], $input['annual_bonus_type_id'], $input['amount']);
        if ($response['status']) {
            return redirect()->back()->withInput()->withErrors($response['error']);
        }
        $emi = $input['emi'];
        if ($response['appraisal_bonus_id'] != null) {
            $emi = $input['amount'];
        }
        $loanRequest = LoanRequest::create(['user_id' => $input['user_id'],'approved_amount' => $input['amount'], 'amount' => $input['amount'], 'appraisal_bonus_id' => $response['appraisal_bonus_id'] ? $response['appraisal_bonus_id'] : null, 'description' => $input['description'], 'emi' => $emi, 'emi_start_date' => $input['emi_start_date'], 'application_date' => $input['application_date']]);
        if (!$loanRequest->validate($input)) {
            return redirect()->back()->withInput()->withErrors($loanRequest->getError());
        }
        //event(new LoanRequested($loanRequest->id, 'approved', $role, $loanRequest->description));
        return redirect("loans")->with('message', 'Saved Successfully');

    }
    
    public function loanRequest()
    {
        $loans = LoanRequest::with('user:name,id')->get();
        return view('loan.request', compact('loans'));
    }

    public function customPayment($id)
    {
        $loan = Loan::with('loan_transactions', 'loan_request')->find($id);
        if (!$loan) {
            return redirect()->back()->withErrors(['Invalid Link followed']);
        }
        $loanTransactions = LoanEmi::where('loan_id', $id)->orderBy('created_at', 'DESC')->get();
        $remainingAmount =  Loan::outstandingAmount($id);
        return View('loan.custom-payment', compact('loan','remainingAmount','loanTransactions'));
        
    }
    public function customPaymentStore(Request $request)
    {
        $loan = Loan::find($request['loan_id']);
        if(!$loan){
            return redirect()->back()->withErrors(['Invalid Link followed']);
        }
        $month_id = Month::getMonth(date('Y-m-d'));
        $remainingAmount = Loan::outstandingAmount($request['loan_id']);
        $loanEmiData = [
            'loan_id' => $loan->id,
            'user_id' => $loan->user_id,
            'month_id' => $month_id??'',
            'amount' => $request['amount'] ? (-abs($request['amount'])??'') : null,
            'actual_amount' => $loan->amount,
            'balance' => $remainingAmount-abs($request['amount']),
            'appraisal_bonus_id' => $loan->appraisal_bonus_id,
            'status' => 'paid',
            'transaction_id' => -1,
            'paid_on' => $request['date'],
            'comment' => $request['comment']??null,
            'is_manual_payment' => 1,
        ];
        

        if($request['amount']>$remainingAmount){
            return redirect::back()->withErrors("Amount cannot be greater than outstanding amount")->withInput();;
        }
        if($request['amount'] < 0){
            return redirect::back()->withErrors("Amount cannot be less than zero ")->withInput();;
        }
        $loanEmi = LoanEmi::create($loanEmiData);

        if($loanEmi->isInvalid()){
            return redirect::back()->withErrors($loanEmi->getErrors())->withInput();;
        }
        return redirect()->back()->with('message', 'Record created successfully');
    }
}
