<?php

namespace App\Http\Controllers\SystemSetting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SystemSetting\SystemSetting;

class SystemSettingController extends Controller
{
    public function index()
    {
        $systemSettings = SystemSetting::all();
        return view('system-setting.index', compact('systemSettings'));
    }

    public function store()
    {
        $systemSetting = SystemSetting::create(request()->all());
        if ($systemSetting->isValid())
            return redirect()->back()->withMessage('Successfully Saved!');
        return redirect()->back()->withErrors($systemSetting->getErrors())->withInput();
    }

    public function edit(SystemSetting $systemSetting)
    {
        return view('system-setting.edit', compact('systemSetting'));
    }

    public function update(SystemSetting $systemSetting)
    {
        $systemSetting->update(request()->all());
        if ($systemSetting->isValid())
            return redirect('system-setting')->with('message', 'Update Successful!');
        return redirect()->back()->withErrors($systemSetting->getErrors())->withInput();
    }

    public function destroy(SystemSetting $systemSetting)
    {
        $systemSetting->delete();
        return redirect()->back()->with('message', 'Successfully Deleted');
    }
}
