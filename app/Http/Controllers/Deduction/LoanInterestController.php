<?php

namespace App\Http\Controllers\Deduction;

use App\Http\Controllers\Controller;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use App\Models\Loan\LoanInterest;
use Illuminate\Http\Request;
use Redirect;

class LoanInterestController extends Controller
{
    public function show($monthId)
    {
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            return Redirect::back()->withErrors('Month not found!');
        }

        $loanInterests = LoanInterest::where('month_id', $monthId)->get();

        return view('deduction.loan-interest-income.show', compact('monthObj', 'loanInterests'));
    }

    public function regenerate(Request $request)
    {
        $response = LoanInterest::regenerateInterest($request->month_id);
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        return redirect('/deduction/loan-interest-income/' . $request->month_id)->with('message', $response['message']);
    }

    public function monthStatusDeduction($monthId)
    {
        $response = MonthSetting::monthStatusToggle($monthId, 'loan-interest-income');
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request, $monthId)
    {
        $amount = $request->interest;

        if (!empty($amount)) {
            $response = LoanInterest::updateData($amount, $monthId);
            if (!empty($response['status']) && $response['status'] == false) {
                return Redirect::back()->withErrors('Something went wrong!');
            }

            return redirect('/deduction/loan-interest-income/' . $monthId);
        }
    }
}
