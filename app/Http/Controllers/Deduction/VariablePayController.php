<?php

namespace App\Http\Controllers\Deduction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\SalaryDependencyNotification\VariablePayNotificationJob;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use Redirect;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\VariablePay\VariablePay;
use App\Models\VariablePay\VariablePayComponent;
use App\Models\Salary\UserSalary;


class VariablePayController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('deduction.variable-pay.index', compact('months'));
    }

    public function show($id)
    {
        $month = Month::find($id);
        $lastMonth = Month::where('month',$month->month - 1)->first();
        if (!$month) {
            return Redirect::back()->withInput()->withErrors(['month not found']);
        }
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $appraisalBonusTypes = AppraisalBonusType::all();
        if(!count($appraisalBonusTypes)>0)
        {
             return Redirect::back()->withInput()->withErrors(['Appraisal Bonus Types not found']);
        }
        $lastMonthPays = VariablePay::where('month_id',$id)->get();
        return view('deduction.variable-pay.show', compact('appraisalBonusTypes','month', 'lastMonthPays','lastMonth','monthList'));
    }

    public function destroy($monthId,$variablePayId)
    {
        $variable_bonus_obj = VariablePay::find($variablePayId);
        if($variable_bonus_obj && $variable_bonus_obj->delete())
        {
            return redirect('/deduction/variable-pay/'.$monthId)->withErrors("Record of ".$variable_bonus_obj->user->name." has been deleted.");
        }
         return redirect('/deduction/variable-pay/'.$monthId)->withErrors(["Entry not found!"]);
    }

    public function lockMonth($monthId){
        $response = MonthSetting::monthStatusToggle($monthId,'variable-pay');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');
        if(MonthSetting::where('month_id',$monthId)->where('key','variable-pay')->first()->value == 'locked')
            dispatch(new VariablePayNotificationJob($monthId));
        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request,$monthId){

        $status = false;
        $data = $request->new_amount;
        $response=[];
        if(!empty($data)){
            foreach($data as $index=>$data2){
                foreach($data2 as $index2=>$amount){
                    $deductionObj = VariablePayComponent::find($index);
                    if($deductionObj){
                        if(!$amount || $amount==0){
                            $deductionObj->delete();
                        }
                        else{
                        $deductionObj->value = $amount;
                            if(!$deductionObj->update()){
                                $status = true;
                                $errors[] = $deductionObj->getErrors();
                            }
                        }
                    }
                }
            }
        }
        if($status)
            return Redirect::back()->withErrors($errors);
        return redirect('/deduction/variable-pay/'.$monthId);
    }

    public function regenerate(Request $request)
    {

        $status = false;
        $errors = '';
        $month = Month::find($request->month_id);
        $lastMonth = Month::getLastMonth($month->id);
        if(!$lastMonth) {
            $response['message'] = "Invalid month id ".$month->id;
            return Redirect::back()->withErrors($response['message']);
        }
        if($month->variablePaySetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        VariablePay::where('month_id', $month->id)->get()->each(function ($lastMonthPay) {
            $lastMonthPay->delete();
        });

        $users = UserSalary::where('month_id',$month->id)->pluck('user_id')->toArray();
        $object_a = Appraisal::orderBy('user_id')->orderBy('effective_date','desc')->whereDate('effective_date', '>=',$lastMonth->getFirstDay())->whereDate('effective_date', '<=',$lastMonth->getLastDay())->whereIn('user_id',$users)->get();
        $appraisals_data = Appraisal::orderBy('user_id')->orderBy('effective_date','desc')->whereDate('effective_date', '>=',$lastMonth->getFirstDay())->whereDate('effective_date', '<=',$lastMonth->getLastDay())->whereIn('user_id',$users)->get()->pluck('user_id');
        $object_b = Appraisal::orderBy('user_id')->orderBy('effective_date','desc')->whereDate('effective_date', '<=',$lastMonth->getFirstDay())->whereNotIn('user_id',$appraisals_data)->whereIn('user_id',$users)->get()->unique('user_id');
        $appraisals = $object_b->merge($object_a);

        if(is_array($appraisals) && !count($appraisals) > 0)
        {
            return Redirect::back()->withErrors("No appraisal found");
        }
        if($appraisals){
            foreach($appraisals as $appraisal)
            {
                if($appraisal->appraisalBonus)
                {
                    foreach($appraisal->appraisalBonus as $bonus)
                    {
                        if($bonus->value_date == null && ($bonus->value != 0))
                        {
                            $lastmonthPay = VariablePay::firstOrCreate(['user_id' => $appraisal->user_id,'month_id' => $month->id, 'appraisal_id' => $appraisal->id]);
                            if(!$lastmonthPay->isValid())
                            {
                                $status = true;
                                $errors = $errors.$lastmonthPay->getErrors();
                            }
                            $lastMonthPayComponent = VariablePayComponent::create(['variable_pay_id' => $lastmonthPay->id,'key' => $bonus->appraisalBonusType->code,'value'=> round($bonus->value/12,2), 'appraisal_bonus_id' => $bonus->id]);
                            if(!$lastMonthPayComponent->isValid())
                            {
                                $status = true;
                                $errors = $errors.$lastMonthPayComponent->getErrors();
                            }
                        }
                    }
                }
            }
        }
        if($status)
        {
            return Redirect::back()->withErrors($errors);
        }
        $lastMonthPays = VariablePay::where('month_id',$month->id)->get();
        $appraisalBonusTypes = AppraisalBonusType::all();
        if(!count($appraisalBonusTypes)>0)
        {
             return Redirect::back()->withInput()->withErrors(['Appraisal Bonus Types not found']);
        }
        return redirect()->back()->withMessage("Regenerated");
    }
}
