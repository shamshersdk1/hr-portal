<?php

namespace App\Http\Controllers\Deduction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use Redirect;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\FixedBonus\FixedBonus;
use App\Models\FixedBonus\FixedBonusComponent;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Salary\UserSalary;
use App\Jobs\SalaryDependencyNotification\FixedBonusNotificationJob;

class FixedBonusController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months)>0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('deduction.fixed-bonus.index', compact('months'));
    }

    public function show($id)
    {
        $month = Month::find($id);
        if (!$month) {
            return Redirect::back()->withInput()->withErrors(['month not found']);
        }
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $appraisalBonusTypes = AppraisalBonusType::all();
        if(!count($appraisalBonusTypes)>0)
        {
             return Redirect::back()->withInput()->withErrors(['Appraisal Bonus Types not found']);
        }
        $monthlyDeductions = FixedBonus::where('month_id',$id)->get();
        return view('deduction.fixed-bonus.show', compact('appraisalBonusTypes','month', 'monthlyDeductions','monthList'));
    }

    public function destroy($id)
    {
       $fixed_bonus_obj = FixedBonus::find($id);
       if($fixed_bonus_obj && $fixed_bonus_obj->delete())
       {
            return redirect('/deduction/fixed-bonus/')->withErrors("Record of ".$fixed_bonus_obj->user->name." has been deleted.");
       }
        return redirect('/deduction/fixed-bonus/')->withErrors(["Entry not found!"]);
    }

    public function lockMonth($monthId){
        $response = MonthSetting::monthStatusToggle($monthId,'fixed-bonus');
        if(!empty($response['status']) && $response['status'] == false)
            return Redirect::back()->withErrors('Something went wrong!');
        if(MonthSetting::where('month_id',$monthId)->where('key','fixed-bonus')->first()->value == 'locked')
            dispatch(new FixedBonusNotificationJob($monthId));
        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request,$monthId){

        $status = false;
        $data = $request->new_amount;
        $response=[];
        if(!empty($data)){
            foreach($data as $index=>$data2){
                foreach($data2 as $index2=>$amount){
                    $deductionObj = FixedBonusComponent::find($index);
                    if($deductionObj){
                        if(!$amount || $amount==0){
                            $deductionObj->delete();
                        }
                        else{
                        $deductionObj->value = $amount;
                            if(!$deductionObj->update()){
                                $status = true;
                                $errors[] = $deductionObj->getErrors();
                            }
                        }
                    }
                }
            }
        }
        if($status)
            return Redirect::back()->withErrors($errors);
        return redirect('/deduction/fixed-bonus/'.$monthId);
    }

    public function regenerate(Request $request)
    {
        $status = false;
        $errors = '';
        $month = Month::find($request->month_id);
        if(!$month) {
            $response['message'] = "Invalid month id ".$request->month_id;
            return $response;
        }
        if($month->FixedBonusSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        FixedBonus::where('month_id', $month->id)->get()->each(function ($monthylDeductionObj) {
            $monthylDeductionObj->delete();
        });
        $users = UserSalary::where('month_id',$month->id)->pluck('user_id')->toArray();
        $appraisals = Appraisal::whereIn('user_id',$users)->whereHas('appraisalBonus', function (Builder $query) use ($month) {
            $query->whereDate('value_date', '>=',$month->getFirstDay())->whereDate('value_date', '<=',$month->getLastDay());
        })->get();
        if(!count($appraisals) > 0)
        {
            return Redirect::back()->withErrors("No appraisal found");
        }
        foreach($appraisals as $appraisal)
        {
            $fixedBonusObj = FixedBonus::create(['user_id' => $appraisal->user_id,'month_id' => $month->id,'appraisal_id' => $appraisal->id]);
            if(!$fixedBonusObj->isValid())
            {
                return Redirect::back()->withErrors($fixedBonusObj->getErrors());
            }
            if($appraisal->appraisalBonus)
            {
                foreach($appraisal->appraisalBonus as $bonus)
                {

                    if(Month::getMonth($bonus->value_date) == $month->id)
                    {
                        $monthylDeductionComponent = FixedBonusComponent::create(['fixed_bonus_id' => $fixedBonusObj->id,'key' => $bonus->appraisalBonusType->code,'value'=> $bonus->value,'appraisal_bonus_id' => $bonus->id]);
                        if(!$monthylDeductionComponent->isValid())
                        {
                            $status = true;
                            $errors = $errors.$monthylDeductionComponent->getErrors();
                        }
                    }
                }
            }
        }
        if($status)
        {
            return Redirect::back()->withErrors($errors);
        }
        $monthlyDeductions = FixedBonus::where('month_id',$month->id)->get();
        $appraisalBonusTypes = AppraisalBonusType::all();
        if(!count($appraisalBonusTypes)>0)
        {
             return Redirect::back()->withInput()->withErrors(['Appraisal Bonus Types not found']);
        }
        return redirect()->back()->withMessage("Regenerated");
    }
}
