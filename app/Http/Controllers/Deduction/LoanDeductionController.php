<?php

namespace App\Http\Controllers\Deduction;

use App\Http\Controllers\Controller;
use App\Jobs\SalaryDependencyNotification\LoanDeductionNotificationJob;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use App\Models\Loan\LoanEmi;
use Illuminate\Http\Request;
use Redirect;
use App\Notifications\SalaryDependencyNotification;
use App\Models\SystemSetting\SystemSetting;
use Notification;


class LoanDeductionController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        return view('deduction.loan-deduction.index', compact('months'));
    }
    public function show($monthId)
    {
        $month = Month::find($monthId);
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $loanDeductions = LoanEmi::where('month_id', $monthId)->where('is_manual_payment','0')->get();
        return view('deduction.loan-deduction.emi', compact('month', 'loanDeductions', 'monthId','monthList'));
    }

    public function monthStatusDeduction($monthId)
    {
        $response = MonthSetting::monthStatusToggle($monthId, 'loan-deduction');
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }
        if(MonthSetting::where('month_id',$monthId)->where('key','loan-deduction')->first()->value == 'locked')
            dispatch(new LoanDeductionNotificationJob($monthId));
        return Redirect::back()->withMessage($response['message']);
    }

    public function regenerate(Request $request)
    {
        $month = Month::find($request->month_id);
        if(!$month) {
            $response['message'] = "Invalid month id ".$request->month_id;
            return $response;
        }
        $response = LoanEmi::regenerateEmi($month->id);
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }
        return redirect('/deduction/loan-deduction/' . $month->id)->with('message', $response['message']);
    }

    public function update(Request $request, $monthId)
    {
        $data = $request->new_amount;
        if (!empty($data)) {
            $response = LoanEmi::updateData($data, $monthId);
            if (!empty($response['status']) && $response['status']) {
                return Redirect::back()->withErrors($response['message']);
            }
            return redirect('/deduction/loan-deduction/' . $monthId);
        }
    }

    public function destroy($loan_deduct_id)
    {
        $record = LoanEmi::find($loan_deduct_id);
        if ($record && $record->delete()) {
            return redirect()->back()->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors("Something Went Wrong!");
    }
}
