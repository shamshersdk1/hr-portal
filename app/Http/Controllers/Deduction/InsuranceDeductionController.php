<?php

namespace App\Http\Controllers\Deduction;

use App\Http\Controllers\Controller;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use App\Models\Deduction\InsuranceDeduction;
use Redirect;
use Illuminate\Http\Request;

class InsuranceDeductionController extends Controller
{
    /**
     * Display a listing of the employee insurance deduction monthly.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $months = Month::getMonthList();
        return view('deduction/insurance-deduction/months', compact('months'));
    }

    public function show($monthId)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $insuranceDeductions = InsuranceDeduction::where('month_id', $monthId)->orderBy('user_id', 'desc')->get();
        $month = Month::find($monthId);
        return view('deduction/insurance-deduction/show', compact('month', 'insuranceDeductions','monthList'));
    }

    public function monthStatusDeduction($monthId)
    {
        $response = MonthSetting::monthStatusToggle($monthId, 'insurance-deduction');
        if (!empty($response['status']) && $response['status'] == false) {
            return redirect()->back()->withErrors('Something went wrong!');
        }
        return redirect()->back()->withMessage($response['message']);
    }

    public function regenerate(Request $request)
    {
        $response = InsuranceDeduction::regenerateInsurance($request->month_id);
        if (!empty($response['status']) && $response['status'] == false) {
            return redirect('/deduction/insurance-deduction/' . $request->month_id)->withErrors('Something went wrong!');
        }
        return redirect('/deduction/insurance-deduction/' . $request->month_id)->withMessage($response['message']);
    }

    public function destroy($id)
    {
        $record = InsuranceDeduction::find($id);
        if ($record && $record->delete()) {
            return redirect()->back()->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors("Something Went Wrong!");
    }
}
