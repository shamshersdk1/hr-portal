<?php

namespace App\Http\Controllers\Deduction;

use App\Http\Controllers\Controller;
use App\Jobs\PrepareSalary\Components\FoodDeductionJob;
use App\Jobs\SalaryDependencyNotification\FoodDeductionNotificationJob;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use App\Models\Deduction\FoodDeduction;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Redirect;
use App\Models\Salary\UserSalary;
use Notification;
use App\Notifications\SalaryDependencyNotification;
use App\Models\SystemSetting\SystemSetting;


class FoodDeductionController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months) > 0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('deduction.food-deduction.index', compact('months'));
    }

    public function show($id)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        $month = Month::find($id);
        if (!$month) {
            return Redirect::back()->withInput()->withErrors(['Month not found']);
        }

        $users = UserSalary::where('month_id',$month->id)->pluck('user_id')->toArray();

        if (!count($users) > 0) {
            return Redirect::back()->withInput()->withErrors(['No User found']);
        }
        $foodDeductionObjs = FoodDeduction::where('month_id', $id)->get();
        if (!count($foodDeductionObjs) > 0) {
            foreach ($users as $user) {
                $foodDeductionObj = FoodDeduction::create(['user_id' => $user, 'month_id' => $id, 'amount' => -1500, 'actual_amount' => -1500]);
                if (!$foodDeductionObj->isValid()) {
                    return Redirect::back()->withInput()->withErrors($foodDeductionObj->getErrors());
                }
            }
        }
        $foodDeductionObjs = FoodDeduction::where('month_id', $id)->get();
        return view('deduction.food-deduction.show', compact('month', 'users', 'foodDeductionObjs','monthList'));
    }

    public function lockMonth($monthId)
    {
        $response = MonthSetting::monthStatusToggle($monthId, 'monthly-food-deduction');
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }
        if(MonthSetting::where('month_id',$monthId)->where('key','monthly-food-deduction')->first()->value == 'locked')
            dispatch(new FoodDeductionNotificationJob($monthId));
        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request, $monthId)
    {
        $status = false;
        $data = $request->new_amount;
        $errors = "";
        if (!empty($data)) {
            foreach ($data as $index => $loan) {
                $deductionObj = FoodDeduction::find($index);
                $deductionObj->amount = $data[$index];
                if (!$deductionObj->save()) {
                    $status = true;
                    $errors = $deductionObj->getErrors();
                }
            }
        }
        if ($status) {
            return Redirect::back()->withErrors($errors);
        }
        return redirect('/deduction/food-deduction/' . $monthId);
    }

    public function destroy($id)
    {

        $foodDeductionObj = FoodDeduction::find($id);

        if ($foodDeductionObj) {
            if ($foodDeductionObj->delete()) {
                return redirect('/deduction/food-deduction/')->withMessage("deleted successfully");
            } else {
                return Redirect::back()->withErrors(["Something went wrong!"]);
            }

        }
        return Redirect::back()->withErrors(["Entry not found!"]);
    }

    public function regenerate(Request $request)
    {
        $month = Month::find($request->month_id);
        if (!$month) {
            $response['message'] = "Invalid month id " . $request->month_id;
            return $response;
        }
        if ($month->monthlyVariableBonusSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        $users = UserSalary::where('month_id',$month->id)->pluck('user_id')->toArray();
        if (!count($users) > 0) {
            return Redirect::back()->withInput()->withErrors(['No User found']);
        }
        FoodDeduction::where('month_id', $month->id)->delete();
        foreach ($users as $user) {
            $foodDeductionObj = FoodDeduction::create(['user_id' => $user, 'month_id' => $month->id, 'amount' => -1500, 'actual_amount' => -1500]);
            if (!$foodDeductionObj->isValid()) {
                return Redirect::back()->withInput()->withErrors($foodDeductionObj->getErrors());
            }
            $foodDeductionObj->save();
        }
        $foodDeductionObjs = FoodDeduction::where('month_id', $month->id)->get();
        return redirect('/deduction/food-deduction/' . $month->id)->withMessage("Regenerated");
    }
}
