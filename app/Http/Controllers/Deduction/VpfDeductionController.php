<?php

namespace App\Http\Controllers\Deduction;

use App\Http\Controllers\Controller;
use App\Jobs\SalaryDependencyNotification\VpfDeductionNotificationJob;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use App\Models\Deduction\VpfDeduction;
use Illuminate\Http\Request;
use Redirect;
use Notification;
use App\Notifications\SalaryDependencyNotification;
use App\Models\SystemSetting\SystemSetting;

class VpfDeductionController extends Controller
{
    public function deleteRecord($vpf_id)
    {
        $vpf = VpfDeduction::find($vpf_id);
        if ($vpf) {
            if ($vpf->delete()) {
                return Redirect::back()->with('message', 'Successfully Deleted');
            } else {
                return Redirect::back()->withErrors(["Something went wrong!"]);
            }
        } else {
            return Redirect::back()->withErrors(["Entry not found!"]);
        }

    }

    public function getVpfDeductions()
    {
        $months = Month::orderBy('month', 'DESC')->orderBy('year', 'DESC')->get();
        return View('deduction/vpf-deduction/index', compact('months'));
    }

    public function viewDeductions($monthId)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();

        $vpfDeductions = VpfDeduction::where('month_id', $monthId)->get();
        $month = Month::find($monthId);

        return View('deduction/vpf-deduction/show', compact('month', 'vpfDeductions','monthList'));
    }

    public function storeData(Request $request)
    {
        $response = VpfDeduction::store($request, $request->month_id);
        if ($response) {
            return Redirect::back()->with('message', 'Successfully Added');
        } else {
            return Redirect::back()->withErrors('Something went wrong!');
        }

    }

    public function regenerate(Request $request)
    {
        $response = VpfDeduction::regenerateVpf($request->month_id);
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        return redirect('/deduction/vpf-deduction/' . $request->month_id);
    }
    public function monthStatus($monthId)
    {
        $response = MonthSetting::monthStatusToggle($monthId, 'vpf-deduction');
        if(MonthSetting::where('month_id',$monthId)->where('key','vpf-deduction')->first()->value == 'locked')
            dispatch(new VpfDeductionNotificationJob($monthId));
        return Redirect::back()->with('message', $response['message']);
    }
}
