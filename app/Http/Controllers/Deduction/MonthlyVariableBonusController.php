<?php

namespace App\Http\Controllers\Deduction;

use App\Http\Controllers\Controller;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonus;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use App\Models\Deduction\MonthlyVariableBonus;
use Illuminate\Http\Request;
use Redirect;

class MonthlyVariableBonusController extends Controller
{
    public function index()
    {
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();
        if (!count($months) > 0) {
            return Redirect::back()->withInput()->withErrors(['no month found']);
        }
        return view('deduction.monthly-variable-bonus.index', compact('months'));
    }

    public function show($id)
    {
        $month = Month::find($id);
        if (!$month) {
            return Redirect::back()->withInput()->withErrors(['month not found']);
        }
        $appraisals = Appraisal::orderBy('user_id')->orderBy('effective_date', 'desc')->whereDate('effective_date', '<', $month->getLastDay())->get()->unique('user_id')->pluck('id');
        $appraisalBonuses = AppraisalBonus::whereIn('appraisal_id', $appraisals->toArray())->where('appraisal_bonus_type_id', AppraisalBonusType::where('code', 'variable-bonus')->first()->id)->get();
        $monthly_variable_bonuses = MonthlyVariableBonus::where('month_id', $id)->get();
        if (!count($monthly_variable_bonuses) > 0) {
            foreach ($appraisalBonuses as $monthly_variable_bonus) {
                $monthly_variable_bonusObj = MonthlyVariableBonus::create(['user_id' => $monthly_variable_bonus->appraisal->user_id, 'month_id' => $id, 'percentage' => 100, 'amount' => -round($monthly_variable_bonus->value / 12), 'actual_amount' => -$monthly_variable_bonus->value]);
                if (!$monthly_variable_bonusObj->isValid()) {
                    return Redirect::back()->withInput()->withErrors($monthly_variable_bonusObj->getErrors());
                }
            }
        }
        return view('deduction.monthly-variable-bonus.show', compact('month', 'monthly_variable_bonuses'));
    }

    public function destroy($id)
    {
        $monthly_variable_bonus_obj = MonthlyVariableBonus::find($id);
        if ($monthly_variable_bonus_obj) {
            if ($monthly_variable_bonus_obj->delete()) {
                return redirect('deduction/monthly-variable-bonus/')->withMessage("deleted successfully");
            } else {
                return Redirect::back()->withErrors(["Something went wrong!"]);
            }
        }
        return Redirect::back()->withErrors(["Entry not found!"]);
    }

    public function lockMonth($monthId)
    {
        $response = MonthSetting::monthStatusToggle($monthId, 'monthly-variable-bonus');
        if (!empty($response['status']) && $response['status'] == false) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        return Redirect::back()->withMessage($response['message']);
    }

    public function update(Request $request, $monthId)
    {
        $status = false;
        $data = $request->new_amount;
        $response = [];
        if (!empty($data)) {
            foreach ($data as $index => $loan) {
                $deductionObj = MonthlyVariableBonus::find($index);
                $deductionObj->amount = $data[$index];
                if (!$deductionObj->save()) {
                    $status = true;
                    $errors[] = $deductionObj->getErrors();
                }
            }
        }
        if ($status) {
            return Redirect::back()->withErrors($errors);
        }

        return redirect('deduction/monthly-variable-bonus/' . $monthId);
    }

    public function regenerate($monthId)
    {
        $month = Month::find($monthId);
        if (!$month) {
            $response['message'] = "Invalid month id " . $monthId;
            return $response;
        }
        if ($month->monthlyVariableBonusSetting == 'locked') {
            $response['message'] = "Cannot regenrate. Month is locked";
            return $response;
        }
        MonthlyVariableBonus::where('month_id', $monthId)->delete();
        $appraisals = Appraisal::orderBy('user_id')->orderBy('effective_date', 'desc')->whereDate('effective_date', '<', $month->getLastDay())->get()->unique('user_id')->pluck('id');
        $appraisalBonuses = AppraisalBonus::whereIn('appraisal_id', $appraisals->toArray())->where('appraisal_bonus_type_id', AppraisalBonusType::where('code', 'variable-bonus')->first()->id)->get();
        $monthly_variable_bonuses = MonthlyVariableBonus::where('month_id', $monthId)->get();
        if (!count($monthly_variable_bonuses) > 0) {

            foreach ($appraisalBonuses as $monthly_variable_bonus) {
                $monthly_variable_bonusObj = MonthlyVariableBonus::create(['user_id' => $monthly_variable_bonus->appraisal->user_id, 'month_id' => $monthId, 'percentage' => 100, 'amount' => -round($monthly_variable_bonus->value / 12), 'actual_amount' => -$monthly_variable_bonus->value]);

                if (!$monthly_variable_bonusObj->isValid()) {
                    return Redirect::back()->withInput()->withErrors($monthly_variable_bonusObj->getErrors());
                }
            }
        }
        $monthly_variable_bonuses = MonthlyVariableBonus::where('month_id', $monthId)->get();
        return view('deduction.monthly-variable-bonus.show', compact('month', 'monthly_variable_bonuses', 'monthId'));
    }
}
