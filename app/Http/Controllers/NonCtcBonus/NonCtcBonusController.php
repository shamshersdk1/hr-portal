<?php

namespace App\Http\Controllers\NonCtcBonus;

use App\Http\Controllers\Controller;
use App\Jobs\SalaryDependencyNotification\NonCtcBonusNotificationJob;
use Illuminate\Http\Request;
use App\Models\DateTime\{Month,MonthSetting};
use App\Models\Bonus\BonusConfirmed;
use App\Models\Salary\UserSalary;
use App\Models\Bonus\Bonus;
use App\Models\Users\User;
use App\Services\Bonus\BonusService;

class NonCtcBonusController extends Controller
{
    public function index($monthId=null)
    {
        $month = Month::find($monthId);
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if(!$month)
            $month = $monthList->first();
        $users = Bonus::where('month_id',$monthId)->groupBy('user_id')->get();
        $bonuses = [];
        foreach($users as $user)
        {
            $bonusUsers = Bonus::where('user_id',$user->user_id)->where('month_id',$monthId)->get();
            if(count($bonusUsers) > 0)
            {
                $bonuses[$user->user_id]['user'] = $user->user;
                $bonuses[$user->user_id]['amount'] = 0;
                foreach($bonusUsers as $bonusUser)
                {
                    $bonuses[$user->user_id]['amount'] += $bonusUser->amount;
                    $bonuses[$user->user_id]['types'][$bonusUser->reference_type]['type'] = $bonusUser->reference_type;
                    $bonuses[$user->user_id]['types'][$bonusUser->reference_type]['count'] = $bonuses[$user->user_id]['types'][$bonusUser->reference_type]['count']??0 + 1;
                    $bonuses[$user->user_id]['types'][$bonusUser->reference_type]['sum'] = $bonuses[$user->user_id]['types'][$bonusUser->reference_type]['sum']??0 + $bonusUser->amount;
                    if($bonusUser->transaction_id == NULL) {
                        $bonuses[$user->user_id]['due_types'][$bonusUser->reference_type]['type'] = $bonusUser->reference_type;
                        $bonuses[$user->user_id]['due_types'][$bonusUser->reference_type]['count'] = $bonuses[$user->user_id]['types'][$bonusUser->reference_type]['count']??0 + 1;
                        $bonuses[$user->user_id]['due_types'][$bonusUser->reference_type]['sum'] = $bonuses[$user->user_id]['types'][$bonusUser->reference_type]['sum']??0 + $bonusUser->amount;
                    }
                }
            }
        }
        return view('non-ctc-bonus.index', compact('month','monthList','bonuses'));
    }
    public function confirmedBonus()
    {
        $users = BonusConfirmed::with('user:id,employee_id,name')->groupBy('user_id')->get();
        $bonuses = [];
        foreach($users as $user)
        {
            $bonusUsers = BonusConfirmed::where('user_id',$user->user_id)->get();
            if(count($bonusUsers) > 0)
            {
                $bonuses[$user->user_id]['user'] = $user->user;
                $bonuses[$user->user_id]['amount'] = 0;
                foreach($bonusUsers as $bonusUser)
                {
                    $bonuses[$user->user_id]['amount'] += $bonusUser->bonus->amount;
                    $bonuses[$user->user_id]['types'][$bonusUser->bonus->reference_type]['type'] = $bonusUser->bonus->reference_type;
                    $bonuses[$user->user_id]['types'][$bonusUser->bonus->reference_type]['count'] = $bonuses[$user->user_id]['types'][$bonusUser->bonus->reference_type]['count']??0 + 1;
                    $bonuses[$user->user_id]['types'][$bonusUser->bonus->reference_type]['sum'] = $bonuses[$user->user_id]['types'][$bonusUser->reference_type]['sum']??0 + $bonusUser->bonus->amount;
                }
            }
        }
        return view('non-ctc-bonus.confirmed-bonuses', compact('bonuses'));
    }
    public function dueBonus()
    {
        $months = Bonus::where('transaction_id',NULL)->groupBy('month_id')->get()->pluck('month_id');
        
        $data = [];
        foreach($months as $monthId) {
            $response = [];
            $bonuses = Bonus::where('month_id', $monthId)->where('transaction_id',NULL)->get();
           
            foreach($bonuses as $bonus)
            {
                if(!isset($response[$bonus->user_id]['amount'])) {
                    $response[$bonus->user_id]['user'] = $bonus->user;
                    $response[$bonus->user_id]['amount'] = 0;
                    $response[$bonus->user_id]['types'][$bonus->reference_type]['sum'] = 0;    
                }
                if(!isset($response[$bonus->user_id]['types'][$bonus->reference_type]['sum'])) {
                    $response[$bonus->user_id]['types'][$bonus->reference_type]['sum'] = 0;
                }
                if(!isset($response[$bonus->user_id]['types'][$bonus->reference_type]['count'])) {
                    $response[$bonus->user_id]['types'][$bonus->reference_type]['count'] = 0;
                }
                
                
                $response[$bonus->user_id]['amount'] += $bonus->amount;
                $response[$bonus->user_id]['types'][$bonus->reference_type]['type'] = $bonus->reference_type;
                $response[$bonus->user_id]['types'][$bonus->reference_type]['count'] = $response[$bonus->user_id]['types'][$bonus->reference_type]['count'] + 1;
                $response[$bonus->user_id]['types'][$bonus->reference_type]['sum'] = $response[$bonus->user_id]['types'][$bonus->reference_type]['sum'] + $bonus->amount;
                
            }
            //  echo "<pre>";
            // print_r($response);
            // die;
            $month = [];
            $month['month'] = $bonus->month;
            $month['bonuses'] = $response;
            $data[] = $month;  
        }
        return view('non-ctc-bonus.due-bonuses', compact('data'));
    }
    public function lockMonth($monthId){
        $response = MonthSetting::monthStatusToggle($monthId,'non-ctc-bonus');
        if(!empty($response['status']) && $response['status'] == false)
            return redirect()->back()->withErrors('Something went wrong!');
        if(MonthSetting::where('month_id',$monthId)->where('key','non-ctc-bonus')->first()->value == 'locked')
            dispatch(new NonCtcBonusNotificationJob($monthId));
        return redirect()->back()->withMessage($response['message']);
    }

    public function deleteAll(){
        BonusConfirmed::truncate();
        return redirect()->back()->withMessage('Deleted!');
    }

    public function dueBonusUser($monthId,$userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return redirect()->back()->withErrors('Invalid User Id');
        }
        $dueBonuses = BonusService::getDueBonues($userId,$monthId);
        $monthObj = Month::find($monthId);
        if (!$monthObj) {
            $monthObj = null;
        }    
        return view('bonus.due-bonuses-user', compact('monthId','userId','dueBonuses'));
    }

    public function approveAll($monthId , $userId)
    {
        $bonuses = Bonus::where(['month_id' => $monthId, 'user_id' => $userId, 'paid_at' => null])->get();
        foreach($bonuses as $bonus)
        {
            $bonusConfirmend = BonusConfirmed::create(['bonus_id' => $bonus->id, 'user_id' => $userId]);
            if($bonusConfirmend->isValid())
            {
                $bonus->paid_at = date('y-m-d');
                $bonus->save();
            }
        }
        return redirect()->back()->withMessage('Approved!');
    }
}
