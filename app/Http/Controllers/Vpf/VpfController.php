<?php

namespace App\Http\Controllers\Vpf;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\User;
use App\Models\Vpf\VPF;
use App\Models\Deduction\VpfDeduction;
use Redirect;
use App\Models\DateTime\Month;
use App\Models\DateTime\MonthSetting;
use Auth;

class VpfController extends Controller
{
    public function index()
    {
        $vpfList = VPF::with('user','creator')->orderBy('id','DESC')->get();
        $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        return View('vpf/index', compact('vpfList','userList'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $vpfObj = VPF::create(['user_id' => $data['user_id'],'amount' => $data['amount'], 'created_by' => $user->id]);
        if($vpfObj->isValid())
            return redirect('/vpf')->with('message', 'Saved!');
        return redirect('/vpf')->withErrors('Something went wrong!');
    }
   
    public function edit($id)
    {
        $vpf = VPF::find($id);
        if ( isset($vpf) )
            return View('/vpf/edit', ['vpf' => $vpf] );
        return Redirect::back()->withErrors($vpf->getErrors());
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $data = $request->all();
        $data = VPF::updateOrCreate(['id'=>$id],['amount' => $data['amount'],'updated_by' => $user->id]);
        if($data->isValid())
            return redirect('/vpf')->with('message', 'Saved!');
        return redirect()->back()->withErrors($response);
    }

    public function destroy($id)
    {
        $vpf = VPF::find($id);
        if($vpf->delete())
            return redirect('/vpf')->with('message','Successfully Deleted');
        return redirect()->back()->withErrors(["Something went wrong!"]);
    }

    public function statusToggle($id)
    {
        $vpf = VPF::find($id);
        ($vpf->status == 'enable') ? $vpf->status = 'disable' : $vpf->status = 'enable';
        $vpf->save();
        return redirect()->back();
    }

    public function create()
    {
        $vpfList = VPF::with('user','creator')->orderBy('id','DESC')->get();
        $userList = User::Where('is_active', 1)->orderBy('name', 'asc')->get();
        return view('/vpf/add', compact('vpfList','userList'));
    }
}
