<?php

namespace App\Http\Controllers\PrepSalary;

use App\Exports\ExcelExport;
use App\Http\Controllers\Controller;
use App\Jobs\PrepareSalary\Transaction\{AdhocPaymentJob,AppraisalBonusJob,AppraisalComponentJob,BonusesJob,EsiPaymentJob,FoodDeductionJob,InsuranceDeductionJob,LoanDeductionJob,PanPaymentJob,PfPaymentJob,TdsJob,VpfJob,PtJob, UserSalaryUpdateJob};
use App\Models\Appraisal\{AppraisalBonusType,AppraisalComponentType};
use App\Models\Bonus\PrepBonus;
use App\Models\DateTime\FinancialYear;
use App\Models\DateTime\Month;
use App\Models\Salary\{PrepAdhocPayment,PrepAppraisalBonus,PrepCurrentGross,PrepFoodDeduction,PrepInsurance,PrepLoanEmi,PrepSalary,PrepSalaryComponent,PrepSalaryComponentType,PrepSalaryExecution,PrepTds,PrepUser,PrepVpf};
use App\Models\Transaction;
use App\Models\Users\User;
use App\Services\Appraisal\AppraisalService;
use Excel;
use Illuminate\Http\Request;
use Redirect;
use App\Services\SalaryService\{DependencyCheckComponent,SalaryService};
use App\Jobs\DummyJob;
use App\Jobs\PrepareSalary\Components\{AppraisalSalaryJob, WorkingDayJob};
use App\Jobs\ComponentPushJob;
use App\Models\Salary\UserSalary;

class PrepSalaryController extends Controller
{
    public function index()
    {
        $prepSalaries = PrepSalary::orderBy('created_at', 'DESC')->get();
        $months = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($months) > 0) {
            return view('prep-salary.prep-salary.index')->with('message', 'no month found.');
        }
        $monthList = [];
        foreach ($months as $month) {
            $monthList[$month->id] = $month->formatMonth();
        }

        if (!$prepSalaries) {
            return view('prep-salary.prep-salary.index')->with('message', 'no records found.');
        }
        return view('prep-salary.prep-salary.index', compact('prepSalaries', 'monthList'));
    }

    private function _userPlayslipData($monthId, $user_id)
    {

        $days_worked = 21;

        // $userWorkingObj = PrepWorkingDay::where('user_id', $user_id)->where('prep_salary_id', $id)->first();
        $month = Month::find($monthId);
        $userWorkingObj = UserSalary::where('user_id', $user_id)->where('month_id', $monthId)->first();
        if ($userWorkingObj) {
            // $days_worked = $userWorkingObj->user_worked_days;
            $workingDays = $userWorkingObj->working_day;
            $days_worked = $userWorkingObj->working_day + $userWorkingObj->lop;
        }

        $user_transactions = Transaction::where([
            ['month_id', '=', $monthId],
            ['user_id', '=', $user_id],
            ['amount', '!=', 0],
            ['reference_type', '!=', 'App\Models\Audited\BankTransfer\BankTransferUser'],
            ['reference_type', '!=', 'App\Models\Audited\BankTransfer\BankTransferHoldUser'],
            ['reference_type', '!=', 'App\Models\Loan'],
        ])->get();
        $net_transfer = 0;
        $total_earning = 0;
        $total_deduction = 0;
        $deduction = [];
        $earning = [];
        $onsite_bonus = 0;
        $bonus = 0;
        $additional_bonus = 0;
        $extra_bonus = 0;
        $performance_bonus = 0;
        $tech_talk_bonus = 0;
        foreach ($user_transactions as $t) {

            if ($t->is_company_expense) {
                continue;
            }

            if ($t->amount > 0) {
                $earning[] = $t;
                $total_earning += $t->amount;
            } else {
                $deduction[] = $t;
                $total_deduction += $t->amount;
            }

            if ($t->reference->reference_type == 'App\Models\Admin\OnSiteBonus') {
                $onsite_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\AdditionalWorkDaysBonus') {
                $additional_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\User\UserTimesheetExtra') {
                $extra_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\PerformanceBonus') {
                $performance_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\TechTalkBonusesUser') {
                $tech_talk_bonus += $t->amount;
            } elseif ($t->reference->reference_type == 'App\Models\Admin\Bonus') {
                $bonus += $t->amount;
            }

            $net_transfer += $t->amount;
        }
        $user = User::find($user_id);
        return compact('deduction', 'earning', 'user', 'total_earning', 'total_deduction', 'net_transfer', 'days_worked', 'onsite_bonus', 'additional_bonus', 'extra_bonus', 'performance_bonus', 'tech_talk_bonus', 'bonus', 'month', 'workingDays');
    }

    public function downloadUserPayslip($monthId, $user_id)
    {
        $data = $this->_userPlayslipData($monthId, $user_id);
        $pdf = \PDF::loadView('prep-salary.prep-salary.pdf.payslip', $data);
        return $pdf->download('payslip-' . $user_id . '.pdf', array('Attachment' => 0));
    }

    public function showUserPayslip($monthId, $user_id)
    {
        $data = $this->_userPlayslipData($monthId, $user_id);
        return view('prep-salary.prep-salary.user-payslip', $data);
    }

    public function showPayslips($monthId = null)
    {
        if ($monthId == null) {
            $month = date('m');
            $year = date('Y');
            $monthObj = Month::where('month', $month)->where('year', $year)->first();

            if ($monthObj == null) {
                return redirect()->back()->withErrors('Month not found!')->withInput();
            }

            $monthId = $monthObj->id;
        } else {
            $monthObj = Month::find($monthId);
        }

        $transactions = Transaction::where('month_id', $monthId)->groupBy('user_id')->where('prep_salary_id','<>',0)->get();
        $monthList = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();

        return view('prep-salary.prep-salary.payslips', compact('transactions', 'monthId', 'monthList', 'monthObj'));
    }

    public function show($id)
    {
        $prepSalarie = PrepSalary::find($id);
        PrepSalaryExecution::componentStatus($id);
        if (!$prepSalarie) {
            return redirect('/prep-salary')->withErrors("no record found.")->withInput();
        }
        return view('prep-salary.prep-salary.show', compact('prepSalarie'));
    }
    public function discardPrepSalary($id)
    {
        $prepSalObj = PrepSalary::find($id);
        if (!$prepSalObj) {
            return redirect('/prep-salary')->withErrors("Prep Salary not found.");
        }
        $prepSalObj->status = 'discarded';
        if (!$prepSalObj->save()) {
            return redirect('/prep-salary')->withErrors("Prep Salary status not saved.");
        }
        return redirect('/prep-salary')->with("message", "Prep Salary discarded.");
    }

    public function closePrepSalary($id)
    {
        $prepSalObj = PrepSalary::find($id);
        if (!$prepSalObj) {
            return redirect('/prep-salary')->withErrors("Prep Salary not found.");
        }
        $prepSalObj->status = 'closed';
        if (!$prepSalObj->save()) {
            return redirect('/prep-salary')->withErrors("Prep Salary status not saved.");
        }
        return redirect('/prep-salary')->with("message", "Prep Salary closed.");
    }

    public function storeSalary(Request $request)
    {
        $data = $request->all();
        $month = Month::find($data['id']);
        if (!$month) {
            return redirect('/prep-salary')->withErrors("no month found.")->withInput();
        }
        $prepSalaryObj = new PrepSalary();
        $prepSalaryObj->month_id = $data['id'];
        $prepSalaryObj->status = 'open';
        if ($prepSalaryObj->isValid()) {
            if ($prepSalaryObj->save()) {
            } else {
                return redirect()->back()->withErrors('Unable To Save')->withInput();
            }
        } else {
            return redirect('/prep-salary')->withErrors($prepSalaryObj->getErrors())->withInput();
        }
        $prepSalaryComponents = PrepSalaryComponentType::orderBy('order')->get();
        foreach ($prepSalaryComponents as $prepSalaryComponent) {
            $prepSalaryComponentObj = new PrepSalaryComponent();
            $prepSalaryComponentObj->prep_salary_id = $prepSalaryObj->id;
            $prepSalaryComponentObj->prep_salary_component_type_id = $prepSalaryComponent->id;
            $prepSalaryComponentObj->is_generated = 0;
            if ($prepSalaryObj->isValid()) {
                if ($prepSalaryComponentObj->save()) {
                } else {
                    return redirect('/prep-salary')->withErrors("Unable to Save")->withInput();
                }
            } else {
                return redirect('/prep-salary')->withErrors($prepSalaryComponentObj->getErrors())->withInput();
            }
        }
        return redirect('/prep-salary/' . $prepSalaryObj->id)->with('message', 'Saved! Of Month ' . $month->formatMonth());
    }

    public function destroy($id)
    {
        $prepSalaryObj = PrepSalary::find($id);
        if ($prepSalaryObj && $prepSalaryObj->delete()) {
            return redirect('/prep-salary')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }

    public function lockMonth($id)
    {
        $prepSalaryObj = PrepSalary::find($id);

        foreach ($prepSalaryObj->components as $component) {
            $componentObj = PrepSalaryComponent::find($component->id);
            $componentObj->status = 'closed';
            if (!$componentObj->update()) {
                return redirect()->back()->withErrors('Something went wrong!')->withInput();
            }
        }
        $prepSalaryObj->status = 'closed';
        if (!$prepSalaryObj->update()) {
            return redirect()->back()->withErrors('Sometbhasbdhhing went wrong!')->withInput();
        }
        return redirect('/prep-salary')->with('message', 'Successfully Locked');
    }

    private function checkDependency($prepSalaryComponentId)
    {
        $response['status'] = false;
        $response['errors'] = [];
        $prepSalaryComponent = PrepSalaryComponent::find($prepSalaryComponentId);
        if (!$prepSalaryComponent) {
            $response['status'] = false;
            $response['errors'] = 'Prep Salary Component Not Found';
        }
        if ($prepSalaryComponent->type && $prepSalaryComponent->type->dependsOn) {
            foreach ($prepSalaryComponent->type->dependsOn as $dependent) {
                foreach ($dependent->dependentOn->prepSalaryComponent as $component) {
                    if ($prepSalaryComponent->prep_salary_id == $component->prep_salary_id && $component->is_generated == 0) {
                        $response['status'] = true;
                        array_push($response['errors'], "Generate Component : " . $dependent->dependentOn->name);
                    }
                }
            }
        }
        return $response;
    }

    private function dependentOn($prepSalaryComponentId)
    {
        $response['status'] = false;
        $response['errors'] = '';
        $prepSalaryComponent = PrepSalaryComponent::find($prepSalaryComponentId);
        if ($prepSalaryComponent->type->dependentOn) {
            foreach ($prepSalaryComponent->type->dependentOn as $dependent) {
                if ($dependent->dependentReverseOn) {
                    foreach ($dependent->dependentReverseOn->prepSalaryComponent as $component) {
                        if ($prepSalaryComponent->prep_salary_id == $component->prep_salary_id && $component->is_generated == 1) {
                            $component->is_generated = 0;
                            if ($component->isValid()) {
                                if (!$component->save()) {
                                    $response['errors'] = $response['errors'] . "Unable to Save";
                                }
                            } else {
                                $response['errors'] = $response['errors'] . $component->getErrors();
                            }
                        }
                    }
                }
            }
        }
        return $response;
    }

    public function generate($id)
    {
        $userObject = \Auth::user();
        if (!$userObject) {
            return redirect()->back()->withErrors('failed to get user')->withInput();
        }
        $prepSalaryComponent = PrepSalaryComponent::find($id);
        if (!$prepSalaryComponent) {
            return redirect()->back()->withErrors('Prepare Salary Component not found')->withInput();
        }
        if ($prepSalaryComponent->salary->status == "closed" || $prepSalaryComponent->salary->status == "finalizing") {
            return redirect()->back()->withErrors("PrepSalary already generated");
        }

        // $response = self::checkDependency($id);
        // if($response['status'])
        // {
        //     return redirect()->back()->withErrors($response['errors'])->withInput();
        // }

        $dependencyCheck = new DependencyCheckComponent($prepSalaryComponent->type->id, $prepSalaryComponent->salary->id);

        $response = $dependencyCheck->checkDependency();

        if (!$response) {
            return redirect()->back()->withErrors('Generate Dependent Component First');
        }

        $response = self::dependentOn($id);

        $strModelName = $prepSalaryComponent->type->service_class;
        if (!class_exists($strModelName)) {
            return redirect()->back()->withErrors('Class Not Exists')->withInput();
        }
        $strModelObj = new $strModelName();
        $strModelObj->setComponent($prepSalaryComponent->id);
        $strModelObj->setUserId($userObject->id);
        $strModelObj->setMonthYear($prepSalaryComponent->salary->month->month, $prepSalaryComponent->salary->month->year);
        $response = $strModelObj->generate();
        $prepSalaryComponent->is_generated = 1;

        if (!$response['status']) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }

        if (!$prepSalaryComponent->update()) {
            return redirect()->back()->withErrors('Something went wrong!')->withInput();
        }
        return redirect()->back()->withMessage('Successfully generated of ' . $prepSalaryComponent->type->name . ' for ' . $prepSalaryComponent->salary->month->formatMonth());
    }

    public function getView($id)
    {
        $prepSalaryComponent = PrepSalaryComponent::find($id);
        if (!$prepSalaryComponent) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        //  $prepUsers = $prepSalaryComponent->salary->prepUsers;
        $prepUsers = PrepUser::where('prep_salary_id', $prepSalaryComponent->salary->id)->with('user')->select('prep_users.*', \DB::raw('(SELECT employee_id FROM users WHERE prep_users.user_id = users.id ) as employee_id'))->orderBy('employee_id')->get();
        $response = [];
        if (count($prepUsers) > 0) {
            foreach (PrepSalaryComponent::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->get() as $prepComponenet) {
                foreach (PrepUser::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->cursor() as $prepUser) {
                    $strModelName = $prepComponenet->type->service_class;
                    $strModelObj = new $strModelName;
                    $strModelObj->setComponent($id);
                    $strModelObj->setUserId($prepUser->user_id);
                    $response[$prepUser->user_id][$prepComponenet->type->code] = $strModelObj->getHtml(); //Remove prep_salary_id
                }
            }
        }
        return view('prep-salary.partials.index', compact('prepUsers', 'response'));
    }

    public function downloadCSV($id)
    {
        $prepSalaryComponent = PrepSalaryComponent::find($id);
        if (!$prepSalaryComponent) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        //  $prepUsers = $prepSalaryComponent->salary->prepUsers;
        $prepUsers = PrepUser::where('prep_salary_id', $prepSalaryComponent->salary->id)->with('user')->select('prep_users.*', \DB::raw('(SELECT employee_id FROM users WHERE prep_users.user_id = users.id ) as employee_id'))->orderBy('employee_id')->get();
        $response = [];

        if (count($prepUsers) > 0) {
            foreach (PrepSalaryComponent::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->get() as $prepComponenet) {
                foreach (PrepUser::where('prep_salary_id', $prepSalaryComponent->prep_salary_id)->cursor() as $prepUser) {
                    $strModelName = $prepComponenet->type->service_class;
                    $strModelObj = new $strModelName;
                    $strModelObj->setComponent($id);
                    $strModelObj->setUserId($prepUser->user_id);
                    $response[$prepUser->user_id][$prepComponenet->type->code] = $strModelObj->getHtml(); //Remove prep_salary_id
                }
            }
        }

        function array_merge_add($x, $y)
        {
            $myArray = $x;
            foreach ($y as $k => $v) {
                if (isset($myArray[$k])) {
                    if (is_numeric($myArray[$k])) {
                        $myArray[$k] = $myArray[$k] + $v;
                    } else {
                        $myArray[$k] = $myArray[$k] . " + " . $v;
                    }

                } else {
                    $myArray[$k] = $v;
                }
            }
            return $myArray;
        }

        function getFlatKeyArray($data, $prefix = '')
        {
            $output = [];
            if (isset($data['key']) && isset($data['value'])) {
                $prefix = substr($prefix, 0, -1);
                $last_pos = strrpos($prefix, "_");
                $prefix = substr($prefix, 0, $last_pos) . "_";
                $output[$prefix . $data['key']] = $data['value'];
            } else {
                foreach ($data as $key => $value) {
                    if (is_array($value)) {
                        if (is_numeric($key) && !(isset($value['key']) && isset($value['value']))) {
                            $output = array_merge_add($output, getFlatKeyArray($value, $prefix));
                        } else {
                            $output = array_merge_add($output, getFlatKeyArray($value, $prefix . $key . '_'));
                        }

                    } else {
                        $output[$prefix . $key] = $value;
                    }
                }
            }
            return $output;
        }

        $users_map = [];
        foreach ($prepUsers as $user) {
            $users_map[$user->user_id] = $user->user;
        }

        $final_response = [];
        $keys = [];
        foreach ($response as $user_id => $data) {
            $user_info = [];
            $user_info['id'] = $users_map[$user_id]->id;
            $user_info['name'] = $users_map[$user_id]->name;
            $user_info['email'] = $users_map[$user_id]->email;
            $user_info['employee_id'] = $users_map[$user_id]->employee_id;

            $final_response[$user_id] = array_merge($user_info, getFlatKeyArray($data));

            foreach ($final_response[$user_id] as $field_key => $field_value) {
                $keys[$field_key] = $field_key;
            }
        }

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=prep-salary-data-' . time() . '.csv');

        echo implode(",", $keys) . "\n";
        foreach ($final_response as $user_id => $data) {
            $line = '';
            foreach ($keys as $key => $value) {
                if (isset($data[$key])) {
                    $line .= $data[$key] . ',';
                } else {
                    $line .= ',';
                }

            }
            echo $line . "\n";
        }

        exit();
    }

    public function generatePayslip($id, $userId = null)
    {
        if ($userId == null) {
            $userObj = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->first();
            $userId = $userObj->id;
        } else {
            $userObj = User::find($userId);
            if (!$userObj) {
                return redirect('/prep-salary')->withErrors("User not found.")->withInput();
            }
        }

        $userList = User::where('is_active', 1)->orderBy('employee_id', 'ASC')->get();

        $prepSalaryObj = PrepSalary::find($id);
        $currentGrossObj = PrepCurrentGross::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();

        if (!$currentGrossObj || !$prepSalaryObj) {
            return redirect('/prep-salary')->withErrors("No record found.")->withInput();
        }

        $appraisalBonusObj = PrepAppraisalBonus::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();

        $appraisalComponentTypes = AppraisalComponentType::all();
        $appraisalBonusTypes = AppraisalBonusType::all();

        $vpfObj = PrepVpf::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();

        $insuranceObj = PrepInsurance::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();

        $loanObj = PrepLoanEmi::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        //loan

        //--bonus itsaving LOAN
        $tdsObj = PrepTds::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        $salaryResponse = self::generateSalaryData($id, $userId);

        $storeSalaryObj = Salary::where('user_id', $userId)->where('month_id', $prepSalaryObj->month->id)->first();
        $foodObj = PrepFoodDeduction::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        $bonusObjs = PrepBonus::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        return view('prep-salary.prep-salary.payslip', compact('bonusObjs', 'foodObj', 'storeSalaryObj', 'tdsObj', 'salaryResponse', 'userObj', 'prepSalaryObj', 'userList', 'currentGrossObj', 'appraisalComponentTypes', 'appraisalBonusObj', 'appraisalBonusTypes', 'vpfObj', 'insuranceObj', 'loanObj'));
    }

    public function generateSalaryData($prepSalaryId, $userId)
    {
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        $currentGrossObj = PrepCurrentGross::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        $response['monthlyGrossSalary'] = 0;
        $response['totalDeductions'] = 0;
        $response['netPayable'] = 0;
        foreach ($currentGrossObj->items as $item) {
            $item->value > 0 ? $response['monthlyGrossSalary'] += $item->value : $response['totalDeductions'] -= $item->value;
            $response['netPayable'] += $item->value;
        }
        $loanObjs = PrepLoanEmi::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        foreach ($loanObjs as $loanObj) {
            $response['totalDeductions'] -= $loanObj->emi_amount;
            $response['netPayable'] += $loanObj->emi_amount;
        }
        $insuranceObjs = PrepInsurance::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        foreach ($insuranceObjs as $insuranceObj) {
            $response['totalDeductions'] -= $insuranceObj->value;
            $response['netPayable'] += $insuranceObj->value;
        }
        $appraisalBonusObjs = PrepAppraisalBonus::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        foreach ($appraisalBonusObjs as $appraisalBonusObj) {
            $response['totalDeductions'] -= $appraisalBonusObj->value;
            $response['netPayable'] += $appraisalBonusObj->value;
        }
        $tdsObj = PrepTds::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        if ($tdsObj) {
            $response['totalDeductions'] -= $tdsObj->componentByKey('tds-for-the-month')->value;
            $response['netPayable'] -= $tdsObj->componentByKey('tds-for-the-month')->value;
        }
        $bonusObjs = PrepBonus::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->get();
        if (count($bonusObjs) > 0) {
            foreach ($bonusObjs as $bonusObj) {
                $response['netPayable'] += $bonusObj->bonus->amount;
            }
        }
        $foodObjs = PrepFoodDeduction::where('prep_salary_id', $prepSalaryObj->id)->where('user_id', $userId)->first();
        $response['totalDeductions'] -= $foodObjs->value;
        $response['netPayable'] += $foodObjs->value;
        return $response;
    }

    public function salaryStore($prepSalaryId, $userId)
    {
        $prepSalaryObj = PrepSalary::find($prepSalaryId);
        $salaryResponse = self::generateSalaryData($prepSalaryId, $userId);
        if (!count(Salary::where('user_id', $userId)->where('month_id', $prepSalaryObj->month->id)->first()) > 0) {
            $salaryObj = Salary::create(['user_id' => $userId, 'month_id' => $prepSalaryObj->month->id, 'monthly_gross_salary' => $salaryResponse['monthlyGrossSalary'],
                'total_deductions' => -$salaryResponse['totalDeductions'], 'net_payable' => $salaryResponse['netPayable']]);
            if (!$salaryObj->isValid()) {
                return redirect()->back()->withErrors($salaryObj->getErrors());
            }
            dispatch(new AppraisalComponentJob($userId, $prepSalaryId));
            dispatch(new AppraisalBonusJob($userId, $prepSalaryId));
            dispatch(new FoodDeductionJob($userId, $prepSalaryId));
            dispatch(new InsuranceDeductionJob($userId, $prepSalaryId));
            dispatch(new LoanDeductionJob($userId, $prepSalaryId));
            dispatch(new TdsJob($userId, $prepSalaryId));
            dispatch(new VpfJob($userId, $prepSalaryId));
            dispatch(new BonusesJob($userId, $prepSalaryId));
            dispatch(new AdhocPaymentJob($userId, $prepSalaryId));

            dispatch(new EsiPaymentJob($userId, $prepSalaryId));
            dispatch(new PfPaymentJob($userId, $prepSalaryId));
        }
        return redirect()->back()->withMessage('Saved Successfully');
    }

    public function finalize($prepSalaryId)
    {
        $prepSalaryObjs = PrepUser::where('prep_salary_id', $prepSalaryId)->get();
        $prepSalary = PrepSalary::find($prepSalaryId);
        if (!$prepSalary) {
            return redirect()->back()->withErrors("Prep Salary Not Found.");
        }
        if ($prepSalary->status == "closed" || $prepSalary->status == "finalizing") {
            return redirect()->back()->withErrors("PrepSalary already generated");
        }
        $prepSalary->status = 'finalizing';
        $prepSalary->save();
        foreach ($prepSalaryObjs as $prepSalaryObj) {
            PrepFoodDeduction::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new FoodDeductionJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepCurrentGross::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->first()->items()->update(['status' => 'processing']);
            dispatch(new AppraisalComponentJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepAppraisalBonus::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new AppraisalBonusJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepInsurance::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new InsuranceDeductionJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepLoanEmi::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new LoanDeductionJob($prepSalaryObj->user_id, $prepSalaryId));

            $obj = PrepTds::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->first()->componentByKey('tds-for-the-month');
            if ($obj) {
                $obj->status = "processing";
                $obj->save();
            }
            dispatch(new TdsJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepVpf::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new VpfJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepBonus::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new BonusesJob($prepSalaryObj->user_id, $prepSalaryId));

            PrepAdhocPayment::where('prep_salary_id', $prepSalaryId)->where('user_id', $prepSalaryObj->user_id)->update(['status' => 'processing']);
            dispatch(new AdhocPaymentJob($prepSalaryObj->user_id, $prepSalaryId));

            dispatch(new UserSalaryUpdateJob($prepSalaryObj->user_id, $prepSalaryId));

        }
        return redirect()->back()->withMessage('Finalizing');
    }

    public function viewTds($id)
    {
        $prepSalary = PrepSalary::find($id);
        if (!$prepSalary) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $prepTdsObjs = PrepTds::where('prep_salary_id', $prepSalary->id)->get();
        return view('prep-salary.prep-salary.tds', compact('prepTdsObjs', 'prepSalary'));
    }

    public function prepSalarySheet($id)
    {
        $prepSalObj = PrepSalary::find($id);
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $data = SalaryService::getSalarySheetData($id);
        $users = User::orderBy('employee_id', 'ASC')->get();
        return view('prep-salary.prep-salary.salary-sheet', compact('data', 'users', 'prepSalObj'));
    }

    public function getUsers($salaryId)
    {
        // $users = User::orderBy('is_active','desc')->orderBy('employee_id')->get();
        // $prepSalaryObj = PrepSalary::find($salaryId);
        $prepSalaryObj = PrepSalary::find($salaryId);
        // if(count($prepSalaryObj->prepUsers?$prepSalaryObj->prepUsers:true) > 0 ){
        //     $prepIds = $prepSalaryObj->prepUsers->pluck('id');
        //     $all_users = User::whereNotIn('id',$prepIds)->get();
        //     $checl = $prepSalaryObj->prepUsers;
        //     $users = $checl->merge($all_users);
        // }
        // else
        $compType = PrepSalaryComponentType::where('code', 'user')->first();
        if ($compType) {
            $prepSalCompObj = PrepSalaryComponent::where('prep_salary_id', $salaryId)->where('prep_salary_component_type_id', $compType->id)->first();
        }

        $prepUsers = PrepUser::where('prep_salary_id', $salaryId)->get();
        $users = UserSalary::where('month_id',$prepSalaryObj->month_id)->get();
        return view('prep-salary.prep-salary.users-view', compact('users', 'prepSalaryObj', 'prepSalCompObj', 'prepUsers'));
    }

    public function setUsers(Request $request, $salaryId)
    {
        //check
        $selectedUsers = $request->all();
        $prepSalaryObj = PrepSalary::find($salaryId);

        if (empty($selectedUsers['options']) || !isset($selectedUsers['options'])) {
            return Redirect::back()->withErrors('Please select at least one user.');
        }

        $users = User::all();
        PrepUser::where('prep_salary_id', $salaryId)->delete();

        foreach ($selectedUsers['options'] as $selectedUser) {
            $user = PrepUser::create(['prep_salary_id' => $salaryId, 'user_id' => $selectedUser]);
        }

        //Can be moved to job
        PrepSalaryExecution::where('prep_salary_id', $prepSalaryObj->id)->delete();
        foreach($prepSalaryObj->prepUsers as $user)
        {
            foreach($prepSalaryObj->components as $component)
            {
                if($component->type->code != 'user')
                {
                    $prepSalaryExecution = PrepSalaryExecution::create(['prep_salary_id' => $prepSalaryObj->id,'component_id' => $component->id, 'user_id' => $user->user_id, 'status' => 'init','counter' => 0]);
                    if($prepSalaryExecution->isInvalid())
                    {
                        $status = true;
                    }
                }
            }
        }

        return redirect('/prep-users/' . $salaryId);
    }

    public function statusMonth($prepSalaryId, $monthId)
    {
        $msg = "";
        $userComponentId = PrepSalaryComponentType::where('code', 'user')->first()->id;
        $prepSalaryComponentObj = PrepSalaryComponent::where('prep_salary_id', $prepSalaryId)->where('prep_salary_component_type_id', $userComponentId)->first();
        if (!$prepSalaryComponentObj) {
            return Redirect::back()->withErrors('Prep Salary Component Not Found!');
        }

        $prepSalaryComponentObj->is_generated = 1;
        if ($prepSalaryComponentObj->status == null || $prepSalaryComponentObj->status == 'open') {
            $prepUsers = PrepUser::where('prep_salary_id', $prepSalaryId)->get();
            if (!$prepUsers || empty($prepUsers) || !isset($prepUsers)) {
                return Redirect::back()->withErrors('Please select at least one user!');
            }
            $prepSalaryComponentObj->status = 'closed';
            $msg = "User Month Locked";
        } else if ($prepSalaryComponentObj->status == 'closed') {
            $prepSalaryComponentObj->status = 'open';
            $msg = "User Month UnLocked";
        }

        if (!$prepSalaryComponentObj->save()) {
            return Redirect::back()->withErrors('Something went wrong!');
        }

        $count = PrepUser::where('prep_salary_id', $prepSalaryId)->count();
        if ($count < 1) {
            return Redirect::back()->withErrors('Please select atleast one user.');
        }

        return Redirect::back()->withMessage($msg);
    }

    public function generateAll($prepSalaryId)
    {

        $prepSalary = PrepSalary::find($prepSalaryId);
        if (!$prepSalary) {
            return Redirect::back()->withErrors("Prep Salary Not Found");
        }
        if ($prepSalary->status == "closed" || $prepSalary->status == "finalizing") {
            return Redirect::back()->withErrors("PrepSalary already generated");
        }
        if ($prepSalary->status != "open" && $prepSalary->status != "processed") {
            return Redirect::back()->withErrors("PrepSalary already processed/processing");
        }
        $check = SalaryService::checkPreCondition($prepSalary->id);
        if($check['status'])
        {
            return redirect()->back()->withErrors($check['errors']);
        }
        $dependencyArray = [];
        PrepSalaryExecution::where('prep_salary_id',$prepSalary->id)->update(['status'=>'init']);
        $components = PrepSalaryComponentType::where('code','<>','user')->orderBy('order')->get();
        foreach($components as $component)
        {
            //$prepSalaryComponent = PrepSalaryComponent::where(['prep_salary_id' => $prepSalary->id, 'prep_salary_component_type_id' => $component->id ,'is_generated' => '0'])->first();
            $prepSalaryComponent = PrepSalaryComponent::where(['prep_salary_id' => $prepSalary->id, 'prep_salary_component_type_id' => $component->id])->first();
            if($prepSalaryComponent)
            {
                $dependencyArray[] =  $prepSalaryComponent->id;
            }
        }
        if(!empty($dependencyArray))
        {
            foreach($prepSalary->prepUsers as $user)
            {
                dispatch(new ComponentPushJob($dependencyArray,$user->user_id));
            }
            if($prepSalary->status == "open")
                $prepSalary->status = "in_progress";
            $prepSalary->save();
        } else {
            return redirect()->back()->withErrors("Everything is generated");
        }

        return redirect()->back()->withMessage("Generating Salary Please Wait!!!");
    }

    public function downloadCSVTds($id)
    {
        $prepSalary = PrepSalary::find($id);
        if (!$prepSalary) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $prepTdsObjs = PrepTds::where('prep_salary_id', $prepSalary->id)->get();
        $data = [];
        foreach ($prepTdsObjs as $index => $prepTdsObj) {
            $data[$prepTdsObj->id]["Sl_No"] = $index + 1;
            $data[$prepTdsObj->id]["Employee Id"] = $prepTdsObj->user->employee_id;
            $data[$prepTdsObj->id]["Employee Name"] = $prepTdsObj->user->name;
            foreach ($prepTdsObj->components as $component) {
                $component->key = str_replace("-", " ", $component->key);
                $component->key = ucwords(strtolower($component->key));
                $data[$prepTdsObj->id][$component->key] = $component->value;
            }
        }
        $filename = "Prep_Tds";
        $export_data = $data;
        $collection = collect($export_data);
        return Excel::download(new ExcelExport($collection), $filename . ".csv");

    }

    public function prepSalarySheetDownload($id)
    {
        $prepSalObj = PrepSalary::find($id);
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $data = SalaryService::getSalarySheetData($id);
        $dataExp = [];
        $i = 0;
        foreach ($data as $index => $obj) {
            $i++;
            foreach ($obj as $index2 => $obj2) {
                $index2 = str_replace("_", " ", $index2);
                $dataExp[$index]['Sl_No'] = $i;
                $dataExp[$index][$index2] = $obj2;
            }
        }
        $filename = "Prep_Salary_Sheet";
        $export_data = $dataExp;
        $collection = collect($export_data);
        return Excel::download(new ExcelExport($collection), $filename . ".csv");

    }

    public static function salarySheetCompare($monthId = null)
    {
        if ($monthId == null) {
            $prepSalObj = PrepSalary::whereNotIn('status', ['open', 'discarded'])->orderBy('created_at', 'DESC')->first();
        } else {
            $monthObj = Month::find($monthId);
            if (!$monthObj) {
                return redirect()->back()->withErrors('Month Not Found')->withInput();
            }

            $prepSalObj = PrepSalary::where('month_id', $monthObj->id)->whereNotIn('status', ['open', 'discarded'])->orderBy('created_at', 'DESC')->first();
        }
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }
        $response = SalaryService::getComparedSalarySheet($prepSalObj->id);

        if ($response['status'] == false) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }

        $data = $response['data']['combined'];
        $prevMonthObj = $response['data']['prev_month'];
        $users = User::orderBy('employee_id', 'ASC')->get();
        $apprCompType = AppraisalComponentType::all();
        $monthList = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();

        return view('prep-salary.prep-salary.salary-sheet-compare', compact('data', 'prepSalObj', 'users', 'apprCompType', 'prevMonthObj', 'monthList'));
    }

    public function salarySheetCompareDownload($prepSalId)
    {
        print_r("here");
        die;
        $prepSalObj = PrepSalary::find($prepSalId);
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }

        $response = SalaryService::getComparedSalarySheet($prepSalObj->id);
        if ($response['status'] == false) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }

        $data = $response['data']['combined'];

        $dataExp = [];
        $i = 0;
        $dataExp[$i]['Sl_No'] = "Sl_No";
        $dataExp[$i]['Employee_ID'] = "Employee_ID";
        $dataExp[$i]['Name'] = "Name";
        $dataExp[$i]['Bank'] = "Bank";
        $type = AppraisalComponentType::all();
        foreach ($type as $obj) {
            $dataExp[$i][$obj->code . '-previous'] = $obj->code;
        }
        foreach ($type as $obj) {
            $dataExp[$i][$obj->code . '-current'] = $obj->code;
        }
        foreach ($data as $index => $obj) {
            $i++;
            $dataExp[$index]['Sl_No'] = $i;
            $dataExp[$index]['Employee_ID'] = $obj['Employee_ID'];
            $dataExp[$index]['Name'] = $obj['Name'];
            $dataExp[$index]['Bank'] = $obj['Bank'];

            foreach ($obj['previous'] as $index2 => $obj2) {
                $dataExp[$index][$index2 . '-previous'] = $obj2 . '';
            }
            foreach ($obj['current'] as $index2 => $obj2) {
                $dataExp[$index][$index2 . '-current'] = $obj2 . '';
            }
        }
        $filename = "Prep_Salary_Sheet";
        $export_data = $dataExp;
        $collection = collect($export_data);
        return Excel::download(new ExcelExport($collection), $filename . ".csv");
    }

    public static function compComparison($prepSalId)
    {
        $prepSalObj = PrepSalary::find($prepSalId);
        if (!$prepSalObj) {
            return redirect()->back()->withErrors('Prep Salary Id Not Found')->withInput();
        }

        $currMonthObj = Month::find($prepSalObj->month->id);
        if (!$currMonthObj) {
            return redirect()->back()->withErrors('Prep Salary Month Not Found')->withInput();
        }

        $prevMonthObj = null;
        if ($currMonthObj->month == 1) {
            $prevMonthObj = Month::where('financial_year_id', $currMonthObj->financial_year_id)->where('month', 12)->first();
        } elseif ($currMonthObj->month == 4) {
            $prevFinanObj = FinancialYear::where('year', $currMonthObj->year - 1)->first();
            if (!$prevFinanObj) {
                return redirect()->back()->withErrors(' Previous Financial Year Not Found')->withInput();
            }
            $prevMonthObj = Month::where('financial_year_id', $prevFinanObj->id)->where('month', $currMonthObj->month - 1)->first();
        } else {
            $prevMonthObj = Month::where('financial_year_id', $currMonthObj->financial_year_id)->where('month', $currMonthObj->month - 1)->first();
        }
        if (!$prevMonthObj) {
            return redirect()->back()->withErrors('Previous Month Not Found')->withInput();
        }

        $prevPrepSal = PrepSalary::where('month_id', $prevMonthObj->id)->first();
        if (!$prevPrepSal) {
            return redirect()->back()->withErrors('Previous Month Prep Sal Not Found')->withInput();
        }

        $url = \Route::current()->uri;
        $response = AppraisalService::getComparedPrepComponents($prepSalId, $prevPrepSal->id, $url);
        if ($response['status'] == false) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }
        $data = $response['data'];

        $users = User::orderBy('employee_id', 'ASC')->get();

        return view('prep-salary.prep-comp-compare', compact('data', 'prepSalObj', 'users', 'prevPrepSal', 'url'));
    }

    public function userGenerate($salaryId)
    {
        $userExecutions = [];
        $prepSalary = PrepSalary::find($salaryId);
        $users = PrepUser::with('user:id,name,employee_id')->where('prep_salary_id',$salaryId)->distinct('user_id')->get()->toArray();
        foreach($users as $user)
        {
            $executions = PrepSalaryExecution::where('prep_salary_id',$salaryId)->where('user_id',$user['user_id'])->get(['id','status']);
            $execution_status = $executions->where('status','<>','completed')->count();
            $userExecutions[$user['user_id']]['is_all_generated'] = $execution_status == 0 ? true:false;
            $userExecutions[$user['user_id']]['executions'] = $executions->toArray();
            $userExecutions[$user['user_id']]['user'] = array('id' => $user['user_id'],'name' => $user['user']['name'],'employee_id' => $user['user']['employee_id']);
        }
        $components = PrepSalaryComponentType::where('code','<>','user')->orderBy('order')->get('name')->toArray();
        return view('prep-salary.prep-salary.user-generate-view',compact('userExecutions','prepSalary','components'));
    }

    public function userGenerateAll($salaryId, $userId)
    {
        $dependencyArray = [];
        $user = User::find($userId);
        PrepSalaryExecution::where('prep_salary_id',$salaryId)->where('user_id',$userId)->update(['status'=>'init']);
        $components = PrepSalaryComponentType::where('code','<>','user')->orderBy('order')->get();
        foreach($components as $component)
        {
            $prepSalaryComponent = PrepSalaryComponent::where(['prep_salary_id' => $salaryId, 'prep_salary_component_type_id' => $component->id])->first();
            if($prepSalaryComponent) {
                $dependencyArray[] =  $prepSalaryComponent->id;
            }
        }
        if(!empty($dependencyArray)) {
            dispatch(new ComponentPushJob($dependencyArray,$userId));
        } else {
            return redirect()->back()->withError("Looks like salary for user:".$user->name." is already generated");
        }
        return redirect()->back()->withMessage("Generating salary for user : ".$user->name);
    }

    public function userGenerateExecution($prepSalaryExecutionId)
    {
        //Add dependent and dependency check
        $dependencyArray = [];
        $prepSalaryExecution = PrepSalaryExecution::find($prepSalaryExecutionId);
        $prepSalaryExecution->status = 'init';
        $prepSalaryExecution->save();
        $dependencyArray[] = $prepSalaryExecution->component_id;
        if(!empty($dependencyArray))
        {
            dispatch(new ComponentPushJob($dependencyArray,$prepSalaryExecution->user_id));
        }
        return redirect()->back()->withMessage("Generating salary for user : ".$prepSalaryExecution->user->name);
    }

    public function userComponentsView($prepSalaryId, $userId)
    {
        $user = User::find($userId);
        $prepUsers = PrepUser::where('prep_salary_id',$prepSalaryId)->get();
        $prepObj = PrepSalary::find($prepSalaryId);
        $prepSalaryComponents = PrepSalaryComponent::where('prep_salary_id',$prepSalaryId)->get();
        $data = [];
        $exeObj = [];
        foreach ($prepSalaryComponents as $prepComponenet) {
            if($prepComponenet->type->code == 'user')
                continue;
            $strModelName = $prepComponenet->type->service_class;
            $strModelObj = new $strModelName;
            $strModelObj->setComponent($prepComponenet->id);
            $strModelObj->setUserId($userId);
            $data[$prepComponenet->type->code] = $strModelObj->getHtml();
            $exeObj = PrepSalaryExecution::where('prep_salary_id',$prepSalaryId)->where('user_id',$userId)->where('component_id',$prepComponenet->id)->first();
            $executions[$prepComponenet->type->code] = $exeObj->toArray();
        }
        return view('prep-salary.prep-salary.user-components-view', compact('data','executions','prepObj','user','prepUsers'));
    }

    public function userCompare($prepSalaryId, $userId)
    {
        $user = User::find($userId);
        $prepSalObj = PrepSalary::find($prepSalaryId);
        if($prepSalObj) {

            $prevMonthObj = Month::where('id','<',$prepSalObj->month_id)->orderBy('id', 'DESC')->first();

            $prevPrepSal = PrepSalary::where('month_id', $prevMonthObj->id)->first();
        }
        if (!$prevPrepSal) {
            return redirect()->back()->withErrors('Previous Month Prep Sal Not Found')->withInput();
        }
        $url = 'prep-salary/tds-comparison/{prepSalId}';
        $response = AppraisalService::getComparedPrepComponents($prepSalObj->id, $prevPrepSal->id, $url);
        $tdsData = [];
        $appData = [];
        if(array_key_exists($userId,$response['data']))
        {
            $tdsData = $response['data'][$userId];
            $test = SalaryService::getSalarySheetData($prepSalObj->id);
            $appraisalData['current'] = $test[$userId];
            $test = SalaryService::getSalarySheetData($prevPrepSal->id);
            if(array_key_exists($userId,$test))
            {
                $appraisalData['previous'] = $test[$userId];
            }
        }
        return view('prep-salary.prep-salary.user-sheet-compare', compact('appData','tdsData','appraisalData', 'prepSalObj', 'prevMonthObj','user'));
    }
}
