<?php

namespace App\Http\Controllers\SalaryDependency;

use App\Http\Controllers\Controller;
use App\Models\DateTime\Month;
use Illuminate\Http\Request;

class SalaryDependencyController extends Controller
{
    public function index($monthId=null)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($monthList) > 0) {
            return view('salary-dependency.index')->with('message', 'no month found.');
        }
        $currMonth = $monthList[0];
        if ($monthId) {
            $currMonth = Month::find($monthId);
        }
        return view('salary-dependency.index', compact('currMonth', 'monthList'));
    }
}
