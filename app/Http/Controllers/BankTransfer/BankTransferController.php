<?php

namespace App\Http\Controllers\BankTransfer;

use App\Http\Controllers\Controller;
use App\Jobs\BankTransfer\ReverseBankTransferJob;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\BankTransfer\BankTransfer;
use App\Models\BankTransfer\BankTransferHoldUser;
use App\Models\BankTransfer\BankTransferUser;
use App\Models\DateTime\Month;
use App\Models\Transaction;
use App\Models\Users\User;
use App\Services\Bank\BankTransferService;
use App\Services\SalaryService\SalaryService;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
//use Excel;
use Redirect;
use App\Jobs\BankTransaction\PT\PtUserDispatchJob;
use App\Jobs\BankTransaction\EsiPayment\EsiPaymentUserDispatchJob;
use App\Jobs\BankTransaction\PanPayment\PanPaymentUserDispatchJob;
use App\Jobs\BankTransaction\PfPayment\PfPaymentUserDispatchJob;
use App\Jobs\BankTransaction\PaymentTransfer\PaymentTransferUserDispatchJob;

class BankTransferController extends Controller
{
    public function index($monthId = null)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($monthList) > 0) {
            return view('bank-transfer.bank-transfer.index')->with('message', 'no month found.');
        }
        $currMonth = $monthList[0];
        if ($monthId) {
            $currMonth = Month::find($monthId);
        }
        $bankTransfers = BankTransfer::where('month_id', $currMonth->id)->get();
        return view('bank-transfer.bank-transfer.index', compact('currMonth', 'monthList', 'bankTransfers'));
    }

    public function complete($bankTransferId)
    {
        $bankTransfers = BankTransfer::find($bankTransferId);
        if (!$bankTransferId) {
            return Redirect::back()->withErrors('Bank Transfer Id not found!');
        }
        $overallStatus = 'pending';
        return view('bank-transfer.bank-transfer.complete', compact('bankTransfers', 'overallStatus'));
    }

    public function completeBankTransfer($bankTransferId)
    {
        
        $bankTransfer = BankTransfer::find($bankTransferId);
        if (!$bankTransferId) {
            return Redirect::back()->withErrors('Bank Transfer Id not found!');
        }
        if($bankTransfer->status == 'completed')
        {
            return Redirect::back()->withErrors('Already Transfered!');
        }
        
        dispatch(new PaymentTransferUserDispatchJob($bankTransferId));
        dispatch(new EsiPaymentUserDispatchJob($bankTransferId));
        dispatch(new PtUserDispatchJob($bankTransferId));
        dispatch(new PanPaymentUserDispatchJob($bankTransferId));
        dispatch(new PfPaymentUserDispatchJob($bankTransferId));
        $bankTransfer->status = 'completed';
        $bankTransfer->save();
        return redirect()->back()->withMessage('Completed');
    }

    public function showSalarySheetCompare($bankTransferId)
    {
        $bankTransferObj = BankTransfer::find($bankTransferId);
        if (!$bankTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $bankTransferId);
        }

        $monthObj = Month::find($bankTransferObj->month_id);
        if (!$monthObj) {
            return redirect()->back()->withErrors('Month Not Found')->withInput();
        }

        $response = SalaryService::getComparedBankTransfer($monthObj->id);
        if ($response['status'] == false) {
            return redirect()->back()->withErrors($response['errors'])->withInput();
        }

        $data = $response['data']['combined'];

        $users = User::orderBy('employee_id', 'ASC')->get();
        $apprCompType = AppraisalComponentType::all();
        $monthList = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();

        return view('bank-transfer.salary-sheet.compare', compact('data', 'users', 'apprCompType', 'monthList', 'bankTransferObj'));
    }

    public function upload(Request $request, $bankTransferId)
    {
        $bankTransferObj = BankTransfer::find($bankTransferId);
        if (!$bankTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $bankTransferId);
        }
        if ($request->file) {
            $response = BankTransferService::uploadFile($request->file, $bankTransferId);
            if (!$response || !$response['status']) {
                return Redirect::back()->withErrors('File Upload Failed!');
            }
        } else {
            return Redirect::back()->withErrors('File not found!');
        }

        if ($response['data']) {
            $result = BankTransferService::transferData($request->file, $bankTransferId);
            if ($result['status'] == false) {
                return Redirect::back()->withErrors($result['message']);
            }
        } else {
            return Redirect::back()->withErrors('File not found!');
        }

        return redirect('/bank-transfer/' . $bankTransferId . '/view-complete')->with('message', 'File Uploaded successfully!');
    }

    public function completeTransfer($bankTransferUserId)
    {
        $transferUserObj = BankTransferUser::find($bankTransferUserId);
        if (!$transferUserObj) {
            return Redirect::back()->withErrors('Bank Transfer User not found!');
        }

        if (!$transferUserObj->mode_of_transfer || $transferUserObj->mode_of_transfer == '') {
            return Redirect::back()->withErrors('Save Mode Of Transfer!');
        }
        if (!$transferUserObj->bank_transaction_id || $transferUserObj->bank_transaction_id == '') {
            return Redirect::back()->withErrors('Save Reference Number!');
        }

        $transferUserObj->status = "completed";

        if (!$transferUserObj->save()) {
            return Redirect::back()->withErrors('Data not saved!');
        }

        return Redirect::back()->with('message', 'Bank Transfer rejected!');
    }

    public function rejectTransfer($bankTransferUserId)
    {
        $transferUserObj = BankTransferUser::find($bankTransferUserId);
        if (!$transferUserObj) {
            return Redirect::back()->withErrors('Bank Transfer User not found!');
        }

        if (!$transferUserObj->comment || $transferUserObj->comment == '') {
            return Redirect::back()->withErrors('Save comment!');
        }

        $transferUserObj->status = "rejected";

        if (!$transferUserObj->save()) {
            return Redirect::back()->withErrors('Data not saved!');
        }
        dispatch(new ReverseBankTransferJob($bankTransferUserId));

        return Redirect::back()->with('message', 'Bank Transfer rejected!');
    }

    public function getHolds($bankTransferId)
    {
        $bankTransferHold = BankTransfer::find($bankTransferId);
        return view('bank-transfer.bank-transfer-hold.index', compact('bankTransferHold'));
    }

    public function release($bankTransferId, $bankTransferHoldId)
    {
        $currentDate = date("Y-m-d");
        $month = Month::getMonthByDate($currentDate);
        $bankTransferObj = BankTransferHoldUser::find($bankTransferHoldId);
        $check = BankTransferHoldUser::where('id', $bankTransferHoldId)->update(['is_processed' => 1]);
        if (!$check) {
            Redirect::back()->withError("Something went wrong");
        }

        ($bankTransferObj->amount > 0) ? $type = "credit" : $type = "debit";
        $transactionObj = Transaction::create(['month_id' => $month->id, 'financial_year_id' => $month->financial_year_id, 'user_id' => $bankTransferObj->user_id, 'reference_id' => $bankTransferObj->id, 'reference_type' => 'App\Models\Audited\BankTransfer\BankTransferHoldUser', 'amount' => $bankTransferObj->amount, 'type' => $type]);
        if (!$transactionObj->isValid()) {
            Redirect::back()->withError($transactionObj->getErrors());
        }

        return Redirect::back()->withMessage("Successfully Released");
    }

    public function showSalarySheet($bankTransferId)
    {
        $data = [];
        $bankTransferObj = BankTransfer::find($bankTransferId);

        if (!$bankTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $bankTransferId);
        }

        $data = Transaction::getSalarySheet($bankTransferObj->month_id);
        return view('bank-transfer.salary-sheet.index', compact('data'));
    }

    public function downloadSalarySheet($bankTransferId)
    {
        $bankTransferObj = BankTransfer::find($bankTransferId);

        if (!$bankTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $bankTransferId);
        }

        $data = Transaction::getSalarySheet($bankTransferObj->month_id);
        $filename = "Salary_Sheet_of_" . $bankTransferObj->month->formatMonth();
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
    }

}
