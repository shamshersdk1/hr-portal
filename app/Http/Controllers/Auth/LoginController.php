<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Redirect;
use View;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function showLogin(Request $request)
    {
        if (!empty($request->user)) {
            return Redirect::to('/users');
        }
        return View::make('auth.login');
    }
    public function doLogout()
    {
        Auth::logout();
        return Redirect::to('/');
    }
}
