<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Models\Bank\AccountMetaKey;
use App\Models\Bank\AccountType;
use Illuminate\Http\Request;

class AccountMetaKeyController extends Controller
{
    public function index()
    {
        $bankAccounts = AccountMetaKey::all();
        return view('bank.bank-account-meta-key.index', compact('bankAccounts'));
    }

    public function show($id)
    {
        $bankAccounts = Account::find($id);
        $bankTypes = AccountType::all();
        if (!$bankTypes && count($bankTypes) == 0) {
            return redirect()->back()->withErrors('No Bank type Found.')->withInput();
        }
        if (!$bankAccounts) {
            return redirect()->back()->withErrors('No Account Found.')->withInput();
        }
        return view('bank.bank-account-meta-key.show', compact('bankAccounts', 'bankTypes'));
    }
    public function create()
    {
        $bankTypes = AccountType::all();
        if (count($bankTypes) == 0) {
            return redirect()->back()->withErrors('No Account type Found. Please Add account type First')->withInput();
        }

        return view('bank.bank-account-meta-key.add', compact('bankTypes'));
    }

    public function edit($id)
    {
        $bankAccount = AccountMetaKey::find($id);
        if (!$bankAccount) {
            return redirect('bank/bank-account-meta-key')->withErrors('Account Meta Key Not found')->withInput();
        }
        $bankTypes = AccountType::all();
        if (count($bankTypes) == 0) {
            return redirect()->back()->withErrors('No Bank type Found .Please Add Bank type First')->withInput();
        }

        return view('bank.bank-account-meta-key.edit', compact('bankTypes', 'bankAccount'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $accountObj = AccountMetaKey::updateOrCreate(['id' => $id], ['bank_account_type_id' => $data['bank_account_type_id'], 'key' => $data['key'], 'is_required' => $data['is_required'], 'field_type' => $data['field_type']]);
        if ($accountObj->isInvalid()) {
            return redirect()->back()->withErrors($accountObj->getErrors())->withInput();
        }
        return redirect('bank/bank-account-meta-key')->with('message', 'Updated!');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (!$data) {
            return redirect()->back()->withErrors('Input Not Found')->withInput();
        }

        $backAccount = AccountMetaKey::create(['bank_account_type_id' => $data['bank_account_type_id'], 'key' => $data['key'], 'is_required' => $data['is_required'], 'field_type' => $data['field_type']]);
        if ($backAccount->isValid()) {
            return redirect('bank/bank-account-meta-key')->withMessage('Saved Successfully');
        } else {
            return redirect()->back()->withErrors($backAccount->getErrors())->withInput();
        }
    }

    public function destroy($id)
    {
        $backAccount = AccountMetaKey::find($id);
        if ($backAccount && $backAccount->delete()) {
            return redirect('/bank/bank-account-meta-key')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Account Meta Key Not Found!')->withInput();
    }
}
