<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Models\Bank\AccountType;
use Illuminate\Http\Request;
use Redirect;

class AccountTypeController extends Controller
{
    public function index()
    {
        $bankAccountTypes = AccountType::all();
        if (!$bankAccountTypes) {
            return Redirect::back()->withInput()->withErrors(['No Account types found!']);
        }
        return view('bank.bank-account-type.index', compact('bankAccountTypes'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $bankTypeObj = AccountType::create(['name' => $data['name'], 'code' => $data['code'],'is_editable' => array_key_exists("is_editable", $data) ? $data['is_editable'] ? $data['is_editable'] : false : null]);
        if ($bankTypeObj->isInvalid()) {
            return redirect('/bank/bank-account-type')->withErrors($bankTypeObj->getErrors())->withInput();
        }
        return redirect('/bank/bank-account-type')->with('message', 'Save successfully!');
    }

    public function show($id)
    {
        $bankType = AccountType::find($id);
        if (!$bankType) {
            return redirect('/bank/bank-account-type')->withErrors('Account type not found!')->withInput();
        }
        return view('bank.bank-account-type.edit', compact('bankType'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $bankTypeObj = AccountType::updateOrCreate(['id' => $id], ['name' => $data['name'], 'code' => $data['code']]);
        if ($bankTypeObj->isInvalid()) {
            return redirect('/bank/bank-account-type')->withErrors($bankTypeObj->getErrors())->withInput();
        }
        return redirect('/bank/bank-account-type')->with('message', 'Updated!');
    }

    public function destroy($id)
    {
        $bankTypeObj = AccountType::find($id);
        if ($bankTypeObj && $bankTypeObj->delete()) {
            return redirect('/bank/bank-account-type')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
