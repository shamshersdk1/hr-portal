<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Models\Bank\Account;
use App\Models\Bank\AccountMetaKey;
use App\Models\Bank\AccountMetaValue;
use App\Models\Bank\AccountType;
use App\Services\Bank\BankService;
use DB;
use Exception;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index()
    {
        $bankAccounts = Account::all();
        return view('bank.bank-account.index', compact('bankAccounts'));
    }

    public function show($id)
    {
        $bankAccount = Account::find($id);
        if (!$bankAccount) {
            return redirect('bank/bank-account')->withErrors('Account Not found')->withInput();
        }

        $keys = AccountMetaKey::where('bank_account_type_id', $bankAccount->bank_account_type_id)->where('is_required', 1)->get();

        return view('bank.bank-account.show', compact('bankAccount', 'keys'));
    }

    public function create($accountTypeId = null, $referId = null)
    {
        $bankAccountObj = null;
        $bankTypes = [];
        $referenceFor = [];
        $referType = null;
        $referArray = null;
        $keys = null;

        if (($accountTypeId != null && $referId == null) || ($referId != null && $accountTypeId == null)) {
            return redirect()->back()->withErrors('Please enter both the values.')->withInput();
        }

        if ($accountTypeId == null && $referId == null) {
            $bankTypes = AccountType::all();
            if (count($bankTypes) == 0) {
                return redirect()->back()->withErrors('No Account type Found. Please Add Bank type First')->withInput();
            }
            $referenceFor = BankService::getReferenceForArray();
        } else {
            $bankAccountObj = AccountType::find($accountTypeId);
            if ($bankAccountObj == null) {
                return redirect()->back()->withErrors('Account type not Found. Please try again!')->withInput();
            }

            $referenceFor = BankService::getReferenceForArray();
            $referType = BankService::getReferenceType($referenceFor[$referId]);

            $referArray = BankService::getReferenceKeysArray($referenceFor[$referId]);

            $keys = AccountMetaKey::where('bank_account_type_id', $accountTypeId)->where('is_required', 1)->get();
        }

        return view('bank.bank-account.add', compact('bankTypes', 'referenceFor', 'accountTypeId', 'bankAccountObj', 'referId', 'referType', 'referArray', 'keys'));
    }

    public function edit($id)
    {
        $bankAccount = Account::find($id);
        if (!$bankAccount) {
            return redirect('bank/bank-account')->withErrors('Account Not found')->withInput();
        }

        $refer = BankService::getNameFromReferenceType($bankAccount->reference_type);
        $referArray = BankService::getReferenceKeysArray($refer);
        $keys = AccountMetaKey::where('bank_account_type_id', $bankAccount->bank_account_type_id)->where('is_required', 1)->get();

        return view('bank.bank-account.edit', compact('bankAccount', 'referArray', 'keys'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        DB::beginTransaction();
        try {
            $accountObj = Account::updateOrCreate(['id' => $id], ['account_number' => $data['account_number'], 'reference_id' => $data['reference_id']]);
            if ($accountObj->isInvalid()) {
                return redirect()->back()->withErrors($accountObj->getErrors())->withInput();
            } else {
                if (!isset($data['meta'])) {
                    DB::commit();
                    return redirect('bank/bank-account')->withMessage('Updated.');
                }

                $metaStatus = AccountMetaValue::updateData($data, $accountObj->id);
                if ($metaStatus == true) {
                    DB::commit();
                    return redirect('bank/bank-account')->withMessage('Updated');
                } else {
                    throw new Exception("Unable to update.");
                }
            }
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (!$data) {
            return redirect()->back()->withErrors('Input Not Found')->withInput();
        }
        if (!$data['bank_account_type_id']) {
            return redirect()->back()->withErrors('Account type not found')->withInput();
        }

        $accountTypeObj = AccountType::where('name', $data['bank_account_type_id'])->first();
        if (!$accountTypeObj) {
            return redirect()->back()->withErrors('Account type not found')->withInput();
        }
        $accountTypeId = $accountTypeObj->id;

        DB::beginTransaction();
        try {
            $backAccount = Account::create(['bank_account_type_id' => $accountTypeId, 'account_number' => $data['account_number'], 'reference_type' => $data['reference_type'], 'reference_id' => $data['reference_id']]);
            if ($backAccount->isValid()) {

                if (!isset($data['meta'])) {
                    DB::commit();
                    return redirect('bank/bank-account')->withMessage('Saved Successfully without any meta values.');
                }
                $metaStatus = AccountMetaValue::saveData($data, $backAccount->id);
                if ($metaStatus == true) {
                    DB::commit();
                    return redirect('bank/bank-account')->withMessage('Saved Successfully');
                } else {
                    throw new Exception("Unable to save.");
                }

            } else {
                return redirect()->back()->withErrors($backAccount->getErrors())->withInput();
            }

        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    public function destroy($id)
    {
        $backAccount = Account::find($id);
        if ($backAccount && $backAccount->delete()) {
            return redirect('/bank/bank-account')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Account Not Found!')->withInput();
    }
}
