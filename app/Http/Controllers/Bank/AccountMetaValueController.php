<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Models\Bank\AccountMetaValue;
use Illuminate\Http\Request;

class AccountMetaValueController extends Controller
{
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $accountObj = AccountMetaValue::updateOrCreate(['id' => $id], ['back_account_id' => $data['back_account_id'], 'back_account_meta_key_id' => $data['back_account_meta_key_id'], 'value' => $data['value']]);
        if ($accountObj->isInvalid()) {
            return redirect()->back()->withErrors($accountObj->getErrors())->withInput();
        }
        return redirect('bank/bank-account')->with('message', 'Updated!');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (!$data) {
            return redirect()->back()->withErrors('Input Not Found')->withInput();
        }

        $backAccount = AccountMetaValue::create(['back_account_id' => $data['back_account_id'], 'back_account_meta_key_id' => $data['back_account_meta_key_id'], 'value' => $data['value']]);
        if ($backAccount->isValid()) {
            return redirect('bank/bank-account')->withMessage('Saved Successfully');
        } else {
            return redirect()->back()->withErrors($backAccount->getErrors())->withInput();
        }
    }

    public function destroy($id)
    {
        $backAccount = AccountMetaValue::find($id);
        if ($backAccount && $backAccount->delete()) {
            return redirect('/bank/bank-account')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Account Meta Key Not Found!')->withInput();
    }
}
