<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Models\Bank\PaymentTransfer;
use Redirect;
use Illuminate\Http\Request;
use App\Services\SalaryService\SalaryService;
use App\Models\Bank\PaymentTransferAccount;

class PaymentTransferController extends Controller
{
    public function index($accountTypeId = null)
    {
        if ($accountTypeId == null) {
            $paymentsTransfers = PaymentTransfer::orderBy('created_at', 'DESC')->get();
        } else {
            $paymentsTransfers = PaymentTransfer::where('account_type_id', $accountTypeId)->orderBy('created_at', 'DESC')->get();
        }

        if (!$paymentsTransfers) {
            return Redirect::back()->withInput()->withErrors(['Payment Transfers not found!']);
        }
        return view('bank.payment-transfer.index', compact('paymentsTransfers', 'accountTypeId'));
    }

    public function show($id)
    {
        $paymentsTransfer = PaymentTransfer::find($id);
        if (!$paymentsTransfer) {
            return Redirect::back()->withInput()->withErrors(['Payment Transfer not found!']);
        }

        return view('bank.payment-transfer.show', compact('paymentsTransfer', 'id'));
    }

    public function update(Request $request, $paymentTransferId)
    {
        $status = false;
        $errors = '';
        $data = request('data');
        foreach($data as $key => $row)
        {
            $payemntTransferAccountObj = PaymentTransferAccount::find($key);
            $payemntTransferAccountObj->account_number = $row['account_number'];
            $payemntTransferAccountObj->amount = $row['amount'];
            $payemntTransferAccountObj->transaction_amount = $row['transaction_amount'];
            $payemntTransferAccountObj->transaction_id = $row['transaction_id'];
            $payemntTransferAccountObj->ifsc_code = $row['ifsc'];
            if(!$payemntTransferAccountObj->save())
            {
                $status = true;
                $errors = $errors.$payemntTransferAccountObj->getErrors();
            }
        }
        if($status)
        {
            return redirect()->back()->withErrors($errors);
        }
        return redirect()->back()->withMessage('Update Successfully');
    }

    public function upload(Request $request, $paymentsTransferId)
    {
        $paymentTransferObj = PaymentTransfer::find($paymentsTransferId);
        if (!$paymentTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $paymentsTransferId);
        }
        if ($request->file) {
            $response = SalaryService::uploadFileDuePayment($request->file, $paymentsTransferId);
            if (!$response || !$response['status']) {
                return Redirect::back()->withErrors('File Upload Failed!');
            }
        } else {
            return Redirect::back()->withErrors('File not found!');
        }

        if ($response['data']) {
            $result = SalaryService::paymentTransferData($request->file, $paymentsTransferId);
            if ($result['status'] == false) {
                return Redirect::back()->withErrors($result['message']);
            }
        } else {
            return Redirect::back()->withErrors('File not found!');
        }

        return redirect('/payment-transfers/' . $paymentsTransferId )->with('message', 'File Uploaded successfully!');
    }
}
