<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use App\Models\Bank\AccountType;
use App\Models\Bank\BankTransaction;
use App\Models\Bank\PaymentTransfer;
use App\Models\Bank\PaymentTransferAccount;
use App\Models\DateTime\FinancialYear;
use App\Models\DateTime\Month;
use DB;
use Exception;
use Illuminate\Http\Request;
use Redirect;

class BankTransactionController extends Controller
{
    //bank-transaction
    public function index($accountType = null)
    {
        $year = date('Y');
        $financialYearObj = FinancialYear::where('status', 'running')->orderBy('year', 'DESC')->first();
        if (!$financialYearObj) {
            return Redirect::back()->withInput()->withErrors(['Financial year not found!']);
        }

        $financialYearId = $financialYearObj->id;

        $accountTypeObj = null;
        if ($accountType == null) {
            $accountTypeObj = AccountType::where('code', 'saving-account')->first();
            if ($accountTypeObj) {
                $accountType = $accountTypeObj->id;
            }
        } else {
            $accountTypeObj = AccountType::find($accountType);
        }
        $transactions = BankTransaction::where('financial_year_id', $financialYearId)->where('account_type_id', $accountType)->groupBy('account_id', 'account_type_id')->selectRaw('sum(amount) as amountPayable, account_id, account_type_id, sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();

        if (!$accountTypeObj) {
            return Redirect::back()->withInput()->withErrors(['Account Type not found!']);
        }

        if (!$transactions) {
            return Redirect::back()->withInput()->withErrors(['No data found!']);
        }

        $accountTypeList = AccountType::all();
        if (!$accountTypeList) {
            return Redirect::back()->withInput()->withErrors(['No account type found!']);
        }

        return view('bank.bank-transaction.index', compact('transactions', 'accountTypeList', 'accountType', 'accountTypeObj'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (!isset($data['options'])) {
            return redirect('/bank/bank-transaction')->withErrors('Select atleast one account.')->withInput();
        }

        $currMonth = date('m');
        $currYear = date('Y');
        $monthObj = Month::where('month', $currMonth)->where('year', $currYear)->first();
        if (!$monthObj) {
            return redirect('/bank/bank-transaction')->withErrors('Month not found.')->withInput();
        }
        $financialYearObj = FinancialYear::where('status', 'running')->orderBy('year', 'DESC')->first();
        if (!$financialYearObj) {
            return redirect('/bank/bank-transaction')->withErrors('Financial Year not found.')->withInput();
        }

        if ($data['account_type_id'] == 0) {
            return redirect('/bank/bank-transaction')->withErrors('Select only one account type.')->withInput();
        }

        DB::beginTransaction();
        try {
            $payment = PaymentTransfer::create(['month_id' => $monthObj->id, 'account_type_id' => $data['account_type_id'], 'status' => 'initiated']);
            if ($payment->isValid()) {
                foreach ($data['options'] as $account => $amount) {
                    $paymentAccount = PaymentTransferAccount::create(['payment_transfer_id' => $payment->id, 'account_id' => $account, 'amount' => $amount]);
                    if ($paymentAccount->isValid()) {
                        //reverse payment
                        $bankTransaction = BankTransaction::create(['financial_year_id' => $financialYearObj->id, 'month_id' => $monthObj->id, 'account_id' => $account, 'account_type_id' => $data['account_type_id'], 'amount' => (-1) * $amount, 'reference_type' => 'App\Models\Bank\PaymentTransfer', 'reference_id' => $payment->id]);
                        if ($bankTransaction->isValid()) {

                        } else {
                            throw new Exception($bankTransaction->getErrors());
                        }
                    } else {
                        throw new Exception($paymentAccount->getErrors());
                    }
                }
                DB::commit();
                return redirect('bank/payment-transfers')->withMessage('Saved Successfully');
            } else {
                return redirect()->back()->withErrors($payment->getErrors())->withInput();
            }
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        }
    }

    //due-payments
    public function showPaymentIndex()
    {
        $accountTypeList = AccountType::all();
        if (!$accountTypeList) {
            return Redirect::back()->withInput()->withErrors(['No account type found!']);
        }

        return view('bank.bank-transaction.payment-index', compact('accountTypeList'));
    }

    public function storePendingTransactions(Request $request, $id)
    {
        $data = $request->all();
        if (!isset($data['options'])) {
            return redirect('/bank/due-payments/' . $id)->withErrors('Select atleast one account.')->withInput();
        }

        $response = PaymentTransfer::transferAmount($data['options'], $id);

        if ($response['status'] == true) {
            return redirect('/bank/due-payments/' . $id)->withMessage('Payments settled successfully.');
        } else {
            return redirect('/bank/due-payments/' . $id)->withErrors($response['errors'])->withInput();
        }
    }

    public function showTransactions(Request $request, $id)
    {
        $data = $request->all();
        if (!isset($data['options'])) {
            return redirect('/bank/due-payments/' . $id)->withErrors('Select atleast one account.')->withInput();
        }

        $accountTypeObj = AccountType::find($id);
        if (!$accountTypeObj) {
            return Redirect::back()->withInput()->withErrors(['Account type not found!']);
        }

        $year = date('Y');
        $financialYearObj = FinancialYear::where('status', 'running')->orderBy('year', 'DESC')->first();
        if (!$financialYearObj) {
            return Redirect::back()->withInput()->withErrors(['Financial year not found!']);
        }
        $financialYearId = $financialYearObj->id;

        $transactions = BankTransaction::where('financial_year_id', $financialYearId)->where('account_type_id', $id)->groupBy('account_id', 'account_type_id')->selectRaw('sum(amount) as amountPayable, account_id, account_type_id, sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
        if (!$transactions) {
            return Redirect::back()->withInput()->withErrors(['No data found!']);
        }

        return view('bank.bank-transaction.transfer-preview', compact('data', 'accountTypeObj', 'transactions'));
    }

    public function showPendingAccountTransactions($id)
    {
        $accountTypeObj = AccountType::find($id);
        if (!$accountTypeObj) {
            return Redirect::back()->withInput()->withErrors(['Account type not found!']);
        }

        $year = date('Y');
        $financialYearObj = FinancialYear::where('status', 'running')->orderBy('year', 'DESC')->first();
        if (!$financialYearObj) {
            return Redirect::back()->withInput()->withErrors(['Financial year not found!']);
        }
        $financialYearId = $financialYearObj->id;

        $transactions = BankTransaction::where('financial_year_id', $financialYearId)->where('account_type_id', $id)->groupBy('account_id', 'account_type_id')->selectRaw('sum(amount) as amountPayable, account_id, account_type_id, sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
        if (!$transactions) {
            return Redirect::back()->withInput()->withErrors(['No data found!']);
        }

        return view('bank.bank-transaction.pending-account', compact('accountTypeObj', 'transactions'));
    }

    public function downloadSalarySheet($bankTransferId)
    {
        $bankTransferObj = BankTransfer::find($bankTransferId);

        if (!$bankTransferObj) {
            return Redirect::back()->withErrors('Invalid Bank Transfer #' . $bankTransferId);
        }

        $data = Transaction::getSalarySheet($bankTransferObj->month_id);
        $filename = "Salary_Sheet_of_" . $bankTransferObj->month->formatMonth();
        $export_data = $data;
        return Excel::create($filename, function ($excel) use ($export_data) {
            $excel->sheet('mySheet', function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data);
            });
        })->download('csv');
    }
}
