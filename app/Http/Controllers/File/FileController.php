<?php

namespace App\Http\Controllers\File;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\File\File;

class FileController extends Controller
{
    public function streamFile($file_id)
    {
        $file = File::find($file_id);
        if (!empty($file)) {
            return response($file->fileContent->content, 200)->withHeaders(array(
                'Content-Description' => 'File Transfer',
                'Content-Type' => 'application/octet-stream',
                'Content-Disposition' => 'attachment; filename="' . $file->name . '"',
            ));
        }
    }
}
