<?php

namespace App\Http\Controllers\Salary;

use App\Http\Controllers\Controller;
use App\Jobs\BankTransfer\BankTransferHoldJob;
use App\Jobs\BankTransfer\BankTransferJob;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\BankTransfer\BankTransfer;
use App\Models\Bank\AccountType;
use App\Models\DateTime\Month;
use App\Models\Transaction;
use App\Models\Users\User;
use DB;
use Illuminate\Http\Request;
use Redirect;
use App\Models\BankTransfer\BankTransferHoldUser;

class SalaryTransferController extends Controller
{
    public function index($monthId = null)
    {
        $months = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($months) > 0) {
            return view('pages.salary-transfer.index')->with('message', 'no month found.');
        }
        $transactions = Transaction::with('user')->where('is_company_expense', 0)->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
        $month = null;
        if ($monthId > 0) {
            $month = Month::find($monthId);
            $transactions = Transaction::with('user')->where('is_company_expense', 0)->where('month_id', $monthId)->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
        }
        $accountType = AccountType::where('code', 'saving-account')->first();
        $accountTypeId = null;
        if ($accountType) {
            $accountTypeId = $accountType->id;
        }
        $holds = BankTransferHoldUser::where('is_processed', 0)->get();

        return view('salary-transfer.index', compact('transactions', 'months', 'month', 'accountTypeId','holds'));
    }
    public function finaliseTransfer(Request $request)
    {
        $holds = $request->hold;
        $process = $request->process;
        $currentDate = date("Y-m-d");
        $month = Month::getMonthByDate($currentDate);
        $bankTransferObj = BankTransfer::Create(['month_id' => $month->id, 'status' => 'open']);
        if ($holds) {
            foreach ($holds as $index => $hold) {
                dispatch(new BankTransferHoldJob($bankTransferObj->id, $index));
            }
        }
        if ($process) {
            foreach ($process as $index => $proce) {
                dispatch(new BankTransferJob($bankTransferObj->id, $index));
            }
        }

        return redirect('/salary-transfer');
    }
    public function getBankData($monthId)
    {
        $bankTransfer = BankTransfer::where('month_id', $monthId)->first();
        $holdUsers = $bankTransfer->holdUsers->unique('user_id');
        $transferUsers = $bankTransfer->transferUsers->unique('user_id');
        return view('pages.salary-transfer.bank-data-status', compact('bankTransfer', 'holdUsers', 'transferUsers'));
    }
    public function preview(Request $request, $monthId = null)
    {
        $checkedUsers = $request->options;

        $transactionsTransferUsers = [];
        if ($checkedUsers) {
            $transactionsTransferUsers = Transaction::where('is_company_expense', 0)->whereIn('user_id', array_values($checkedUsers))->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
            $transactionsHoldUsers = Transaction::where('is_company_expense', 0)->whereNotIn('user_id', array_values($checkedUsers))->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
        } else {
            // $transactionsTransferUsers = Transaction::where('is_company_expense', 0)->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
            $transactionsHoldUsers = Transaction::where('is_company_expense', 0)->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
        }

        $monthObj = null;
        if ($monthId) {
            $monthObj = Month::find($monthId);
            if (!$monthObj) {
                return Redirect::back()->withErrors(['Month not found']);
            }

            if ($checkedUsers) {
                $transactionsTransferUsers = Transaction::where('is_company_expense', 0)->where('month_id', $monthId)->whereIn('user_id', array_values($checkedUsers))->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
                $transactionsHoldUsers = Transaction::where('is_company_expense', 0)->where('month_id', $monthId)->whereNotIn('user_id', array_values($checkedUsers))->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
            } else {
                // $transactionsTransferUsers = Transaction::where('is_company_expense', 0)->where('month_id', $monthId)->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
                $transactionsHoldUsers = Transaction::where('is_company_expense', 0)->where('month_id', $monthId)->groupBy('user_id')->selectRaw('sum(amount) as amountPayable, user_id,sum(case when amount>=0 then amount else 0 end) as totalAllowance,sum(case when amount<0 then amount else 0 end) as totalDeduction')->get();
            }

        }
        return view('salary-transfer.preview', compact('transactionsTransferUsers', 'transactionsHoldUsers', 'monthObj'));
    }
    public function downloadCSV($monthId = null)
    {
        $users = User::orderBy('employee_id', 'ASC')->get();
        if (!$users || empty($users)) {
            return Redirect::back()->withErrors(['Users not found']);
        }
        $data = [];
        if (!$monthId) {
            //here
            $inProgressMonthId = Month::getInProgressMonth();
            if (!$inProgressMonthId) {
                return Redirect::back()->withErrors(['In Progress month id not found!!']);
            }
            $monthObj = Month::find($inProgressMonthId);
            if (!$monthObj) {
                return Redirect::back()->withErrors(['In Progress month object not found!!']);
            }
            // $monthObj = Month::where('status', 'in_progress')->orderBy('year', 'desc')->orderBy('month', 'desc')->first();
            $monthId = $monthObj->id;
        } else {
            $monthObj = Month::find($monthId);
        }
        $trans = DB::table('transactions')
            ->select('user_id', 'reference_type', 'reference_id', DB::raw('SUM(amount) as amount'))
            ->where('month_id', $monthId)
            ->groupBy('user_id', 'reference_type', 'reference_id')
            ->orderBy('reference_type', 'ASC')
            ->orderBy('amount', 'DESC')
            ->get();

        foreach ($trans as $transaction) {
            $userObj = User::find($transaction->user_id);
            $data[$userObj->id]['ins-deduction'] = 0;
            $data[$userObj->id]['food-deduction'] = 0;
            $data[$userObj->id]['adhoc-payment'] = 0;
            $data[$userObj->id]['loan-deduction'] = 0;
            $data[$userObj->id]['vpf'] = 0;
            $data[$user->id]['onsite'] = 0;
            $data[$user->id]['additional-workday'] = 0;
            $data[$user->id]['extra-hour'] = 0;
            $data[$user->id]['tech-talk'] = 0;
            $data[$user->id]['performance'] = 0;
            $data[$user->id]['referral'] = 0;
            $data[$user->id]['other-bonus'] = 0;

            if ($transaction->reference_type == 'App\Models\Appraisal\AppraisalComponentType') {
                $data[$userObj->id]['appraisal-comp'][$transaction->reference_id] = $transaction->amount;
            } else if ($transaction->reference_type == 'App\Models\Admin\Insurance\InsuranceDeduction') {
                $data[$userObj->id]['ins-deduction'] += $transaction->amount;
            } else if ($transaction->reference_type == 'App\Models\Audited\FoodDeduction\FoodDeduction') {
                $data[$userObj->id]['food-deduction'] += $transaction->amount;
            } else if ($transaction->reference_type == 'App\Models\Admin\Bonus') {
                $bonusObj = Bonus::find($transaction->reference_id);
                if ($bonusObj) {
                    if ($bonusObj->type == 'onsite') {
                        $data[$user->id]['onsite'] += $transaction->amount;
                    } else if ($bonusObj->type == 'additional') {
                        $data[$user->id]['additional-workday'] += $transaction->amount;
                    } else if ($bonusObj->type == 'extra') {
                        $data[$user->id]['extra-hour'] += $transaction->amount;
                    } else if ($bonusObj->type == 'techtalk') {
                        $data[$user->id]['tech-talk'] += $transaction->amount;
                    } else if ($bonusObj->type == 'performance') {
                        $data[$user->id]['performance'] += $transaction->amount;
                    } else if ($bonusObj->type == 'referral') {
                        $data[$user->id]['referral'] += $transaction->amount;
                    } else {
                        $data[$user->id]['other-bonus'] += $transaction->amount;
                    }
                }
            } else if ($transaction->reference_type == 'App\Jobs\PrepareSalary\AdhocPaymentJob') {
                $data[$userObj->id]['adhoc-payment'] += $transaction->amount;
            } else if ($transaction->reference_type == 'App\Models\Appraisal\AppraisalBonus') {
                $apprBonusObj = AppraisalBonus::find($transaction->reference_id);
                if ($apprBonusObj) {
                    $data[$userObj->id]['appraisal-bonus'][$apprBonusObj->appraisal_bonus_type_id] = $transaction->amount;
                }
            } else if ($transaction->reference_type == 'App\Jobs\PrepareSalary\LoanDeductionJob') {
                $data[$userObj->id]['loan-deduction'] += $transaction->amount;
            } else if ($transaction->reference_type == 'App\Jobs\PrepareSalary\VpfJob') {
                $data[$userObj->id]['vpf'] += $transaction->amount;
            }
        }
        $apprCompType = AppraisalComponentType::all();
        $appBonusType = AppraisalBonusType::all();
        $months = Month::orderBy('year', 'DESC')->orderBy('month', 'DESC')->get();

        $transUsers = DB::select(DB::raw("SELECT DISTINCT user_id
            from transactions WHERE is_company_expense = 0 order by user_id"));
        $userArray = [];
        foreach ($transUsers as $user) {
            $userArray[$user->user_id] = $user->user_id;
        }
        return view('pages.salary.download-csv', compact('userArray', 'users', 'monthObj', 'months', 'apprCompType', 'data', 'appBonusType'));
    }
}
