<?php

namespace App\Http\Controllers\Appraisal;

use App\Http\Controllers\Controller;
use App\Models\Appraisal\AppraisalType;
use Illuminate\Http\Request;

class AppraisalTypeController extends Controller
{
    public function index()
    {
        $appraisalTypes = AppraisalType::all();
        return view('appraisal.appraisal-type.index', compact('appraisalTypes'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $appraisalTypeObj = AppraisalType::create(['name' => $data['name'], 'code' => $data['code']]);
        if ($appraisalTypeObj->isInvalid()) {
            return redirect('/appraisal/appraisal-type')->withErrors($appraisalTypeObj->getErrors())->withInput();
        }
        return redirect('/appraisal/appraisal-type')->with('message', 'save successfully!');
    }

    public function show($id)
    {
        $appraisalType = AppraisalType::find($id);
        if (!$appraisalType) {
            return redirect('/appraisal/appraisal-type')->withErrors('Appraisal type not found!')->withInput();
        }
        return view('appraisal.appraisal-type.edit', compact('appraisalType'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $appraisalTypeObj = AppraisalType::updateOrCreate(['id' => $id], ['name' => $data['name'], 'code' => $data['code']]);
        if ($appraisalTypeObj->isInvalid()) {
            return redirect('/appraisal/appraisal-type')->withErrors($appraisalTypeObj->getErrors())->withInput();
        }
        return redirect('/appraisal/appraisal-type')->with('message', 'Updated!');
    }

    public function destroy($id)
    {
        $appraisalTypeObj = AppraisalType::find($id);
        if ($appraisalTypeObj && $appraisalTypeObj->delete()) {
            return redirect('/appraisal/appraisal-type')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Something went wrong!')->withInput();
    }
}
