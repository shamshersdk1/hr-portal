<?php

namespace App\Http\Controllers\Appraisal;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidateAppraisalRequest;
use App\Models\Appraisal\Appraisal;
use App\Models\Appraisal\AppraisalBonus;
use App\Models\Appraisal\AppraisalBonusType;
use App\Models\Appraisal\AppraisalComponent;
use App\Models\Appraisal\AppraisalComponentType;
use App\Models\Appraisal\AppraisalType;
use App\Models\Users\User;
use DB;
use App\Models\DateTime\Month;
use App\Events\AppraisalUpdate;

class AppraisalController extends Controller
{
    public function index()
    {
        $appraisals = Appraisal::with(['user:id,name,employee_id,is_active,joining_date,confirmation_date','type:id,name'])->whereDate('effective_date','<=',date("Y-m-t"))->orderBy('effective_date','desc')->get()->unique('user_id');
        return view('appraisal.appraisal.index', compact('appraisals'));
    }

    public function show($userId)
    {
        $user = User::find($userId);
        $appraisals = Appraisal::where('user_id',$userId)->orderBy('effective_date','desc')->get();
        $bonusTypes = AppraisalBonusType::all();
        if (!$bonusTypes && !count($bonusTypes) > 0) {
            return redirect()->back()->withErrors('No Bonus type Found.')->withInput();
        }
        if (!count($appraisals) > 0) {
            return redirect()->back()->withErrors('No Appraisal type Found.')->withInput();
        }
        return view('appraisal.appraisal.show', compact('appraisals', 'bonusTypes','user'));
    }
    public function create()
    {
        $appraisalTypes = AppraisalType::all();
        if (!count($appraisalTypes) > 0) {
            return redirect()->back()->withErrors('No Appraisal type Found .Please Add Appraisal type First')->withInput();
        }
        $appraisalComponentTypes = AppraisalComponentType::all();
        if (!$appraisalComponentTypes || empty($appraisalComponentTypes)) {
            return redirect()->back()->withErrors('No Appraisal type Found.')->withInput();
        }
        $users = User::where('is_active', 1)->orderBy('name', 'asc')->get();
        if (!$users && empty($users)) {
            return redirect()->back()->withErrors('No User Found.')->withInput();
        }
        $appraisalBonusType = AppraisalBonusType::all();
        return view('appraisal.appraisal.add', compact('appraisalComponentTypes', 'users', 'appraisalTypes', 'appraisalBonusType'));

    }

    public function edit($id)
    {
        $appraisal = Appraisal::with('user', 'type')->find($id);
        if (!$appraisal) {
            return redirect('appraisal')->withErrors('Appraisal Not found')->withInput();
        }
        $users = User::where('is_active', 1)->orderBy('name', 'asc')->get();
        $appraisalTypes = AppraisalType::all();
        $appraisalComponentTypes = AppraisalComponentType::where('is_computed', 0)->get();
        $appraisalComponent = $appraisal->appraisalComponent;
        $appraisalComponentObj = [];
        foreach ($appraisalComponentTypes as $index => $type) {
            if ($type->is_computed == 0) {
                $appraisalComponentObj[$index]['name'] = $type->name;
                $appraisalComponentObj[$index]['id'] = $type->id;
                $appraisalComponentObj[$index]['value'] = 0;
                $appraisalComponentObj[$index]['is_computed'] = $type->is_computed;
                $appraisalBonusType = AppraisalBonusType::all();
                foreach ($appraisalComponent as $component) {
                    if ($component->component_id === $type->id) {
                        $appraisalComponentObj[$index]['value'] = $component->value;
                    }
                }
            }
        }
        $appraisalBonus = $appraisal->appraisalBonus;
        $appraisalBonusObj = [];
        if ($appraisalBonusType && count($appraisalBonusType) > 0) {
            foreach ($appraisalBonusType as $index => $type) {
                $appraisalBonusObj[$index]['name'] = $type->description;
                $appraisalBonusObj[$index]['id'] = $type->id;
                $appraisalBonusObj[$index]['value'] = 0;
                $appraisalBonusObj[$index]['value_date'] = 0;
                foreach ($appraisalBonus as $bonus) {
                    if ($bonus->appraisal_bonus_type_id === $type->id) {
                        $appraisalBonusObj[$index]['value'] = $bonus->value;
                        $appraisalBonusObj[$index]['value_date'] = $bonus->value_date;

                    }
                }
            }
        }
        return view('appraisal.appraisal.edit', compact('appraisal', 'users', 'appraisalTypes', 'appraisalComponentTypes', 'appraisalComponent', 'appraisalComponentObj', 'appraisalBonusType', 'appraisalBonusObj'));
    }

    public function update(ValidateAppraisalRequest $request, $id)
    {
        $data = $request->all();
        $appraisalObj = Appraisal::updateOrCreate(['id' => $id], ['user_id' => $data['user_id'], 'effective_date' => $data['effective_date'], 'type_id' => $data['type_id']]);
        if ($appraisalObj->isInvalid()) {
            return redirect()->back()->withErrors($appraisalObj->getErrors())->withInput();
        }
        if (isset($data['component'])) {
            $appraisalComponent = $data['component'];
            foreach ($appraisalComponent as $key => $component) {
                if (isset($component)) {
                    $appraisalComponent = AppraisalComponent::updateOrCreate(['appraisal_id' => $appraisalObj->id, 'component_id' => $key],
                        ['appraisal_id' => $appraisalObj->id, 'component_id' => $key, 'value' => $component]);
                    if ($appraisalComponent->isInvalid()) {
                        return redirect()->back()->withErrors($appraisalComponent->getErrors())->withInput();
                    }
                }
            }
        }
        $errors = "";
        $bonusStatus = false;
        if (isset($data['bonus'])) {
            $bonuses = $data['bonus'];
            foreach ($bonuses as $key => $bonus) {
                $obj = AppraisalBonus::where('appraisal_id',$appraisalObj->id)->where('appraisal_bonus_type_id', $key)->first();
                if($obj)
                {
                    $obj->appraisal_id = $appraisalObj->id;
                    $obj->appraisal_bonus_type_id = $key;
                    $obj->value = $bonus['value'] ? $bonus['value'] : 0;
                    $obj->value_date = !empty($bonus['value_date']) ? $bonus['value_date'] : null;
                    if(!$obj->save())
                    {
                        $errors = $errors.$obj->getErrors();
                        $bonusStatus = true;
                    }
                }
            }

        }
        if($bonusStatus)
        {
            return redirect()->back()->withErrors($errors)->withInput();
        }
        event(new AppraisalUpdate($appraisalObj->id));
        return redirect()->back()->with('message', 'Updated!');
    }

    public function store(ValidateAppraisalRequest $request)
    {
        $data = $request->all();
        if (!$data) {
            return redirect()->back()->withErrors('Input Not Found')->withInput();

        }

        // $lastAppraisal = Appraisal::where('user_id', $data['user_id'])->orderBy('id', 'desc')->first();
        // if ($lastAppraisal && $lastAppraisal->effective_date > $data['effective_date']) {
        //     return redirect()->back()->withErrors('Current effective date should be greater than last effective date')->withInput();
        // }
        $checkAppraisal = Appraisal::where('user_id', $data['user_id'])->where('type_id', $data['type_id'])->where('effective_date', $data['effective_date'])->first();
        if ($checkAppraisal) {
            return redirect()->back()->withErrors('Appraisal already exists!')->withInput();
        }
        DB::beginTransaction();
        $appraisal = Appraisal::create(['user_id' => $data['user_id'], 'type_id' => $data['type_id'], 'effective_date' => !empty($data['effective_date']) ? $data['effective_date'] : null]);
        if ($appraisal->isValid()) {
            if (isset($data['component'])) {
                $appraisalComponents = $data['component'];
                    foreach ($appraisalComponents as $key => $component) {
                            $appraisalComponent = AppraisalComponent::create(['appraisal_id' => $appraisal->id, 'component_id' => $key, 'value' => $component ?? null]);
                            if (!$appraisalComponent->isValid()) {
                                return redirect()->back()->withErrors($appraisalComponent->getErrors())->withInput();
                            }
                    }
            }
            if (isset($data['bonus'])) {
                $bonuses = $data['bonus'];
                foreach ($bonuses as $key => $bonus) {
                    $appraisalBonus = AppraisalBonus::create(['appraisal_id' => $appraisal->id,'appraisal_bonus_type_id' => $key, 'value' => $bonus['value'] ? $bonus['value'] : 0, 'value_date' => !empty($bonus['value_date']) ? $bonus['value_date'] : null]);
                    if (!$appraisalBonus->isValid()) {
                        return redirect()->back()->withErrors($appraisalBonus->getErrors())->withInput();
                    }
                }
            }
            DB::commit();
            event(new AppraisalUpdate($appraisal->id));
            return redirect('appraisal')->withMessage('Saved Successfully');
        }
        DB::rollBack();
        return redirect()->back()->withErrors($appraisal->getErrors())->withInput();
    }

    public function destroy($id)
    {
        $appraisalObj = Appraisal::find($id);
        $count = Appraisal::where('user_id',$appraisalObj->user_id)->count();
        if ($appraisalObj && $appraisalObj->delete()) {
            if($count > 1)
                return redirect()->back()->with('message', 'Successfully Deleted');
            return redirect('appraisal')->with('message', 'Successfully Deleted');
        }
        return redirect()->back()->withErrors('Appraisal Not Found!')->withInput();
    }
    public function monthlySummary($monthId = null)
    {
        $monthList = Month::orderBy('year', 'desc')->orderBy('month', 'desc')->get();
        if (!count($monthList) > 0) {
            return back()->with('message', 'no month found.');
        }
        $currMonth = $monthList->first();
        if ($monthId) {
            $currMonth = Month::find($monthId);
        }
        $to = $currMonth->getLastDay();
        $appraisals =  Appraisal::orderBy('effective_date','desc')->whereDate('effective_date','<=',$to)
                            ->whereHas('user', function (Builder $query) use ($currMonth) {
                                $query->where('is_active',1)->orWhere(function ($query) use ($currMonth) {
                                    $query->where('release_date','<>', NULL)->where('release_date', '>=', $currMonth->getFirstDay());
                                });
                            })->get()->unique('user_id');

        $appraisalTypes =  AppraisalComponentType::all();
        $appraisalBonusTypes =  AppraisalBonusType::all();
        return view('appraisal.appraisal.monthly-summary', compact('appraisals','appraisalTypes','appraisalBonusTypes','currMonth','monthList'));
    }

    public function test()
    {
        $appraisal = Appraisal::first();
        $bonusTypes = AppraisalBonusType::all();
        return view('mail.audits.appraisal-audit', compact('appraisal','bonusTypes'));
    }
}
