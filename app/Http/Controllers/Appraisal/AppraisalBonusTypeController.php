<?php

namespace App\Http\Controllers\Appraisal;

use App\Http\Controllers\Controller;
use App\Models\Appraisal\AppraisalBonusType;

class AppraisalBonusTypeController extends Controller
{
    public function index()
    {
        $appraisalBonusType = AppraisalBonusType::all();
        return view('appraisal.appraisal-bonus-type.index', compact('appraisalBonusType'));
    }

    public function store()
    {
        $appraisalBonusType = AppraisalBonusType::create(request()->all());
        if ($appraisalBonusType->isValid())
            return redirect()->back()->withMessage('Successfully Saved!');
        return redirect()->back()->withErrors($appraisalBonusType->getErrors())->withInput();
    }

    public function edit(AppraisalBonusType $appraisalBonusType)
    {
        return view('appraisal.appraisal-bonus-type.edit', compact('appraisalBonusType'));
    }

    public function update(AppraisalBonusType $appraisalBonusType)
    {
        $appraisalBonusType->update(request()->all());
        if ($appraisalBonusType->isValid())
            return redirect('/appraisal/appraisal-bonus-type')->with('message', 'Update Successful!');
        return redirect()->back()->withErrors($appraisalBonusType->getErrors())->withInput();
    }

    public function destroy(AppraisalBonusType $appraisalBonusType)
    {
        $appraisalBonusType->delete();
        return redirect()->back()->with('message', 'Successfully Deleted');
    }
}
