<?php

namespace App\Http\Livewire;

use App\Models\Bonus\Bonus;
use App\Models\Bonus\BonusConfirmed;
use App\Models\DateTime\Month;
use App\Models\Users\User;
use App\Services\Bonus\BonusService;
use Livewire\Component;

class BonusDueUser extends Component
{
    public $monthId;
    public $userId;
    public $dueBonuses;



    public function mount($data)
    {
        $this->monthId = $data['monthId'];
        $this->userId = $data['userId'];
        $this->dueBonuses = $data['dueBonuses'];
    }

    public function approveAllBonuses($index)
    {
        $dueBonusObjs = BonusService::getDueBonues($this->userId,$this->monthId);
        foreach($dueBonusObjs[$index]['bonuses'] as $key => $bonus)
        {

            $bonus = Bonus::find($bonus->id);
            $bonus->paid_at = date('Y-m-d');
            $bonus->save();
            $bonusObj = BonusConfirmed::create(['bonus_id' => $bonus->id, 'user_id' => $bonus->user_id]);   
        }
    }

    public function render()
    {
      
        $user = User::find($this->userId);
        if (!$user) {
            return redirect()->back()->withErrors('Invalid User Id');
        }
        return view('livewire.bonus-due-user',compact('user'));
    }
}
