<?php

namespace App\Http\Livewire;

use App\Models\Bonus\Bonus;
use App\Models\Bonus\BonusConfirmed;
use Livewire\Component;

class BonusDueUserItem extends Component
{

    public $item,$index,$bonus,$amount,$project,$rate,$details,$annualGross;

    public function mount($data1)
    {
        $this->item = $data1['item'];
        $this->index = $data1['index'];
        $this->bonus = $data1['bonus'];
        $this->amount = $data1['item']['amount'];
        $this->project = $data1['item']['project'] ?? '';
        $this->rate = $data1['item']['rate'] ?? '';
        $this->details = $data1['item']['description'] ?? '';
        $this->annualGross = $data1['item']['annual_gross_salary'] ?? '';
    }

    public function approveBonus()
    {
        $bonus = Bonus::find($this->item['id']);
        $bonus->paid_at = date('Y-m-d');
        $bonus->save();
        $this->item['paid_at'] = $bonus->paid_at;

        $bonusObj = BonusConfirmed::create(['bonus_id' => $this->item['id'], 'user_id' => $this->item['user_id']]);
        if($bonusObj->isInvalid())
            \Log::info($bonusObj->getErrors());
    }

    public function rejectBonus()
    {
        $bonus = Bonus::find($this->item['id']);
        if($bonus->transaction_id == null)
        {
            $bonus->paid_at = null;
            $bonus->save();
            $this->item['paid_at'] = null;
            BonusConfirmed::where(['bonus_id' => $this->item['id'], 'user_id' => $this->item['user_id'] ])->delete();
        }
    }

    public function updatedAmount($value)
    {
        $bonus = Bonus::find($this->item['id']);
        $bonus->amount = $this->amount;
        $bonus->save();
    }

    public function render()
    {
        return view('livewire.bonus-due-user-item');
    }
}
