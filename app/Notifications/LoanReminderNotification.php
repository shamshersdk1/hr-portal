<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use App\Models\Users\User;

class LoanReminderNotification extends Notification
{
    use Queueable;

    public $message;
    public $title;
    public $color;

    public function __construct($message,$title,$color)
    {
        $this->message = $message;
        $this->titleName = $title;
        $this->colorCode = $color;
    }
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {
        $message = $this->message;
        return (new SlackMessage)
            ->attachment(function ($attachment) use ($message) {
                $attachment->title($this->titleName)->color($this->colorCode)->content($message);
        });             
    }
}

