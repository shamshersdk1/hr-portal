<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AppraisalAuditNotification extends Notification  implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $subject;
    public $user;
    public $date;
    public $appraisal;
    public $bonusTypes;

    public function __construct($subject,$user,$date,$appraisal,$bonusTypes)
    {
        $this->subject = $subject;
        $this->user = $user;
        $this->date = $date;
        $this->appraisal = $appraisal;
        $this->bonusTypes = $bonusTypes;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject($this->subject)
        ->markdown('mail.audits.appraisal-audit',['user'=>$this->user,'date'=>$this->date,'appraisal'=>$this->appraisal,'bonusTypes'=>$this->bonusTypes]);
    }
}
