<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SalaryDependencyNotification extends Notification
{
    use Queueable;

    protected $subject, $data, $date;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($subject, $data, $date)
    {
        $this->subject = $subject;
        $this->data = $data;
        $this->date = $date;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

       

        return (new MailMessage)
        ->subject($this->subject)
        ->markdown('mail.salary-dependency.salary-dependency',['data'=>$this->data, 'date'=>$this->date]);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
