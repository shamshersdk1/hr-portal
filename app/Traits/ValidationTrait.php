<?php
namespace App\Traits;

use Validator;

trait ValidationTrait
{
    public $errors;

    public function validate($data)
    {
        if (empty($this->rules)) {
            return true;
        }

        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        return true;
    }

    public function getError()
    {
        return $this->errors;
    }
}
