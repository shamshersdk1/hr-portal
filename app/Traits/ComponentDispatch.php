<?php
namespace App\Traits;
use App\Models\Salary\{PrepSalaryComponent, PrepSalary, PrepSalaryExecution, PrepUser};
use App\Services\SalaryService\SalaryService;
trait ComponentDispatch
{
    abstract protected function getJobName();
    abstract protected function setJobName();
    abstract protected function getComponent();
    abstract protected function setComponent();
    abstract protected function setPrepTableName();
    abstract protected function getPrepTableName();
    abstract protected function checkLock();



    public function dispatchComponentJob()
    {
        $jobName = $this->getJobName();
        $prepSalaryComponent = PrepSalaryComponent::find($this->getComponent()) ;
        if(!$prepSalaryComponent)
            \Log::info('prep salary component not found');
        $prepSalaryObj = PrepSalary::find($prepSalaryComponent->prep_salary_id);
        foreach($prepSalaryObj->prepUsers as $prepUser)
        {
            $prepSalaryExecution = PrepSalaryExecution::firstOrCreate(['prep_salary_id' => $prepSalaryObj->id,'component_id' => $prepSalaryComponent->id, 'user_id' => $prepUser->user_id],['status' => 'init','counter' => 0]);
            if($prepSalaryExecution->isValid())
            {
                if($prepSalaryExecution->status!="completed")
                    dispatch(new $jobName($prepUser->user_id, $prepSalaryObj->id));
            }
        }
    }

    public function generate()
    {
        $response['message'] = "";
        $response['errors'] = "";
        $response['status'] = true;
        $prepSalaryComponentObj = PrepSalaryComponent::find($this->componentId);
        foreach($prepSalaryComponentObj->dependentComponent as $dependent)
        {
            $obj = PrepSalaryComponent::where('prep_salary_id',$prepSalaryComponentObj->prep_salary_id)->where('prep_salary_component_type_id',$dependent->component_id)->first();
            $obj->is_generated = 0;
            $obj->save();
        }
        $data = $prepSalaryComponentObj->dependentComponent;
        if (!$prepSalaryComponentObj) {
            $response['errors'] = "No component found";
            $response['status'] = false;
            return $response;
        }
        $lockStatus = $this->checkLock();
        $deleteStatus = $this->deleteSelfEntries();
        if($lockStatus['status'] || (!$deleteStatus))
        {
            $response['errors'] = $lockStatus['errors']??"500";
            $response['status'] = false;
            return $response;
        }
        $users = PrepUser::where('prep_salary_id', $prepSalaryComponentObj->prep_salary_id)->get();
        foreach ($users as $user) {
            $jobName = $this->getJobName();
            if($jobName == 'App\Jobs\PrepareSalary\Components\TDSSalaryJob')
                dispatch(new $jobName($user->user_id, $prepSalaryComponentObj->prep_salary_id,$prepSalaryComponentObj->id));
            else
                dispatch(new $jobName($user->user_id, $prepSalaryComponentObj->prep_salary_id));
        }
        return $response;
    }

    private function deleteSelfEntries()
    {
        $prepSalaryComponentObj = PrepSalaryComponent::find($this->componentId);
        $modelName = $this->getPrepTableName();
        $modelName::where('prep_salary_id',$prepSalaryComponentObj->prep_salary_id)->delete();
        return true;
    }
}
