# Changelog

All notable changes to this project will be documented in this file.

## [0.0.7] - 2019-08-13

### Added

-   Appraisal.

## [0.0.6] - 2019-08-13

### Added

-   Appraisal Component.

## [0.0.5] - 2019-08-12

### Added

-   Loan Deduction.

## [0.0.4] - 2019-08-12

### Added

-   Insurance Deduction.

###Changed

-   Month
-   MonthSetting

## [0.0.3] - 2019-08-12

### Added

-   Food Deduction.
-   VPF Deduction

## [0.0.2] - 2019-08-12

### Added

-   Appraisal Bonus Component Type.

## [0.0.1] - 2019-08-09

### Added

-   Appraisal Component Type.

### Changed

-   Web Pack Configuration.
