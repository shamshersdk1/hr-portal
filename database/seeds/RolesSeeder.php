<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolesData = [
            [
                'name' => 'admin',
                'guard_name' => 'web',
            ],
            [
                'name' => 'account',
                'guard_name' => 'web',
            ],
            [
                'name' => 'hr',
                'guard_name' => 'web',
            ],
        ];
        DB::table('hr_roles')->delete();
        DB::table('hr_roles')->insert($rolesData);
    }
}
