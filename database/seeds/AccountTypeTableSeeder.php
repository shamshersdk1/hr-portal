<?php

use App\Models\Bank\AccountType;
use Illuminate\Database\Seeder;

class AccountTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accountTypes = [
            [
                'name' => 'Savings Account',
                'code' => 'saving-account',

            ],
            [
                'name' => 'UAN Account',
                'code' => 'uan-account',

            ],
            [
                'name' => 'ESI Account',
                'code' => 'esi-account',

            ],
            [
                'name' => 'PAN Account',
                'code' => 'pan-account',

            ],
        ];
        DB::table('account_types')->delete();
        DB::table('account_types')->insert($accountTypes);

        $accountTypeObj = AccountType::where('code', 'saving-account')->first();
        if ($accountTypeObj) {
            $metaKeys = [
                'bank_account_type_id' => $accountTypeObj->id,
                'key' => 'ifsc',
                'is_required' => 1,
                'field_type' => 'string',
            ];
            DB::table('account_meta_keys')->delete();
            DB::table('account_meta_keys')->insert($metaKeys);
        }
    }
}
