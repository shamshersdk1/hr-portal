<?php

use Illuminate\Database\Seeder;

class SystemSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $systemSettings = [
            [
                'key' => 'AppraisalAuditUserEmailNotify',
                'value' => '0',

            ],
            [
                'key' => 'AppraisalAuditAdminEmailNotify',
                'value' => '1',

            ],
            [
                'key' => 'AppraisalAuditAdminEmailNotityEmailAddress',
                'value' => 'hr-notify@geekyants.com',

            ],

            [
                'key' => 'UserAuditUserEmailNotify',
                'value' => '0',

            ],
            [
                'key' => 'UserAuditAdminEmailNotify',
                'value' => '1',

            ],
            [
                'key' => 'UserAuditAdminEmailNotifyEmailAddress',
                'value' => 'hr-notify@geekyants.com',

            ],

            [
                'key' => 'BankAuditUserEmailNotify',
                'value' => '0',

            ],
            [
                'key' => 'BankAuditAdminEmailNotify',
                'value' => '1',

            ],
            [
                'key' => 'BankAuditAdminEmailNotifyEmailAddress',
                'value' => 'hr-notify@geekyants.com',

            ],

            [
                'key' => 'ItSavingAuditUserEmailNotify',
                'value' => '0',

            ],
            [
                'key' => 'ItSavingAuditAdminEmailNotify',
                'value' => '1',

            ],
            [
                'key' => 'ItSavingAuditAdminEmailNotifyEmailAddress',
                'value' => 'hr-notify@geekyants.com',

            ],
        ];
        DB::table('system_settings')->insert($systemSettings);
    }
}
